cmake_minimum_required(VERSION 3.14.0)
project(vtx)

# Versioning
set(RELEASE_VERSION_MAJOR 1)
set(RELEASE_VERSION_MINOR 0)
set(RELEASE_VERSION_PATCH 0)

set(RELEASE_IS_PRERELEASE TRUE)
set(RELEASE_VERSION_PRERELEASE_KIND BETA)
set(RELEASE_VERSION_PRERELEASE_BUILD 1)

if(${RELEASE_IS_PRERELEASE})
    string(TOLOWER ${RELEASE_VERSION_PRERELEASE_KIND} LOWER_PRERELEASE_KIND)
    set(RELEASE_VERSION_STRING "${RELEASE_VERSION_MAJOR}.${RELEASE_VERSION_MINOR}.${RELEASE_VERSION_PATCH}-${LOWER_PRERELEASE_KIND}.${RELEASE_VERSION_PRERELEASE_BUILD}")
else(${RELEASE_IS_PRERELEASE})
    set(RELEASE_VERSION_STRING "${RELEASE_VERSION_MAJOR}.${RELEASE_VERSION_MINOR}.${RELEASE_VERSION_PATCH}")
endif(${RELEASE_IS_PRERELEASE})

message(STATUS "vtx version ${RELEASE_VERSION_STRING}")

if(WIN32)
    if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
        # Linking doesn't work so well with Clang with the dllexport/dllimport faff
        message(WARNING "Compiling with Clang on Windows may not work so well - Try gcc if you run into linker errors with __imp__* symbols\n(Oh and please let me know if you can get this working)")
    endif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
endif(WIN32)

# Export compile commands for editor autocomplete
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
if(APPLE)
    set(CMAKE_INSTALL_RPATH "@loader_path/../lib;@loader_path")
else(APPLE)
    set(CMAKE_INSTALL_RPATH "$ORIGIN/../lib:$ORIGIN")
endif(APPLE)

# Add uninstall target
configure_file("cmake/uninstall.cmake" "cmake/uninstall.cmake" COPYONLY)
add_custom_target(uninstall "${CMAKE_COMMAND}" -P "cmake/uninstall.cmake")

# Add installprerequisites target
set(platformPlugins "")
find_package(Qt5Gui REQUIRED)
foreach(plugin ${Qt5Gui_PLUGINS})
    get_target_property(loc ${plugin} LOCATION)

    list(APPEND platformPlugins ${loc})
endforeach()
list(FILTER platformPlugins INCLUDE REGEX ".*platforms.*") # Only include platform plugins
string (REPLACE ";" "$<SEMICOLON>" platformPluginsStr "${platformPlugins}")

configure_file("cmake/installprerequisites.cmake" "cmake/installprerequisites.cmake" COPYONLY)
add_custom_target(installprerequisites "${CMAKE_COMMAND}" -DCMAKE_INSTALL_PREFIX="${CMAKE_INSTALL_PREFIX}" -DQT_PLATFORM_PLUGINS="${platformPluginsStr}" -P "cmake/installprerequisites.cmake")

# Set up code coverage
option(VTX_ENABLE_TEST_COVERAGE "Whether to generate coverage statistics for tests (adds a `test_coverage` command)" FALSE)
if(VTX_ENABLE_TEST_COVERAGE)
    include("cmake/CodeCoverage.cmake")
    append_coverage_compiler_flags()
endif(VTX_ENABLE_TEST_COVERAGE)

set(TRANSLATIONS
    lang/lang_en_US.ts
    # lang/lang_ja_JA.ts
    )

foreach(val ${TRANSLATIONS})
    message(STATUS "Translation: " ${val})
endforeach(val ${TRANSLATIONS})

# Sub-projects have tests
enable_testing()

add_subdirectory(./vtx)
add_subdirectory(./vmx)

# More coverage setup
if(VTX_ENABLE_TEST_COVERAGE)
    setup_target_for_coverage_gcovr_html(
        NAME test_coverage
        EXECUTABLE ctest -j ${PROCESSOR_COUNT}
        DEPENDENCIES ${VMX_TEST_TARGETS} ${VTX_TEST_TARGETS}
        BASE_DIRECTORY "${CMAKE_SOURCE_DIR}"

        # Exclude auto-generated files, tests, and 3rd party libs
        EXCLUDE "${CMAKE_BINARY_DIR}" "vmx/test" "vtx/test" "vmx/include/vmx/refl.hpp" "vmx/include/vmx/util/Result.hpp"
    )
endif(VTX_ENABLE_TEST_COVERAGE)

if(APPLE)
    set(CPACK_GENERATOR Bundle)
    set(CPACK_BUNDLE_NAME "vtx")
    set(CPACK_BUNDLE_PLIST ${CMAKE_SOURCE_DIR}/vtx/resources/Raw/Info.plist)
    set(CPACK_BUNDLE_ICON ${CMAKE_SOURCE_DIR}/vtx/resources/Raw/vtx.icns)
endif(APPLE)
include(CPack)
