Vtx
===

Vtx is an IM client using [Matrix](https://matrix.org/)!

## Dependencies

- [Qt 5](https://www.qt.io/)
    - Qt Core
    - Qt Network
    - Qt Widgets
    - Qt Sql
- [result](https://github.com/oktal/result) (Bundled in the code)
- [refl-cpp](https://github.com/veselink1/refl-cpp) (Bundled in the code)
- [QtPromise](https://github.com/simonbrunel/qtpromise) (Downloaded with CMake)

In addition, Vtx uses C++20 and thus requires a fairly new compiler.

Icons are from Google's Material icon set.

## What's here

- `vtx`: The actual graphical client lives here
- `vmx`: A Matrix client library (maybe one day I'll split this off into its own repo)
