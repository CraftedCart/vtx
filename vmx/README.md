Vmx
===

Matrix library

## Dependencies

- Qt 5.9
    - Qt Core
    - Qt Network
- refl-cpp (Bundled in the code)
- QtPromise (Downloaded by CMake's FetchContent module)
