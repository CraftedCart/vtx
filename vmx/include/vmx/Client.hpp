#ifndef VTX_VMX_CLIENT_HPP
#define VTX_VMX_CLIENT_HPP

#include "vmx/Session.hpp"
#include "vmx/util/Result.hpp"
#include "vmx/clientapi/r0/session/Login.hpp"
#include "vmx/clientapi/ApiError.hpp"
#include "vmx_export.h"
#include <QtPromise>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QString>
#include <optional>
#include <functional>

namespace Vmx {
    struct VMX_EXPORT NetError {
        QNetworkReply::NetworkError error;
        QString errorString;

        NetError(const QNetworkReply::NetworkError &error, const QString &errorString);
    };

    class VMX_EXPORT Client : public QObject {
        Q_OBJECT

        private:
            QNetworkAccessManager *netManager = new QNetworkAccessManager(this);

            /** @brief We store all replies so we can abort all of them when the client is destroyed */
            QVector<QNetworkReply*> activeReplies;

        public:
            Client(QObject *parent = nullptr);
            ~Client();

            // TODO: Make this return a future or do something with signal/slots or something
            // also.. y'know actually bother with the whole "login flows" stuff

            /**
             * @brief Authenticate with a homeserver with a username and password
             *
             * Attempt to obtain an access token with the `m.login.password` login type. This will store the access
             * token/identity information in the client on success.
             *
             * @param username The local part of an MXID (Eg: for MXID `@alice:matrix.org`, this would be `alice`)
             * @param password The password for the user account
             * @param deviceId You can opt to reuse an existing device ID (or create a new one) by specifying its ID
             *                 here, otherwise leave this as `std::nullopt` to let the homeserver generate a device ID.
             * @param initialDeviceDisplayName A publically visible display name to assign to a newly-created device. If
             *                                 logging in with an already known device, this is ignored.
             */
            [[nodiscard]]
            QtPromise::QPromise<Vmx::ClientApi::R0::Session::Login::PostResponse> logInWithPassword(
                    QString username,
                    QString password,
                    QUrl homeserverUrl,
                    std::optional<QString> deviceId = {},
                    std::optional<QString> initialDeviceDisplayName = {}
                    );

            /**
             * @brief Make a HTTP request to the homeserver
             *
             * @note The return value will auto-delete itself after calling its reply callback
             */
            template<typename RequestT>
            [[nodiscard]]
            QtPromise::QPromise<typename RequestT::Response> request(
                    const RequestT &req,
                    const QUrl &homeserverUrl,
                    const std::optional<QString> &accessToken = {}
                    );

            /**
             * @brief Make a HTTP request to the homeserver
             *
             * @note The return value will auto-delete itself after calling its reply callback
             */
            template<typename RequestT>
            [[nodiscard]]
            QtPromise::QPromise<typename RequestT::Response> request(const RequestT &req, const Session &session);

        private:
            void registerReply(QNetworkReply *reply);
            void onReplyDestroyed(QObject *reply);
    };

    namespace Internal {
        template<typename SuccessT>
        void onRequestFinished(
                QNetworkReply *netReply,
                const QtPromise::QPromiseResolve<SuccessT> &resolve,
                const QtPromise::QPromiseReject<SuccessT> &reject
                );

        /** @brief Does not try to parse the response as JSON on success */
        template<>
        inline void onRequestFinished<QByteArray>(
                QNetworkReply *netReply,
                const QtPromise::QPromiseResolve<QByteArray> &resolve,
                const QtPromise::QPromiseReject<QByteArray> &reject
                );
    }
}

#include "vmx/Client.ipp"

#endif
