#ifndef VTX_VMX_CLIENT_IPP
#define VTX_VMX_CLIENT_IPP

#include "vmx/clientapi/r0/session/Login.hpp"
#include "vmx/Serde.hpp"
#include <QJsonDocument>
#include <QUrlQuery>
#include <QNetworkReply>

namespace Vmx {
    namespace Internal {
        template<typename SuccessT>
        void onRequestFinished(
                QNetworkReply *netReply,
                const QtPromise::QPromiseResolve<SuccessT> &resolve,
                const QtPromise::QPromiseReject<SuccessT> &reject
                ) {
            int httpStatus = netReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

            if (netReply->error() == QNetworkReply::NoError || (httpStatus >= 400 && httpStatus < 500)) {
                QByteArray data = netReply->readAll();

                QJsonDocument doc = QJsonDocument::fromJson(data);
                if (doc.isNull()) {
                    qDebug() << "Failed to parse JSON response";
                    reject(ClientApi::ApiError(
                                ClientApi::BadResponseError{Serde::WrongFormat()},
                                httpStatus
                                ));
                    netReply->deleteLater();
                    return;
                }

                if (!doc.isObject()) {
                    qDebug() << "JSON root is not an object";
                    reject(ClientApi::ApiError(
                                ClientApi::BadResponseError{Serde::InvalidType()},
                                httpStatus
                                ));
                    netReply->deleteLater();
                    return;
                }

                QJsonObject root = doc.object();
                if (root.contains(QLatin1String("errcode"))) {
                    if (root[QLatin1String("errcode")].isString()) {
                        // It's an error
                        Util::Result<ClientApi::ApiError, Serde::DeserializeError> res = Serde::deserialize<ClientApi::ApiError>(root);
                        if (res.isOk()) {
                            ClientApi::ApiError apiError = res.unwrap();
                            apiError.httpStatus = (Http::Status) httpStatus;
                            reject(apiError);
                            netReply->deleteLater();
                            return;
                        } else {
                            reject(ClientApi::ApiError(
                                        ClientApi::BadResponseError{res.unwrapErr()},
                                        httpStatus
                                        ));
                            netReply->deleteLater();
                            return;
                        }
                    } else {
                        // We got an errcode that wasn't a string??
                        reject(ClientApi::ApiError(
                                    ClientApi::BadResponseError{Serde::InvalidType()},
                                    httpStatus
                                    ));
                        netReply->deleteLater();
                        return;
                    }
                } else {
                    // Hopefully we got an ok response from the server
                    Util::Result<SuccessT, Serde::DeserializeError> res = Serde::deserialize<SuccessT>(root);
                    if (res.isOk()) {
                        resolve(res.unwrap());
                        netReply->deleteLater();
                        return;
                    } else {
                        qDebug() << "Failed to deserialize JSON";
                        reject(ClientApi::ApiError(
                                    ClientApi::BadResponseError{res.unwrapErr()},
                                    httpStatus
                                    ));
                        netReply->deleteLater();
                        return;
                    }
                }
            } else {
                reject(NetError(netReply->error(), netReply->errorString()));
                netReply->deleteLater();
            }
        }

        template<>
        inline void onRequestFinished<QByteArray>(
                QNetworkReply *netReply,
                const QtPromise::QPromiseResolve<QByteArray> &resolve,
                const QtPromise::QPromiseReject<QByteArray> &reject
                ) {
            int httpStatus = netReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

            if (netReply->error() == QNetworkReply::NoError) {
                QByteArray data = netReply->readAll();
                netReply->deleteLater();
                resolve(data);
                return;
            } else if ((httpStatus >= 400 && httpStatus < 500)) {
                QByteArray data = netReply->readAll();

                QJsonDocument doc = QJsonDocument::fromJson(data);
                if (doc.isNull()) {
                    qDebug() << "Failed to parse JSON response";
                    reject(ClientApi::ApiError(
                                ClientApi::BadResponseError{Serde::WrongFormat()},
                                httpStatus
                                ));
                    netReply->deleteLater();
                    return;
                }

                if (!doc.isObject()) {
                    qDebug() << "JSON root is not an object";
                    reject(ClientApi::ApiError(
                                ClientApi::BadResponseError{Serde::InvalidType()},
                                httpStatus
                                ));
                    netReply->deleteLater();
                    return;
                }

                QJsonObject root = doc.object();
                if (root.contains(QLatin1String("errcode"))) {
                    if (root[QLatin1String("errcode")].isString()) {
                        // It's an error
                        Util::Result<ClientApi::ApiError, Serde::DeserializeError> res = Serde::deserialize<ClientApi::ApiError>(root);
                        if (res.isOk()) {
                            ClientApi::ApiError apiError = res.unwrap();
                            apiError.httpStatus = (Http::Status) httpStatus;
                            reject(apiError);
                            netReply->deleteLater();
                            return;
                        } else {
                            reject(ClientApi::ApiError(
                                        ClientApi::BadResponseError{res.unwrapErr()},
                                        httpStatus
                                        ));
                            netReply->deleteLater();
                            return;
                        }
                    } else {
                        // We got an errcode that wasn't a string??
                        reject(ClientApi::ApiError(
                                    ClientApi::BadResponseError{Serde::InvalidType()},
                                    httpStatus
                                    ));
                        netReply->deleteLater();
                        return;
                    }
                } else {
                    // ...how did we get here?
                    qDebug() << "Unknown error";
                    reject(NetError(netReply->error(), netReply->errorString()));
                    netReply->deleteLater();
                    return;
                }
            } else {
                reject(NetError(netReply->error(), netReply->errorString()));
                netReply->deleteLater();
            }
        }
    }

    template<typename RequestT>
    QtPromise::QPromise<typename RequestT::Response> Client::request(
            const RequestT &req,
            const QUrl &homeserverUrl,
            const std::optional<QString> &accessToken
            ) {
        QUrl endpoint = homeserverUrl;

        // Fill in placeholders in the endpoint path
        QString endpointPath = RequestT::METADATA.path;
        for_each(refl::reflect<RequestT>().members, [&](auto member) {
            if constexpr (refl::descriptor::has_attribute<Serde::Path>(member)) {
                // If a member is marked as a serde path field...
                static_assert(is_readable(member), "Member is not readable");

                constexpr Serde::Path pathInfo = refl::descriptor::get_attribute<Serde::Path>(member);
                endpointPath.replace(QLatin1String("$") + pathInfo.name, Serde::valueToString(member(req)));
            }
        });
        endpoint.setPath(endpointPath);

        // Fill in all query parameters
        QUrlQuery query;
        for_each(refl::reflect<RequestT>().members, [&](auto member) {
            if constexpr (refl::descriptor::has_attribute<Serde::Query>(member)) {
                // If a member is marked as a serde query field...
                static_assert(is_readable(member), "Member is not readable");

                constexpr Serde::Query queryInfo = refl::descriptor::get_attribute<Serde::Query>(member);

                using FieldT = typename decltype(member)::value_type;
                if constexpr (Serde::Internal::IsOptional<FieldT>) {
                    if (member(req).has_value()) {
                        query.addQueryItem(queryInfo.name, Serde::valueToString(*member(req)));
                    }
                } else {
                    query.addQueryItem(queryInfo.name, Serde::valueToString(member(req)));
                }
            }
        });
        endpoint.setQuery(query);

        QNetworkRequest netReq = QNetworkRequest(endpoint);

        if constexpr (RequestT::METADATA.requiresAuthentication) {
            if (Q_UNLIKELY(!accessToken)) {
                qFatal("Tried to make request to %s which requires authentication, but we're not logged in", qUtf8Printable(req.METADATA.path));
            }

            netReq.setRawHeader(QByteArray("Authorization"), (QLatin1String("Bearer ") + *accessToken).toUtf8());
        }

        // See which HTTP method we're using and do the appropriate request
        if constexpr (RequestT::METADATA.method == QNetworkAccessManager::GetOperation) {
            // qDebug() << "GET" << endpoint.toDisplayString();

            return QtPromise::QPromise<typename RequestT::Response>{[this, netReq](
                    const QtPromise::QPromiseResolve<typename RequestT::Response>& resolve,
                    const QtPromise::QPromiseReject<typename RequestT::Response>& reject
                    ) {
                QNetworkReply *netReply = netManager->get(netReq);
                registerReply(netReply);
                QObject::connect(netReply, &QNetworkReply::finished, [=]() {
                    Internal::onRequestFinished(netReply, resolve, reject);
                });
            }};
        } else if constexpr (RequestT::METADATA.method == QNetworkAccessManager::PostOperation) {
            // qDebug() << "POST" << endpoint.toDisplayString();

            QJsonDocument doc(Serde::serialize(req));
            netReq.setHeader(QNetworkRequest::ContentTypeHeader, QLatin1String("application/json"));
            return QtPromise::QPromise<typename RequestT::Response>{[&](
                    const QtPromise::QPromiseResolve<typename RequestT::Response>& resolve,
                    const QtPromise::QPromiseReject<typename RequestT::Response>& reject
                    ) {
                QNetworkReply *netReply = netManager->post(netReq, doc.toJson(QJsonDocument::Compact));
                registerReply(netReply);
                QObject::connect(netReply, &QNetworkReply::finished, [=]() {
                    Internal::onRequestFinished(netReply, resolve, reject);
                });
            }};
        } else if constexpr (RequestT::METADATA.method == QNetworkAccessManager::PutOperation) {
            // qDebug() << "PUT" << endpoint.toDisplayString();

            QJsonDocument doc(Serde::serialize(req));
            netReq.setHeader(QNetworkRequest::ContentTypeHeader, QLatin1String("application/json"));
            return QtPromise::QPromise<typename RequestT::Response>{[&](
                    const QtPromise::QPromiseResolve<typename RequestT::Response>& resolve,
                    const QtPromise::QPromiseReject<typename RequestT::Response>& reject
                    ) {
                QNetworkReply *netReply = netManager->put(netReq, doc.toJson(QJsonDocument::Compact));
                registerReply(netReply);
                QObject::connect(netReply, &QNetworkReply::finished, [=]() {
                    Internal::onRequestFinished(netReply, resolve, reject);
                });
            }};
        } else {
            static_assert(Vmx::alwaysFalse<RequestT>, "Unknown HTTP method when trying to make request");
        }
    }

    template<typename RequestT>
    QtPromise::QPromise<typename RequestT::Response> Client::request(const RequestT &req, const Session &session) {
        return request(req, session.homeserverUrl, session.accessToken);
    }
}

#endif
