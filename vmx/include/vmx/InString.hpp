#ifndef VTX_VMX_INSTRING_HPP
#define VTX_VMX_INSTRING_HPP

#include "vmx/Serde.hpp"
#include "vmx_export.h"
#include <QString>
#include <QMultiHash>
#include <QAtomicInteger>
#include <QMutex>

namespace Vmx {
    /**
     * @brief An interned QString - useful for saving memory!
     */
    class VMX_EXPORT InString {
        private:
            struct RefCountInternString {
                const QString str;
                QAtomicInteger<size_t> refCount;
            };

            friend uint qHash(const InString &key, uint seed);

        public:
            static const InString EMPTY;

        private:
            static QMultiHash<uint, RefCountInternString*> stringTable;
            static QMutex stringTableMutex;

            RefCountInternString *internString;

        public:
            InString();
            InString(const InString &other);
            InString(const QString &str);
            // InString(const QLatin1String &str);
            // InString(const char *str);
            ~InString();

            bool isEmpty() const;
            QString toQString() const;

            static void printStringTable();

            bool operator==(const InString &other) const;
            bool operator!=(const InString &other) const;
            InString& operator=(const InString &other);
            operator QString() const;

        private:
            RefCountInternString* getInternString(const QString &str);
            void destroyInternString(RefCountInternString *internStr);
    };

    VMX_EXPORT uint qHash(const InString &key, uint seed);
}

namespace Vmx::Serde {
    template<> [[nodiscard]] VMX_EXPORT Util::Result<InString, DeserializeError> valueFromJson(const QJsonValue &value);
    template<> [[nodiscard]] VMX_EXPORT QJsonValue valueToJson(const InString &value);
    template<> [[nodiscard]] VMX_EXPORT QString valueToString(const InString &value);
}

#endif
