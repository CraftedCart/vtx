#ifndef VTX_VMX_MEMBERSHIP_HPP
#define VTX_VMX_MEMBERSHIP_HPP

#include "vmx/Serde.hpp"
#include "vmx_export.h"

namespace Vmx {
    enum class Membership : quint8 {
        INVITE,
        JOIN,
        KNOCK,
        LEAVE,
        BAN,
    };
}

namespace Vmx::Serde {
    template<>
    [[nodiscard]]
    VMX_EXPORT Util::Result<Vmx::Membership, DeserializeError> valueFromJson(const QJsonValue &value);

    template<>
    [[nodiscard]]
    VMX_EXPORT Util::Result<Vmx::Membership, DeserializeError> valueFromString(const QString &value);

    template<>
    [[nodiscard]]
    VMX_EXPORT QString valueToString(const Vmx::Membership &value);
}

#endif
