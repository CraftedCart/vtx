#ifndef VTX_VMX_METADATA_HPP
#define VTX_VMX_METADATA_HPP

#include "vmx_export.h"
#include <QNetworkAccessManager>
#include <QLatin1String>

namespace Vmx {
    struct VMX_EXPORT Metadata {
        const QLatin1String path;
        const QNetworkAccessManager::Operation method;
        const bool requiresAuthentication;
    };
}

#endif
