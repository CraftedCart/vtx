#ifndef VTX_VMX_SERDE_HPP
#define VTX_VMX_SERDE_HPP

#include "vmx/util/Result.hpp"
#include "vmx/refl.hpp"
#include "vmx_export.h"
#include <QJsonObject>
#include <QJsonValue>
#include <QHash>
#include <optional>
#include <type_traits>

namespace Vmx::Serde {
    //////// ERROR TYPES ////////

    /** @brief Not JSON data */
    struct WrongFormat {};

    /** @brief Failed to deserialize due to a field of invalid type */
    struct InvalidType {};

    /** @brief Failed to deserialize due to a field with an invalid value */
    struct InvalidValue {};

    /** @brief Failed to deserialize due to missing a field */
    struct MissingField {};

    using DeserializeError = std::variant<WrongFormat, InvalidType, InvalidValue, MissingField>;

    //////// INTERNAL ////////
    namespace Internal {
        //////// CONCEPTS ////////

        template<typename T>
        concept IsOptional = std::is_same_v<T, std::optional<typename T::value_type>>;

        template<typename T>
        concept IsQVector = std::is_same_v<T, QVector<typename T::value_type>>;

        template<typename T>
        concept IsQHash = std::is_same_v<T, QHash<typename T::key_type, typename T::mapped_type>>;

        template<typename T, typename AttrT>
        concept ReflHasAttr = refl::is_reflectable<T>() && refl::descriptor::has_attribute<AttrT>(refl::reflect<T>());

        //////// DESERIALIZATION ////////

        template<typename MemberT, size_t MemberN, typename ObjT>
        [[nodiscard]]
        VMX_EXPORT Util::Result<void, DeserializeError> deserializeOptional(
                const QJsonObject &json,
                refl::descriptor::field_descriptor<MemberT, MemberN> member,
                ObjT &obj
                );

        template<typename MemberT, size_t MemberN, typename ObjT>
        [[nodiscard]]
        VMX_EXPORT Util::Result<void, DeserializeError> deserializeNonOptional(
                const QJsonObject &json,
                refl::descriptor::field_descriptor<MemberT, MemberN> member,
                ObjT &obj
                );

        template<typename MemberT, size_t MemberN, typename ObjT>
        VMX_EXPORT void deserializeOptional(
                const ObjT &bbj,
                refl::descriptor::field_descriptor<MemberT, MemberN> member,
                QJsonObject &json
                );

        template<typename MemberT, size_t MemberN, typename ObjT>
        VMX_EXPORT void deserializeNonOptional(
                const ObjT &obj,
                refl::descriptor::field_descriptor<MemberT, MemberN> member,
                QJsonObject &json
                );

        //////// SERIALIZATION ////////

        VMX_EXPORT void mergeJsonObjects(QJsonObject &dest, const QJsonValue &src);
    }

    //////// ATTRIBUTES ////////

    /** @brief Marks a type as deserializeable */
    struct VMX_EXPORT Deserialize : refl::attr::usage::type {};

    template<typename T>
    concept IsDeserialize = Internal::ReflHasAttr<T, Deserialize>;

    /** @brief Marks a type as serializeable */
    struct VMX_EXPORT Serialize : refl::attr::usage::type {};

    template<typename T>
    concept IsSerialize = Internal::ReflHasAttr<T, Serialize>;

    /** @brief Marks a field in a (de)serializable type as one that should be included when (de)serializing */
    struct VMX_EXPORT Field : refl::attr::usage::field {
        const char *name;

        constexpr Field() : name("NAME NOT SET") {}
        constexpr Field(const char *name) : name(name) {}
    };

    /** @brief If a field isn't present, deserialize it to the default value instead of erroring */
    struct VMX_EXPORT Default : refl::attr::usage::field {};

    /** @brief Marks a field as a map that should recieve all JSON key-value pairs */
    struct VMX_EXPORT FieldPropertyMap : refl::attr::usage::field {};

    /**
     * @brief Marks a type as a wrapper for a single field
     *
     * The type must have one reflected field, and also a constructor that takes the inner type
     */
    struct VMX_EXPORT NewType : refl::attr::usage::type {};

    template<typename T>
    concept IsNewType = Internal::ReflHasAttr<T, NewType>;

    template<typename T>
    struct Variant : refl::attr::usage::type {};

    /**
     * @brief Marks a type as a std::variant with some alternatives
     *
     * On serialization, `tagName` is the key to specify which variant is in use
     */
    template<typename... Ts>
    struct Variant<std::variant<Ts...>> : refl::attr::usage::type {
        static constexpr refl::type_list<refl::type_descriptor<Ts>...> alternatives;

        const char *tagName;

        constexpr Variant(const char *tagName) : tagName(tagName) {}
    };

    template<typename T>
    concept IsVariant = Internal::ReflHasAttr<T, Variant<T>>;

    /** @brief Marks a type as a variant alternative */
    struct VariantAlt : refl::attr::usage::type {
        const char *typeName;

        constexpr VariantAlt(const char *typeName) : typeName(typeName) {}
    };

    struct VMX_EXPORT Flatten : refl::attr::usage::field {};

    /** @brief Marks a field in a serializable type as one that goes in the URL path */
    struct VMX_EXPORT Path : refl::attr::usage::field {
        const char *name;

        constexpr Path(const char *name) : name(name) {}
    };

    /** @brief Marks a field in a serializable type as one that goes in the URL as a query parameter */
    struct VMX_EXPORT Query : refl::attr::usage::field {
        const char *name;

        constexpr Query(const char *name) : name(name) {}
    };

    //////// FUNCS ////////

    template<typename T>
    requires IsDeserialize<T>
    [[nodiscard]]
    VMX_EXPORT Util::Result<T, DeserializeError> deserialize(const QJsonObject &json);

    template<typename T>
    [[nodiscard]]
    VMX_EXPORT Util::Result<T, DeserializeError> valueFromJson(const QJsonValue &value) = delete;

    template<typename T>
    [[nodiscard]]
    VMX_EXPORT Util::Result<T, DeserializeError> valueFromString(const QString &value) = delete;

    template<typename T>
    requires IsSerialize<T>
    [[nodiscard]]
    VMX_EXPORT QJsonObject serialize(const T &obj);

    template<typename T>
    [[nodiscard]]
    VMX_EXPORT QJsonValue valueToJson(const T &value) = delete;

    template<typename T>
    [[nodiscard]]
    VMX_EXPORT QString valueToString(const T &value) = delete;

    //////// SPECIALIZATIONS ////////

    template<> [[nodiscard]] VMX_EXPORT Util::Result<QJsonValue, DeserializeError> valueFromJson(const QJsonValue &value);
    template<> [[nodiscard]] VMX_EXPORT Util::Result<QJsonObject, DeserializeError> valueFromJson(const QJsonValue &value);
    template<> [[nodiscard]] VMX_EXPORT Util::Result<QJsonArray, DeserializeError> valueFromJson(const QJsonValue &value);
    template<> [[nodiscard]] VMX_EXPORT Util::Result<QString, DeserializeError> valueFromJson(const QJsonValue &value);
    template<> [[nodiscard]] VMX_EXPORT Util::Result<qint32, DeserializeError> valueFromJson(const QJsonValue &value);
    template<> [[nodiscard]] VMX_EXPORT Util::Result<quint32, DeserializeError> valueFromJson(const QJsonValue &value);
    template<> [[nodiscard]] VMX_EXPORT Util::Result<qint64, DeserializeError> valueFromJson(const QJsonValue &value);
    template<> [[nodiscard]] VMX_EXPORT Util::Result<quint64, DeserializeError> valueFromJson(const QJsonValue &value);
    template<> [[nodiscard]] VMX_EXPORT Util::Result<double, DeserializeError> valueFromJson(const QJsonValue &value);
    template<> [[nodiscard]] VMX_EXPORT Util::Result<bool, DeserializeError> valueFromJson(const QJsonValue &value);
    template<> [[nodiscard]] VMX_EXPORT Util::Result<QDateTime, DeserializeError> valueFromJson(const QJsonValue &value);
    template<> [[nodiscard]] VMX_EXPORT Util::Result<QUrl, DeserializeError> valueFromJson(const QJsonValue &value);

    template<> [[nodiscard]] VMX_EXPORT QJsonValue valueToJson(const QJsonValue &value);
    template<> [[nodiscard]] VMX_EXPORT QJsonValue valueToJson(const QJsonObject &value);
    template<> [[nodiscard]] VMX_EXPORT QJsonValue valueToJson(const QJsonArray &value);
    template<> [[nodiscard]] VMX_EXPORT QJsonValue valueToJson(const QString &value);
    template<> [[nodiscard]] VMX_EXPORT QJsonValue valueToJson(const qint32 &value);
    template<> [[nodiscard]] VMX_EXPORT QJsonValue valueToJson(const quint32 &value);
    template<> [[nodiscard]] VMX_EXPORT QJsonValue valueToJson(const qint64 &value);
    template<> [[nodiscard]] VMX_EXPORT QJsonValue valueToJson(const quint64 &value);
    template<> [[nodiscard]] VMX_EXPORT QJsonValue valueToJson(const double &value);
    template<> [[nodiscard]] VMX_EXPORT QJsonValue valueToJson(const bool &value);
    template<> [[nodiscard]] VMX_EXPORT QJsonValue valueToJson(const QDateTime &value);
    template<> [[nodiscard]] VMX_EXPORT QJsonValue valueToJson(const QUrl &value);

    template<> [[nodiscard]] VMX_EXPORT QString valueToString(const QString &value);
    template<> [[nodiscard]] VMX_EXPORT QString valueToString(const qint32 &value);
    template<> [[nodiscard]] VMX_EXPORT QString valueToString(const quint32 &value);
    template<> [[nodiscard]] VMX_EXPORT QString valueToString(const qint64 &value);
    template<> [[nodiscard]] VMX_EXPORT QString valueToString(const quint64 &value);
    template<> [[nodiscard]] VMX_EXPORT QString valueToString(const double &value);
    template<> [[nodiscard]] VMX_EXPORT QString valueToString(const bool &value);
    template<> [[nodiscard]] VMX_EXPORT QString valueToString(const QDateTime &value);
    template<> [[nodiscard]] VMX_EXPORT QString valueToString(const QUrl &value);

    //////// PARTIAL SPECIALIZATIONS ////////

    template<typename T>
    requires IsDeserialize<T>
    [[nodiscard]]
    VMX_EXPORT Util::Result<T, DeserializeError> valueFromJson(const QJsonValue &value);

    template<typename T>
    requires IsNewType<T>
    [[nodiscard]]
    VMX_EXPORT Util::Result<T, DeserializeError> valueFromJson(const QJsonValue &value);

    template<typename T>
    requires Internal::IsQVector<T>
    [[nodiscard]]
    VMX_EXPORT Util::Result<T, DeserializeError> valueFromJson(const QJsonValue &value);

    template<typename T>
    requires Internal::IsQHash<T>
    [[nodiscard]]
    VMX_EXPORT Util::Result<T, DeserializeError> valueFromJson(const QJsonValue &value);

    template<typename T>
    requires IsSerialize<T>
    [[nodiscard]]
    VMX_EXPORT QJsonValue valueToJson(const T &value);

    template<typename T>
    requires IsNewType<T>
    [[nodiscard]]
    VMX_EXPORT QJsonValue valueToJson(const T &value);

    template<typename T>
    requires Internal::IsQVector<T>
    [[nodiscard]]
    VMX_EXPORT QJsonValue valueToJson(const T &value);

    template<typename T>
    requires IsVariant<T>
    [[nodiscard]]
    VMX_EXPORT QJsonValue valueToJson(const T &value);

    template<typename T>
    requires IsNewType<T>
    [[nodiscard]]
    VMX_EXPORT QString valueToString(const T &value);
}

#include "vmx/Serde.ipp"

#endif
