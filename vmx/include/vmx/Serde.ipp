#ifndef VTX_VMX_SERDE_IPP
#define VTX_VMX_SERDE_IPP

#include <QJsonArray>

namespace Vmx::Serde {
    template<typename T>
    requires IsDeserialize<T>
    Util::Result<T, DeserializeError> deserialize(const QJsonObject &json) {
        T obj;

        std::optional<DeserializeError> failReason = std::nullopt;

        // Loop over all members
        for_each(refl::reflect<T>().members, [&](auto member) {
            if (failReason) return;

            if constexpr (refl::descriptor::has_attribute<Field>(member)) {
                // If a member is marked as a serde field...
                static_assert(is_writable(member), "Member is not writable");

                using FieldT = typename decltype(member)::value_type;

                if constexpr (Internal::IsOptional<FieldT>) {
                    // The field is optional - a missing JSON field is ok
                    Util::Result<void, DeserializeError> res = Internal::deserializeOptional(json, member, obj);
                    if (res.isErr()) failReason = res.unwrapErr();
                    return;
                } else {
                    // The field is NOT optional - a missing JSON field is not ok, unless the Default attr is specified
                    Util::Result<void, DeserializeError> res = Internal::deserializeNonOptional(json, member, obj);
                    if (res.isErr()) failReason = res.unwrapErr();
                    return;
                }
            } else if constexpr (refl::descriptor::has_attribute<FieldPropertyMap>(member)) {
                // Else if a member wants *all* JSON fields
                static_assert(is_writable(member), "Member is not writable");

                using FieldT = typename decltype(member)::value_type;

                Util::Result<FieldT, DeserializeError> value = valueFromJson<FieldT>(json);

                // Write the deserialized value to the struct
                if (value.isOk()) {
                    member(obj) = std::move(value.unwrap());
                } else if (std::holds_alternative<MissingField>(value.unwrapErr()) && refl::descriptor::has_attribute<Default>(member)) {
                    // If the field is missing, but the field has the Default attribute, default construct it
                    member(obj) = FieldT();
                } else {
                    failReason = value.unwrapErr();
                }
            }
        });

        if (failReason) {
            return Util::Err(*failReason);
        } else {
            return Util::Ok(std::move(obj));
        }
    }

    template<typename T>
    requires IsSerialize<T>
    QJsonObject serialize(const T &obj) {
        QJsonObject json;

        // Loop over all members
        for_each(refl::reflect<T>().members, [&](auto member) {
            if constexpr (refl::descriptor::has_attribute<Field>(member)) {
                // If a member is marked as a serde field...
                Field fieldInfo = refl::descriptor::get_attribute<Field>(member);

                static_assert(is_readable(member), "Member is not readable");

                using FieldT = typename decltype(member)::value_type;

                if constexpr (Internal::IsOptional<FieldT>) {
                    // The field is optional
                    if (member(obj).has_value()) {
                        if constexpr (refl::descriptor::has_attribute<Flatten>(member)) {
                        Internal::mergeJsonObjects(json, valueToJson(*member(obj)));
                        } else {
                            json.insert(fieldInfo.name, valueToJson(*member(obj)));
                        }
                    }
                } else {
                    // The field is NOT optional
                    if constexpr (refl::descriptor::has_attribute<Flatten>(member)) {
                        Internal::mergeJsonObjects(json, valueToJson(member(obj)));
                    } else {
                        json.insert(fieldInfo.name, valueToJson(member(obj)));
                    }
                }
            }
        });

        return json;
    }

    template<typename T>
    requires Internal::IsQVector<T>
    Util::Result<T, DeserializeError> valueFromJson(const QJsonValue &value) {
        using ValueT = typename T::value_type;

        if (!value.isArray()) return Util::Err(DeserializeError(InvalidType()));

        QJsonArray array = value.toArray();

        QVector<ValueT> vec;
        vec.reserve(array.size());

        for (const QJsonValue &val : array) {
            Util::Result<ValueT, DeserializeError> opt = valueFromJson<ValueT>(val);
            if (opt.isErr()) return Util::Err(opt.unwrapErr());

            vec.append(opt.unwrap());
        }

        return Util::Ok(vec);
    }

    template<typename T>
    requires IsDeserialize<T>
    Util::Result<T, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isObject()) {
            return deserialize<T>(value.toObject());
        } else {
            return Util::Err(DeserializeError(InvalidType()));
        }
    }

    template<typename T>
    requires IsNewType<T>
    Util::Result<T, DeserializeError> valueFromJson(const QJsonValue &value) {
        constexpr auto tInfo = refl::reflect<T>();
        using NewTypeT = typename decltype(tInfo)::type;

        // Get the first field
        constexpr auto member = find_one(tInfo.members, [](auto) { return true; });
        using InnerFieldT = typename decltype(member)::value_type;

        Util::Result<InnerFieldT, DeserializeError> newTypeInner = valueFromJson<InnerFieldT>(value);

        if (newTypeInner.isOk()) {
            return Util::Ok(NewTypeT(std::move(newTypeInner.unwrap())));
        } else {
            return Util::Err(newTypeInner.unwrapErr());
        }
    }

    template<typename T>
    requires Internal::IsQHash<T>
    Util::Result<T, DeserializeError> valueFromJson(const QJsonValue &value) {
        using K = typename T::key_type;
        using V = typename T::mapped_type;

        if (!value.isObject()) {
            return Util::Err(DeserializeError(InvalidType()));
        }
        QJsonObject obj = value.toObject();

        QHash<K, V> map;
        for (auto iter = obj.begin(), end = obj.end(); iter != end; ++iter) {
            // If the key is a QString, we don't need to bother converting it to a QJsonValue and deserializing that
            std::optional<K> key;
            if constexpr (std::is_same_v<K, QString>) {
                key = iter.key();
            } else {
                const QJsonValue& jsonKey = QJsonValue(iter.key());
                Util::Result<K, DeserializeError> keyRes = valueFromJson<K>(jsonKey);

                if (keyRes.isErr()) {
                    return Util::Err(keyRes.unwrapErr());
                } else {
                    key = keyRes.unwrap();
                }
            }

            const QJsonValue& jsonValue = iter.value();
            Util::Result<V, DeserializeError> value = valueFromJson<V>(jsonValue);
            if (value.isErr()) {
                return Util::Err(value.unwrapErr());
            } else {
                map[*key] = std::move(value.unwrap());
            }
        }

        return Util::Ok(map);
    }

    template<typename T>
    requires IsSerialize<T>
    QJsonValue valueToJson(const T &value) {
        return serialize<T>(value);
    }

    template<typename T>
    requires IsNewType<T>
    QJsonValue valueToJson(const T &value) {
        constexpr auto tInfo = refl::reflect<T>();
        // Get the first field
        constexpr auto member = find_one(tInfo.members, [](auto) { return true; });
        using InnerFieldT = typename decltype(member)::value_type;

        return valueToJson<InnerFieldT>(member(value));
    }

    template<typename T>
    requires Internal::IsQVector<T>
    QJsonValue valueToJson(const T &value) {
        QJsonArray array;

        for (const auto &entry : value) {
            array.append(valueToJson(entry));
        }

        return array;
    }

    template<typename T>
    requires IsVariant<T>
    QJsonValue valueToJson(const T &value) {
        QJsonObject json;

        constexpr auto tInfo = refl::reflect<T>();
        Variant<T> variantInfo = refl::descriptor::get_attribute<Serde::Variant<T>>(tInfo);

        bool shouldStopIterating = false;
        for_each(Variant<T>::alternatives, [&](auto type) {
            if (shouldStopIterating) return;

            using AltType = typename decltype(type)::type;

            static_assert(refl::is_reflectable<AltType>(), "All Serde::Variant alternatives must be reflectable and marked with Serde::VariantAlt");
            constexpr auto altTypeInfo = refl::reflect<AltType>();

            static_assert(refl::descriptor::has_attribute<VariantAlt>(altTypeInfo), "All Serde::Variant alternatives must marked with Serde::VariantAlt");
            constexpr VariantAlt variantAltInfo = refl::descriptor::get_attribute<VariantAlt>(altTypeInfo);

            if (std::holds_alternative<AltType>(value)) {
                const AltType& alt = std::get<AltType>(value);

                json = Serde::serialize(alt);
                json.insert(variantInfo.tagName, variantAltInfo.typeName);

                shouldStopIterating = true;
            }
        });

        return json;
    }

    template<typename T>
    requires IsNewType<T>
    QString valueToString(const T &value) {
        constexpr auto tInfo = refl::reflect<T>();
        // Get the first field
        constexpr auto member = find_one(tInfo.members, [](auto) { return true; });
        using InnerFieldT = typename decltype(member)::value_type;

        return valueToString<InnerFieldT>(member(value));
    }

    namespace Internal {
        template<typename MemberT, size_t MemberN, typename ObjT>
        Util::Result<void, DeserializeError> deserializeOptional(
                const QJsonObject &json,
                refl::descriptor::field_descriptor<MemberT, MemberN> member,
                ObjT &obj
                ) {
            using FieldT = typename decltype(member)::value_type;
            using InnerFieldT = typename FieldT::value_type;

            Field fieldInfo = refl::descriptor::get_attribute<Field>(member);

            if (!json.contains(fieldInfo.name)) {
                member(obj) = std::nullopt;
                return Util::Ok();
            }

            Util::Result<InnerFieldT, DeserializeError> value = valueFromJson<InnerFieldT>(json.value(fieldInfo.name));

            // Write the deserialized value to the struct
            if (value.isOk()) {
                member(obj) = std::optional<InnerFieldT>(std::move(value.unwrap()));
                return Util::Ok();
            } else {
                return Util::Err(value.unwrapErr());
            }
        }

        template<typename MemberT, size_t MemberN, typename ObjT>
        Util::Result<void, DeserializeError> deserializeNonOptional(
                const QJsonObject &json,
                refl::descriptor::field_descriptor<MemberT, MemberN> member,
                ObjT &obj
                ) {
            using FieldT = typename decltype(member)::value_type;

            Field fieldInfo = refl::descriptor::get_attribute<Field>(member);

            // Non-optional, so fail if we're missing the field, unless if it has the Default attr, in which case
            // default construct it
            if (!json.contains(fieldInfo.name)) {
                if (refl::descriptor::has_attribute<Default>(member)) {
                    member(obj) = FieldT();
                    return Util::Ok();
                } else {
                    return Util::Err(DeserializeError(MissingField()));
                }
            }

            Util::Result<FieldT, DeserializeError> value = valueFromJson<FieldT>(json.value(fieldInfo.name));

            // Write the deserialized value to the struct
            if (value.isOk()) {
                member(obj) = std::move(value.unwrap());
                return Util::Ok();
            } else {
                return Util::Err(value.unwrapErr());
            }
        }
    }
}

#endif

