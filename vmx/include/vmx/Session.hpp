#ifndef VTX_VMX_SESSION_HPP
#define VTX_VMX_SESSION_HPP

#include "vmx/Serde.hpp"
#include "vmx_export.h"
#include <QUrl>
#include <QString>
#include <QMetaType>

namespace Vmx {
    struct VMX_EXPORT Session {
        QString accessToken;
        QUrl homeserverUrl;
    };
}

Q_DECLARE_METATYPE(Vmx::Session)

REFL_AUTO(
        type(Vmx::Session, Vmx::Serde::Serialize(), Vmx::Serde::Deserialize()),
        field(accessToken, Vmx::Serde::Field("access_token")),
        field(homeserverUrl, Vmx::Serde::Field("homeserver_url"))
        )

#endif
