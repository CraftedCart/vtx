/**
 * @file
 * @brief Misc convenience functions and what not go in here
 */

#ifndef VTX_VMX_VMX_HPP
#define VTX_VMX_VMX_HPP

#include "vmx_export.h"
#include <QLatin1String>
#include <memory>

namespace Vmx {
    template<typename T>
    using Box = std::unique_ptr<T>;

    template<typename T>
    using Arc = std::shared_ptr<T>;

    template<typename T>
    using Weak = std::weak_ptr<T>;

    template<std::size_t SIZE>
    constexpr QLatin1String constexprStr(const char (&str)[SIZE]);

    template<typename...>
    constexpr std::false_type alwaysFalse{};
}

#include "vmx/Vmx.ipp"

#endif
