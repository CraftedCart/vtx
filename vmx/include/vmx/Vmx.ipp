#ifndef VTX_VMX_VMX_IPP
#define VTX_VMX_VMX_IPP

namespace Vmx {
    template<std::size_t SIZE>
    constexpr QLatin1String constexprStr(const char (&str)[SIZE]) {
        return QLatin1String(str, SIZE - 1);
    }
}

#endif
