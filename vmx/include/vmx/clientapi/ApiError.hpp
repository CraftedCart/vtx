#ifndef VTX_VMX_CLIENTAPI_APIERROR_HPP
#define VTX_VMX_CLIENTAPI_APIERROR_HPP

#include "vmx/http/Status.hpp"
#include "vmx/Serde.hpp"
#include "vmx_export.h"
#include <QJsonObject>
#include <QString>
#include <QHash>
#include <variant>

namespace Vmx::ClientApi {
    enum class MxApiError {
        /**
         * @brief Forbidden access, e.g. joining a room without permission, failed login.
         */
        M_FORBIDDEN,

        /**
         * @brief The access token specified was not recognised.
         *
         * An additional response parameter, soft_logout, might be present on the response for 401 HTTP status codes.
         * See the soft logout section for more information.
         */
        M_UNKNOWN_TOKEN,

        /** @brief No access token was specified for the request. */
        M_MISSING_TOKEN,

        /**
         * @brief Request contained valid JSON, but it was malformed in some way, e.g. missing required keys, invalid
         * values for keys.
         */
        M_BAD_JSON,

        /**
         * @brief Request did not contain valid JSON.
         */
        M_NOT_JSON,

        /**
         * @brief No resource was found for this request.
         */
        M_NOT_FOUND,

        /**
         * @brief Too many requests have been sent in a short period of time. Wait a while then try again.
         */
        M_LIMIT_EXCEEDED,

        /**
         * @brief An unknown error has occurred.
         */
        M_UNKNOWN,

        /**
         * @brief The server did not understand the request.
         */
        M_UNRECOGNIZED,

        /**
         * @brief The request was not correctly authorized. Usually due to login failures.
         */
        M_UNAUTHORIZED,

        /**
         * @brief The user ID associated with the request has been deactivated.
         *
         * Typically for endpoints that prove authentication, such as /login.
         */
        M_USER_DEACTIVATED,

        /**
         * @brief Encountered when trying to register a user ID which has been taken.
         */
        M_USER_IN_USE,

        /**
         * @brief Encountered when trying to register a user ID which is not valid.
         */
        M_INVALID_USERNAME,

        /**
         * @brief Sent when the room alias given to the createRoom API is already in use.
         */
        M_ROOM_IN_USE,

        /**
         * @brief Sent when the initial state given to the createRoom API is invalid.
         */
        M_INVALID_ROOM_STATE,

        /**
         * @brief Sent when a threepid given to an API cannot be used because the same threepid is already in use.
         */
        M_THREEPID_IN_USE,

        /**
         * @brief Sent when a threepid given to an API cannot be used because no record matching the threepid was found.
         */
        M_THREEPID_NOT_FOUND,

        /**
         * @brief Authentication could not be performed on the third party identifier.
         */
        M_THREEPID_AUTH_FAILED,

        /**
         * @brief The server does not permit this third party identifier.
         *
         * This may happen if the server only permits, for example, email addresses from a particular domain.
         */
        M_THREEPID_DENIED,

        /**
         * @brief The client's request used a third party server, eg. identity server, that this server does not trust.
         */
        M_SERVER_NOT_TRUSTED,

        /**
         * @brief The client's request to create a room used a room version that the server does not support.
         */
        M_UNSUPPORTED_ROOM_VERSION,

        /**
         * @brief The client attempted to join a room that has a version the server does not support.
         *
         * Inspect the room_version property of the error response for the room's version.
         */
        M_INCOMPATIBLE_ROOM_VERSION,

        /**
         * @brief The state change requested cannot be performed, such as attempting to unban a user who is not banned.
         */
        M_BAD_STATE,

        /**
         * @brief The room or resource does not permit guests to access it.
         */
        M_GUEST_ACCESS_FORBIDDEN,

        /**
         * @brief A Captcha is required to complete the request.
         */
        M_CAPTCHA_NEEDED,

        /**
         * @brief The Captcha provided did not match what was expected.
         */
        M_CAPTCHA_INVALID,

        /**
         * @brief A required parameter was missing from the request.
         */
        M_MISSING_PARAM,

        /**
         * @brief A parameter that was specified has the wrong value.
         *
         * For example, the server expected an integer and instead received a string.
         */
        M_INVALID_PARAM,

        /**
         * @brief The request or entity was too large.
         */
        M_TOO_LARGE,

        /**
         * @brief The resource being requested is reserved by an application service, or the application service making
         *        the request has not created the resource.
         */
        M_EXCLUSIVE,

        /**
         * @brief The request cannot be completed because the homeserver has reached a resource limit imposed on it.
         *
         * For example, a homeserver held in a shared hosting environment may reach a resource limit if it starts using
         * too much memory or disk space. The error MUST have an admin_contact field to provide the user receiving the
         * error a place to reach out to. Typically, this error will appear on routes which attempt to modify state (eg:
         * sending messages, account data, etc) and not routes which only read state (eg: /sync, get account data, etc).
         */
        M_RESOURCE_LIMIT_EXCEEDED,

        /**
         * @brief The user is unable to reject an invite to join the server notices room.
         *
         * See the Server Notices module for more information.
         */
        M_CANNOT_LEAVE_SERVER_NOTICE_ROOM,
    };

    struct OtherApiError { QString errorCode; };
    struct BadResponseError { Serde::DeserializeError error; };

    using ApiErrorCode = std::variant<MxApiError, OtherApiError, BadResponseError>;

    struct ApiError {
        ApiErrorCode errorCode;
        QString message;
        QHash<QString, QJsonValue> additionalProperties;
        Http::Status httpStatus;

        ApiError() = default;

        ApiError(const BadResponseError& badResp, Http::Status httpStatus) :
            errorCode(badResp),
            httpStatus(httpStatus) {}

        ApiError(const BadResponseError& badResp, int httpStatus) :
            errorCode(badResp),
            httpStatus(static_cast<Http::Status>(httpStatus)) {}
    };

    VMX_EXPORT ApiErrorCode stringToErrorCode(const QString& str);
    VMX_EXPORT QString errorCodeToString(MxApiError err);
}

namespace Vmx::Serde {
    template<>
    [[nodiscard]]
    VMX_EXPORT Util::Result<Vmx::ClientApi::ApiErrorCode, DeserializeError> valueFromJson(const QJsonValue &value);
}

REFL_AUTO(
        type(Vmx::ClientApi::ApiError, Vmx::Serde::Deserialize()),
        field(errorCode, Vmx::Serde::Field("errcode")),
        field(message, Vmx::Serde::Field("error")),
        field(additionalProperties, Vmx::Serde::FieldPropertyMap())
        // httpStatus field intentionally omitted
        )

#endif
