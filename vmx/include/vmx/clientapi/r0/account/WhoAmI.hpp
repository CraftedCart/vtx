#ifndef VTX_VMX_CLIENTAPI_R0_ACCOUNT_WHOAMI_HPP
#define VTX_VMX_CLIENTAPI_R0_ACCOUNT_WHOAMI_HPP

#include "vmx/Metadata.hpp"
#include "vmx/id/UserId.hpp"
#include "vmx/refl.hpp"
#include "vmx/Vmx.hpp"
#include "vmx_export.h"
#include <QMetaType>

namespace Vmx::ClientApi::R0::Account::WhoAmI {
    struct VMX_EXPORT GetResponse {
        /** @brief The user ID that owns the access token */
        Id::UserId userId;
    };

    struct VMX_EXPORT GetRequest {
        static constexpr Metadata METADATA = {
            Vmx::constexprStr("/_matrix/client/r0/account/whoami"), // path
            QNetworkAccessManager::GetOperation, // method
            true, // requiresAuthentication
        };
        using Response = GetResponse;
    };
}

REFL_AUTO(
        type(Vmx::ClientApi::R0::Account::WhoAmI::GetResponse, Vmx::Serde::Deserialize()),
        field(userId, Vmx::Serde::Field("user_id"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Account::WhoAmI::GetRequest, Vmx::Serde::Serialize())
        )

Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Account::WhoAmI::GetResponse);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Account::WhoAmI::GetRequest);

#endif
