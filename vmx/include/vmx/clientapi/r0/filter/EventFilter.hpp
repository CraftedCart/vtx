#ifndef VTX_VMX_CLIENTAPI_R0_FILTER_EVENTFILTER_HPP
#define VTX_VMX_CLIENTAPI_R0_FILTER_EVENTFILTER_HPP

#include "vmx/id/UserId.hpp"
#include "vmx/Serde.hpp"
#include "vmx_export.h"
#include <QString>
#include <QVector>
#include <optional>

namespace Vmx::ClientApi::R0::Filter {
    struct VMX_EXPORT EventFilter {
        /** @brief The maximum number of events to return */
        std::optional<quint32> limit;

        /**
         * @brief A list of sender IDs to exclude
         *
         * If this list is absent then no senders are excluded. A matching sender will be excluded even if it is listed
         * in the `'senders'` filter.
         */
        std::optional<QVector<Id::UserId>> notSenders;

        /**
         * @brief A list of senders IDs to include
         *
         * If this list is absent then all senders are included.
         */
        std::optional<QVector<Id::UserId>> senders;

        /**
         * @brief A list of event types to exclude
         *
         * If this list is absent then no event types are excluded. A matching type will be excluded even if it is
         * listed in the 'types' filter. A '*' can be used as a wildcard to match any sequence of characters.
         */
        std::optional<QVector<QString>> notTypes;

        /**
         * @brief A list of event types to include.
         *
         * If this list is absent then all event types are included. A '*' can be used as a wildcard to match any
         * sequence of characters.
         */
        std::optional<QVector<QString>> types;
    };
}

REFL_AUTO(
        type(Vmx::ClientApi::R0::Filter::EventFilter, Vmx::Serde::Serialize()),
        field(limit, Vmx::Serde::Field("limit")),
        field(notSenders, Vmx::Serde::Field("not_senders")),
        field(senders, Vmx::Serde::Field("senders")),
        field(notTypes, Vmx::Serde::Field("not_types")),
        field(types, Vmx::Serde::Field("types"))
        )

#endif
