#ifndef VTX_VMX_CLIENTAPI_R0_FILTER_EVENTFORMAT_HPP
#define VTX_VMX_CLIENTAPI_R0_FILTER_EVENTFORMAT_HPP

#include "vmx/Serde.hpp"
#include "vmx_export.h"

namespace Vmx::ClientApi::R0::Filter {
    enum class EventFormat : quint8 {
        CLIENT,
        FEDERATION,
    };

    VMX_EXPORT uint qHash(EventFormat key, uint seed);
}

namespace Vmx::Serde {
    template<>
    [[nodiscard]]
    VMX_EXPORT Util::Result<ClientApi::R0::Filter::EventFormat, DeserializeError> valueFromJson(const QJsonValue &value);

    template<>
    [[nodiscard]]
    VMX_EXPORT QJsonValue valueToJson(const ClientApi::R0::Filter::EventFormat &value);

    template<>
    [[nodiscard]]
    VMX_EXPORT QString valueToString(const ClientApi::R0::Filter::EventFormat &value);
}

#endif
