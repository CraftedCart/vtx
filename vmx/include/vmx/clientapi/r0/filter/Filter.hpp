#ifndef VTX_VMX_CLIENTAPI_R0_FILTER_FILTER_HPP
#define VTX_VMX_CLIENTAPI_R0_FILTER_FILTER_HPP

#include "vmx/clientapi/r0/filter/FilterDef.hpp"
#include "vmx/id/FilterId.hpp"
#include "vmx/Serde.hpp"
#include "vmx_export.h"
#include <variant>

namespace Vmx::ClientApi::R0::Filter {
    using Filter = std::variant<FilterDef, Id::FilterId>;
}

namespace Vmx::Serde {
    template<>
    [[nodiscard]]
    VMX_EXPORT QString valueToString(const ClientApi::R0::Filter::Filter &value);
}

#endif

