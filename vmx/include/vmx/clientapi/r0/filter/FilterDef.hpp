#ifndef VTX_VMX_CLIENTAPI_R0_FILTER_FILTERDEF_HPP
#define VTX_VMX_CLIENTAPI_R0_FILTER_FILTERDEF_HPP

#include "vmx/clientapi/r0/filter/EventFormat.hpp"
#include "vmx/clientapi/r0/filter/EventFilter.hpp"
#include "vmx/clientapi/r0/filter/RoomFilter.hpp"
#include "vmx/Serde.hpp"
#include "vmx_export.h"
#include <QString>
#include <QVector>
#include <optional>

namespace Vmx::ClientApi::R0::Filter {
    struct VMX_EXPORT FilterDef {
        /**
         * @brief List of event fields to include
         *
         * If this list is absent then all fields are included. The entries may include '.' characters to indicate
         * sub-fields. So ['content.body'] will include the 'body' field of the 'content' object. A literal '.'
         * character in a field name may be escaped using a '\'. A server may include more fields than were requested.
         */
        std::optional<QVector<QString>> eventFields;

        /**
         * @brief The format to use for events
         *
         * 'client' will return the events in a format suitable for clients. 'federation' will return the raw event as
         * received over federation. The default is 'client'. One of: ["client", "federation"]
         */
        std::optional<EventFormat> eventFormat;

        /** @brief The presence updates to include. */
        std::optional<EventFilter> presence;

        /** @brief The user account data that isn't associated with rooms to include. */
        std::optional<EventFilter> accountData;

        /** @brief Filters to be applied to room data. */
        std::optional<RoomFilter> room;
    };
}

REFL_AUTO(
        type(Vmx::ClientApi::R0::Filter::FilterDef, Vmx::Serde::Serialize()),
        field(eventFields, Vmx::Serde::Field("event_fields")),
        field(eventFormat, Vmx::Serde::Field("event_format")),
        field(presence, Vmx::Serde::Field("presence")),
        field(accountData, Vmx::Serde::Field("account_data")),
        field(room, Vmx::Serde::Field("room"))
        )

#endif
