#ifndef VTX_VMX_CLIENTAPI_R0_FILTER_ROOMFILTER_HPP
#define VTX_VMX_CLIENTAPI_R0_FILTER_ROOMFILTER_HPP

#include "vmx/clientapi/r0/filter/RoomEventFilter.hpp"
#include "vmx/clientapi/r0/filter/StateFilter.hpp"
#include "vmx/id/RoomId.hpp"
#include "vmx/Serde.hpp"
#include "vmx_export.h"
#include <QVector>
#include <optional>

namespace Vmx::ClientApi::R0::Filter {
    struct VMX_EXPORT RoomFilter {
        /**
         * @brief A list of room IDs to exclude
         *
         * If this list is absent then no rooms are excluded. A matching room will be excluded even if it is listed in
         * the `'rooms'` filter. This filter is applied before the filters in `ephemeral`, `state`, `timeline` or
         * `account_data`
         */
        std::optional<QVector<Id::RoomId>> notRooms;

        /**
         * @brief A list of room IDs to include
         *
         * If this list is absent then all rooms are included.  This filter is applied before the filters in
         * `ephemeral`, `state`, `timeline` or `account_data`
         */
        std::optional<QVector<Id::RoomId>> rooms;

        /**
         * @brief The events that aren't recorded in the room history, e.g. typing and receipts, to include for rooms.
         */
        std::optional<RoomEventFilter> ephemeral;

        /** @brief Include rooms that the user has left in the sync, default `false` */
        std::optional<bool> includeLeave;

        /** @brief The state events to include for rooms */
        std::optional<StateFilter> state;

        /** @brief The message and state update events to include for rooms */
        std::optional<RoomEventFilter> timeline;

        /** @brief The per user account data to include for rooms */
        std::optional<RoomEventFilter> accountData;
    };
}

REFL_AUTO(
        type(Vmx::ClientApi::R0::Filter::RoomFilter, Vmx::Serde::Serialize()),
        field(notRooms, Vmx::Serde::Field("not_rooms")),
        field(rooms, Vmx::Serde::Field("rooms")),
        field(ephemeral, Vmx::Serde::Field("ephemeral")),
        field(includeLeave, Vmx::Serde::Field("include_leave")),
        field(state, Vmx::Serde::Field("state")),
        field(timeline, Vmx::Serde::Field("timeline")),
        field(accountData, Vmx::Serde::Field("account_data"))
        )

#endif
