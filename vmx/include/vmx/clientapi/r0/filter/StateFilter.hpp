#ifndef VTX_VMX_CLIENTAPI_R0_FILTER_STATEFILTER_HPP
#define VTX_VMX_CLIENTAPI_R0_FILTER_STATEFILTER_HPP

#include "vmx/id/UserId.hpp"
#include "vmx/id/RoomId.hpp"
#include "vmx/Serde.hpp"
#include "vmx_export.h"
#include <QString>
#include <QVector>
#include <optional>

namespace Vmx::ClientApi::R0::Filter {
    struct VMX_EXPORT StateFilter {
        /** @brief The maximum number of events to return */
        std::optional<quint32> limit;

        /**
         * @brief A list of sender IDs to exclude
         *
         * If this list is absent then no senders are excluded. A matching sender will be excluded even if it is listed
         * in the `'senders'` filter.
         */
        std::optional<QVector<Id::UserId>> notSenders;

        /**
         * @brief A list of senders IDs to include
         *
         * If this list is absent then all senders are included.
         */
        std::optional<QVector<Id::UserId>> senders;

        /**
         * @brief A list of event types to exclude
         *
         * If this list is absent then no event types are excluded. A matching type will be excluded even if it is
         * listed in the 'types' filter. A '*' can be used as a wildcard to match any sequence of characters.
         */
        std::optional<QVector<QString>> notTypes;

        /**
         * @brief A list of event types to include.
         *
         * If this list is absent then all event types are included. A '*' can be used as a wildcard to match any
         * sequence of characters.
         */
        std::optional<QVector<QString>> types;

        /**
         * @brief A list of room IDs to exclude
         *
         * If this list is absent then no rooms are excluded. A matching room will be excluded even if it is listed in
         * the `'rooms'` filter.
         */
        std::optional<QVector<Id::RoomId>> notRooms;

        /**
         * @brief A list of room IDs to include
         *
         * If this list is absent then all rooms are included.
         */
        std::optional<QVector<Id::RoomId>> rooms;

        /**
         * @brief If true, enables lazy-loading of membership events.
         *
         * See https://matrix.org/docs/spec/client_server/r0.6.1#lazy-loading-room-members for more information.
         * Defaults to `false`.
         */
        std::optional<bool> lazyLoadMembers;

        /**
         * @brief If `true`, sends all membership events for all events, even if they have already been sent to the client
         *
         * Does not apply unless `lazy_load_members` is true. See
         * https://matrix.org/docs/spec/client_server/r0.6.1#lazy-loading-room-members for more information. Defaults to
         * `false`.
         */
        std::optional<bool> includeRedundantMembers;

        /**
         * @brief If `true`, includes only events with a `url` key in their content.
         *
         * If `false`, excludes those events. If omitted, `url` key is not considered for filtering.
         */
        std::optional<bool> containsUrl;
    };
}

REFL_AUTO(
        type(Vmx::ClientApi::R0::Filter::StateFilter, Vmx::Serde::Serialize()),
        field(limit, Vmx::Serde::Field("limit")),
        field(notSenders, Vmx::Serde::Field("not_senders")),
        field(senders, Vmx::Serde::Field("senders")),
        field(notTypes, Vmx::Serde::Field("not_types")),
        field(types, Vmx::Serde::Field("types")),
        field(notRooms, Vmx::Serde::Field("not_rooms")),
        field(rooms, Vmx::Serde::Field("rooms")),
        field(lazyLoadMembers, Vmx::Serde::Field("lazy_load_members")),
        field(includeRedundantMembers, Vmx::Serde::Field("include_redundant_members")),
        field(containsUrl, Vmx::Serde::Field("contains_url"))
        )

#endif
