#ifndef VTX_VMX_CLIENTAPI_R0_KEY_KEYALGO_HPP
#define VTX_VMX_CLIENTAPI_R0_KEY_KEYALGO_HPP

#include "vmx/Serde.hpp"
#include "vmx_export.h"

namespace Vmx::ClientApi::R0::Key {
    enum class KeyAlgo : quint8 {
        ED25519,
        CURVE25519,
        SIGNED_CURVE25519,
    };

    VMX_EXPORT uint qHash(KeyAlgo key, uint seed);
}

namespace Vmx::Serde {
    template<>
    [[nodiscard]]
    VMX_EXPORT Util::Result<ClientApi::R0::Key::KeyAlgo, DeserializeError> valueFromJson(const QJsonValue &value);
}

#endif
