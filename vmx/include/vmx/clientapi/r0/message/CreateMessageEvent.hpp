#ifndef VTX_VMX_CLIENTAPI_R0_MESSAGE_CREATEMESSAGEEVENT_HPP
#define VTX_VMX_CLIENTAPI_R0_MESSAGE_CREATEMESSAGEEVENT_HPP

#include "vmx/Metadata.hpp"
#include "vmx/id/RoomId.hpp"
#include "vmx/id/EventId.hpp"
#include "vmx/Serde.hpp"
#include "vmx/refl.hpp"
#include "vmx/Vmx.hpp"
#include "vmx_export.h"
#include <QJsonObject>

namespace Vmx::ClientApi::R0::Message::CreateMessageEvent {
    struct VMX_EXPORT PutResponse {
        /** @brief The generated unique identifier for the event */
        Id::EventId eventId;
    };

    struct VMX_EXPORT PutRequest {
        static constexpr Metadata METADATA = {
            Vmx::constexprStr("/_matrix/client/r0/rooms/$roomId/send/$eventType/$txnId"), // path
            QNetworkAccessManager::PutOperation, // method
            true, // requiresAuthentication
        };
        using Response = PutResponse;

        Id::RoomId roomId;
        QString eventType;
        QString txnId;

        QJsonObject content;
    };
}

REFL_AUTO(
        type(Vmx::ClientApi::R0::Message::CreateMessageEvent::PutResponse, Vmx::Serde::Deserialize()),
        field(eventId, Vmx::Serde::Field("event_id"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Message::CreateMessageEvent::PutRequest, Vmx::Serde::Serialize()),
        field(roomId, Vmx::Serde::Path("roomId")),
        field(eventType, Vmx::Serde::Path("eventType")),
        field(txnId, Vmx::Serde::Path("txnId")),
        field(content, Vmx::Serde::Field(), Vmx::Serde::Flatten())
        )

#endif
