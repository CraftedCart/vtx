#ifndef VTX_VMX_CLIENTAPI_R0_MESSAGE_GETMESSAGEEVENTS_HPP
#define VTX_VMX_CLIENTAPI_R0_MESSAGE_GETMESSAGEEVENTS_HPP

#include "vmx/Metadata.hpp"
#include "vmx/clientapi/r0/filter/Filter.hpp"
#include "vmx/id/RoomId.hpp"
#include "vmx/event/SyncRoomEvent.hpp"
#include "vmx/event/SyncStateEvent.hpp"
#include "vmx/Serde.hpp"
#include "vmx/refl.hpp"
#include "vmx/Vmx.hpp"
#include "vmx_export.h"
#include <optional>

namespace Vmx::ClientApi::R0::Message::GetMessageEvents {
    enum class Direction : quint8 {
        BACKWARDS,
        FORWARDS,
    };

    struct VMX_EXPORT GetResponse {
        /**
         * @brief The token the pagination starts from
         *
         * If `dir=b` this will be the token supplied in `from`.
         */
        std::optional<QString> start;

        /**
         * @brief The token the pagination ends at
         *
         * If `dir=b` this token should be used again to request even earlier events.
         */
        std::optional<QString> end;

        /**
         * @brief A list of room events
         *
         * The order depends on the dir parameter. For dir=b events will be in reverse-chronological order, for dir=f in
         * chronological order, so that events start at the from point.
         */
        QVector<Event::SyncRoomEvent> chunk;

        /**
         * @brief A list of state events relevant to showing the chunk
         *
         * For example, if `lazy_load_members` is enabled in the filter then this may contain the membership events for
         * the senders of events in the chunk.
         *
         * Unless `include_redundant_members` is true, the server may remove membership events which would have already
         * been sent to the client in prior calls to this endpoint, assuming the membership of those members has not
         * changed.
         */
        QVector<Event::SyncStateEvent> state;
    };

    struct VMX_EXPORT GetRequest {
        static constexpr Metadata METADATA = {
            Vmx::constexprStr("/_matrix/client/r0/rooms/$roomId/messages"), // path
            QNetworkAccessManager::GetOperation, // method
            true, // requiresAuthentication
        };
        using Response = GetResponse;

        /** @brief The room to get events from */
        Id::RoomId roomId;

        /**
         * @brief The token to start returning events from
         *
         * This token can be obtained from a `prev_batch` token returned for each room by the sync API, or from a start or
         * end token returned by a previous request to this endpoint.
         */
        QString from;

        /**
         * @brief The token to stop returning events at
         *
         * This token can be obtained from a `prev_batch` token returned for each room by the sync endpoint, or from a
         * `start` or `end` token returned by a previous request to this endpoint.
         */
        std::optional<QString> to;

        /** @brief The direction to return events from */
        Direction dir;

        /**
         * @brief The maximum number of events to return
         *
         * Default: 10
         */
        std::optional<quint32> limit;

        /** @brief A JSON RoomEventFilter to filter returned events with */
        std::optional<Filter::Filter> filter;
    };
}

namespace Vmx::Serde {
    template<>
    [[nodiscard]]
    VMX_EXPORT QString valueToString(const Vmx::ClientApi::R0::Message::GetMessageEvents::Direction &value);
}

REFL_AUTO(
        type(Vmx::ClientApi::R0::Message::GetMessageEvents::GetResponse, Vmx::Serde::Deserialize()),
        field(start, Vmx::Serde::Field("start")),
        field(end, Vmx::Serde::Field("end")),
        field(chunk, Vmx::Serde::Field("chunk"), Vmx::Serde::Default()),
        field(state, Vmx::Serde::Field("state"), Vmx::Serde::Default())
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Message::GetMessageEvents::GetRequest, Vmx::Serde::Serialize()),
        field(roomId, Vmx::Serde::Path("roomId")),
        field(from, Vmx::Serde::Query("from")),
        field(to, Vmx::Serde::Query("to")),
        field(dir, Vmx::Serde::Query("dir")),
        field(limit, Vmx::Serde::Query("limit")),
        field(filter, Vmx::Serde::Query("filter"))
        )

#endif
