#ifndef VTX_VMX_CLIENTAPI_R0_SESSION_LOGIN_HPP
#define VTX_VMX_CLIENTAPI_R0_SESSION_LOGIN_HPP

#include "vmx/Metadata.hpp"
#include "vmx/id/UserId.hpp"
#include "vmx/Session.hpp"
#include "vmx/Serde.hpp"
#include "vmx/Vmx.hpp"
#include "vmx_export.h"
#include <QJsonObject>
#include <QString>
#include <QMetaType>
#include <variant>

namespace Vmx::ClientApi::R0::Session::Login {
    /** @brief A full user id (`@alice:matrix.org`) or just the local part (`alice`) */
    struct VMX_EXPORT MatrixId {
        QString id;

        bool operator==(const MatrixId &other) const = default;
    };

    /** @brief A third party identifier for a Matrix user */
    struct VMX_EXPORT ThirdPartyId {
        /**
         * @brief The namespace in which the identifier exists
         *
         * Eg: `email` or `msisdn`
         */
        QString medium;

        /**
         * @brief The third-party address for the user
         *
         * Eg: If `medium` is `email`, this would be the user's email address (such as `alice@example.com`)
         */
        QString address;

        bool operator==(const ThirdPartyId &other) const = default;
    };

    /** @brief A phone-number identifier for a Matrix user */
    struct VMX_EXPORT PhoneId {
        /**
         * @brief The country code the phone number is from
         *
         * The two-letter uppercase ISO-3166-1 alpha-2 country code that the phone number should be parsed as if it were
         * dialed from
         */
        QString country;

        /** @brief The phone number */
        QString phone;

        bool operator==(const PhoneId &other) const = default;
    };

    /** @brief A way to identify a user */
    using UserInfo = std::variant<MatrixId, ThirdPartyId, PhoneId>;

    ////////////////

    /** @brief Log in with a password */
    struct VMX_EXPORT Password {
        QString password;

        bool operator==(const Password &other) const = default;
    };

    /** @brief Log in with an access token */
    struct VMX_EXPORT Token {
        QString token;

        bool operator==(const Token &other) const = default;
    };

    using LoginInfo = std::variant<Password, Token>;

    ////////////////

    struct VMX_EXPORT HomeserverInfo {
        QString baseUrl;

        bool operator==(const HomeserverInfo &other) const = default;
    };

    struct VMX_EXPORT IdentityServerInfo {
        QString baseUrl;

        bool operator==(const IdentityServerInfo &other) const = default;
    };

    struct VMX_EXPORT DiscoveryInfo {
        HomeserverInfo homeserver;
        std::optional<IdentityServerInfo> identityServer;

        /** @brief Application-dependent keys using Java package naming convention */
        QHash<QString, QJsonObject> additionalProperties;

        bool operator==(const DiscoveryInfo &other) const = default;
    };

    ////////////////

    using DeviceId = QString;

    ////////////////

    struct VMX_EXPORT PostResponse {
        /** @brief The fully qualified Matrix ID (Eg: `@alice:matrix.org`) */
        Id::UserId userId;

        /** @brief The access token to authenticate the account as the given device */
        QString accessToken;

        /**
         * @brief The server name of the homeserver on which the account is registered
         *
         * @deprecated The Matrix spec has deprecated this parameter
         */
        std::optional<QString> homeserver;

        /**
         * @brief ID of the logged in device
         *
         * Will be the same as the `deviceId` in the request if one was given.
         */
        DeviceId deviceId;

        /** @brief Optional client configuration provided by the server.  */
        std::optional<DiscoveryInfo> wellKnown;

        Vmx::Session makeSession() const;

        bool operator==(const PostResponse &other) const = default;
    };

    struct VMX_EXPORT PostRequest {
        static constexpr Metadata METADATA = {
            Vmx::constexprStr("/_matrix/client/r0/login"), // path
            QNetworkAccessManager::PostOperation, // method
            false, // requiresAuthentication
        };
        using Response = PostResponse;

        /** @brief Which user we want to authenticate */
        UserInfo user;

        /** @brief How we want to log in */
        LoginInfo loginInfo;

        /** @brief The device ID we should use, or nullopt if the server should generate one */
        std::optional<DeviceId> deviceId;

        /** @brief The initial device display name if a new device is to be created */
        std::optional<QString> initialDeviceDisplayName;

        bool operator==(const PostRequest &other) const = default;
    };
}

REFL_AUTO(
        type(Vmx::ClientApi::R0::Session::Login::MatrixId, Vmx::Serde::Serialize(), Vmx::Serde::VariantAlt("m.id.user")),
        field(id, Vmx::Serde::Field("user"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Session::Login::ThirdPartyId, Vmx::Serde::Serialize(), Vmx::Serde::VariantAlt("m.id.thirdparty")),
        field(medium, Vmx::Serde::Field("medium")),
        field(address, Vmx::Serde::Field("address"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Session::Login::PhoneId, Vmx::Serde::Serialize(), Vmx::Serde::VariantAlt("m.id.phone")),
        field(country, Vmx::Serde::Field("country")),
        field(phone, Vmx::Serde::Field("phone"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Session::Login::UserInfo, Vmx::Serde::Variant<Vmx::ClientApi::R0::Session::Login::UserInfo>("type"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Session::Login::Password, Vmx::Serde::Serialize(), Vmx::Serde::VariantAlt("m.login.password")),
        field(password, Vmx::Serde::Field("password"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Session::Login::Token, Vmx::Serde::Serialize(), Vmx::Serde::VariantAlt("m.login.token")),
        field(token, Vmx::Serde::Field("token"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Session::Login::LoginInfo, Vmx::Serde::Variant<Vmx::ClientApi::R0::Session::Login::LoginInfo>("type"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Session::Login::HomeserverInfo, Vmx::Serde::Deserialize()),
        field(baseUrl, Vmx::Serde::Field("base_url"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Session::Login::IdentityServerInfo, Vmx::Serde::Deserialize()),
        field(baseUrl, Vmx::Serde::Field("base_url"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Session::Login::DiscoveryInfo, Vmx::Serde::Deserialize()),
        field(homeserver, Vmx::Serde::Field("m.homeserver")),
        field(identityServer, Vmx::Serde::Field("m.identity_server")),
        field(additionalProperties, Vmx::Serde::FieldPropertyMap())
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Session::Login::PostResponse, Vmx::Serde::Deserialize()), field(userId, Vmx::Serde::Field("user_id")),
        field(accessToken, Vmx::Serde::Field("access_token")),
        field(homeserver, Vmx::Serde::Field("home_server")),
        field(deviceId, Vmx::Serde::Field("device_id")),
        field(wellKnown, Vmx::Serde::Field("well_known"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Session::Login::PostRequest, Vmx::Serde::Serialize()),
        field(user, Vmx::Serde::Field("identifier")),
        field(loginInfo, Vmx::Serde::Field(), Vmx::Serde::Flatten()),
        field(deviceId, Vmx::Serde::Field("device_id")),
        field(initialDeviceDisplayName, Vmx::Serde::Field("initial_device_display_name"))
        )

Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Session::Login::MatrixId);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Session::Login::ThirdPartyId);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Session::Login::PhoneId);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Session::Login::Password);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Session::Login::Token);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Session::Login::HomeserverInfo);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Session::Login::IdentityServerInfo);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Session::Login::DiscoveryInfo);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Session::Login::PostResponse);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Session::Login::PostRequest);

#endif
