#ifndef VTX_VMX_CLIENTAPI_R0_SYNC_SYNCEVENTS_HPP
#define VTX_VMX_CLIENTAPI_R0_SYNC_SYNCEVENTS_HPP

#include "vmx/Metadata.hpp"
#include "vmx/id/RoomId.hpp"
#include "vmx/id/UserId.hpp"
#include "vmx/event/SentEvent.hpp"
#include "vmx/event/SyncStateEvent.hpp"
#include "vmx/event/StrippedStateEvent.hpp"
#include "vmx/clientapi/r0/filter/Filter.hpp"
#include "vmx/clientapi/r0/key/KeyAlgo.hpp"
#include "vmx/refl.hpp"
#include "vmx/Vmx.hpp"
#include "vmx_export.h"
#include <QHash>
#include <QVector>
#include <QJsonObject>
#include <QString>
#include <QMetaType>
#include <optional>

namespace Vmx::ClientApi::R0::Sync::SyncEvents {
    /** @brief General information about a room which clients may need to render */
    struct VMX_EXPORT RoomSummary {
        /**
         * @brief The user IDs which can be used to generate a room name if a room does not have a name
         *
         * This field is required if the `m.room.name` or `m.room.canonical_alias` state events are unset or empty.
         *
         * This should be the first 5 members of the room, ordered by stream ordering, which are joined or invited. The
         * list must never include the client's own user ID. When no joined or invited members are available, this
         * should consist of the banned and left users. More than 5 members may be provided, however less than 5 should
         * only be provided when there are less than 5 members to represent.
         *
         * When lazy-loading room members is enabled, the membership events for the heroes MUST be included in the
         * state, unless they are redundant. When the list of users changes, the server notifies the client by sending a
         * fresh list of heroes. If there are no changes since the last sync, this field may be omitted.
         */
        std::optional<QVector<Id::UserId>> heroes;

        /**
         * @brief The number of users with `membership` of `join`, including the client's own user ID
         *
         * May be omitted if this hasn't changed since the last sync - required otherwise.
         */
        std::optional<quint32> joinedMemberCount;

        /**
         * @brief The number of users with `membership` of `invite`
         *
         * May be omitted if this hasn't changed since the last sync - required otherwise.
         */
        std::optional<quint32> invitedMemberCount;
    };

    struct VMX_EXPORT State {
        /** @brief A list of state events */
        QVector<Event::SyncStateEvent> events;
    };

    /** @brief The timeline of messages and stage changed received when syncing */
    struct VMX_EXPORT Timeline {
        /** @brief The list of received events */
        QVector<Event::SyncRoomEvent> events;

        /** @brief True if the number of events was limited by the `limit` on the filter */
        std::optional<bool> limited;

        /** @brief A token that can be supplied to the `from` parameter of the `rooms/{roomId}/messages` endpoint. */
        std::optional<QString> prevBatch;
    };

    /**
     * @brief Short lived events that aren't recorded in the room timeline/state
     *
     * Eg: Typing notifications
     */
    struct VMX_EXPORT Ephemeral {
        /** @brief The list of received ephemeral events */
        QVector<Event::Event> events;
    };

    struct VMX_EXPORT AccountData {
        /** @brief The list of received account data events */
        QVector<Event::Event> events;
    };

    /** @brief Counts of unread notifications for this room. */
    struct VMX_EXPORT UnreadNotificationCounts {
        /** @brief The number of unread notifications for this room with the highlight flag set */
        std::optional<quint32> highlightCount;

        /** @brief The total number of unread notifications for this room */
        std::optional<quint32> notificationCount;
    };

    /** @brief Updates to joined rooms */
    struct VMX_EXPORT JoinedRoom {
        /** @brief Updates to general room information which clients may need to render to users */
        RoomSummary summary;

        /**
         * @brief Updates to the state, between the time indicated by the `since` parameter, and the start of the
         *        `timeline` (or all state up to the start of the `timeline`, if `since` is not given, or `full_state`
         *        is true).
         *
         * N.B. state updates for `m.room.member` events will be incomplete if `lazy_load_members` is enabled in the
         * `/sync` filter, and only return the member events required to display the senders of the timeline events in
         * this response.
         */
        State state;

        Timeline timeline;
        Ephemeral ephemeral;
        AccountData accountData;
        UnreadNotificationCounts unreadNotifications;
    };

    struct VMX_EXPORT InviteState {
        /** @brief The stripped state events that form the invite state */
        QVector<Event::StrippedStateEvent> events;
    };

    struct VMX_EXPORT InvitedRoom {
        /** @brief The state of the room that the user has been invited to */
        std::optional<InviteState> inviteState;
    };

    struct VMX_EXPORT LeftRoom {
        /** @brief The state updates for the room up to the start of the timeline */
        std::optional<State> state;

        /** @brief The timeline of messages and state events up to the point where the user left */
        std::optional<Timeline> timeline;

        /** @brief The private data the user has attached to the room */
        std::optional<AccountData> accountData;
    };

    struct VMX_EXPORT Rooms {
        /** @brief The rooms that the user has joined */
        QHash<Id::RoomId, JoinedRoom> join;

        /** @brief The rooms that the user has been invited to */
        QHash<Id::RoomId, InvitedRoom> invite;

        /** @brief The rooms that the user has left or been kicked/banned from */
        QHash<Id::RoomId, LeftRoom> leave;
    };

    struct VMX_EXPORT Presence {
        /** @brief The list of received prescence events */
        QVector<Event::SentEvent> events;
    };

    struct VMX_EXPORT ToDevice {
        /** @brief The list of received events sent to a device */
        QVector<Event::SentEvent> events;
    };

    /** @brief Information on E2E device updates */
    struct VMX_EXPORT DeviceLists {
        /**
         * @brief List of users who have updated their device identity keys, or who now share an encrypted room with the
         *        client since the previous sync response.
         */
        QVector<Id::UserId> changed;

        /**
         * @brief List of users with whom we do not share any encrypted rooms anymore since the previous sync response.
         */
        QVector<Id::UserId> left;
    };

    struct VMX_EXPORT GetResponse {
        /** @brief The batch token to supply in the `since` param of the next `/sync` request. */
        QString nextBatch;

        /** @brief Updates to rooms */
        std::optional<Rooms> rooms;

        /** @brief Updates to the prescence status of other users */
        std::optional<Presence> presence;

        /** @brief Information on the send-to-device messages for the client device */
        std::optional<ToDevice> toDevice;

        /** @brief The global private data created by the user */
        std::optional<AccountData> accountData;

        /** @brief Information on E2E device updates - only present on incremental syncs */
        std::optional<DeviceLists> deviceLists;

        /**
         * @brief For each key algorithm, the number of unclaimed one-time keys currently held on the server for this
         *        device.
         */
        QHash<Key::KeyAlgo, quint32> deviceOneTimeKeysCount;
    };

    struct VMX_EXPORT GetRequest {
        static constexpr Metadata METADATA = {
            Vmx::constexprStr("/_matrix/client/r0/sync"), // path
            QNetworkAccessManager::GetOperation, // method
            true, // requiresAuthentication
        };
        using Response = GetResponse;

        /**
         * @brief The ID of a filter created using the filter API or a filter JSON object encoded as a string
         *
         * The server will detect whether it is an ID or a JSON object by whether the first character is a "{" open
         * brace. Passing the JSON inline is best suited to one off requests. Creating a filter using the filter API is
         * recommended for clients that reuse the same filter multiple times, for example in long poll requests.
         *
         * See https://matrix.org/docs/spec/client_server/r0.6.1#filtering for more information.
         */
        std::optional<Filter::Filter> filter;

        /** @brief A point in time to continue a sync from */
        std::optional<QString> since;

        /**
         * @brief Controls whether to include the full room state of all rooms the user is a member of
         *
         * If this is set to true, then all state events will be returned, even if since is non-empty. The timeline will
         * still be limited by the since parameter. In this case, the timeout parameter will be ignored and the query
         * will return immediately, possibly with an empty timeline.
         *
         * If false, and since is non-empty, only state which has changed since the point indicated by since will be returned.
         *
         * By default, this is false.
         */
        std::optional<bool> fullState;

        // TODO: set_presence

        /**
         * The maximum time to wait, in milliseconds, before returning this request. If no events (or other data) become
         * available before this time elapses, the server will return a response with empty fields.
         *
         * By default, this is 0, so the server will return immediately even if the response is empty.
         */
        std::optional<quint32> timeout;
    };
}

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::RoomSummary, Vmx::Serde::Deserialize()),
        field(heroes, Vmx::Serde::Field("heroes")),
        field(joinedMemberCount, Vmx::Serde::Field("joined_member_count")),
        field(invitedMemberCount, Vmx::Serde::Field("invited_member_count"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::State, Vmx::Serde::Deserialize()),
        field(events, Vmx::Serde::Field("events"), Vmx::Serde::Default())
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::Timeline, Vmx::Serde::Deserialize()),
        field(events, Vmx::Serde::Field("events"), Vmx::Serde::Default()),
        field(limited, Vmx::Serde::Field("limited")),
        field(prevBatch, Vmx::Serde::Field("prev_batch"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::Ephemeral, Vmx::Serde::Deserialize()),
        field(events, Vmx::Serde::Field("events"), Vmx::Serde::Default())
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::AccountData, Vmx::Serde::Deserialize()),
        field(events, Vmx::Serde::Field("events"), Vmx::Serde::Default())
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::UnreadNotificationCounts, Vmx::Serde::Deserialize()),
        field(highlightCount, Vmx::Serde::Field("highlight_count")),
        field(notificationCount, Vmx::Serde::Field("notification_count"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom, Vmx::Serde::Deserialize()),
        field(summary, Vmx::Serde::Field("summary")),
        field(state, Vmx::Serde::Field("state")),
        field(timeline, Vmx::Serde::Field("timeline")),
        field(ephemeral, Vmx::Serde::Field("ephemeral")),
        field(accountData, Vmx::Serde::Field("account_data")),
        field(unreadNotifications, Vmx::Serde::Field("unread_notifications"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::InviteState, Vmx::Serde::Deserialize()),
        field(events, Vmx::Serde::Field("events"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::InvitedRoom, Vmx::Serde::Deserialize()),
        field(inviteState, Vmx::Serde::Field("invite_state"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::LeftRoom, Vmx::Serde::Deserialize()),
        field(state, Vmx::Serde::Field("state")),
        field(timeline, Vmx::Serde::Field("timeline")),
        field(accountData, Vmx::Serde::Field("account_data"))
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::Rooms, Vmx::Serde::Deserialize()),
        field(join, Vmx::Serde::Field("join"), Vmx::Serde::Default()),
        field(invite, Vmx::Serde::Field("invite"), Vmx::Serde::Default()),
        field(leave, Vmx::Serde::Field("leave"), Vmx::Serde::Default())
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::Presence, Vmx::Serde::Deserialize()),
        field(events, Vmx::Serde::Field("events"), Vmx::Serde::Default())
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::ToDevice, Vmx::Serde::Deserialize()),
        field(events, Vmx::Serde::Field("events"), Vmx::Serde::Default())
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::DeviceLists, Vmx::Serde::Deserialize()),
        field(changed, Vmx::Serde::Field("changed"), Vmx::Serde::Default()),
        field(left, Vmx::Serde::Field("left"), Vmx::Serde::Default())
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::GetResponse, Vmx::Serde::Deserialize()),
        field(nextBatch, Vmx::Serde::Field("next_batch")),
        field(rooms, Vmx::Serde::Field("rooms")),
        field(presence, Vmx::Serde::Field("presence")),
        field(toDevice, Vmx::Serde::Field("to_device")),
        field(accountData, Vmx::Serde::Field("account_data")),
        field(deviceLists, Vmx::Serde::Field("device_lists")),
        field(deviceOneTimeKeysCount, Vmx::Serde::Field("device_one_time_keys_count"), Vmx::Serde::Default())
        )

REFL_AUTO(
        type(Vmx::ClientApi::R0::Sync::SyncEvents::GetRequest, Vmx::Serde::Serialize()),
        field(filter, Vmx::Serde::Query("filter")),
        field(since, Vmx::Serde::Query("since")),
        field(fullState, Vmx::Serde::Query("full_state")),
        field(timeout, Vmx::Serde::Query("timeout"))
        )

Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::RoomSummary);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::State);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::Timeline);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::Ephemeral);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::AccountData);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::UnreadNotificationCounts);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::InviteState);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::InvitedRoom);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::LeftRoom);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::Rooms);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::Presence);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::ToDevice);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::DeviceLists);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::GetResponse);
Q_DECLARE_METATYPE(Vmx::ClientApi::R0::Sync::SyncEvents::GetRequest);

#endif
