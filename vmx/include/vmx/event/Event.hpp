#ifndef VTX_VMX_EVENT_EVENT_HPP
#define VTX_VMX_EVENT_EVENT_HPP

#include "vmx/Serde.hpp"
#include "vmx_export.h"
#include <QJsonObject>
#include <QString>

namespace Vmx::Event {
    /** @brief The base for all received event types */
    class VMX_EXPORT Event {
        public:
            /**
             * @brief The type of event (eg: `m.room.message`)
             *
             * The type of event. This SHOULD be namespaced similar to Java package naming conventions e.g.
             * `com.example.subdomain.event.type`
             */
            QString type;

            /** @brief The fields of `content` will vary depending on the event type */
            QJsonObject content;

            /** @brief The entire event as JSON */
            QJsonObject json;

        public:
            Event() = default;

            // Copy ctor
            Event(const Event &other) = default;

            Event(const QString &type, const QJsonObject &content, const QJsonObject &json);
    };
}

REFL_AUTO(
        type(Vmx::Event::Event, Vmx::Serde::Deserialize()),
        field(type, Vmx::Serde::Field("type")),
        field(content, Vmx::Serde::Field("content")),
        field(json, Vmx::Serde::FieldPropertyMap())
        )

#endif
