#ifndef VTX_VMX_EVENT_ROOMEVENT_HPP
#define VTX_VMX_EVENT_ROOMEVENT_HPP

#include "vmx/event/SyncRoomEvent.hpp"
#include "vmx/id/RoomId.hpp"
#include "vmx_export.h"

namespace Vmx::Event {
    class VMX_EXPORT RoomEvent : public SyncRoomEvent {
        public:
            Id::RoomId roomId;

        public:
            RoomEvent() = default;

            // Copy ctor
            RoomEvent(const RoomEvent &other) = default;

            RoomEvent(
                    const QString &type,
                    const QJsonObject &content,
                    const QJsonObject &json,
                    const Id::UserId &sender,
                    const Id::EventId &eventId,
                    const QDateTime &originServerTs,
                    std::optional<UnsignedData> &&unsignedData,
                    const QString &roomId
                    );
    };
}

REFL_AUTO(
        type(Vmx::Event::RoomEvent, bases<Vmx::Event::SyncRoomEvent>, Vmx::Serde::Deserialize()),
        field(roomId, Vmx::Serde::Field("room_id"))
        )

#endif
