#ifndef VTX_VMX_EVENT_SENTEVENT_HPP
#define VTX_VMX_EVENT_SENTEVENT_HPP

#include "vmx/event/Event.hpp"
#include "vmx/id/UserId.hpp"
#include "vmx_export.h"
#include <QJsonObject>
#include <QString>

namespace Vmx::Event {
    /** @brief A state event with a limited set of fields */
    class VMX_EXPORT SentEvent : public Event {
        public:
            /** @brief The user ID of who sent the event */
            Id::UserId sender;

        public:
            SentEvent() = default;

            // Copy ctor
            SentEvent(const SentEvent &other) = default;

            SentEvent(
                    const QString &type,
                    const QJsonObject &content,
                    const QJsonObject &json,
                    const Id::UserId &sender
                    );
    };
}

REFL_AUTO(
        type(Vmx::Event::SentEvent, bases<Vmx::Event::Event>, Vmx::Serde::Deserialize()),
        field(sender, Vmx::Serde::Field("sender"))
        )

#endif
