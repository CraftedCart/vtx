#ifndef VTX_VMX_EVENT_STATEEVENT_HPP
#define VTX_VMX_EVENT_STATEEVENT_HPP

#include "vmx/event/SyncStateEvent.hpp"
#include "vmx/id/RoomId.hpp"
#include "vmx_export.h"

namespace Vmx::Event {
    class VMX_EXPORT StateEvent : public SyncStateEvent {
        public:
            Id::RoomId roomId;

        public:
            StateEvent() = default;

            StateEvent(
                    const QString &type,
                    const QJsonObject &content,
                    const QJsonObject &json,
                    const Id::UserId &sender,
                    const Id::EventId &eventId,
                    const QDateTime &originServerTs,
                    std::optional<UnsignedData> &&unsignedData,
                    const QString &stateKey,
                    const std::optional<QJsonObject> &&prevContent,
                    const QString &roomId
                    );
    };
}

REFL_AUTO(
        type(Vmx::Event::StateEvent, bases<Vmx::Event::SyncStateEvent>, Vmx::Serde::Deserialize()),
        field(roomId, Vmx::Serde::Field("room_id"))
        )

#endif
