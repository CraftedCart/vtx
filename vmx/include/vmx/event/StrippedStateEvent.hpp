#ifndef VTX_VMX_EVENT_STRIPPEDSTATEEVENT_HPP
#define VTX_VMX_EVENT_STRIPPEDSTATEEVENT_HPP

#include "vmx/event/SentEvent.hpp"
#include "vmx_export.h"
#include <QJsonObject>
#include <QString>

namespace Vmx::Event {
    /** @brief A state event with a limited set of fields */
    class VMX_EXPORT StrippedStateEvent : public SentEvent {
        public:
            QString stateKey;

        public:
            StrippedStateEvent() = default;

            // Copy ctor
            StrippedStateEvent(const StrippedStateEvent &other) = default;

            StrippedStateEvent(
                    const QString &type,
                    const QJsonObject &content,
                    const QJsonObject &json,
                    const Id::UserId &sender,
                    const QString &stateKey
                    );
    };
}

REFL_AUTO(
        type(Vmx::Event::StrippedStateEvent, bases<Vmx::Event::SentEvent>, Vmx::Serde::Deserialize()),
        field(stateKey, Vmx::Serde::Field("state_key"))
        )

#endif
