#ifndef VTX_VMX_EVENT_SYNCROOMEVENT_HPP
#define VTX_VMX_EVENT_SYNCROOMEVENT_HPP

#include "vmx/event/SentEvent.hpp"
#include "vmx/event/UnsignedData.hpp"
#include "vmx/id/EventId.hpp"
#include "vmx/id/UserId.hpp"
#include "vmx_export.h"
#include <QDateTime>
#include <QMetaType>
#include <optional>

namespace Vmx::Event {
    /** @brief A room event, received via the /sync endpoint */
    class VMX_EXPORT SyncRoomEvent : public SentEvent {
        public:
            /** @brief The globally unique event identifier */
            Id::EventId eventId;

            /** @brief The timestamp of when the event was sent on the originating homeserver */
            QDateTime originServerTs;

            /** @brief Optional extra information about the event */
            std::optional<UnsignedData> unsignedData;

        public:
            SyncRoomEvent() = default;

            // Copy ctor
            SyncRoomEvent(const SyncRoomEvent &other) = default;

            // Copy heavier objects
            SyncRoomEvent(
                    const QString &type,
                    const QJsonObject &content,
                    const QJsonObject &json,
                    const Id::UserId &sender,
                    const Id::EventId &eventId,
                    const QDateTime &originServerTs,
                    std::optional<UnsignedData> &unsignedData
                    );

            // Move heavier objects
            SyncRoomEvent(
                    const QString &type,
                    const QJsonObject &content,
                    const QJsonObject &json,
                    const Id::UserId &sender,
                    const Id::EventId &eventId,
                    const QDateTime &originServerTs,
                    std::optional<UnsignedData> &&unsignedData
                    );
    };
}

REFL_AUTO(
        type(Vmx::Event::SyncRoomEvent, bases<Vmx::Event::SentEvent>, Vmx::Serde::Deserialize()),
        field(eventId, Vmx::Serde::Field("event_id")),
        field(originServerTs, Vmx::Serde::Field("origin_server_ts")),
        field(unsignedData, Vmx::Serde::Field("unsigned"))
        )

#endif
