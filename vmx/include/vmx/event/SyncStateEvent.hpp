#ifndef VTX_VMX_EVENT_SYNCSTATEEVENT_HPP
#define VTX_VMX_EVENT_SYNCSTATEEVENT_HPP

#include "vmx/event/SyncRoomEvent.hpp"
#include "vmx_export.h"
#include <QJsonObject>
#include <optional>

namespace Vmx::Event {
    /** @brief A state event, received via the /sync endpoint */
    class VMX_EXPORT SyncStateEvent : public SyncRoomEvent {
        public:
            /**
             * @brief A unique key which defines the overwriting semantics for this piece of room state.
             *
             * This value is often a zero-length string. The presence of this key makes this event a State Event.
             *
             * State keys starting with an ``@`` are reserved for referencing user IDs, such as room members. With the
             * exception of a few events, state events set with a given user's ID as the state key MUST only be set by
             * that user.
             */
            QString stateKey;

            /**
             * @brief The previous `content` for this event. If there is no previous content, this will be `nullopt`.
             */
            std::optional<QJsonObject> prevContent;

        public:
            SyncStateEvent() = default;

            // Copy ctor
            SyncStateEvent(const SyncStateEvent &other) = default;

            // Copy heavier objects
            SyncStateEvent(
                    const QString &type,
                    const QJsonObject &content,
                    const QJsonObject &json,
                    const Id::UserId &sender,
                    const Id::EventId &eventId,
                    const QDateTime &originServerTs,
                    std::optional<UnsignedData> &unsignedData,
                    const QString &stateKey,
                    const std::optional<QJsonObject> &prevContent
                    );

            // Move heavier objects
            SyncStateEvent(
                    const QString &type,
                    const QJsonObject &content,
                    const QJsonObject &json,
                    const Id::UserId &sender,
                    const Id::EventId &eventId,
                    const QDateTime &originServerTs,
                    std::optional<UnsignedData> &&unsignedData,
                    const QString &stateKey,
                    const std::optional<QJsonObject> &&prevContent
                    );
    };
}

REFL_AUTO(
        type(Vmx::Event::SyncStateEvent, bases<Vmx::Event::SyncRoomEvent>, Vmx::Serde::Deserialize()),
        field(stateKey, Vmx::Serde::Field("state_key")),
        field(prevContent, Vmx::Serde::Field("prev_content"))
        )

#endif
