#ifndef VTX_VMX_ID_EVENTID_HPP
#define VTX_VMX_ID_EVENTID_HPP

#include "vmx/Serde.hpp"
#include "vmx/InString.hpp"
#include "vmx_export.h"
#include <QString>

namespace Vmx::Id {
    /**
     * @brief A full event id (Eg: `$vSsk-jEkV0TbYPVjTNeENyNJTYNKpiaglCwD8a2Ie9c`)
     *
     * The format of event identifiers has changed between different room versions in Matrix.
     *
     * Original IDs use a randomized localpart followed by the homeserver domain/port (Eg: `$h29iv0s8:example.com`)
     *
     * As of room v3, event IDs changed to being a hash of the event, encoded with base64 (Eg:
     * `$acR1l0raoZnm60CBwAVgqbZqoO/mYU81xysh1u7XcJk`)
     *
     * As of room v4, the base64 hash was changed to use `-` and `_` for the 62nd and 63rd characters, instead of `+`
     * and `/` (Eg: `$Rqnc-F-dvnEYJTyHq_iKxU2bZ1CI92-kuZq3a5lr5Zg`)
     */
    class VMX_EXPORT EventId {
        public:
            InString fullId;

        public:
            EventId() = default;
            EventId(const InString &fullId);
            EventId(const QString &fullId);

            bool operator==(const EventId &other) const = default;
    };

    VMX_EXPORT uint qHash(const EventId &key, uint seed);
}

Q_DECLARE_METATYPE(Vmx::Id::EventId);

REFL_AUTO(
        type(Vmx::Id::EventId, Vmx::Serde::NewType()),
        field(fullId)
        )

#endif
