#ifndef VTX_VMX_ID_FILTERID_HPP
#define VTX_VMX_ID_FILTERID_HPP

#include "vmx/InString.hpp"
#include "vmx/Serde.hpp"
#include "vmx_export.h"
#include <QString>

namespace Vmx::Id {
    /**
     * @brief A filter identifier
     */
    class VMX_EXPORT FilterId {
        public:
            InString fullId;

        public:
            FilterId() = default;
            FilterId(const InString &fullId);
            FilterId(const QString &fullId);

            bool operator==(const FilterId &other) const = default;
    };

    VMX_EXPORT uint qHash(const FilterId &key, uint seed);
}

Q_DECLARE_METATYPE(Vmx::Id::FilterId);

REFL_AUTO(
        type(Vmx::Id::FilterId, Vmx::Serde::NewType()),
        field(fullId)
        )

#endif
