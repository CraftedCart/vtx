#ifndef VTX_VMX_ID_ROOMID_HPP
#define VTX_VMX_ID_ROOMID_HPP

#include "vmx/Serde.hpp"
#include "vmx/InString.hpp"
#include "vmx_export.h"
#include <QString>

namespace Vmx::Id {
    /**
     * @brief A full room id (Eg: `!xYvNcQPhnkrdUmYczI:matrix.org`)
     *
     * Not to be confused with a room alias (which looks like `#thisweekinmatrix:matrix.org`)
     */
    class VMX_EXPORT RoomId {
        public:
            InString fullId;

        public:
            RoomId() = default;
            RoomId(const InString &fullId);
            RoomId(const QString &fullId);

            bool operator==(const RoomId &other) const = default;
    };

    VMX_EXPORT uint qHash(const RoomId &key, uint seed);
}

Q_DECLARE_METATYPE(Vmx::Id::RoomId);

REFL_AUTO(
        type(Vmx::Id::RoomId, Vmx::Serde::NewType()),
        field(fullId)
        )

#endif
