#ifndef VTX_VMX_ID_USERID_HPP
#define VTX_VMX_ID_USERID_HPP

#include "vmx/Serde.hpp"
#include "vmx/InString.hpp"
#include "vmx_export.h"
#include <QString>

namespace Vmx::Id {
    /** @brief A full user id (Eg: `@alice:matrix.org`) */
    class VMX_EXPORT UserId {
        public:
            InString fullId;

        public:
            UserId() = default;
            UserId(const InString &fullId);
            UserId(const QString &fullId);

            bool operator==(const UserId &other) const = default;
    };

    VMX_EXPORT uint qHash(const UserId &key, uint seed);
}

Q_DECLARE_METATYPE(Vmx::Id::UserId);

REFL_AUTO(
        type(Vmx::Id::UserId, Vmx::Serde::NewType()),
        field(fullId)
        )

#endif
