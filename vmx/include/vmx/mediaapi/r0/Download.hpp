#ifndef VTX_VMX_MEDIAAPI_R0_DOWNLOAD_HPP
#define VTX_VMX_MEDIAAPI_R0_DOWNLOAD_HPP

#include "vmx/Metadata.hpp"
#include "vmx/refl.hpp"
#include "vmx/Vmx.hpp"
#include "vmx_export.h"
#include <QMetaType>
#include <QByteArray>
#include <QUrl>

namespace Vmx::MediaApi::R0::Download {
    struct VMX_EXPORT GetRequest {
        static constexpr Metadata METADATA = {
            Vmx::constexprStr("/_matrix/media/r0/download/$serverName/$mediaId"), // path
            QNetworkAccessManager::GetOperation, // method
            true, // requiresAuthentication
        };
        using Response = QByteArray;


        /** @brief The server name from the mxc:// URI (the authoritory component) */
        QString serverName;

        /** @brief The media ID from the mxc:// URI (the path component) */
        QString mediaId;

        /**
         * @brief Indicates to the server that it should not attempt to fetch the media if it is deemed remote.
         *
         * This is to prevent routing loops where the server contacts itself. Defaults to true if not provided.
         */
        std::optional<bool> allowRemote;
    };
}

REFL_AUTO(
        type(Vmx::MediaApi::R0::Download::GetRequest, Vmx::Serde::Serialize()),
        field(serverName, Vmx::Serde::Path("serverName")),
        field(mediaId, Vmx::Serde::Path("mediaId")),
        field(allowRemote, Vmx::Serde::Query("allow_remote"))
        )

Q_DECLARE_METATYPE(Vmx::MediaApi::R0::Download::GetRequest);

#endif
