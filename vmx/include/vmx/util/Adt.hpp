#ifndef VTX_VMX_UTIL_ADT_HPP
#define VTX_VMX_UTIL_ADT_HPP

#include "vmx_export.h"
#include <type_traits>
#include <utility>
#include <variant>

/**
 * @brief Helpers and terrifying template messes to work with algebraic data types (stuff in std::variant mostly)
 */
namespace Vmx::Util::Adt {
    /** @brief Takes a pile of callable types and re-exports all their `operator()` functions */
    template<typename... Ts>
    struct Overload : Ts... {
        /**
         * @brief Move callable objects into ourselves
         *
         * If we inherit from a callable Ts that have member vars, this would move stuff into the member vars for the Ts
         * we inherit from.
         */
        Overload(Ts&&... fns) : std::decay_t<Ts>(std::forward<Ts>(fns))... {}

        // Exposes operator() from all bases we inherit from
        using Ts::operator()...;
    };

    // Deduction guide, used for CTAD (Class template argument deduction)
    template<typename... Ts>
    Overload(Ts&&...) -> Overload<Ts...>;

    /**
     * @brief Picks an appropriate given overload to call for a std::variant
     *
     * Stores a variant. Invoking `operator()` with some callables on this will call std::visit on the variant and
     * pick an appropriate callable to call.
     */
    template<typename... VariantTs>
    class VariantMatcher {
        private:
            const std::variant<VariantTs...>& variant;

        public:
            VariantMatcher(const std::variant<VariantTs...>& variant) : variant(variant) {}

            template<typename... Ts>
            auto operator()(Ts&&... fns) {
                return std::visit(Overload(std::forward<Ts>(fns)...), variant);
            }
    };

    // Deduction guide, used for CTAD (Class template argument deduction)
    template<typename... Ts>
    VariantMatcher(const std::variant<Ts...>&) -> VariantMatcher<Ts...>;

    /**
     * @brief Match against variant and pick an appropriate function to call
     *
     * Calling this function with a variant will return a callable. Invoke this callable with a list of callable objects
     * (such as lambdas) and an appropriate callable from the list will be called.
     *
     * Example usage:
     * @code
     * std::variant<MatrixId, ThirdPartyId, Phone> info = MatrixId{"@alice:matrix.org"};
     *
     * Adt::matchVariant(info) (
     *     [](const MatrixId& id) {
     *         qInfo() << "It's a matrix id!" << id.id;
     *     },
     *     [](const ThirdPartyId& id) {
     *         qInfo() << "It's a 3pid!" << id.medium << id.address;
     *     },
     *     [](auto&&) {
     *         qInfo() << "Who knows what it is!";
     *     }
     * );
     * @endcode
     *
     * ...prints...
     *
     * @code
     * It's a matrix id! "@alice:matrix.org"
     * @endcode
     */
    template<typename... VariantTs>
    VariantMatcher<VariantTs...> matchVariant(const std::variant<VariantTs...>& variant) {
        return VariantMatcher(variant);
    }
}

#endif
