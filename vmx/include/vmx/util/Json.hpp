#ifndef VTX_VMX_UTIL_JSON_HPP
#define VTX_VMX_UTIL_JSON_HPP

#include "vmx/util/Result.hpp"
#include <QJsonObject>
#include <QHash>
#include <QString>
#include <variant>

#define VMX_JSON_GET_X_OR_BAIL(json, jsonFieldName, varName, isFunc, toFunc) \
    if ((json).contains(jsonFieldName)) { \
        if ((json)[jsonFieldName].isFunc()) { \
            varName = (json)[jsonFieldName].toFunc(); \
        } else { \
            return ::Vmx::Util::Err(::Vmx::Util::Json::DeserializeError(::Vmx::Util::Json::InvalidType{jsonFieldName})); \
        } \
    } else { \
        return ::Vmx::Util::Err(::Vmx::Util::Json::DeserializeError(::Vmx::Util::Json::MissingField{jsonFieldName})); \
    }

#define VMX_JSON_GET_X_OR_NULLOPT(json, jsonFieldName, varName, isFunc, toFunc) \
    if ((json).contains(jsonFieldName)) { \
        if ((json)[jsonFieldName].isFunc()) { \
            varName = (json)[jsonFieldName].toFunc(); \
        } else { \
            return ::Vmx::Util::Err(::Vmx::Util::Json::DeserializeError(::Vmx::Util::Json::InvalidType{jsonFieldName})); \
        } \
    } else { \
        varName = {}; \
    }

#define VMX_JSON_GET_STRING_OR_BAIL(json, jsonFieldName, varName) VMX_JSON_GET_X_OR_BAIL(json, jsonFieldName, varName, isString, toString)
#define VMX_JSON_GET_STRING_OR_NULLOPT(json, jsonFieldName, varName) VMX_JSON_GET_X_OR_NULLOPT(json, jsonFieldName, varName, isString, toString)

#define VMX_JSON_GET_OBJECT_OR_BAIL(json, jsonFieldName, varName) VMX_JSON_GET_X_OR_BAIL(json, jsonFieldName, varName, isObject, toObject)
#define VMX_JSON_GET_OBJECT_OR_NULLOPT(json, jsonFieldName, varName) VMX_JSON_GET_X_OR_NULLOPT(json, jsonFieldName, varName, isObject, toObject)

#define VMX_JSON_GET_ARRAY_OR_BAIL(json, jsonFieldName, varName) VMX_JSON_GET_X_OR_BAIL(json, jsonFieldName, varName, isArray, toArray)
#define VMX_JSON_GET_ARRAY_OR_NULLOPT(json, jsonFieldName, varName) VMX_JSON_GET_X_OR_NULLOPT(json, jsonFieldName, varName, isArray, toArray)

namespace Vmx::Util::Json {
    /** @brief Failed to deserialize due to invalid valid JSON */
    struct NotJson {};

    /** @brief Failed to deserialize due to a field of invalid type */
    struct InvalidType { QString field; };

    /** @brief Failed to deserialize due to missing a field */
    struct MissingField { QString field; };

    using DeserializeError = std::variant<NotJson, InvalidType, MissingField>;

    /**
     * @brief Fills `map` with all JSON objects in `obj`
     *
     * @return
     * - Ok() on success
     * - Err(InvalidType) if a value in a key-value pair is not a JSON object
     */
    Result<void, InvalidType> fillPropertyMap(const QJsonObject &obj, QHash<QString, QJsonObject> &map);
}

#endif
