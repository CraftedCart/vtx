#include "vmx/Client.hpp"
#include "vmx/clientapi/r0/sync/SyncEvents.hpp"
#include "vmx/event/RoomEvent.hpp"

using namespace Vmx::Util;

namespace Vmx {
    NetError::NetError(const QNetworkReply::NetworkError &error, const QString &errorString) :
        error(error),
        errorString(errorString) {}

    Client::Client(QObject *parent) : QObject(parent) {
        netManager->setTransferTimeout(60 * 1000); // 60 seconds
    }

    Client::~Client() {
        // Abort all active network requests
        qDebug() << "Aborting" << activeReplies.length() << "pending network requests";
        for (QNetworkReply *reply : activeReplies) {
            reply->abort();
        }
    }

    QtPromise::QPromise<Vmx::ClientApi::R0::Session::Login::PostResponse> Client::logInWithPassword(
            QString username,
            QString password,
            QUrl homeserverUrl,
            std::optional<QString> deviceId,
            std::optional<QString> initialDeviceDisplayName
            ) {
        using namespace Vmx::ClientApi;
        using namespace Vmx::ClientApi::R0::Session;

        Login::PostRequest req;
        req.user = Login::MatrixId{username};
        req.loginInfo = Login::Password{password};
        req.deviceId = deviceId;
        req.initialDeviceDisplayName = initialDeviceDisplayName;

        return request(req, homeserverUrl);
    }

    void Client::registerReply(QNetworkReply *reply) {
        activeReplies.append(reply);

        // When the reply is destroyed, remove it from the activeReplies list
        QObject::connect(reply, &QNetworkReply::destroyed, this, &Client::onReplyDestroyed);
    }

    void Client::onReplyDestroyed(QObject *reply) {
        bool removed = activeReplies.removeOne(static_cast<QNetworkReply*>(reply));
        Q_ASSERT(removed && "Network reply was destroyed but could not be removed from Client::activeReplies");
    }
}
