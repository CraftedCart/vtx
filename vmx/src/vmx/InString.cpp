#include "vmx/InString.hpp"
#include <QMutexLocker>
#include <QDebug>

using namespace Vmx::Util;

namespace Vmx {
    QMultiHash<uint, InString::RefCountInternString*> InString::stringTable;
    QMutex InString::stringTableMutex;

    // Empty QString, not a null QString
    const InString InString::EMPTY = InString(QString(""));

    InString::RefCountInternString* InString::getInternString(const QString &str) {
        QMutexLocker lock(&stringTableMutex);

        uint hash = qHash(str);
        QList<RefCountInternString*> matches = stringTable.values(hash);

        for (RefCountInternString *match : matches) {
            if (match->str == str) {
                return match;
            }
        }

        // No matches found - insert it
        RefCountInternString *newStr = new RefCountInternString{str, 0};
        stringTable.insert(hash, newStr);

        return newStr;
    }

    void InString::destroyInternString(InString::RefCountInternString *internStr) {
        uint hash = qHash(internStr->str);
        stringTable.remove(hash, internStr);
        delete internStr;
    }

    InString::InString() {
        this->internString = EMPTY.internString;
        this->internString->refCount.ref();
    }

    InString::InString(const InString &other) {
        this->internString = other.internString;
        this->internString->refCount.ref();
    }

    InString::InString(const QString &str) {
        this->internString = getInternString(str);
        this->internString->refCount.ref();
    }

    // InString::InString(const QLatin1String &str) {
        // InString(QString(str));
    // }

    // InString::InString(const char *str) {
        // InString(QString(str));
    // }

    InString::~InString() {
        QMutexLocker lock(&stringTableMutex);
        if (!this->internString->refCount.deref()) {
            destroyInternString(this->internString);
        }
    }

    bool InString::isEmpty() const {
        return internString == EMPTY.internString;
    }

    QString InString::toQString() const {
        return internString->str;
    }

    void InString::printStringTable() {
        QHash<uint, RefCountInternString*>::iterator iter;
        for (iter = stringTable.begin(); iter != stringTable.end(); ++iter) {
            qDebug() << iter.key() << iter.value()->str << "Refs:" << iter.value()->refCount;
        }
    }

    bool InString::operator==(const InString &other) const {
        return internString == other.internString;
    }

    bool InString::operator!=(const InString &other) const {
        return internString != other.internString;
    }

    InString& InString::operator=(const InString &other) {
        {
            QMutexLocker lock(&stringTableMutex);
            if (!this->internString->refCount.deref()) {
                destroyInternString(this->internString);
            }
        }

        this->internString = other.internString;
        this->internString->refCount.ref();

        return *this;
    }

    InString::operator QString() const {
        return internString->str;
    }

    uint qHash(const InString &key, uint seed) {
        return qHash(key.internString->str, seed);
    }
}

namespace Vmx::Serde {
    template<>
    Util::Result<InString, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isString()) {
            return Ok(InString(value.toString()));
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }

    template<>
    QJsonValue valueToJson(const InString &value) {
        return value.operator QString();
    }

    template<>
    QString valueToString(const InString &value) {
        return value;
    }
}
