#include "vmx/Membership.hpp"

namespace Vmx::Serde {
    template<>
    Util::Result<Membership, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (!value.isString()) {
            return Util::Err(DeserializeError(InvalidType()));
        }

        return valueFromString<Membership>(value.toString());
    }

    template<>
    Util::Result<Membership, DeserializeError> valueFromString(const QString &value) {
        if (value == QLatin1String("invite")) return Util::Ok(Membership::INVITE);
        if (value == QLatin1String("join")) return Util::Ok(Membership::JOIN);
        if (value == QLatin1String("knock")) return Util::Ok(Membership::KNOCK);
        if (value == QLatin1String("leave")) return Util::Ok(Membership::LEAVE);
        if (value == QLatin1String("ban")) return Util::Ok(Membership::BAN);

        // No matching string
        return Util::Err(DeserializeError(InvalidValue()));
    }

    template<>
    QString valueToString(const Membership &value) {
        switch (value) {
            case Membership::INVITE:
                return QLatin1String("invite");
            case Membership::JOIN:
                return QLatin1String("join");
            case Membership::KNOCK:
                return QLatin1String("knock");
            case Membership::LEAVE:
                return QLatin1String("leave");
            case Membership::BAN:
                return QLatin1String("ban");
        }
    }
}
