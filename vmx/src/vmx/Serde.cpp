#include "vmx/Serde.hpp"
#include "vmx/util/Json.hpp"

using namespace Vmx::Util;

namespace Vmx::Serde {
    template<>
    Util::Result<QJsonValue, DeserializeError> valueFromJson(const QJsonValue &value) {
        return Ok(value);
    }

    template<>
    Util::Result<QJsonObject, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isObject()) {
            return Ok(value.toObject());
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }

    template<>
    Util::Result<QJsonArray, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isArray()) {
            return Ok(value.toArray());
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }

    template<>
    Util::Result<QString, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isString()) {
            return Ok(value.toString());
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }

    template<>
    Util::Result<qint32, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isDouble()) {
            return Ok((qint32) value.toDouble());
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }

    template<>
    Util::Result<quint32, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isDouble()) {
            return Ok((quint32) value.toDouble());
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }

    template<>
    Util::Result<qint64, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isDouble()) {
            return Ok((qint64) value.toDouble());
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }

    template<>
    Util::Result<quint64, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isDouble()) {
            return Ok((quint64) value.toDouble());
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }

    template<>
    Util::Result<double, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isDouble()) {
            return Ok(value.toDouble());
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }

    template<>
    Util::Result<bool, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isBool()) {
            return Ok(value.toBool());
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }

    template<>
    Util::Result<QDateTime, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isDouble()) {
            return Ok(QDateTime::fromMSecsSinceEpoch(value.toDouble()));
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }

    template<>
    Util::Result<QUrl, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isString()) {
            return Ok(QUrl(value.toString()));
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }

    template<>
    QJsonValue valueToJson(const QJsonValue &value) {
        return value;
    }

    template<>
    QJsonValue valueToJson(const QJsonObject &value) {
        return value;
    }

    template<>
    QJsonValue valueToJson(const QJsonArray &value) {
        return value;
    }

    template<>
    QJsonValue valueToJson(const QString &value) {
        return value;
    }

    template<>
    QJsonValue valueToJson(const qint32 &value) {
        return value;
    }

    template<>
    QJsonValue valueToJson(const quint32 &value) {
        return (double) value;
    }

    template<>
    QJsonValue valueToJson(const qint64 &value) {
        return value;
    }

    template<>
    QJsonValue valueToJson(const quint64 &value) {
        return (double) value;
    }

    template<>
    QJsonValue valueToJson(const double &value) {
        return value;
    }

    template<>
    QJsonValue valueToJson(const bool &value) {
        return value;
    }

    template<>
    QJsonValue valueToJson(const QDateTime &value) {
        return value.toMSecsSinceEpoch();
    }

    template<>
    QJsonValue valueToJson(const QUrl &value) {
        return value.toString();
    }

    template<>
    QString valueToString(const QString &value) {
        return value;
    }

    template<>
    QString valueToString(const qint32 &value) {
        return QString::number(value);
    }

    template<>
    QString valueToString(const quint32 &value) {
        return QString::number(value);
    }

    template<>
    QString valueToString(const qint64 &value) {
        return QString::number(value);
    }

    template<>
    QString valueToString(const quint64 &value) {
        return QString::number(value);
    }

    template<>
    QString valueToString(const double &value) {
        return QString::number(value);
    }

    template<>
    QString valueToString(const bool &value) {
        return value ? QLatin1String("true") : QLatin1String("false");
    }

    template<>
    QString valueToString(const QDateTime &value) {
        return QString::number(value.toMSecsSinceEpoch());
    }

    template<>
    QString valueToString(const QUrl &value) {
        return value.toString();
    }

    namespace Internal {
        void mergeJsonObjects(QJsonObject &dest, const QJsonValue &src) {
            Q_ASSERT(src.isObject());
            QJsonObject jsonObj = src.toObject();

            for (auto iter = jsonObj.begin(), end = jsonObj.end(); iter != end; ++iter) {
                dest.insert(iter.key(), iter.value());
            }
        }
    }
}
