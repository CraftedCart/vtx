#include "vmx/clientapi/ApiError.hpp"
#include <QHash>

using namespace Vmx::Util;

namespace Vmx::ClientApi {
    static const QHash<QString, MxApiError> STR_TO_MX_ERROR_CODE = {
        {QLatin1String("M_FORBIDDEN"), MxApiError::M_FORBIDDEN},
        {QLatin1String("M_UNKNOWN_TOKEN"), MxApiError::M_UNKNOWN_TOKEN},
        {QLatin1String("M_MISSING_TOKEN"), MxApiError::M_MISSING_TOKEN},
        {QLatin1String("M_BAD_JSON"), MxApiError::M_BAD_JSON},
        {QLatin1String("M_NOT_JSON"), MxApiError::M_NOT_JSON},
        {QLatin1String("M_NOT_FOUND"), MxApiError::M_NOT_FOUND},
        {QLatin1String("M_LIMIT_EXCEEDED"), MxApiError::M_LIMIT_EXCEEDED},
        {QLatin1String("M_UNKNOWN"), MxApiError::M_UNKNOWN},
        {QLatin1String("M_UNRECOGNIZED"), MxApiError::M_UNRECOGNIZED},
        {QLatin1String("M_UNAUTHORIZED"), MxApiError::M_UNAUTHORIZED},
        {QLatin1String("M_USER_DEACTIVATED"), MxApiError::M_USER_DEACTIVATED},
        {QLatin1String("M_USER_IN_USE"), MxApiError::M_USER_IN_USE},
        {QLatin1String("M_INVALID_USERNAME"), MxApiError::M_INVALID_USERNAME},
        {QLatin1String("M_ROOM_IN_USE"), MxApiError::M_ROOM_IN_USE},
        {QLatin1String("M_INVALID_ROOM_STATE"), MxApiError::M_INVALID_ROOM_STATE},
        {QLatin1String("M_THREEPID_IN_USE"), MxApiError::M_THREEPID_IN_USE},
        {QLatin1String("M_THREEPID_NOT_FOUND"), MxApiError::M_THREEPID_NOT_FOUND},
        {QLatin1String("M_THREEPID_AUTH_FAILED"), MxApiError::M_THREEPID_AUTH_FAILED},
        {QLatin1String("M_THREEPID_DENIED"), MxApiError::M_THREEPID_DENIED},
        {QLatin1String("M_SERVER_NOT_TRUSTED"), MxApiError::M_SERVER_NOT_TRUSTED},
        {QLatin1String("M_UNSUPPORTED_ROOM_VERSION"), MxApiError::M_UNSUPPORTED_ROOM_VERSION},
        {QLatin1String("M_INCOMPATIBLE_ROOM_VERSION"), MxApiError::M_INCOMPATIBLE_ROOM_VERSION},
        {QLatin1String("M_BAD_STATE"), MxApiError::M_BAD_STATE},
        {QLatin1String("M_GUEST_ACCESS_FORBIDDEN"), MxApiError::M_GUEST_ACCESS_FORBIDDEN},
        {QLatin1String("M_CAPTCHA_NEEDED"), MxApiError::M_CAPTCHA_NEEDED},
        {QLatin1String("M_CAPTCHA_INVALID"), MxApiError::M_CAPTCHA_INVALID},
        {QLatin1String("M_MISSING_PARAM"), MxApiError::M_MISSING_PARAM},
        {QLatin1String("M_INVALID_PARAM"), MxApiError::M_INVALID_PARAM},
        {QLatin1String("M_TOO_LARGE"), MxApiError::M_TOO_LARGE},
        {QLatin1String("M_EXCLUSIVE"), MxApiError::M_EXCLUSIVE},
        {QLatin1String("M_RESOURCE_LIMIT_EXCEEDED"), MxApiError::M_RESOURCE_LIMIT_EXCEEDED},
        {QLatin1String("M_CANNOT_LEAVE_SERVER_NOTICE_ROOM"), MxApiError::M_CANNOT_LEAVE_SERVER_NOTICE_ROOM},
    };

    ApiErrorCode stringToErrorCode(const QString& str) {
        if (STR_TO_MX_ERROR_CODE.contains(str)) {
            return STR_TO_MX_ERROR_CODE.value(str);
        } else {
            return OtherApiError{str};
        }
    }

    QString errorCodeToString(MxApiError err) {
        return STR_TO_MX_ERROR_CODE.key(err);
    }
}

namespace Vmx::Serde {
    template<>
    Result<Vmx::ClientApi::ApiErrorCode, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isString()) {
            return Ok(ClientApi::stringToErrorCode(value.toString()));
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }
}
