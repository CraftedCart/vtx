#include "vmx/clientapi/r0/filter/EventFormat.hpp"
#include <QHash> // for qHash functions

using namespace Vmx::Util;
using namespace Vmx::ClientApi::R0::Filter;

namespace Vmx::ClientApi::R0::Filter {
    uint qHash(EventFormat fmt, uint seed) {
        return ::qHash((quint8) fmt, seed);
    }
}

namespace Vmx::Serde {
    template<>
    Result<EventFormat, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isString()) {
            QString str = value.toString();
            if (str == QLatin1String("client")) return Ok(EventFormat::CLIENT);
            else if (str == QLatin1String("federation")) return Ok(EventFormat::FEDERATION);
            else return Err(DeserializeError(InvalidValue()));
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }

    template<>
    QJsonValue valueToJson(const ClientApi::R0::Filter::EventFormat &value) {
        return valueToString(value);
    }

    template<>
    QString valueToString(const EventFormat &value) {
        switch (value) {
            case EventFormat::CLIENT:
                return QLatin1String("client");
            case EventFormat::FEDERATION:
                return QLatin1String("federation");
        }
    }
}
