#include "vmx/clientapi/r0/filter/Filter.hpp"
#include "vmx/util/Adt.hpp"
#include <QJsonDocument>

using namespace Vmx::ClientApi::R0::Filter;
using namespace Vmx::Util;

namespace Vmx::Serde {
    template<>
    QString valueToString(const ClientApi::R0::Filter::Filter &value) {
        return Adt::matchVariant(value) (
            [](const FilterDef &def) {
                return QString(QJsonDocument(Serde::serialize(def)).toJson(QJsonDocument::Compact));
            },
            [](const Id::FilterId &id) {
                return id.fullId.operator QString();
            }
        );
    }
}

