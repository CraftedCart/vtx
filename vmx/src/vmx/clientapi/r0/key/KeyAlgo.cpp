#include "vmx/clientapi/r0/key/KeyAlgo.hpp"
#include <QHash> // for qHash functions

using namespace Vmx::Util;
using namespace Vmx::ClientApi::R0::Key;

namespace Vmx::ClientApi::R0::Key {
    uint qHash(KeyAlgo key, uint seed) {
        return ::qHash((quint8) key, seed);
    }
}

namespace Vmx::Serde {
    template<>
    Result<KeyAlgo, DeserializeError> valueFromJson(const QJsonValue &value) {
        if (value.isString()) {
            QString str = value.toString();
            if (str == QLatin1String("ed25519")) return Ok(KeyAlgo::ED25519);
            else if (str == QLatin1String("curve25519")) return Ok(KeyAlgo::CURVE25519);
            else if (str == QLatin1String("signed_curve25519")) return Ok(KeyAlgo::SIGNED_CURVE25519);
            else return Err(DeserializeError(InvalidValue()));
        } else {
            return Err(DeserializeError(InvalidType()));
        }
    }
}
