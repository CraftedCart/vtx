#include "vmx/clientapi/r0/message/GetMessageEvents.hpp"

using namespace Vmx::ClientApi::R0::Message::GetMessageEvents;

namespace Vmx::Serde {
    template<>
    QString valueToString(const Direction &value) {
        switch (value) {
            case Direction::BACKWARDS:
                return QLatin1String("b");
            case Direction::FORWARDS:
                return QLatin1String("f");
        }
    }
}
