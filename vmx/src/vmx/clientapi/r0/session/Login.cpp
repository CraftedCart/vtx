#include "vmx/clientapi/r0/session/Login.hpp"

namespace Vmx::ClientApi::R0::Session::Login {
    Vmx::Session PostResponse::makeSession() const {
        Vmx::Session session;
        session.accessToken = accessToken;

        if (wellKnown) {
            session.homeserverUrl = wellKnown->homeserver.baseUrl;
        }

        return session;
    }
}
