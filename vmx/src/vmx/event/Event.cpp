#include "vmx/event/Event.hpp"

namespace Vmx::Event {
    Event::Event(const QString &type, const QJsonObject &content, const QJsonObject &json) :
        type(type),
        content(content),
        json(json) {}
}
