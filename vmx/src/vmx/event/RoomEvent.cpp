#include "vmx/event/RoomEvent.hpp"

namespace Vmx::Event {
    RoomEvent::RoomEvent(
            const QString &type,
            const QJsonObject &content,
            const QJsonObject &json,
            const Id::UserId &sender,
            const Id::EventId &eventId,
            const QDateTime &originServerTs,
            std::optional<UnsignedData> &&unsignedData,
            const QString &roomId
            ) :
        SyncRoomEvent(
                type,
                content,
                json,
                sender,
                eventId,
                originServerTs,
                std::move(unsignedData)
                ),
        roomId(roomId) {}
}
