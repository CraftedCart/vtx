#include "vmx/event/SentEvent.hpp"

namespace Vmx::Event {
    SentEvent::SentEvent(
            const QString &type,
            const QJsonObject &content,
            const QJsonObject &json,
            const Id::UserId &sender
            ) :
        Event(type, content, json),
        sender(sender) {}
}
