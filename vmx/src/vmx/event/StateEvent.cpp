#include "vmx/event/StateEvent.hpp"
#include "vmx/event/RoomEvent.hpp"

namespace Vmx::Event {
    StateEvent::StateEvent(
            const QString &type,
            const QJsonObject &content,
            const QJsonObject &json,
            const Id::UserId &sender,
            const Id::EventId &eventId,
            const QDateTime &originServerTs,
            std::optional<UnsignedData> &&unsignedData,
            const QString &stateKey,
            const std::optional<QJsonObject> &&prevContent,
            const QString &roomId
            ) :
        SyncStateEvent(
                type,
                content,
                json,
                sender,
                eventId,
                originServerTs,
                std::move(unsignedData),
                stateKey,
                std::move(prevContent)
                ),
        roomId(roomId) {}
}
