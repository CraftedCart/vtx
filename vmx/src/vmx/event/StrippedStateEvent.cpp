#include "vmx/event/StrippedStateEvent.hpp"

namespace Vmx::Event {
    StrippedStateEvent::StrippedStateEvent(
            const QString &type,
            const QJsonObject &content,
            const QJsonObject &json,
            const Id::UserId &sender,
            const QString &stateKey
            ) :
        SentEvent(type, content, json, sender),
        stateKey(stateKey) {}
}
