#include "vmx/event/SyncRoomEvent.hpp"

namespace Vmx::Event {
    SyncRoomEvent::SyncRoomEvent(
            const QString &type,
            const QJsonObject &content,
            const QJsonObject &json,
            const Id::UserId &sender,
            const Id::EventId &eventId,
            const QDateTime &originServerTs,
            std::optional<UnsignedData> &unsignedData
            ) :
        SentEvent(type, content, json, sender),
        eventId(eventId),
        originServerTs(originServerTs),
        unsignedData(unsignedData ? std::optional(UnsignedData(*unsignedData)) : std::nullopt) {}

    SyncRoomEvent::SyncRoomEvent(
            const QString &type,
            const QJsonObject &content,
            const QJsonObject &json,
            const Id::UserId &sender,
            const Id::EventId &eventId,
            const QDateTime &originServerTs,
            std::optional<UnsignedData> &&unsignedData
            ) :
        SentEvent(type, content, json, sender),
        eventId(eventId),
        originServerTs(originServerTs),
        unsignedData(std::move(unsignedData)) {}
}
