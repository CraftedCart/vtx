#include "vmx/event/SyncStateEvent.hpp"

namespace Vmx::Event {
    SyncStateEvent::SyncStateEvent(
            const QString &type,
            const QJsonObject &content,
            const QJsonObject &json,
            const Id::UserId &sender,
            const Id::EventId &eventId,
            const QDateTime &originServerTs,
            std::optional<UnsignedData> &unsignedData,
            const QString &stateKey,
            const std::optional<QJsonObject> &prevContent
            ) :
        SyncRoomEvent(
                type,
                content,
                json,
                sender,
                eventId,
                originServerTs,
                unsignedData
                ),
        stateKey(stateKey),
        prevContent(prevContent) {}

    SyncStateEvent::SyncStateEvent(
            const QString &type,
            const QJsonObject &content,
            const QJsonObject &json,
            const Id::UserId &sender,
            const Id::EventId &eventId,
            const QDateTime &originServerTs,
            std::optional<UnsignedData> &&unsignedData,
            const QString &stateKey,
            const std::optional<QJsonObject> &&prevContent
            ) :
        SyncRoomEvent(
                type,
                content,
                json,
                sender,
                eventId,
                originServerTs,
                std::move(unsignedData)
                ),
        stateKey(stateKey),
        prevContent(std::move(prevContent)) {}
}
