#include "vmx/event/UnsignedData.hpp"
#include "vmx/event/RoomEvent.hpp"

namespace Vmx::Event {
    UnsignedData::UnsignedData(const UnsignedData &other) :
        age(other.age),
        redactedBecause(other.redactedBecause ? std::optional(std::make_unique<RoomEvent>(**other.redactedBecause)) : std::nullopt),
        transactionId(other.transactionId),
        json(other.json) {}

    // Empty dtor needed to make std::unique_ptr happy
    UnsignedData::~UnsignedData() {}

    UnsignedData& UnsignedData::operator=(const UnsignedData &other) {
        age = other.age;
        redactedBecause = other.redactedBecause ? std::optional(std::make_unique<RoomEvent>(**other.redactedBecause)) : std::nullopt;
        transactionId = other.transactionId;
        json = other.json;

        return *this;
    }
}
