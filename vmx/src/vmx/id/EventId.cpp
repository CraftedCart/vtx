#include "vmx/id/EventId.hpp"

namespace Vmx::Id {
    EventId::EventId(const InString &fullId) : fullId(fullId) {}
    EventId::EventId(const QString &fullId) : fullId(fullId) {}

    uint qHash(const EventId &key, uint seed) {
        return qHash(key.fullId, seed);
    }
}
