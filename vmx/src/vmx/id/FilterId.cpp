#include "vmx/id/FilterId.hpp"

namespace Vmx::Id {
    FilterId::FilterId(const InString &fullId) : fullId(fullId) {}
    FilterId::FilterId(const QString &fullId) : fullId(fullId) {}

    uint qHash(const FilterId &key, uint seed) {
        return qHash(key.fullId, seed);
    }
}
