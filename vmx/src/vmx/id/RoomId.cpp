#include "vmx/id/RoomId.hpp"

namespace Vmx::Id {
    RoomId::RoomId(const InString &fullId) : fullId(fullId) {}
    RoomId::RoomId(const QString &fullId) : fullId(fullId) {}

    uint qHash(const RoomId &key, uint seed) {
        return qHash(key.fullId, seed);
    }
}
