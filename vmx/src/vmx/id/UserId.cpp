#include "vmx/id/UserId.hpp"

namespace Vmx::Id {
    UserId::UserId(const InString& fullId) : fullId(fullId) {}
    UserId::UserId(const QString& fullId) : fullId(fullId) {}

    uint qHash(const UserId &key, uint seed) {
        return qHash(key.fullId, seed);
    }
}
