#include "vmx/util/Json.hpp"
#include <QDebug>

namespace Vmx::Util::Json {
    Result<void, InvalidType> fillPropertyMap(const QJsonObject &obj, QHash<QString, QJsonObject> &map) {
        for (auto iter = obj.begin(), end = obj.end(); iter != end; ++iter) {
            const QJsonValue& value = iter.value();
            if (value.isObject()) {
                map[iter.key()] = value.toObject();
            } else {
                return Err(InvalidType());
            }
        }

        return Ok();
    }
}
