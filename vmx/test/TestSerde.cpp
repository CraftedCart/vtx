#include "vmx/Serde.hpp"
#include <QtTest/QtTest>

using namespace Vmx;
using namespace Vmx::Util;

#define COUNT_OF(x) ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))

struct StringNewType {
    QString inner;

    StringNewType() {}
    StringNewType(const QString &inner) : inner(inner) {}

    bool operator==(const StringNewType &other) const = default;
};

REFL_AUTO(
        type(StringNewType, Vmx::Serde::NewType()),
        field(inner)
        )

struct SimpleStruct {
    QString str;
    quint32 num;
    std::optional<QDateTime> dateTime;
    std::optional<bool> isThing;

    bool operator==(const SimpleStruct &other) const = default;
};

REFL_AUTO(
        type(SimpleStruct, Vmx::Serde::Deserialize(), Vmx::Serde::Serialize()),
        field(str, Vmx::Serde::Field("str")),
        field(num, Vmx::Serde::Field("num")),
        field(dateTime, Vmx::Serde::Field("date_time")),
        field(isThing, Vmx::Serde::Field("is_thing"))
        )

struct ComplexStruct {
    StringNewType name;
    SimpleStruct simple;
    std::optional<QVector<StringNewType>> people;

    bool operator==(const ComplexStruct &other) const = default;
};

REFL_AUTO(
        type(ComplexStruct, Vmx::Serde::Deserialize(), Vmx::Serde::Serialize()),
        field(name, Vmx::Serde::Field("name")),
        field(simple, Vmx::Serde::Field("simple")),
        field(people, Vmx::Serde::Field("people"))
        )

struct FlattenStruct {
    QString name;
    SimpleStruct simple;

    bool operator==(const FlattenStruct &other) const = default;
};

REFL_AUTO(
        type(FlattenStruct, Vmx::Serde::Serialize()),
        field(name, Vmx::Serde::Field("name")),
        field(simple, Vmx::Serde::Field(), Vmx::Serde::Flatten())
        )

class TestSerde : public QObject {
    Q_OBJECT

    private:
        QJsonObject makeJsonObject(
                const QString &key1 = "boop",
                const QJsonValue &value1 = "floop",
                const QString &key2 = "shoop",
                const QJsonValue &value2 = 16.8
                ) {
            QJsonObject obj;
            obj[key1] = value1;
            obj[key2] = value2;
            return obj;
        }

        QJsonArray makeJsonArray(
                const QJsonValue &value1 = "floop",
                const QJsonValue &value2 = 16.8
                ) {
            QJsonArray arr;
            arr.append(value1);
            arr.append(value2);
            return arr;
        }

    private slots:
        void initTestCase() {}

        // Value from JSON {{{

        void jsonValueFromJson() {
            static const QJsonValue VALUES[] = {
                QString("Hey, you reading this right now! You're cute~ ^.^"),
                42.1337,
                makeJsonObject(),
                makeJsonArray(),
                QJsonValue(true),
                QJsonValue(),
            };

            for (uint i = 0; i < COUNT_OF(VALUES); i++) {
                Result<QJsonValue, Serde::DeserializeError> res = Serde::valueFromJson<QJsonValue>(VALUES[i]);
                QVERIFY(res.isOk());
                QCOMPARE(res.unwrap(), VALUES[i]);
            }
        }

        void jsonObjectFromJson() {
            QJsonValue jsonObject = makeJsonObject();

            QJsonValue jsonStr = QString("Hey, you reading this right now! You're cute~ ^.^");
            QJsonValue jsonDouble = 42.1337;
            QJsonValue jsonArray = makeJsonArray();
            QJsonValue jsonNull = QJsonValue();

            Result<QJsonObject, Serde::DeserializeError> res = Serde::valueFromJson<QJsonObject>(jsonObject);
            QVERIFY(res.isOk());
            QCOMPARE(res.unwrap(), jsonObject);

            QVERIFY(Serde::valueFromJson<QJsonObject>(jsonStr).isErr());
            QVERIFY(Serde::valueFromJson<QJsonObject>(jsonDouble).isErr());
            QVERIFY(Serde::valueFromJson<QJsonObject>(jsonArray).isErr());
            QVERIFY(Serde::valueFromJson<QJsonObject>(jsonNull).isErr());
        }

        void jsonArrayFromJson() {
            QJsonValue jsonArray = makeJsonArray();

            QJsonValue jsonStr = QString("Hey, you reading this right now! You're cute~ ^.^");
            QJsonValue jsonDouble = 42.1337;
            QJsonValue jsonObject = makeJsonObject();
            QJsonValue jsonNull = QJsonValue();

            Result<QJsonArray, Serde::DeserializeError> res = Serde::valueFromJson<QJsonArray>(jsonArray);
            QVERIFY(res.isOk());
            QCOMPARE(res.unwrap(), jsonArray);

            QVERIFY(Serde::valueFromJson<QJsonArray>(jsonStr).isErr());
            QVERIFY(Serde::valueFromJson<QJsonArray>(jsonDouble).isErr());
            QVERIFY(Serde::valueFromJson<QJsonArray>(jsonObject).isErr());
            QVERIFY(Serde::valueFromJson<QJsonArray>(jsonNull).isErr());
        }

        void qStringFromJson() {
            QJsonValue jsonStr = QString("Hey, you reading this right now! You're cute~ ^.^");

            QJsonValue jsonDouble = 42.1337;
            QJsonValue jsonObject = makeJsonObject();
            QJsonValue jsonArray = makeJsonArray();
            QJsonValue jsonNull = QJsonValue();

            Result<QString, Serde::DeserializeError> res = Serde::valueFromJson<QString>(jsonStr);
            QVERIFY(res.isOk());
            QCOMPARE(res.unwrap(), jsonStr);

            QVERIFY(Serde::valueFromJson<QString>(jsonDouble).isErr());
            QVERIFY(Serde::valueFromJson<QString>(jsonObject).isErr());
            QVERIFY(Serde::valueFromJson<QString>(jsonArray).isErr());
            QVERIFY(Serde::valueFromJson<QString>(jsonNull).isErr());
        }

        void doubleFromJson() {
            QJsonValue jsonDouble = 42.1337;

            QJsonValue jsonStr = QString("Hey, you reading this right now! You're cute~ ^.^");
            QJsonValue jsonObject = makeJsonObject();
            QJsonValue jsonArray = makeJsonArray();
            QJsonValue jsonNull = QJsonValue();

            Result<double, Serde::DeserializeError> res = Serde::valueFromJson<double>(jsonDouble);
            QVERIFY(res.isOk());
            QCOMPARE(res.unwrap(), jsonDouble);

            QVERIFY(Serde::valueFromJson<double>(jsonStr).isErr());
            QVERIFY(Serde::valueFromJson<double>(jsonObject).isErr());
            QVERIFY(Serde::valueFromJson<double>(jsonArray).isErr());
            QVERIFY(Serde::valueFromJson<double>(jsonNull).isErr());
        }

        void dateTimeFromJson() {
            QJsonValue jsonDouble = 42.0;
            QJsonValue jsonDoubleNow = QDateTime::currentMSecsSinceEpoch();;

            QJsonValue jsonStr = QString("Hey, you reading this right now! You're cute~ ^.^");
            QJsonValue jsonObject = makeJsonObject();
            QJsonValue jsonArray = makeJsonArray();
            QJsonValue jsonNull = QJsonValue();

            Result<QDateTime, Serde::DeserializeError> res = Serde::valueFromJson<QDateTime>(jsonDouble);
            QVERIFY(res.isOk());
            QCOMPARE(res.unwrap(), QDateTime::fromMSecsSinceEpoch(jsonDouble.toDouble()));

            Result<QDateTime, Serde::DeserializeError> resNow = Serde::valueFromJson<QDateTime>(jsonDoubleNow);
            QVERIFY(resNow.isOk());
            QCOMPARE(resNow.unwrap(), QDateTime::fromMSecsSinceEpoch(jsonDoubleNow.toDouble()));

            QVERIFY(Serde::valueFromJson<double>(jsonStr).isErr());
            QVERIFY(Serde::valueFromJson<double>(jsonObject).isErr());
            QVERIFY(Serde::valueFromJson<double>(jsonArray).isErr());
            QVERIFY(Serde::valueFromJson<double>(jsonNull).isErr());
        }

        void newTypeFromJson() {
            QJsonValue json = "nyaaa~";

            Result<StringNewType, Serde::DeserializeError> res = Serde::valueFromJson<StringNewType>(json);
            QVERIFY(res.isOk());
            QCOMPARE(res.unwrap(), StringNewType("nyaaa~"));
        }

        void validStructFromJson() {
            QJsonObject root;
            root["name"] = "Hello";

            QJsonObject simple;
            simple["str"] = "nya~";
            simple["num"] = 16.4;
            simple["date_time"] = 10000;
            // missing simple["is_thing"]

            root["simple"] = simple;

            QJsonArray people = {
                "Alice",
                "Bob",
                "Matti",
                "James",
            };
            root["people"] = people;

            Result<ComplexStruct, Serde::DeserializeError> res = Serde::valueFromJson<ComplexStruct>(root);
            QVERIFY(res.isOk());

            ComplexStruct expected = {
                StringNewType("Hello"),
                {
                    "nya~",
                    16,
                    QDateTime::fromMSecsSinceEpoch(10000),
                    std::nullopt,
                },
                QVector<StringNewType>{
                    StringNewType("Alice"),
                    StringNewType("Bob"),
                    StringNewType("Matti"),
                    StringNewType("James"),
                },
            };

            QCOMPARE(res.unwrap(), expected);
        }

        void structWithMissingFieldsFromJson() {
            QJsonObject root;
            root["name"] = "Hello";

            QJsonObject simple;
            simple["num"] = 16.4;
            simple["date_time"] = 10000;
            // missing simple["is_thing"]

            root["simple"] = simple;

            Result<ComplexStruct, Serde::DeserializeError> res = Serde::valueFromJson<ComplexStruct>(root);
            QVERIFY(res.isErr());
            QVERIFY(std::holds_alternative<Serde::MissingField>(res.unwrapErr()));
        }

        // }}}

        // Value to JSON {{{

        void jsonValueToJson() {
            static const QJsonValue VALUES[] = {
                QString("Hey, you reading this right now! You're cute~ ^.^"),
                42.1337,
                makeJsonObject(),
                makeJsonArray(),
                QJsonValue(true),
                QJsonValue(),
            };

            for (uint i = 0; i < COUNT_OF(VALUES); i++) {
                QJsonValue res = Serde::valueToJson<QJsonValue>(VALUES[i]);
                QCOMPARE(res, VALUES[i]);
            }
        }

        void qStringToJson() {
            QString value = "*headpats*";

            QJsonValue res = Serde::valueToJson(value);
            QVERIFY(res.isString());
            QCOMPARE(res.toString(), value);
        }

        void doubleToJson() {
            double value = 12121212.1212121;

            QJsonValue res = Serde::valueToJson(value);
            QVERIFY(res.isDouble());
            QCOMPARE(res.toDouble(), value);
        }

        void qDateTimeToJson() {
            QDateTime value = QDateTime::currentDateTimeUtc();

            QJsonValue res = Serde::valueToJson(value);
            QVERIFY(res.isDouble());
            QCOMPARE(res.toDouble(), value.toMSecsSinceEpoch());
        }

        void newTypeToJson() {
            StringNewType value = StringNewType("*boops you uwu*");

            QJsonValue res = Serde::valueToJson(value);
            QVERIFY(res.isString());
            QCOMPARE(res.toString(), value.inner);
        }

        void structSerialize() {
            ComplexStruct complex = {
                StringNewType("Hello"),
                {
                    "nya~",
                    16,
                    QDateTime::fromMSecsSinceEpoch(10000),
                    std::nullopt,
                },
                QVector<StringNewType>{
                    StringNewType("Alice"),
                    StringNewType("Bob"),
                    StringNewType("Matti"),
                    StringNewType("James"),
                },
            };

            QJsonObject res = Serde::serialize(complex);

            QJsonObject expected;
            expected["name"] = "Hello";

            QJsonObject simple;
            simple["str"] = "nya~";
            simple["num"] = 16;
            simple["date_time"] = 10000;
            // missing simple["is_thing"]

            expected["simple"] = simple;

            QJsonArray people = {
                "Alice",
                "Bob",
                "Matti",
                "James",
            };
            expected["people"] = people;

            QCOMPARE(res, expected);
        }

        void structFlattenSerialize() {
            FlattenStruct flatten = {
                "Twily",
                {
                    "nya~",
                    16,
                    QDateTime::fromMSecsSinceEpoch(10000),
                    true,
                },
            };

            QJsonObject res = Serde::serialize(flatten);

            QJsonObject expected;
            expected["name"] = "Twily";

            expected["str"] = "nya~";
            expected["num"] = 16;
            expected["date_time"] = 10000;
            expected["is_thing"] = true;

            QCOMPARE(res, expected);
        }

        // }}}

        void cleanupTestCase() {}
};

QTEST_APPLESS_MAIN(TestSerde)
#include "TestSerde.moc"
