#include "vmx/clientapi/r0/session/Login.hpp"
#include <QtTest/QtTest>

using namespace Vmx;
using namespace Vmx::Util;
using namespace Vmx::ClientApi::R0::Session;

class TestLogin : public QObject {
    Q_OBJECT

    private slots:
        void initTestCase() {}

        void serializeRequest() {
            Login::PostRequest req;
            req.user = Login::MatrixId{"azurediamond"};
            req.loginInfo = Login::Password{"hunter2"};
            req.deviceId = {};
            req.initialDeviceDisplayName = "Not IRC";

            QJsonObject json = Serde::serialize(req);

            QJsonObject expected = {
                {"type", "m.login.password"},
                {"identifier", QJsonObject{
                    {"type", "m.id.user"},
                    {"user", "azurediamond"},
                }},
                {"password", "hunter2"},
                {"initial_device_display_name", "Not IRC"},
            };

            QCOMPARE(json, expected);
        }

        void deserializeResponse() {
            QJsonObject json = {
                {"user_id", "@cheeky_monkey:matrix.org"},
                {"access_token", "abc123"},
                {"device_id", "GHTYAJCE"},
                {"well_known", QJsonObject{
                    {"m.homeserver", QJsonObject{
                        {"base_url", "https://example.org"},
                    }},
                    {"m.identity_server", QJsonObject{
                        {"base_url", "https://id.example.org"},
                    }},
                }},
            };

            Result<Login::PostResponse, Serde::DeserializeError> res = Serde::deserialize<Login::PostResponse>(json);
            QVERIFY(res.isOk());

            Login::PostResponse expected = {
                .userId = Id::UserId("@cheeky_monkey:matrix.org"),
                .accessToken = "abc123",
                .homeserver = {},
                .deviceId = "GHTYAJCE",
                .wellKnown = Login::DiscoveryInfo{
                    .homeserver = Login::HomeserverInfo{"https://example.org"},
                    .identityServer = Login::IdentityServerInfo{"https://id.example.org"},
                    .additionalProperties = {
                        {"m.homeserver", QJsonObject{
                            {"base_url", "https://example.org"},
                        }},
                        {"m.identity_server", QJsonObject{
                            {"base_url", "https://id.example.org"},
                        }},
                    },
                },
            };

            QCOMPARE(res.unwrap(), expected);
        }

        void cleanupTestCase() {}
};

QTEST_APPLESS_MAIN(TestLogin)
#include "TestLogin.moc"
