#ifndef VTX_VTX_COMPILERSUPPORT_HPP
#define VTX_VTX_COMPILERSUPPORT_HPP

#if defined(__unix__) || (defined(__APPLE__) && defined(__MACH__))
    #define VTX_IS_POSIX
#endif

#if defined(__GLIBC__) && !defined(__UCLIBC__) && !defined(__MUSL__)
    #define VTX_USING_GLIBC
#endif

#if defined(_MSC_VER)
    #define VTX_DEBUG_BREAK() __debugbreak()
#elif defined(__i386__) || defined(__x86_64__)
    #define VTX_DEBUG_BREAK() asm volatile("int $0x03")
#elif defined(VTX_IS_POSIX)
    #include <signal.h>
    #define VTX_DEBUG_BREAK() raise(SIGTRAP)
#else
    #define VTX_DEBUG_BREAK()
#endif

#endif
