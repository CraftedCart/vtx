#ifndef VTX_VTX_ITER_HPP
#define VTX_VTX_ITER_HPP

#include "vtx_export.h"
#include <iterator>

namespace Vtx::Iter {
    template<typename T>
    struct ReverseIterWrapper {
        T &iterable;
    };

    template<typename T>
    VTX_EXPORT inline auto begin(ReverseIterWrapper<T> w) { return std::rbegin(w.iterable); }

    template<typename T>
    VTX_EXPORT inline auto end(ReverseIterWrapper<T> w) { return std::rend(w.iterable); }

    /** @brief Makes an iterator that iterates in reverse order */
    template<typename T>
    VTX_EXPORT inline ReverseIterWrapper<T> reverse(T &iterable) { return { iterable }; }

    template<typename T>
    struct SkipIterWrapper {
        T &iterable;
        std::size_t toSkip;
    };

    template<typename T>
    VTX_EXPORT inline auto begin(SkipIterWrapper<T> w) { return std::next(std::begin(w.iterable), w.toSkip); }

    template<typename T>
    VTX_EXPORT inline auto end(SkipIterWrapper<T> w) { return std::end(w.iterable); }

    /** @brief Makes an iterator that skips `toSkip` items from the beginning */
    template<typename T>
    VTX_EXPORT inline SkipIterWrapper<T> skip(T &iterable, std::size_t toSkip) { return { iterable, toSkip }; }

    template<typename T>
    struct TakeIterWrapper {
        T &iterable;
        std::size_t toTake;
    };

    template<typename T>
    VTX_EXPORT inline auto begin(TakeIterWrapper<T> w) { return std::begin(w.iterable); }

    template<typename T>
    VTX_EXPORT inline auto end(TakeIterWrapper<T> w) { return std::next(std::begin(w.iterable), w.toTake); }

    /** @brief Makes an iterator that only iterates up to `toTake` times */
    template<typename T>
    VTX_EXPORT inline TakeIterWrapper<T> take(T &iterable, std::size_t toTake) { return { iterable, toTake }; }
}

#endif
