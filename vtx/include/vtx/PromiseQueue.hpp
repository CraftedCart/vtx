#ifndef VTX_VTX_PROMISEQUEUE_HPP
#define VTX_VTX_PROMISEQUEUE_HPP

#include "vtx/TaskInfo.hpp"
#include "vtx_export.h"
#include "vmx/clientapi/ApiError.hpp"
#include "vmx/Client.hpp"
#include <QtPromise>
#include <QVector>
#include <QObject>
#include <functional>
#include <optional>

namespace Vtx::Mx {
    /**
     * @brief Executes async tasks in order, automatically retrying/backing off on various kinds of network errors
     */
    class VTX_EXPORT PromiseQueue : public QObject {
        Q_OBJECT

        private:
            struct VTX_EXPORT QueuedTask {
                /**
                 * @brief A callable to execute an async task
                 *
                 * This emits `onOperationSuccess()` or `onOperationFail()` when complete
                 */
                std::function<void()> exec;

                /** @brief Optional user-visible information about the task */
                std::optional<TaskInfo*> taskInfo;

                /** @brief Rejects an async task (after any retries) due to an API error */
                std::function<void(const Vmx::ClientApi::ApiError&)> rejectApiError;

                /** @brief Rejects an async task (after any retries) due to a network error */
                std::function<void(const Vmx::NetError&)> rejectNetError;

                /** @brief Rejects an async task (after any retries) due to an unknown error */
                std::function<void()> rejectUnknownError;
            };

        private:
            QVector<QueuedTask> queued;

            static const int DEFAULT_BACKOFF_MS;
            static const int MAX_BACKOFF_MS;
            static const int MAX_RETRIES;

            /**
             * @brief In the case of a network error, retry after this many milliseconds
             *
             * On each network error, this'll get doubled until the maximum, `MAX_BACKOFF_MS`, has been reached. Once we
             * get a success, this'll be reset.
             */
            int lastBackoffMs = DEFAULT_BACKOFF_MS;

            /**
             * @brief The number of times we've retried the current operation
             *
             * We keep track of this so we know to give up after a `MAX_RETRIES` attempts.
             */
            int retryCount = 0;

        public:
            // Use super ctor
            using QObject::QObject;

            // TODO: Do we need a dtor to reject all queued operations?

            /**
             * @brief Adds an async task to the queue
             *
             * If the newly added task is the only task in the queue, it will start executing immediately
             */
            template<typename T>
            QtPromise::QPromise<T> enqueue(
                    std::function<QtPromise::QPromise<T>()> &&func,
                    std::optional<TaskInfo*> taskInfo = {}
                    );

        private:
            void runNextOp();
            void resetBackoff();

        private slots:
            void retryOp();

            void onOperationSuccess();
            void onOperationFail(const Vmx::ClientApi::ApiError &err);
            void onOperationFail(const Vmx::NetError &err);
            void onOperationFail();
    };
}

#include "vtx/PromiseQueue.ipp"

#endif
