#ifndef VTX_VTX_PROMISEQUEUE_IPP
#define VTX_VTX_PROMISEQUEUE_IPP

namespace Vtx::Mx {
    template<typename T>
    QtPromise::QPromise<T> PromiseQueue::enqueue(
            std::function<QtPromise::QPromise<T>()> &&func,
            std::optional<TaskInfo*> taskInfo
            ) {
        return QtPromise::QPromise<T>{[this, func, taskInfo](
                const QtPromise::QPromiseResolve<T>& resolve,
                const QtPromise::QPromiseReject<T>& reject
                ) {
            auto lambda = [this, func, resolve, reject]() {
                func()
                    .then([this, resolve](const T &success) {
                        emit onOperationSuccess();
                        resolve(success);
                    })
                    .fail([this](const Vmx::ClientApi::ApiError &err) {
                        emit onOperationFail(err);
                        // reject called by onOperationFail handler
                    })
                    .fail([this](const Vmx::NetError &err) {
                        emit onOperationFail(err);
                        // reject called by onOperationFail handler
                    })
                    .fail([this]() {
                        emit onOperationFail();
                        // reject called by onOperationFail handler
                    });
            };

            queued.append(QueuedTask {
                .exec = lambda,
                .taskInfo = taskInfo,
                .rejectApiError = [reject](const Vmx::ClientApi::ApiError &err) { reject(err); },
                .rejectNetError = [reject](const Vmx::NetError &err) { reject(err); },
                .rejectUnknownError = [reject]() { reject(); },
            });

            if (queued.length() == 1) {
                if (taskInfo) {
                    (*taskInfo)->start();
                }

                lambda();
            }
        }};
    }
}

#endif
