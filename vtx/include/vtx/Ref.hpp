#ifndef VTX_VTX_REF_HPP
#define VTX_VTX_REF_HPP

#include "vtx_export.h"
#include <QAtomicInt>

namespace Vtx {
    template<typename T>
    class EnableRefFromThis;

    /**
     * @brief A ref counted shared pointer
     *
     * This shared pointer allows access to its ref count (with `getRefCount()`), and you can also manually
     * increment/decrement the ref counter with `ref()`/`deref()` (useful when working with async code).
     *
     * `Ref<T>` can be implicitly casted to a `T*`, so it is not necessary to do `&*object` when passing a `Ref<T>` into
     * a function that accepts `T*`.
     */
    template<typename T>
    class VTX_EXPORT Ref {
        friend class EnableRefFromThis<T>;

        private:
            T *data = nullptr;
            QAtomicInt *refCount = nullptr;

        public:
            /** @brief Creates a `Ref<T>` storing `nullptr` */
            Ref() = default;

            /**
             * @brief Copies a `Ref<T>` and increments its ref counter
             *
             * This does not call the copy constructor of the contained object, it only copies the pointer to the
             * object.
             */
            Ref(const Ref<T> &other);

            /**
             * @brief Decrements the ref counter for the stored object, and `delete`s it if the ref counter hit zero.
             */
            ~Ref();

            /** @brief Returns whether the `Ref<T>` is storing `nullptr` */
            bool isNull() const { return data == nullptr; }

            /** @brief Returns the number of references to the object */
            int getRefCount() const { return refCount->loadAcquire(); }

            /**
             * @brief Manually increment the ref counter
             *
             * This should always be paired with a call to `deref()`
             */
            void ref() const;

            /**
             * @brief Manually decrement the ref counter
             *
             * This should always be paired with a call to `ref()`
             */
            void deref() const;

            /**
             * @brief Construct a `Ref<T>` that manages a newly created object
             *
             * `args` will be perfectly forwarded to `T`'s constructor'
             */
            template<typename... Args>
            static Ref<T> create(Args &&...args);

            operator T*();
            operator const T*() const;

            /** @brief Check if two `Ref<T>` objects point to the same object */
            bool operator==(const Ref<T> &other) const;

            T* operator->() { return data; }
            const T* operator->() const { return data; }
            T& operator*() { return *data; }
            const T& operator*() const { return *data; }

            Ref<T>& operator=(const Ref<T> &other);

        private:
            Ref(T *data, QAtomicInt *refCount);
    };

    template<typename T>
    class VTX_EXPORT EnableRefFromThis {
        friend class Ref<T>;

        private:
            QAtomicInt *refFromThis_refCount;

        public:
            Ref<T> refFromThis();
    };
}

#include "vtx/Ref.ipp"

#endif
