#ifndef VTX_VTX_REF_IPP
#define VTX_VTX_REF_IPP

#include <type_traits>

namespace Vtx {
    template<typename T>
    Ref<T>::Ref(const Ref<T> &other) {
        if (!other.isNull()) {
            other.refCount->ref();

            data = other.data;
            refCount = other.refCount;
        }
    }

    template<typename T>
    Ref<T>::Ref(T *data, QAtomicInt *refCount) : data(data), refCount(refCount) {}

    template<typename T>
    Ref<T>::~Ref() {
        if (!isNull() && !refCount->deref()) {
            delete data;
            delete refCount;
        }
    }

    template<typename T>
    void Ref<T>::ref() const {
        Q_ASSERT(refCount != nullptr);

        refCount->ref();
    }

    template<typename T>
    void Ref<T>::deref() const {
        Q_ASSERT(refCount != nullptr);

        if (!refCount->deref()) {
            delete data;
            delete refCount;
        }
    }

    template<typename T>
    template<typename... Args>
    Ref<T> Ref<T>::create(Args &&...args) {
        T *data = new T(std::forward<Args>(args)...);
        QAtomicInt *refCount = new QAtomicInt(1);

        if constexpr (std::is_base_of_v<EnableRefFromThis<T>, T>) {
            data->refFromThis_refCount = refCount;
        }

        return Ref<T>(data, refCount);
    }

    template<typename T>
    Ref<T>::operator T*() {
        return data;
    }

    template<typename T>
    Ref<T>::operator const T*() const {
        return data;
    }

    template<typename T>
    bool Ref<T>::operator==(const Ref<T> &other) const {
        return data == other.data;
    }

    template<typename T>
    Ref<T>& Ref<T>::operator=(const Ref<T> &other) {
        if (!other.isNull()) {
            other.refCount->ref();
        }

        if (!isNull() && !refCount->deref()) {
            // The last reference has been released
            delete data;
            delete refCount;
        }

        data = other.data;
        refCount = other.refCount;

        return *this;
    }

    template<typename T>
    Ref<T> EnableRefFromThis<T>::refFromThis() {
        refFromThis_refCount->ref();
        return Ref<T>(static_cast<T*>(this), refFromThis_refCount);
    }
}

#endif
