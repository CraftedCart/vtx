#ifndef VTX_VTX_TASKINFO_HPP
#define VTX_VTX_TASKINFO_HPP

#include "vtx_export.h"
#include <QObject>

namespace Vtx {
    /**
     * @brief Represents a user-visible task
     *
     * Tasks that signal their start and finish with a `TaskInfo` object are shown in the UI on the status bar.
     */
    class VTX_EXPORT TaskInfo : public QObject {
        Q_OBJECT

        private:
            QString name;

        public:
            TaskInfo() = default;
            TaskInfo(const QString &name);

            void setName(const QString &name);
            const QString& getName() const;

            void start();
            void finishAndDelete();
    };
}

#endif
