#ifndef VTX_VTX_USERINFO_HPP
#define VTX_VTX_USERINFO_HPP

#include "vmx/Session.hpp"
#include "vmx/id/UserId.hpp"
#include "vtx_export.h"
#include <QObject>
#include <optional>

namespace Vtx {
    class VTX_EXPORT UserInfo : public QObject {
        Q_OBJECT

        private:
            std::optional<Vmx::Session> session;

            /** @brief The batch token to supply in the since param of the next /sync request */
            std::optional<QString> syncNextBatchToken;

            Vmx::Id::UserId userId;

        public:
            void setSession(const std::optional<Vmx::Session> &session);
            const std::optional<Vmx::Session>& getSession() const;

            void setSyncNextBatchToken(const std::optional<QString> &syncNextBatchToken);
            const std::optional<QString>& getSyncNextBatchToken() const;

            void setUserId(const Vmx::Id::UserId &userId);
            const Vmx::Id::UserId& getUserId() const;

        signals:
            /** @brief Emitted when the user should no-longer be known and logged in */
            void removed(UserInfo *user);
    };
}

#endif
