#ifndef VTX_VTX_VTXINSTANCE_HPP
#define VTX_VTX_VTXINSTANCE_HPP

#include "vtx/config/AppConfig.hpp"
#include "vtx/db/Database.hpp"
#include "vtx/TaskInfo.hpp"
#include "vtx/mx/Matrix.hpp"
#include "vtx_export.h"
#include <QVector>
#include <QApplication>

/**
 * @brief Namespace where all Vtx client code resides in
 */
namespace Vtx {
    class VTX_EXPORT VtxInstance : public QObject {
        Q_OBJECT

        private:
            /**
             * @brief The singleton instance of this object
             */
            VTX_EXPORT static VtxInstance *instance;

            QApplication *app;

            Config::AppConfig appConfig;
            Db::Database database;

            QVector<TaskInfo*> activeTasks;

            Mx::Matrix *matrix;

            UserInfo *activeUser = nullptr;

        private:
            VtxInstance() = default; // Init work is done in createInstance
            ~VtxInstance();

            void setupConnections();

        public:
            /**
             * @brief Creates the singleton instance of this object
             *
             * Command line args are passed to the QApplication constructor
             *
             * @param argc The number of command line arguments
             * @param argv[] The command line arguments
             * @param testMode Whether we're running tests
             *
             * @return Whether instance creation succeeded (on failure, destroyInstance should still be called however)
             */
            VTX_EXPORT static bool createInstance(int &argc, char *argv[], bool testMode = false);

            /**
             * @brief Getter for the singleton instance
             *
             * @return The singleton instance if it has been created
             */
            VTX_EXPORT static VtxInstance* getInstance();

            /**
             * @brief Used to clear out state between test runs
             */
            VTX_EXPORT static bool resetInstance(bool testMode);

            /**
             * @brief Deletes the singleton instance
             */
            VTX_EXPORT static void destroyInstance();

            /**
             * @brief Getter for the QApplication
             *
             * Alternatively, you could use the global qApp macro provided by Qt
             *
             * @return The QApplication
             */
            QApplication* getApp();

            /**
             * @brief Calls the exec function on the QApplcation, entering the event loop and staying in there until
             *        the application requests to quit.
             *
             * @return The return code for the application
             */
            int execApp();

            Config::AppConfig& getAppConfig();
            Db::Database& getDatabase();

            void addTask(TaskInfo *task);
            void removeTask(TaskInfo *task);

            UserInfo* getActiveUser();
            void setActiveUser(UserInfo *user);

            Mx::Matrix* getMatrix();
            Vmx::Client* getMxClient();

        signals:
            void taskListChanged(const QVector<TaskInfo*> &tasks);
            void activeUserChanged(UserInfo *activeUser);

        private slots:
            void onUserAdded(UserInfo *user);
    };
}

#endif
