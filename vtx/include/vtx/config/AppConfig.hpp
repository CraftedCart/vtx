#ifndef VTX_VTX_CONFIG_APPCONFIG_HPP
#define VTX_VTX_CONFIG_APPCONFIG_HPP

#include "vtx/config/PrivateConfig.hpp"
#include "vtx/config/PublicConfig.hpp"
#include "vtx/sync/DataMutex.hpp"
#include "vtx_export.h"

namespace Vtx::Config {
    class VTX_EXPORT AppConfig {
        private:
            Sync::DataMutex<PrivateConfig> privateConfig;
            Sync::DataMutex<PublicConfig> publicConfig;

        public:
            void writeAsync();
            void writeSync();
            void readSync();
    };
}

#endif
