#ifndef VTX_VTX_CONFIG_PRIVATECONFIG_HPP
#define VTX_VTX_CONFIG_PRIVATECONFIG_HPP

#include "vmx/Serde.hpp"
#include "vtx_export.h"

namespace Vtx::Config {
    /**
     * @brief The private config houses sensitive data. If you're the kind-of person who likes to make their dotfiles
     *        public, the private config should not go there.
     */
    class VTX_EXPORT PrivateConfig {
        public:
            // nothing yet...

        public:
            void preWrite();
            void postWrite();
            void postRead();
    };
}

REFL_AUTO(
        type(Vtx::Config::PrivateConfig, Vmx::Serde::Serialize(), Vmx::Serde::Deserialize())
        )

#endif
