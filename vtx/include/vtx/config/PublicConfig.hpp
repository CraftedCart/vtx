#ifndef VTX_VTX_CONFIG_PUBLICCONFIG_HPP
#define VTX_VTX_CONFIG_PUBLICCONFIG_HPP

#include "vmx/Serde.hpp"
#include "vtx_export.h"

namespace Vtx::Config {
    /**
     * @brief The private config houses non-sensitive persistant data. If you're the kind-of person who likes to make
     *        their dotfiles public, the public config is fine to put there.
     */
    class VTX_EXPORT PublicConfig {
        public:
            void preWrite();
            void postWrite();
            void postRead();
    };
}

REFL_AUTO(
        type(Vtx::Config::PublicConfig, Vmx::Serde::Serialize(), Vmx::Serde::Deserialize())
        )

#endif
