#ifndef VTX_VTX_DB_DATABASE_HPP
#define VTX_VTX_DB_DATABASE_HPP

#include "vtx/Ref.hpp"
#include "vtx/UserInfo.hpp"
#include "vtx/mx/EventLoc.hpp"
#include "vtx/mx/EventLocRange.hpp"
#include "vtx_export.h"
#include "vmx/id/RoomId.hpp"
#include "vmx/id/EventId.hpp"
#include "vmx/util/Result.hpp"
#include <QtPromise>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QThreadPool>

// Forward declarations
namespace Vtx::Mx {
    class Event;
    class Room;
    class TimelineSection;
}

namespace Vtx::Db {
    class VTX_EXPORT Database {
        private:
            QSqlDatabase db;

            /**
             * @brief All database operations happen on their own thread, so we don't stall the UI
             *
             * This is a thread pool with 1 thread that never expires
             */
            QThreadPool dbPool;
            QThread *dbThread;

            uint transactionDepth = 0; // Used for keeping track of nested transactions
            QSqlQuery queryInsertEvent;
            QSqlQuery queryInsertCurrentStateEvent;
            QSqlQuery queryGetNewestEvents;
            QSqlQuery queryGetEventsBefore;
            QSqlQuery queryGetTimelineSectionOrderRange;
            QSqlQuery queryDeleteFromTimelineSectionBeforeOrder;
            QSqlQuery queryGetCurrentStateEvent;
            QSqlQuery queryInsertPrevBatchToken;
            QSqlQuery queryDeletePrevBatchToken;
            QSqlQuery queryGetPrevBatchToken;
            QSqlQuery queryInsertUser;
            QSqlQuery queryGetAllUsers;
            QSqlQuery queryInsertUserRoom;
            QSqlQuery queryGetAllRoomMemberships;
            QSqlQuery queryGetTimelineMaxSectionId;
            QSqlQuery queryGetEventLocForId;
            QSqlQuery queryShiftEventLocUp;
            QSqlQuery queryMakeSavepoint;
            QSqlQuery queryReleaseSavepoint;
            QSqlQuery queryRollbackSavepoint;

        public:
            /**
             * @brief Opens the local db, performs migrations, and initializes prepared statements
             *
             * @param useMemoryDb Whether to only use an in-memory database (mostly used for tests)
             *
             * @return
             * - Resolves if opening/initializing the local db succeeded
             * - Rejects(QString) if it failed, with a human-readable error message
             */
            QtPromise::QPromise<void> init(bool useMemoryDb = false);

            /** @brief Closes the local db */
            void deinit();

            /** @brief Inserts or replaces an event in the local db */
            QtPromise::QPromise<void> insertEvent(const Mx::Event *event, const Mx::Room *room);

            /**
             * @brief Inserts or replaces an event in the local db and shifts further events in the same section up by 1
             */
            QtPromise::QPromise<void> insertEventInMiddle(const Mx::Event *event, const Mx::Room *room);

            /**
             * @brief Inserts or replaces a room current state event in the local db
             *
             * The event must also have been added with `Database::insertEvent`.
             */
            QtPromise::QPromise<void> insertCurrentStateEvent(const Mx::Event *event, const Mx::Room *room);

            /** @brief Returns the newest events for a timeline section */
            QtPromise::QPromise<QVector<Ref<Mx::Event>>> getNewestEvents(const Mx::Room *room, int num, int sectionId);

            /** @brief Returns events in a timeline section are ordered before `loc.order` */
            QtPromise::QPromise<QVector<Ref<Mx::Event>>> getEventsBefore(const Mx::Room *room, int num, const Mx::EventLoc &loc);

            /**
             * @brief Delete events in a timeline section with a location before `loc`.
             *
             * This can be useful to fix up non-contiguous timeline sections, by deleting older events that break the
             * contiguity.
             */
            QtPromise::QPromise<void> deleteSectionEventsBefore(const Mx::Room *room, const Mx::EventLoc &loc);

            /** May return a null event if it doesn't exist */
            QtPromise::QPromise<Ref<Mx::Event>> getCurrentStateEvent(const Mx::Room *room, QString type, QString stateKey);

            /** @brief Returns the min and max `order` values stored in the local db for a given `TimelineSection` */
            QtPromise::QPromise<std::optional<Mx::EventLocRange>> getTimelineSectionOrderRange(const Mx::TimelineSection *section);

            QtPromise::QPromise<void> insertPrevBatchToken(
                    const Mx::TimelineSection *section,
                    const UserInfo *user,
                    QString prevBatchToken
                    );
            QtPromise::QPromise<void> deletePrevBatchToken(const Mx::TimelineSection *section, const UserInfo *user);
            QtPromise::QPromise<std::optional<QString>> getPrevBatchToken(const Mx::TimelineSection *section, const UserInfo *user);

            QtPromise::QPromise<std::optional<int>> getMaxSectionIdForRoom(const Mx::Room *room);

            /**
             * - Outer optional: Whether the event exists or not
             * - Inner optional: Whether the event's location is null or not
             */
            QtPromise::QPromise<std::optional<std::optional<Mx::EventLoc>>> getEventLocForId(Vmx::Id::RoomId roomId, Vmx::Id::EventId eventId);

            void saveUsers();
            void loadUsers();

            /**
             * @brief Starts a database transaction
             *
             * Transactions can be nested, so if a transaction is currently in progress, this will simply increment a
             * "depth" counter
             */
            QtPromise::QPromise<void> startTransaction();

            /**
             * @brief Finishes a database transaction
             *
             * Transactions can be nested, so if we're in a nested transaction, the "depth" counter will simply be
             * decremented and this will return `true`.
             */
            QtPromise::QPromise<void> commitTransaction();

            bool makeSavepoint(const QString &name);
            bool releaseSavepoint(const QString &name);
            bool rollbackSavepoint(const QString &name);

        private:
            /** @brief Creates database tables if they don't already exist */
            void initTables();

            /** @brief Creates and populates the meta table */
            void initMetaTable();

            /** @brief Initializes prepared statements */
            void initPreparedStatements();

            /**
             * @brief Checks if the database needs to be adjusted for a change in version, and performs migrations if
             *        needed
             */
            Vmx::Util::Result<void, QString> checkMigrations();
    };
}

#endif
