#ifndef VTX_VTX_MODEL_ROOMLISTMODEL_HPP
#define VTX_VTX_MODEL_ROOMLISTMODEL_HPP

#include "vtx/mx/RoomList.hpp"
#include "vtx_export.h"
#include <QAbstractListModel>

namespace Vtx::Model {
    class VTX_EXPORT RoomListModel : public QAbstractListModel {
        Q_OBJECT

        private:
            Mx::RoomList *roomList;

        public:
            RoomListModel(Mx::RoomList *roomList, QObject *parent = nullptr);

            virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
            virtual QVariant data(const QModelIndex &index, int role) const override;
            virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

            const Vmx::Id::RoomId& roomIdForIndex(const QModelIndex &index);
    };
}

#endif
