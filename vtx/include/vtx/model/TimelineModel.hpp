#ifndef VTX_VTX_MODEL_TIMELINEMODEL_HPP
#define VTX_VTX_MODEL_TIMELINEMODEL_HPP

#include "vtx/mx/Room.hpp"
#include "vtx/mx/TimelineSection.hpp"
#include "vtx_export.h"
#include <QAbstractListModel>

namespace Vtx::Model {
    class VTX_EXPORT TimelineModel : public QAbstractListModel {
        Q_OBJECT

        public:
            static const quint16 ROLE_REF_EVENT = Qt::UserRole;

        private:
            Mx::Room *room = nullptr;
            Ref<Mx::TimelineSection> section;

        public:
            TimelineModel(QObject *parent = nullptr);

            void setSection(Mx::Room *room, Ref<Mx::TimelineSection> section);
            Mx::Room* getRoom();
            Ref<Mx::TimelineSection>& getSection();

            virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
            virtual QVariant data(const QModelIndex &index, int role) const override;
            virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

            void requestPastMessages(UserInfo *user);
            bool isRequestingPastMessages();

        signals:
            void beginRequestingPastMessages();
            void finishedRequestingPastMessages();

            void currentSectionIdChanged(int oldId, int newId);
    };
}

#endif
