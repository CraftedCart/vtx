#ifndef VTX_VTX_MX_EVENT_HPP
#define VTX_VTX_MX_EVENT_HPP

#include "vtx/mx/EventLoc.hpp"
#include "vtx/Ref.hpp"
#include "vtx_export.h"
#include "vmx/event/SyncRoomEvent.hpp"
#include "vmx/event/UnsignedData.hpp"
#include "vmx/id/UserId.hpp"
#include "vmx/id/EventId.hpp"
#include "vmx/InString.hpp"
#include <QObject>
#include <QPointer>

namespace Vtx::Mx {
    /**
     * @brief An event (can be a regular event or state event) in a room
     */
    class VTX_EXPORT Event : public QObject {
        Q_OBJECT

        private:
            /** @brief The user ID of who sent the event */
            Vmx::Id::UserId sender;

            /** @brief The globally unique event identifier */
            Vmx::Id::EventId eventId;

            /** @brief The timestamp of when the event was sent on the originating homeserver */
            QDateTime originServerTs;

            /** @brief Optional extra information about the event */
            std::optional<Vmx::Event::UnsignedData> unsignedData;

            /**
             * @brief The type of event (eg: `m.room.message`)
             *
             * The type of event. This SHOULD be namespaced similar to Java package naming conventions e.g.
             * `com.example.subdomain.event.type`
             */
            Vmx::InString type;

            /** @brief The fields of `content` will vary depending on the event type */
            QJsonObject content;

            /** @brief The entire event as JSON */
            QJsonObject json;

            /** @brief Weak pointer to the previous event in the timeline */
            QPointer<Event> prevEvent;

            /**
             * @brief Where the event in the timeline, relative to other events
             *
             * May be `nullopt` for state events we've recieved for a room that we haven't found in the timeline yet
             */
            std::optional<EventLoc> eventLoc;

        public:
            Event() = default;
            Event(const Vmx::Event::SyncRoomEvent &event);
            Event(const Vmx::Id::EventId &eventId, const QJsonObject &json, const std::optional<EventLoc> &loc = {});

            const Vmx::Id::UserId& getSender() const;
            const Vmx::Id::EventId& getEventId() const;
            const QDateTime& getOriginServerTs() const;
            const std::optional<Vmx::Event::UnsignedData>& getUnsignedData() const;
            const Vmx::InString& getType() const;
            const QJsonObject& getContent() const;
            const QJsonObject& getJson() const;

            /** @brief Returns the state key for a state event, or `nullopt` if the event isn't a state event */
            std::optional<QString> getStateKey() const;

            void setPrevEvent(Event *prevEvent);
            const Event* getPrevEvent() const;

            void setEventLoc(const std::optional<EventLoc> &eventLoc);
            const std::optional<EventLoc>& getEventLoc() const;

            bool isStateEvent() const;

            /** @note Should generally not be used - exists for testing purposes */
            Ref<Event> clone() const;
    };
}

Q_DECLARE_METATYPE(Vtx::Ref<Vtx::Mx::Event>)

#endif
