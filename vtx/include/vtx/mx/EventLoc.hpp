#ifndef VTX_VTX_MX_EVENTLOC_HPP
#define VTX_VTX_MX_EVENTLOC_HPP

#include "vtx_export.h"
#include <QtGlobal> // For Q_ASSERT

namespace Vtx::Mx {
    // TODO: Replace ints with size_t when Qt 6 comes around

    /**
     * @brief Represents the relative location of an event in a (potentially) gappy timeline
     *
     * Each event's location can be identified with 2 numbers, its section and order.
     *
     * In the event of a gappy timeline, the section number identifies which contiguous block of events an event lives
     * in. The order number identifies where the event is inside the section.
     *
     * Note that ONLY the `order` number can be compared with `>`, `>=` , `<`, or `<=`, not `section`. Trying to compare
     * two `EventLoc`s with differing sections will trigger an assertion failure.
     *
     * This is because when jumping around a timeline, there is no reliable way to know where a past section fits in
     * relative to other past sections. Say, for example, we jump back to a past pinned message in 2018, this may create
     * a timeline section `-1`. If we when jump forward to a search result in 2019, this would create timeline section
     * `-2`. Note that we can't reliably determine which direction we jump, nor can we use the send time of events to
     * determine order as it's possible for sequential events to go back and forward in time.
     *
     * In all cases, the *highest numbered* section ID should always be the newest timeline section.
     *
     * Both of these numbers can grow in both positive and negative directions.
     */
    struct VTX_EXPORT EventLoc {
        int section;
        int order;

        inline EventLoc& operator++();
        inline EventLoc& operator--();

        inline EventLoc& operator+=(int val);
        inline EventLoc& operator-=(int val);

        inline EventLoc operator+(int num) const;
        inline EventLoc operator-(int num) const;

        inline bool operator==(const EventLoc &other) const;
        inline bool operator!=(const EventLoc &other) const;
        inline bool operator>(const EventLoc &other) const;
        inline bool operator>=(const EventLoc &other) const;
        inline bool operator<(const EventLoc &other) const;
        inline bool operator<=(const EventLoc &other) const;
    };

    inline EventLoc& EventLoc::operator++() {
        ++order;
        return *this;
    }

    inline EventLoc& EventLoc::operator--() {
        --order;
        return *this;
    }

    inline EventLoc& EventLoc::operator+=(int val) {
        order += val;
        return *this;
    }

    inline EventLoc& EventLoc::operator-=(int val) {
        order -= val;
        return *this;
    }

    inline bool EventLoc::operator==(const EventLoc &other) const {
        return section == other.section && order == other.order;
    }

    inline bool EventLoc::operator!=(const EventLoc &other) const {
        return section != other.section || order != other.order;
    }

    inline EventLoc EventLoc::operator+(int num) const {
        return EventLoc{section, order + num};
    }

    inline EventLoc EventLoc::operator-(int num) const {
        return EventLoc{section, order - num};
    }

    inline bool EventLoc::operator>(const EventLoc &other) const {
        Q_ASSERT(section == other.section);
        return order > other.order;
    }

    inline bool EventLoc::operator>=(const EventLoc &other) const {
        Q_ASSERT(section == other.section);
        return order >= other.order;
    }

    inline bool EventLoc::operator<(const EventLoc &other) const {
        Q_ASSERT(section == other.section);
        return order < other.order;
    }

    inline bool EventLoc::operator<=(const EventLoc &other) const {
        Q_ASSERT(section == other.section);
        return order <= other.order;
    }
}

VTX_EXPORT QDebug operator<<(QDebug dbg, const Vtx::Mx::EventLoc &value);

#endif
