#ifndef VTX_VTX_MX_EVENTLOCRANGE_HPP
#define VTX_VTX_MX_EVENTLOCRANGE_HPP

#include "vtx/mx/EventLoc.hpp"
#include "vtx_export.h"

namespace Vtx::Mx {
    /**
     * @brief Represents a contiguous range of relative event locations
     */
    struct VTX_EXPORT EventLocRange {
        EventLoc min;
        EventLoc max;

        inline bool operator==(const EventLocRange &other) const;
        inline bool operator!=(const EventLocRange &other) const;
    };

    inline bool EventLocRange::operator==(const EventLocRange &other) const {
        return min == other.min && max == other.max;
    }

    inline bool EventLocRange::operator!=(const EventLocRange &other) const {
        return min != other.min || max != other.max;
    }
}

VTX_EXPORT QDebug operator<<(QDebug dbg, const Vtx::Mx::EventLocRange &value);

#endif
