#ifndef VTX_VTX_MX_MATRIX_HPP
#define VTX_VTX_MX_MATRIX_HPP

#include "vtx/mx/RoomList.hpp"
#include "vtx/UserInfo.hpp"
#include "vtx/PromiseQueue.hpp"
#include "vtx_export.h"
#include "vmx/Client.hpp"
#include "vmx/clientapi/r0/sync/SyncEvents.hpp"
#include "vmx/Vmx.hpp"
#include <QObject>
#include <QCache>
#include <QPixmap>
#include <QTimer>
#include <optional>
#include <variant>

namespace Vtx::Mx {
    /**
     * @brief An error for when image data could not be understood
     */
    struct VTX_EXPORT CouldNotParseImage {};

    struct AlreadyLoggedIn {};
    using LoginRejectError = std::variant<AlreadyLoggedIn>;

    /**
     * @brief The class that keeps track of all rooms, users, etc.
     */
    class VTX_EXPORT Matrix : public QObject {
        Q_OBJECT

        private:
            Vmx::Client *client;
            QVector<UserInfo*> users;
            /** @brief Users that currently are in a sync loop */
            QVector<UserInfo*> syncingUsers;

            PromiseQueue *globalQueue;
            // TODO: Per-room request queues

            RoomList rooms;

            // TODO: Known foreign users list, perhaps sweeping it every now and then (use ref counted pointers and
            //       check when we're the only user left?) for users we've forgotten about - so we can cache stuff like
            //       people's profile pictures and stuff
            //
            //       ...though then again, that can change from room-to-room so maybe it's better to do it on a per-room
            //       basis?

            QCache<QUrl, Vmx::Arc<QPixmap>> pixmapCache;

            QTimer purgeTimer;

        public:
            Matrix(QObject *parent = nullptr);
            ~Matrix();

            /**
             * @brief Returns the underlying Matrix client object that requests go through.
             *
             * Directly making requests on this object bypasses queues, request backoff on failure, request retry on
             * M_LIMIT_EXCEEDED, etc.
             */
            Vmx::Client* getClient();

            /**
             * @brief Add a user account
             *
             * This claims ownership over `user`
             */
            void addUser(UserInfo *user);

            /** @brief Removed and deletes the user */
            void removeUser(UserInfo *user);

            const QVector<UserInfo*>& getUsers() const;

            /** @brief Returns nullptr if the user is not found */
            UserInfo* findUserById(const Vmx::Id::UserId &id);

            RoomList& getRooms();
            const RoomList& getRooms() const;

            /**
             * @brief Merge a joined room fetched from the `/sync` endpoint into a room's timeline and current state,
             *        and store events in the local database
             */
            void mergeSyncedRoom(
                    const UserInfo *user,
                    const Vmx::Id::RoomId &roomId,
                    const Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom &joinedRoom
                    );

            /**
             * @brief Attempts to free memory by purging events and other data that aren't in use elsewhere
             *
             * Any purged data should be stored in the local SQLite database anyway
             */
            void purgeUnused();

            void stopAutoPurge();

            /**
             * @brief Fetches an image from an mxx URL and caches it.
             *
             * On success, the promise is resolves with a ref counted `QPixmap`, otherwise it may be rejected with a
             * `CouldNotParseImage` error, a `Vmx::ClientApi::ApiError`, a `Vmx::NetError`, or an unknown error.
             */
            [[nodiscard]]
            QtPromise::QPromise<Vmx::Arc<QPixmap>> getMxcPixmap(
                    const QUrl &mxc,
                    const Vmx::Session &session
                    );

            /** @brief Make a request and queue it on the global request queue */
            template<typename RequestT>
            [[nodiscard]]
            QtPromise::QPromise<typename RequestT::Response> requestQueuedGlobal(
                    const RequestT &req,
                    const QUrl &homeserverUrl,
                    const std::optional<QString> &accessToken = {},
                    std::optional<TaskInfo*> taskInfo = {}
                    );

            /** @brief Make a request and queue it on the global request queue */
            template<typename RequestT>
            [[nodiscard]]
            QtPromise::QPromise<typename RequestT::Response> requestQueuedGlobal(
                    const RequestT &req,
                    const Vmx::Session &session,
                    std::optional<TaskInfo*> taskInfo = {}
                    );

            /** @brief Make a request and run it on its own dedicated `PromiseQueue` */
            template<typename RequestT>
            [[nodiscard]]
            QtPromise::QPromise<typename RequestT::Response> requestImmediate(
                    const RequestT &req,
                    const QUrl &homeserverUrl,
                    const std::optional<QString> &accessToken = {},
                    std::optional<TaskInfo*> taskInfo = {}
                    );

            /** @brief Make a request and run it on its own dedicated `PromiseQueue` */
            template<typename RequestT>
            [[nodiscard]]
            QtPromise::QPromise<typename RequestT::Response> requestImmediate(
                    const RequestT &req,
                    const Vmx::Session &session,
                    std::optional<TaskInfo*> taskInfo = {}
                    );

            Vmx::Util::Result<void, LoginRejectError> onLoginSuccess(const Vmx::ClientApi::R0::Session::Login::PostResponse &resp);

            void syncUserRepeat(UserInfo *user);
            void initialSyncThenSyncUserRepeat(UserInfo *user);
            void startSyncingAllUsers();

        signals:
            void userAdded(UserInfo *user);
    };
}

#include "vtx/mx/Matrix.ipp"

#endif
