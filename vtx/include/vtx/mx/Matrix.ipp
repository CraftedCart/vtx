#ifndef VTX_VTX_MX_MATRIX_IPP
#define VTX_VTX_MX_MATRIX_IPP

namespace Vtx::Mx {
    template<typename RequestT>
    QtPromise::QPromise<typename RequestT::Response> Matrix::requestQueuedGlobal(
            const RequestT &req,
            const QUrl &homeserverUrl,
            const std::optional<QString> &accessToken,
            std::optional<TaskInfo*> taskInfo
            ) {
        return globalQueue->enqueue<typename RequestT::Response>([=]() {
            return client->request(req, homeserverUrl, accessToken);
        }, taskInfo);
    }

    template<typename RequestT>
    QtPromise::QPromise<typename RequestT::Response> Matrix::requestQueuedGlobal(
            const RequestT &req,
            const Vmx::Session &session,
            std::optional<TaskInfo*> taskInfo
            ) {
        return globalQueue->enqueue<typename RequestT::Response>([=]() {
            return client->request(req, session);
        }, taskInfo);
    }

    template<typename RequestT>
    QtPromise::QPromise<typename RequestT::Response> Matrix::requestImmediate(
            const RequestT &req,
            const QUrl &homeserverUrl,
            const std::optional<QString> &accessToken,
            std::optional<TaskInfo*> taskInfo
            ) {
        // When making an *immediate* request, a dedicated queue is created for it, and destroyed when the request
        // finishes
        PromiseQueue *queue = new PromiseQueue(this);

        return queue->enqueue<typename RequestT::Response>([=]() {
            return client->request(req, homeserverUrl, accessToken);
        }, taskInfo)
        .finally([queue]() {
            delete queue;
        });
    }

    template<typename RequestT>
    QtPromise::QPromise<typename RequestT::Response> Matrix::requestImmediate(
            const RequestT &req,
            const Vmx::Session &session,
            std::optional<TaskInfo*> taskInfo
            ) {
        return requestImmediate(req, session.homeserverUrl, session.accessToken, taskInfo);
    }
}

#endif
