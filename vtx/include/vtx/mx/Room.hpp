#ifndef VTX_VTX_MX_ROOM_HPP
#define VTX_VTX_MX_ROOM_HPP

#include "vtx/mx/Timeline.hpp"
#include "vtx/mx/RoomState.hpp"
#include "vtx_export.h"
#include "vmx/id/RoomId.hpp"
#include "vmx/Membership.hpp"
#include <QObject>

// Forward declarations
class TestRoom;

namespace Vtx::Mx {
    /**
     * @brief A room is a place where users can send and receive events. Rooms can also have state, made up of state
     *        events.
     */
    class VTX_EXPORT Room : public QObject {
        Q_OBJECT

        friend class ::TestRoom;

        private:
            /** @brief The unique ID for the room */
            Vmx::Id::RoomId roomId;

            /**
             * @brief The user-facing name for the room, derived from its state events
             *
             * This may be derived from an `m.room.name` event if it exists, `m.room.canonical_alias`, room heroes, room
             * members, or failing all that, the room ID.
             */
            QString displayName;

            Timeline timeline = Timeline(this);
            RoomState currentState = RoomState(this);

            /** @brief The membership states of logged in users for this room */
            QHash<const UserInfo*, Vmx::Membership> userMemberships;

            /**
             * @brief The users used to generate a room name if a room does not have a name
             *
             * Different users will have different heroes
             */
            QHash<const UserInfo*, QVector<Vmx::Id::UserId>> heroes;

            bool requestingPastMessages = false;

        public:
            Room(const Vmx::Id::RoomId &roomId);

            /**
             * @brief Create a new room object initially populated from a `JoinedRoom` struct from the `/sync` endpoint
             */
            Room(const UserInfo *user, const Vmx::Id::RoomId &roomId, const Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom &room);

            /**
             * @brief Merge a `JoinedRoom` from the `/sync` endpoint into this room object. Also stores events in the
             *        local db
             */
            void mergeSyncedRoom(const UserInfo *user, const Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom &room);

            /** @brief Returns the unique ID for the room */
            const Vmx::Id::RoomId& getRoomId() const;

            /** @brief Returns the user-facing name for the room, derived from its state events */
            const QString& getDisplayName() const;

            const Timeline& getTimeline() const;
            Timeline& getTimeline();

            RoomState& getCurrentState();
            const RoomState& getCurrentState() const;

            /** @brief If a user has a display name in this room, return it, otherwise return the user's ID */
            QString getUserDisplayName(const Vmx::Id::UserId &userId);

            void setUserMembership(const UserInfo *user, Vmx::Membership membership);
            /** If a user has no membership status stored, Vmx::Membership::LEAVE will be assumed */
            Vmx::Membership getUserMembership(const UserInfo *user);
            const QHash<const UserInfo*, Vmx::Membership>& getUserMemberships() const;

            /**
             * @brief Attempts to free memory by purging events and other data that aren't in use elsewhere
             *
             * Any purged data should be stored in the local SQLite database anyway
             */
            void purgeUnused();

            /**
             * @brief Returns whether a event stored in the room timeline can be purged from memory
             *
             * @return `true` if the event either
             *         - Has a ref count of 1
             *         - Has a ref count of 2, is a state event, and is stored in the room's current state object
             */
            bool isTimelineEventPurgeable(const Ref<Event> &event);

        private:
            void mergeSummary(const UserInfo *user, const Vmx::ClientApi::R0::Sync::SyncEvents::RoomSummary &summary);

            /**
             * @brief Merges all unique hero lists from all known logged-in accounts that are in the room, ensuring
             *        each hero only appears once
             */
            QVector<Vmx::Id::UserId> dedupHeroes();

            /** @brief Recomputes the display name based on the room's state */
            void updateDisplayName();

        private slots:
            void onUserRemoved(UserInfo *user);
    };
}

#endif
