#ifndef VTX_VTX_MX_ROOMLIST_HPP
#define VTX_VTX_MX_ROOMLIST_HPP

#include "vtx/mx/Room.hpp"
#include "vtx_export.h"
#include "vmx/id/RoomId.hpp"
#include <QModelIndex>
#include <QVector>
#include <QObject>

namespace Vtx::Mx {
    /**
     * @brief A list of all joined rooms
     */
    class VTX_EXPORT RoomList : public QObject {
        Q_OBJECT

        private:
            QVector<Room*> rooms;

        public:
            ~RoomList();

            void append(Room *value);

            const Room* at(int i) const;
            int length() const;

            /** @brief Returns nullptr if the room is not found */
            Room* findById(const Vmx::Id::RoomId &id);

            Room* operator[](int i);
            const Room* operator[](int i) const;

            // Range-based for stuff
            using iterator = QVector<Room*>::iterator;
            using const_iterator = QVector<Room*>::const_iterator;

            iterator begin();
            const_iterator begin() const;
            iterator end();
            const_iterator end() const;

        signals:
            void onBeginInsertRows(const QModelIndex &parent, int first, int last);
            void onEndInsertRows();
    };
}

#endif
