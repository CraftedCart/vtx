#ifndef VTX_VTX_MX_ROOMSTATE_HPP
#define VTX_VTX_MX_ROOMSTATE_HPP

#include "vtx/mx/Event.hpp"
#include "vtx/Ref.hpp"
#include "vtx_export.h"
#include "vmx/clientapi/r0/sync/SyncEvents.hpp"
#include "vmx/InString.hpp"
#include <optional>
#include <tuple>

// Forward declarations
namespace Vtx::Mx { class Room; }

namespace Vtx::Mx {
    /**
     * @brief Caches a room's current state, and can also be used to fetch current state from the local db if not cached
     */
    class VTX_EXPORT RoomState : public QObject {
        Q_OBJECT

        private:
            /** @brief The room that the `RoomState` object is associated with */
            Room *room;

            // This effectively acts as a cache for the local db
            // Mutable since this may be modified in a const getter if we fetch an event from the db
            //
            // TODO: Mutex this
            //
            //    type                 stateKey
            mutable QHash<Vmx::InString, QHash<Vmx::InString, Ref<Event>>> state;

        public:
            RoomState(Room *room);

            /** @brief Returns the number of state events stored in memory */
            int cachedSize() const;

            /**
             * @brief Add or replace an existing state event to this `RoomState` object
             *
             * @note This does not add/replace the state event in the local db
             */
            Ref<Event> mergeEvent(const Vmx::Event::SyncStateEvent &value);

            /**
             * @brief Add or replace an existing state event to this `RoomState` object
             *
             * @note This does not add/replace the state event in the local db
             */
            void mergeEvent(const Ref<Event> &value);

            /**
             * @brief Adds a state event if it does not already exist
             *
             * @note This does not add/replace the state event in the local db
             *
             * @return The newly created `Ref<Event>` if the event is new and was added to the room state, or `nullopt`
             *         if it isn't new.
             */
            std::optional<Ref<Event>> mergeEventIfNew(const Vmx::Event::SyncStateEvent &value);

            /**
             * @brief Adds a state event if it does not already exist
             *
             * Does not claim ownershop of the event
             */
            bool mergeEventIfNew(const Ref<Event> &value);

            /** @brief Check if `event` is stored in this `RoomState` cache */
            bool hasEvent(const Ref<Event> &event);

            /**
             * @brief Fetch a state event for the room
             *
             * If the state event cannot be found in the cache, the local db will be queried for the event and cached if
             * it exists.
             */
            std::optional<Ref<Event>> get(const Vmx::InString &type, const Vmx::InString &stateKey) const;

            /**
             * @brief Fetch a state event for the room
             *
             * If the state event cannot be found in the cache, the local db will NOT be queried for the event.
             */
            std::optional<Ref<Event>> getCached(const Vmx::InString &type, const Vmx::InString &stateKey) const;

            /**
             * @brief Removes all state events that match `predicate` from the cache
             *
             * @note This does not remove events from the local db
             *
             * @param predicate A function that takes `(type, stateKey, event)` and returns `true` if the event should
             *                  be removed.
             */
            void removeAll(std::function<bool(const Vmx::InString&, const Vmx::InString, const Ref<Event>&)> predicate);
    };
}

#endif
