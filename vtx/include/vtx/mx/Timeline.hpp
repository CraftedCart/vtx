#ifndef VTX_VTX_MX_TIMELINE_HPP
#define VTX_VTX_MX_TIMELINE_HPP

#include "vtx/mx/TimelineSection.hpp"
#include "vtx_export.h"
#include "vmx/clientapi/r0/sync/SyncEvents.hpp"
#include <QMap>

// Forward declarations
namespace Vtx::Mx { class Room; }

namespace Vtx::Mx {
    /**
     * @brief A room's event timeline
     *
     * As timelines can have gaps in them, a `Timeline` is made up of multiple `TimelineSection`s, with each section
     * representing a contiguous region of events.
     */
    class VTX_EXPORT Timeline : public QObject {
        Q_OBJECT

        private:
            Room *room;

            // TODO: Change this to size_t when Qt 6 rolls around
            // Maps section IDs to `TimelineSection` objects
            QMap<int, Ref<TimelineSection>> sections;

            int currentSectionId = 0;

        public:
            Timeline(Room *room);

            /** @brief Returns the room the timeline is associated with */
            Room* getRoom();

            /** @brief Returns the room the timeline is associated with */
            const Room* getRoom() const;

            /**
             * @brief Returns the newest timeline section, to which synced events should be appended to
             *
             * If the current section does not exist in memory, it will be created then returned.
             */
            Ref<TimelineSection> getCurrentSection();

            Ref<TimelineSection> getSection(int sectionId);

            Ref<TimelineSection> appendNewSection();

            /**
             * @brief Attempts to free memory by purging events and other data that aren't in use elsewhere
             *
             * Any purged data should be stored in the local SQLite database anyway
             */
            void purgeUnused();

            int numCachedSections() const;

            /** @brief Fetches a `TimelineSection` with a given section ID */
            Ref<TimelineSection>& operator[](int i);

            /** @brief Fetches a `TimelineSection` with a given section ID */
            const Ref<TimelineSection> operator[](int i) const;

        signals:
            void currentSectionIdChanged(int oldId, int newId);
    };
}

#endif
