#ifndef VTX_VTX_MX_TIMELINESECTION_HPP
#define VTX_VTX_MX_TIMELINESECTION_HPP

#include "vtx/mx/EventLocRange.hpp"
#include "vtx/mx/Event.hpp"
#include "vtx/UserInfo.hpp"
#include "vtx/Ref.hpp"
#include "vtx_export.h"
#include <QModelIndex>
#include <QList>
#include <QObject>
#include <optional>

// Forward declarations
namespace Vtx::Mx { class Timeline; }

namespace Vtx::Mx {
    /**
     * @brief A contiguous block of events in a potentially gappy timelime
     */
    class VTX_EXPORT TimelineSection : public QObject, public EnableRefFromThis<TimelineSection> {
        Q_OBJECT

        private:
            Timeline *timeline;

            QList<Ref<Event>> events;

            // This acts somewhat as a cache for the local db, hence why it's mutable
            mutable QHash<const UserInfo*, QString> prevBatchTokens;
            // TODO: Store next batch tokens too

            int sectionId;

            /** Will be nullopt if the timeline is empty */
            std::optional<EventLocRange> range;

            /** Will be nullopt if the timeline is empty */
            std::optional<EventLocRange> persistentRange;

            bool requestingPastMessages = false;

        public:
            TimelineSection(Timeline *timeline, int sectionId);

            void append(Ref<Event> &value);
            void prepend(Ref<Event> &value);

            /** @brief Insert an event in the middle of the event list */
            void insert(Ref<Event> &value);

            /** @brief Appends an event if the front of the timeline section lines up with the new event */
            bool maybeAppendSyncedEvent(Ref<Event> &value);
            void prependNoUpdate(Ref<Event> &value);

            void removeFromStart(int num);
            void removeFromEnd(int num);

            const Ref<Event>& at(int i) const;
            int length() const;
            bool isEmpty() const;

            const std::optional<EventLocRange>& getRange();
            const std::optional<EventLocRange>& getPersistentRange();

            Ref<Event>& operator[](int i);
            const Ref<Event>& operator[](int i) const;

            // Range-based for stuff
            using iterator = QList<Ref<Event>>::iterator;
            using const_iterator = QList<Ref<Event>>::const_iterator;
            using reverse_iterator = QList<Ref<Event>>::reverse_iterator;
            using const_reverse_iterator = QList<Ref<Event>>::const_reverse_iterator;

            iterator begin();
            const_iterator begin() const;
            iterator end();
            const_iterator end() const;

            reverse_iterator rbegin();
            const_reverse_iterator rbegin() const;
            reverse_iterator rend();
            const_reverse_iterator rend() const;

            iterator erase(iterator pos);

            int getSectionId() const;

            Timeline* getTimeline();
            const Timeline* getTimeline() const;

            /** @brief Sets the `prev_batch` token for a uses and saves it in the local db */
            void setPrevBatchToken(const UserInfo *user, const QString &prevBatchToken);
            std::optional<QString> getPrevBatchToken(const UserInfo *user) const;
            bool hasPrevBatchTokenForUser(const UserInfo *user) const;
            void removePrevBatchToken(const UserInfo *user);

            void requestPastMessages(UserInfo *user, int max = 100, bool allowRecurseOnError = true);
            bool isRequestingPastMessages();

        private:
            void requestPastMessagesFromNetwork(UserInfo *user, int max);

        signals:
            void onBeginInsertRows(const QModelIndex &parent, int first, int last);
            void onEndInsertRows();

            void onBeginRemoveRows(const QModelIndex &parent, int first, int last);
            void onEndRemoveRows();

            // TODO: Remove rows signal

            void beginRequestingPastMessages();
            void finishedRequestingPastMessages();

        private slots:
            void onUserRemoved(UserInfo *user);
    };
}

#endif
