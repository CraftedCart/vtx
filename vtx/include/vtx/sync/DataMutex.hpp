#ifndef VTX_VTX_SYNC_DATAMUTEX_HPP
#define VTX_VTX_SYNC_DATAMUTEX_HPP

#include "vtx_export.h"
#include <QMutex>

namespace Vtx::Sync {
    /**
     * @brief Grants access to data protected by a `DataMutex`. The mutex will be unlocked when the MutexGuard is
     *        destroyed.
     */
    template<typename T>
    class VTX_EXPORT MutexGuard {
        private:
            T *data;
            QMutex *mutex;

        public:
            MutexGuard(T *data, QMutex *mutex);
            ~MutexGuard();

            void set(T &&oj);

            T* operator->();
            T& operator*();
    };

    /**
     * @brief A mutex that protects multiple simultaneous access to a variable
     */
    template<typename T>
    class VTX_EXPORT DataMutex {
        private:
            QMutex mutex;
            T storage;

        public:
            /** @brief Locks the mutex and grants access to the data */
            MutexGuard<T> lock();

    };
}

#include "vtx/sync/DataMutex.ipp"

#endif
