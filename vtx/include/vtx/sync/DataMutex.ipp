#ifndef VTX_VTX_SYNC_DATAMUTEX_IPP
#define VTX_VTX_SYNC_DATAMUTEX_IPP

namespace Vtx::Sync {
    template<typename T>
    MutexGuard<T>::MutexGuard(T *data, QMutex *mutex) : data(data), mutex(mutex) {
        mutex->lock();
    }

    template<typename T>
    MutexGuard<T>::~MutexGuard() {
        mutex->unlock();
    }

    template<typename T>
    void MutexGuard<T>::set(T &&obj) {
        *data = std::move(obj);
    }

    template<typename T>
    T* MutexGuard<T>::operator->() {
        return data;
    }

    template<typename T>
    T& MutexGuard<T>::operator*() {
        return *data;
    }

    template<typename T>
    MutexGuard<T> DataMutex<T>::lock() {
        return MutexGuard<T>(&storage, &mutex);
    }
}

#endif
