#ifndef VTX_VTX_UI_VTXWINDOW_HPP
#define VTX_VTX_UI_VTXWINDOW_HPP

#include "vtx_export.h"
#include "vmx/Client.hpp"
#include <QMainWindow>
#include <optional>

namespace Ui {
    class VTX_EXPORT VtxWindow;
}

namespace Vtx { class UserInfo; }

namespace Vtx::UI {
    class VTX_EXPORT VtxWindow : public QMainWindow {
        Q_OBJECT

        private:
            Ui::VtxWindow *ui;

        public:
            explicit VtxWindow(QWidget *parent = nullptr);
            ~VtxWindow();

        private:
            QtPromise::QPromise<void> fetchAccountData(UserInfo *user);

            void switchToMainView();

        public slots:
            void onLoginSuccess(const Vmx::ClientApi::R0::Session::Login::PostResponse &resp);
    };
}

#endif
