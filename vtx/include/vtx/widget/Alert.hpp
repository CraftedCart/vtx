#ifndef VTX_VTX_WIDGET_ALERT_HPP
#define VTX_VTX_WIDGET_ALERT_HPP

#include "vtx_export.h"
#include <QLabel>

namespace Vtx::Widget {
    class VTX_EXPORT Alert : public QWidget {
        Q_OBJECT

        private:
            QLabel *label;
            QLabel *detailLabel;

        public:
            explicit Alert(QWidget *parent = nullptr);

            virtual void paintEvent(QPaintEvent *event) override;

            void setText(const QString &text, const std::optional<QString> &detailText = {});
    };
}

#endif
