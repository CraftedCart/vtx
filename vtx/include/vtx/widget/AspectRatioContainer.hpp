#ifndef VTX_VTX_WIDGET_ASPECTRATIOPIXMAP_HPP
#define VTX_VTX_WIDGET_ASPECTRATIOPIXMAP_HPP

#include "vtx_export.h"
#include <QWidget>

namespace Vtx::Widget {
    class VTX_EXPORT AspectRatioContainer : public QWidget
    {
        Q_OBJECT

        private:
            QWidget *widget = nullptr;
            QSize desiredSize;

        public:
            explicit AspectRatioContainer(QWidget *parent = nullptr);

            virtual int heightForWidth(int width) const override;
            virtual bool hasHeightForWidth() const override;
            virtual QSize sizeHint() const override;
            virtual QSize minimumSizeHint() const override;

            void setWidget(QWidget *widget);

            void setDesiredSize(const QSize &desiredSize);
            const QSize& getDesiredSize() const;

        protected:
            virtual void resizeEvent(QResizeEvent *event) override;

        private:
            void resizeInnerWidget();
            QSize getInnerWidgetSize(const QSize &maxSize) const;
    };
}

#endif
