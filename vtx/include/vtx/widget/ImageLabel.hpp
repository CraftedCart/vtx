#ifndef VTX_VTX_WIDGET_IMAGELABEL_HPP
#define VTX_VTX_WIDGET_IMAGELABEL_HPP

#include "vtx_export.h"
#include "vmx/Vmx.hpp"
#include <QLabel>

namespace Vtx::Widget {
    /**
     * @brief A label that displays an image, scaled with smooth interpolation (as opposed to Qt's default fast but ugly
     *        interpolation).
     */
    class VTX_EXPORT ImageLabel : public QLabel {
        Q_OBJECT

        private:
            Vmx::Arc<QPixmap> pm;

        public:
            using QLabel::QLabel; // Inherit ctor

            virtual void resizeEvent(QResizeEvent *event) override;

        public slots:
            void setPixmap(Vmx::Arc<QPixmap> pm);
    };
}

#endif
