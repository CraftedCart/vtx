#ifndef VTX_VTX_WIDGET_LOGINWIDGET_HPP
#define VTX_VTX_WIDGET_LOGINWIDGET_HPP

#include "vmx/clientapi/r0/session/Login.hpp"
#include "vtx_export.h"
#include <QWidget>

namespace Ui {
    class VTX_EXPORT LoginWidget;
}

namespace Vtx::Widget {
    class VTX_EXPORT LoginWidget : public QWidget {
        Q_OBJECT

        private:
            Ui::LoginWidget *ui;
            QUrl homeserverUrl = QUrl("https://matrix.org");

        public:
            explicit LoginWidget(QWidget *parent = nullptr);
            ~LoginWidget();

        signals:
            void loginSuccess(const Vmx::ClientApi::R0::Session::Login::PostResponse& resp);

        private slots:
            void changeHomeserver();
            void logIn();

        public slots:
            void showErrorMessage(const QString &friendlyErrorString, const std::optional<QString> &detailErrorString);
    };
}

#endif
