#ifndef VTX_VTX_WIDGET_MAINVIEW_HPP
#define VTX_VTX_WIDGET_MAINVIEW_HPP

#include "vtx/model/RoomListModel.hpp"
#include "vtx/model/TimelineModel.hpp"
#include "vtx_export.h"
#include <QWidget>
#include <QModelIndex>

namespace Ui {
    class VTX_EXPORT MainView;
}

namespace Vtx::Widget {
    class VTX_EXPORT MainView : public QWidget {
        Q_OBJECT

        private:
            Ui::MainView *ui;

            Model::RoomListModel *roomListModel;
            Model::TimelineModel *timelineModel;

        public:
            explicit MainView(QWidget *parent = nullptr);
            ~MainView();

        private slots:
            void send();

            void roomSelectionChanged(const QModelIndex &current, const QModelIndex &previous);
    };
}

#endif
