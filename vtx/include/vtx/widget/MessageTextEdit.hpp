#ifndef VTX_VTX_WIDGET_MESSAGETEXTEDIT_HPP
#define VTX_VTX_WIDGET_MESSAGETEXTEDIT_HPP

#include "vtx_export.h"
#include <QTextEdit>

namespace Vtx::Widget {
    class VTX_EXPORT MessageTextEdit : public QTextEdit {
        Q_OBJECT

        public:
            MessageTextEdit(QWidget *parent = nullptr);

            virtual QSize sizeHint() const override;
            virtual QSize minimumSizeHint() const override;
            virtual bool eventFilter(QObject *watched, QEvent *event) override;

        signals:
            /** @brief Emitted when the user presses return/enter */
            void sendRequested();

        private slots:
            void onTextChanged();
    };
}

#endif
