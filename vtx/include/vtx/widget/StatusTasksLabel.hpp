#ifndef VTX_VTX_WIDGET_STATUSTASKSLABEL_HPP
#define VTX_VTX_WIDGET_STATUSTASKSLABEL_HPP

#include "vtx/TaskInfo.hpp"
#include "vtx/widget/Throbber.hpp"
#include "vtx_export.h"
#include <QLabel>

namespace Vtx::Widget {
    class VTX_EXPORT StatusTasksLabel : public QWidget {
        Q_OBJECT

        private:
            Throbber *throbber;
            QLabel *label;

        public:
            explicit StatusTasksLabel(QWidget *parent = nullptr);

            virtual void resizeEvent(QResizeEvent *event) override;

        private slots:
            void onTaskListChanged(const QVector<TaskInfo*> &tasks);
    };
}

#endif
