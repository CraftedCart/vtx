#ifndef VTX_VTX_WIDGET_THROBBER_HPP
#define VTX_VTX_WIDGET_THROBBER_HPP

#include "vtx_export.h"
#include <QElapsedTimer>
#include <QWidget>

namespace Vtx::Widget {
    /**
     * @brief An animated spinning circle used to indicate that the application is busy performing some task
     */
    class VTX_EXPORT Throbber : public QWidget {
        Q_OBJECT

        private:
            QElapsedTimer elapsedTimer;

        public:
            explicit Throbber(QWidget *parent = nullptr);
            virtual ~Throbber();

            virtual void paintEvent(QPaintEvent *event) override;
    };
}

#endif
