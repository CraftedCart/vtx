#ifndef VTX_VTX_WIDGET_TIMELINEITEM_HPP
#define VTX_VTX_WIDGET_TIMELINEITEM_HPP

#include "vtx/mx/Event.hpp"
#include "vtx/mx/Room.hpp"
#include "vtx/Ref.hpp"
#include "vtx_export.h"
#include <QLabel>
#include <QWidget>

namespace Vtx::Widget {
    class VTX_EXPORT TimelineItem : public QWidget {
        Q_OBJECT

        private:
            enum class EventStyle : quint8 {
                MESSAGE, ///< Regular 'ol messages
                STATUS, ///< Usually seen in state changes, such as a person joining a room, or the room name being changed
                DEBUG, ///< We don't know how to display this message nicely so use a debug-looking format
            };

        private:
            Ref<Mx::Event> event;
            const Mx::Room *room = nullptr;

        public:
            explicit TimelineItem(QWidget *parent = nullptr);

            void setEvent(const Ref<Mx::Event> &event, const Mx::Room *room);

        private:
            void styleAsMessage();
            void styleAsStatus();
            void styleAsDebug();

            void styleMembershipEvent(QLabel *label);

            void setUserNameVisibility(bool visible, QLabel *nameLabel);

            EventStyle getStyleForEvent(const Mx::Event *event);
    };
}

#endif
