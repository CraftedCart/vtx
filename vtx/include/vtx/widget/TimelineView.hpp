#ifndef VTX_VTX_WIDGET_TIMELINEVIEW_HPP
#define VTX_VTX_WIDGET_TIMELINEVIEW_HPP

#include "vtx/widget/TimelineItem.hpp"
#include "vtx/widget/Throbber.hpp"
#include "vtx/model/TimelineModel.hpp"
#include "vtx_export.h"
#include <QScrollArea>
#include <QVBoxLayout>
#include <QPersistentModelIndex>

class TestTimelineView;

namespace Vtx::Widget {
    class VTX_EXPORT TimelineView : public QScrollArea {
        Q_OBJECT

        friend class ::TestTimelineView;

        private:
            QWidget *content;
            QVBoxLayout *messagesLayout;

            /** @brief Shown when we're loading more past messages */
            Throbber *topThrobber;

            Model::TimelineModel *model = nullptr;

            int scrollMax = 0;

            QPersistentModelIndex minLoaded; ///< The model index of the furthest-back message/event widget
            QPersistentModelIndex maxLoaded; ///< The model index of the newest message/event widget

        public:
            explicit TimelineView(QWidget *parent = nullptr);

            void setModel(Model::TimelineModel *model);

        protected:
            virtual void resizeEvent(QResizeEvent *event) override;

        private:
            TimelineItem* widgetFromIndex(const QModelIndex &index);
            void populateMessages();

            void offsetScroll(int offset);
            void scrollToBottom();
            /** @brief Check if we are scrolled at the bottom */
            bool isAtBottom();

            void loadMorePast(bool shouldRecurse = true);

        private slots:
            void onModelReset();
            void onRowsInserted(const QModelIndex &parent, int first, int last);

            void onScroll(int offsetPx);
            void onScrollRangeChanged(int min, int max);

            void onBeginRequestingPastMessages();
            void onFinishedRequestingPastMessages();

            void checkShouldLoadMore_timer();
            void checkShouldLoadMore(bool shouldRecurse);

            void onCurrentSectionIdChanged(int oldId, int newId);
    };
}

#endif
