#ifndef VTX_VTX_WIDGET_USERMENU_HPP
#define VTX_VTX_WIDGET_USERMENU_HPP

#include "vtx/widget/LoginWidget.hpp"
#include "vtx/UserInfo.hpp"
#include "vtx_export.h"
#include <QLabel>
#include <QPushButton>

namespace Vtx::Widget {
    class VTX_EXPORT UserMenu : public QPushButton {
        Q_OBJECT

        private:
            const QPixmap pixmapExpandMore = QPixmap(":/Vtx/Images/expand_more.png");
            QMenu *menu;

            LoginWidget *loginWidget;

        public:
            explicit UserMenu(QWidget *parent = nullptr);

            virtual void paintEvent(QPaintEvent *event) override;

        private slots:
            void buildMenu();

            void addNewUser();
            void onLoginSuccess(const Vmx::ClientApi::R0::Session::Login::PostResponse &resp);

            void onActiveUserChanged(UserInfo *user);
    };
}

#endif
