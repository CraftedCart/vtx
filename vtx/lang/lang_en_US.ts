<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>LoginWidget</name>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.ui" line="55"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sign in to your Matrix account on &lt;span style=&quot;font-weight:600;&quot;&gt;https://matrix.org&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.ui" line="77"/>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.ui" line="105"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.ui" line="147"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.ui" line="192"/>
        <location filename="../src/vtx/widget/LoginWidget.ui" line="353"/>
        <source>Sign in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.ui" line="275"/>
        <source>Vtx</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <location filename="../src/vtx/widget/MainView.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/MainView.ui" line="144"/>
        <source>Send a message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/MainView.ui" line="157"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <location filename="../src/vtx/db/Database.cpp" line="103"/>
        <source>No writable config location found!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/db/Database.cpp" line="110"/>
        <source>Failed to make config dir %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/db/Database.cpp" line="120"/>
        <source>Database init error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/db/Database.cpp" line="402"/>
        <source>Database querying metadata failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/db/Database.cpp" line="403"/>
        <source>Database migration_version not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/db/Database.cpp" line="409"/>
        <source>Local database was last modified using a newer version of vtx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/db/Database.cpp" line="411"/>
        <source>Local database version is less than 1 - This should not happen!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TimelineItem</name>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="55"/>
        <source>$actor reinvited $affected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="56"/>
        <source>$affected accepted the invite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="57"/>
        <source>$affected rejected the invite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="57"/>
        <source>$actor withdrew $affected&apos;s invitation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="58"/>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="63"/>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="69"/>
        <source>$actor banned $affected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="61"/>
        <source>$affected changed their name/avatar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="62"/>
        <source>$affected left the room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="62"/>
        <source>$actor kicked $affected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="66"/>
        <source>$actor invited $affected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="67"/>
        <source>$affected joined the room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="68"/>
        <source>$affected is still not in the room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="72"/>
        <source>$actor unbanned $affected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="73"/>
        <source>$affected is still banned</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Vtx::Mx::Matrix</name>
    <message>
        <location filename="../src/vtx/mx/Matrix.cpp" line="247"/>
        <source>Initial syncing for user %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Vtx::Mx::TimelineSection</name>
    <message>
        <location filename="../src/vtx/mx/TimelineSection.cpp" line="422"/>
        <source>Fetching past messages</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Vtx::UI::VtxWindow</name>
    <message>
        <location filename="../src/vtx/ui/VtxWindow.cpp" line="58"/>
        <source>Fetching account data for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/ui/VtxWindow.cpp" line="83"/>
        <source>Received a bad response from the server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/ui/VtxWindow.cpp" line="85"/>
        <source>Response was in the wrong format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/ui/VtxWindow.cpp" line="86"/>
        <source>Invalid type in response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/ui/VtxWindow.cpp" line="87"/>
        <source>Invalid value in response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/ui/VtxWindow.cpp" line="88"/>
        <source>Missing field in response</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Vtx::VtxInstance</name>
    <message>
        <location filename="../src/vtx/VtxInstance.cpp" line="22"/>
        <source>Failed to open local database</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Vtx::Widget::LoginWidget</name>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.cpp" line="25"/>
        <source>Logging in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.cpp" line="52"/>
        <source>Received a bad response from the server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.cpp" line="54"/>
        <source>Response was in the wrong format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.cpp" line="55"/>
        <source>Invalid type in response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.cpp" line="56"/>
        <source>Invalid value in response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.cpp" line="57"/>
        <source>Missing field in response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.cpp" line="73"/>
        <source>Network error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.cpp" line="86"/>
        <source>Change homeserver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.cpp" line="87"/>
        <source>Homeserver URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/LoginWidget.cpp" line="97"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sign in to your Matrix account on &lt;span style=&quot;font-weight:600;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Vtx::Widget::MainView</name>
    <message>
        <location filename="../src/vtx/widget/MainView.cpp" line="63"/>
        <source>Sending message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Vtx::Widget::StatusTasksLabel</name>
    <message>
        <location filename="../src/vtx/widget/StatusTasksLabel.cpp" line="40"/>
        <source>%1 tasks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Vtx::Widget::TimelineItem</name>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="206"/>
        <source>Vtx error: Cannot style event as a status: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="233"/>
        <source>Reason: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="238"/>
        <location filename="../src/vtx/widget/TimelineItem.cpp" line="241"/>
        <source>User %1&apos;s membership went from %2 -&gt; %3</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Vtx::Widget::UserMenu</name>
    <message>
        <location filename="../src/vtx/widget/UserMenu.cpp" line="125"/>
        <source>Add new user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/vtx/widget/UserMenu.cpp" line="146"/>
        <source>You are already logged in as this account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VtxWindow</name>
    <message>
        <location filename="../src/vtx/ui/VtxWindow.ui" line="14"/>
        <source>Vtx</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
