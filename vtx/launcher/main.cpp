#include "vtx_export.h"

namespace Vtx {
    VTX_EXPORT int vtxLaunch(int argc, char *argv[]);
}

int main(int argc, char *argv[]) {
    return Vtx::vtxLaunch(argc, argv);
}

