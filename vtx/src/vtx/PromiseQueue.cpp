#include "vtx/PromiseQueue.hpp"
#include "vmx/util/Adt.hpp"
#include <QTimer>

namespace Vtx::Mx {
    // Define statics
    const int PromiseQueue::DEFAULT_BACKOFF_MS = 1000;
    const int PromiseQueue::MAX_BACKOFF_MS = 30000;
    const int PromiseQueue::MAX_RETRIES = 20;

    void PromiseQueue::runNextOp() {
        if (queued[0].taskInfo) {
            (*queued[0].taskInfo)->finishAndDelete();
        }

        queued.removeFirst();
        if (queued.length() > 0) {
            const QueuedTask &task = queued[0];
            if (task.taskInfo) {
                (*task.taskInfo)->start();
            }

            task.exec();
        }
    }

    void PromiseQueue::resetBackoff() {
        lastBackoffMs = DEFAULT_BACKOFF_MS;
        retryCount = 0;
    }

    void PromiseQueue::retryOp() {
        Q_ASSERT(queued.length() > 0 && "retryOp called by there's no operations queued up");

        if (queued.length() > 0) {
            queued[0].exec();
        }
    }

    void PromiseQueue::onOperationSuccess() {
        resetBackoff();
        runNextOp();
    }

    void PromiseQueue::onOperationFail(const Vmx::ClientApi::ApiError &err) {
        Vmx::Util::Adt::matchVariant(err.errorCode) (
            [this, &err](const Vmx::ClientApi::MxApiError &e) {
                // Check if we're rate limited and retry after an error
                if (e == Vmx::ClientApi::MxApiError::M_LIMIT_EXCEEDED) {
                    int retryAfterMs = 10000;

                    QJsonValue val = err.additionalProperties.value(QLatin1String("retry_after_ms"));
                    if (val.isDouble()) {
                        retryAfterMs = val.toInt();
                    } else {
                        qWarning() << "Got M_LIMIT_EXCEEDED but retry_after_ms does not exist/is not a number - defaulting to 10000 ms";
                    }

                    qWarning() << "Rate limited (M_LIMIT_EXCEEDED) - retrying after" << retryAfterMs << "ms";
                    QTimer::singleShot(retryAfterMs, this, &PromiseQueue::retryOp);
                } else {
                    qCritical() << "Matrix API error" << Vmx::ClientApi::errorCodeToString(e) << err.message << "- dropping request";
                    resetBackoff();
                    queued[0].rejectApiError(err);

                    runNextOp();
                }
            },
            [this, &err](const Vmx::ClientApi::OtherApiError &e) {
                qCritical() << "Other API error" << e.errorCode << err.message << "- dropping request";
                resetBackoff();
                queued[0].rejectApiError(err);

                runNextOp();
            },
            [this, &err](const Vmx::ClientApi::BadResponseError&) {
                if (err.httpStatus == Vmx::Http::HTTP_TOO_MANY_REQUESTS) {
                    if (retryCount >= MAX_RETRIES) {
                        qCritical() << "Bad response error (Got TOO_MANY_REQUESTS) - giving up after" << MAX_RETRIES << "attempts";
                        resetBackoff();
                        queued[0].rejectApiError(err);

                        runNextOp();
                        return;
                    }

                    qCritical() << "Bad response error (Got TOO_MANY_REQUESTS) - retrying after" << lastBackoffMs << "ms";
                    QTimer::singleShot(lastBackoffMs, this, &PromiseQueue::retryOp);

                    lastBackoffMs = qMin(lastBackoffMs * 2, MAX_BACKOFF_MS);
                    retryCount++;
                } else {
                    qCritical() << "Bad response error - dropping request";
                    resetBackoff();
                    queued[0].rejectApiError(err);

                    runNextOp();
                }
            }
        );
    }

    void PromiseQueue::onOperationFail(const Vmx::NetError &err) {
        if (retryCount >= MAX_RETRIES) {
            qCritical() << "Network error" << err.errorString << "- giving up after" << MAX_RETRIES << "attempts";
            resetBackoff();
            queued[0].rejectNetError(err);

            runNextOp();
            return;
        }

        qCritical() << "Network error" << err.errorString << "- retrying after" << lastBackoffMs << "ms";
        QTimer::singleShot(lastBackoffMs, this, &PromiseQueue::retryOp);

        lastBackoffMs = qMin(lastBackoffMs * 2, MAX_BACKOFF_MS);
        retryCount++;
    }

    void PromiseQueue::onOperationFail() {
        qCritical() << "Unknown error - dropping request";
        queued[0].rejectUnknownError();

        runNextOp();
    }
}
