#include "vtx/TaskInfo.hpp"
#include "vtx/VtxInstance.hpp"

namespace Vtx {
    TaskInfo::TaskInfo(const QString &name) : name(name) {}

    void TaskInfo::setName(const QString &name) {
        this->name = name;
    }

    const QString& TaskInfo::getName() const {
        return name;
    }

    void TaskInfo::start() {
        VtxInstance::getInstance()->addTask(this);
    }

    void TaskInfo::finishAndDelete() {
        VtxInstance::getInstance()->removeTask(this);
        deleteLater();
    }
}
