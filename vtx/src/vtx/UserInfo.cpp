#include "vtx/UserInfo.hpp"

namespace Vtx {
    void UserInfo::setSession(const std::optional<Vmx::Session> &session) {
        this->session = session;
    }

    const std::optional<Vmx::Session>& UserInfo::getSession() const {
        return session;
    }

    void UserInfo::setSyncNextBatchToken(const std::optional<QString> &syncNextBatchToken) {
        this->syncNextBatchToken = syncNextBatchToken;
    }

    const std::optional<QString>& UserInfo::getSyncNextBatchToken() const {
        return syncNextBatchToken;
    }

    void UserInfo::setUserId(const Vmx::Id::UserId &userId) {
        this->userId = userId;
    }

    const Vmx::Id::UserId& UserInfo::getUserId() const {
        return userId;
    }
}
