#include "vtx/VtxInstance.hpp"
#include <QMessageBox>

namespace Vtx {
    //Define statics
    VtxInstance *VtxInstance::instance = nullptr;

    bool VtxInstance::createInstance(int &argc, char *argv[], bool testMode) {
        instance = new VtxInstance();

        instance->app = new QApplication(argc, argv);
        instance->matrix = new Mx::Matrix(instance);

        QString dbInitError = QString(); // Null QString means ok
        instance->database.init(testMode)
            .fail([&dbInitError](const QString &err) { dbInitError = err; })
            .fail([&dbInitError]() { dbInitError = QLatin1String("Unknown database init error"); })
            .wait();

        if (!dbInitError.isNull()) {
            QMessageBox msgBox;
            msgBox.setText(tr("Failed to open local database"));
            msgBox.setInformativeText(dbInitError);
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.exec();

            return false;
        }

        // Save config and users on quit
        if (!testMode) {
            instance->connect(instance->app, &QApplication::aboutToQuit, []() {
                instance->getAppConfig().writeSync();
                instance->getDatabase().saveUsers();
            });
        } else {
            instance->matrix->stopAutoPurge();
        }

        instance->setupConnections();

        return true;
    }

    void VtxInstance::setupConnections() {
        connect(instance->matrix, &Mx::Matrix::userAdded, instance, &VtxInstance::onUserAdded);
    }

    VtxInstance* VtxInstance::getInstance() {
        return instance;
    }

    bool VtxInstance::resetInstance(bool testMode) {
        instance->activeUser = nullptr;
        // appConfig is not reset...

        delete instance->matrix;
        instance->matrix = new Mx::Matrix(instance);
        instance->matrix->stopAutoPurge();

        instance->database.deinit();

        bool dbInitError = false;
        instance->database.init(testMode)
            .fail([&dbInitError]() { dbInitError = true; })
            .wait();
        if (dbInitError) return false;

        instance->setupConnections();

        return true;
    }

    void VtxInstance::destroyInstance() {
        delete instance;
        instance = nullptr;
    }

    VtxInstance::~VtxInstance() {
        // Delete this first to flush any pending events, which may depend on other stuff we're about to delete
        delete app;

        qDeleteAll(activeTasks);
        delete matrix;
        database.deinit();
    }

    QApplication* VtxInstance::getApp() {
        return app;
    }

    int VtxInstance::execApp() {
        int ret = app->exec();

        return ret;
    }

    Config::AppConfig& VtxInstance::getAppConfig() {
        return appConfig;
    }

    Db::Database& VtxInstance::getDatabase() {
        return database;
    }

    void VtxInstance::addTask(TaskInfo *task) {
        activeTasks.append(task);
        qDebug() << "Started task" << task->getName();

        emit taskListChanged(activeTasks);
    }

    void VtxInstance::removeTask(TaskInfo *task) {
        activeTasks.removeOne(task);
        qDebug() << "Removing task" << task->getName();

        emit taskListChanged(activeTasks);
    }

    UserInfo* VtxInstance::getActiveUser() {
        Q_ASSERT(activeUser != nullptr);
        return activeUser;
    }

    void VtxInstance::setActiveUser(UserInfo *user) {
        activeUser = user;
        emit activeUserChanged(user);
    }

    Mx::Matrix* VtxInstance::getMatrix() {
        return matrix;
    }

    Vmx::Client* VtxInstance::getMxClient() {
        return matrix->getClient();
    }

    void VtxInstance::onUserAdded(UserInfo *user) {
        qDebug() << "YYAAAYY";
        // If no user is active (which should only be the case if no-one is logged in), make the newly added user the
        // active one
        if (activeUser == nullptr) {
            activeUser = user;
            emit activeUserChanged(user);
        }
    }
}
