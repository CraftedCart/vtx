#include "vtx/config/AppConfig.hpp"
#include "vmx/Serde.hpp"
#include <QJsonDocument>
#include <QThreadPool>
#include <QRunnable>
#include <QStandardPaths>
#include <QDir>
#include <QDebug>

using namespace Vmx;
using namespace Vmx::Util;

namespace Vtx::Config {
    void AppConfig::writeAsync() {
        QThreadPool::globalInstance()->start(QRunnable::create([this]() {
            writeSync();
        }));
    }

    void AppConfig::writeSync() {
        QString configPathStr = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
        if (configPathStr.isEmpty()) {
            qCritical() << "No writable config location found!";
            return;
        }

        QDir configPath = configPathStr;
        if (!configPath.mkpath(QLatin1String("."))) {
            qCritical() << "Failed to make config dir" << configPath.path();
            return;
        }

        // Write private config
        {
            auto privateGuard = privateConfig.lock();

            privateGuard->preWrite();

            QJsonObject privateObj = Serde::serialize(*privateGuard);
            QJsonDocument doc(privateObj);

            QString path = configPath.filePath("vtx_private.json");
            QFile file(path);
            if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
                qCritical() << "Failed to open" << path << "for writing";
            } else {
                if (file.write(doc.toJson(QJsonDocument::Compact)) == -1) {
                    qCritical() << "Failed to write to" << path;
                }
                file.close();
            }

            privateGuard->postWrite();
        }

        // Write public config
        {
            auto publicGuard = publicConfig.lock();

            publicGuard->preWrite();

            QJsonObject publicObj = Serde::serialize(*publicGuard);
            QJsonDocument doc(publicObj);

            QString path = configPath.filePath("vtx_public.json");
            QFile file(path);
            if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
                qCritical() << "Failed to open" << path << "for writing";
            } else {
                if (file.write(doc.toJson(QJsonDocument::Compact)) == -1) {
                    qCritical() << "Failed to write to" << path;
                }
                file.close();
            }

            publicGuard->postWrite();
        }
    }

    void AppConfig::readSync() {
        QString configPathStr = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
        if (configPathStr.isEmpty()) {
            qCritical() << "No writable config location found!";
            return;
        }

        QDir configPath = configPathStr;
        if (!configPath.mkpath(QLatin1String("."))) {
            return;
        }

        // Read private config
        {
            auto privateGuard = privateConfig.lock();

            QString path = configPath.filePath("vtx_private.json");
            QFile file(path);
            if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
                qCritical() << "Failed to open" << path << "for reading";
            } else {
                QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
                file.close();

                if (!doc.isObject()) {
                    qCritical() << path << "does not contain a JSON object";
                } else {
                    Result<PrivateConfig, Serde::DeserializeError> res = Serde::deserialize<PrivateConfig>(doc.object());
                    if (res.isErr()) {
                        qCritical() << "Failed to deserialize" << path;
                    } else {
                        privateGuard.set(res.unwrap());
                    }
                }
            }

            privateGuard->postRead();
        }

        // Read public config
        {
            auto publicGuard = publicConfig.lock();

            QString path = configPath.filePath("vtx_public.json");
            QFile file(path);
            if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
                qCritical() << "Failed to open" << path << "for reading";
            } else {
                QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
                file.close();

                if (!doc.isObject()) {
                    qCritical() << path << "does not contain a JSON object";
                } else {
                    Result<PublicConfig, Serde::DeserializeError> res = Serde::deserialize<PublicConfig>(doc.object());
                    if (res.isErr()) {
                        qCritical() << "Failed to deserialize" << path;
                    } else {
                        publicGuard.set(res.unwrap());
                    }
                }
            }

            publicGuard->postRead();
        }
    }
}
