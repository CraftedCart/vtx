#include "vtx/config/PrivateConfig.hpp"
#include "vtx/UserInfo.hpp"
#include "vtx/VtxInstance.hpp"

namespace Vtx::Config {
    void PrivateConfig::preWrite() {}

    void PrivateConfig::postWrite() {}

    void PrivateConfig::postRead() {}
}
