#include "vtx/db/Database.hpp"
#include "vtx/mx/Event.hpp"
#include "vtx/mx/Room.hpp"
#include "vtx/VtxInstance.hpp"
#include <QDir>
#include <QStandardPaths>
#include <QSqlError>
#include <QJsonDocument>
#include <QRunnable>
#include <QCoreApplication>
#include <QDebug>

using namespace Vmx::Util;

static const int CURRENT_DB_MIGRATION_VERSION = 1;
static const QString CREATE_TABLE_VTX_META = QLatin1String(
        "CREATE TABLE IF NOT EXISTS vtx_meta ("
            "key TEXT PRIMARY KEY NOT NULL,"
            "value BLOB"
        ")"
        );
static const QString CREATE_TABLE_EVENTS = QLatin1String(
        "CREATE TABLE IF NOT EXISTS events ("
            "room_id TEXT NOT NULL,"
            "event_id TEXT NOT NULL,"
            "event_json TEXT NOT NULL,"
            "loc_section INT,"
            "loc_order INT,"
            "PRIMARY KEY (room_id, event_id)"
        ")"
        );
static const QString CREATE_TABLE_ROOM_CURRENT_STATE_EVENTS = QLatin1String(
        "CREATE TABLE IF NOT EXISTS room_current_state_events ("
            "room_id TEXT NOT NULL,"
            "event_type TEXT NOT NULL,"
            "event_state_key TEXT NOT NULL,"
            "event_id TEXT NOT NULL,"
            "PRIMARY KEY (room_id, event_type, event_state_key)"
        ")"
        );
static const QString CREATE_TABLE_TIMELINE_SECTION_PREV_BATCH_TOKENS = QLatin1String(
        "CREATE TABLE IF NOT EXISTS timeline_section_prev_batch_tokens ("
            "room_id TEXT NOT NULL,"
            "section_id INT NOT NULL,"
            "user_id TEXT NOT NULL,"
            "token_prev_batch TEXT,"
            "PRIMARY KEY (room_id, section_id, user_id)"
        ")"
        );
static const QString CREATE_TABLE_USERS = QLatin1String(
        "CREATE TABLE IF NOT EXISTS users ("
            "user_id TEXT NOT NULL,"
            "access_token TEXT,"
            "homeserver_url TEXT NOT NULL,"
            "sync_next_batch_token TEXT,"
            "PRIMARY KEY (user_id)"
        ")"
        );
static const QString CREATE_TABLE_USER_ROOMS = QLatin1String(
        "CREATE TABLE IF NOT EXISTS user_rooms ("
            "room_id TEXT NOT NULL,"
            "user_id TEXT NOT NULL,"
            "membership TEXT NOT NULL," // One of: ["invite", "join", "knock", "leave", "ban"]
            "PRIMARY KEY (room_id, user_id)"
        ")"
        );

#define ASYNC_DB_TASK(ReturnT, ...) \
        QtPromise::QPromise<ReturnT>{[=]( \
                const QtPromise::QPromiseResolve<ReturnT>& resolve, \
                const QtPromise::QPromiseReject<ReturnT>& reject \
                ) { \
            auto task = [=]() { \
                Q_ASSERT(QThread::currentThread() == dbThread); \
                \
                __VA_ARGS__ \
            }; \
            \
            if (QThread::currentThread() == dbThread) { \
                task(); \
            } else { \
                dbPool.start(task); \
            } \
        }}

namespace Vtx::Db {
    QtPromise::QPromise<void> Database::init(bool useMemoryDb) {
        dbPool.setExpiryTimeout(-1); // Never expire a thread
        dbPool.setMaxThreadCount(1);

        dbPool.start([this]() {
            dbThread = QThread::currentThread();
        });

        return ASYNC_DB_TASK(void,
            if (useMemoryDb) {
                db = QSqlDatabase::addDatabase(QLatin1String("QSQLITE"));
                db.setDatabaseName(QLatin1String(":memory:"));
            } else {
                QString configPathStr = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
                if (configPathStr.isEmpty()) {
                    qCritical() << "No writable config location found!";
                    reject(QCoreApplication::tr("No writable config location found!"));
                    return;
                }

                QDir configPath = configPathStr;
                if (!configPath.mkpath(QLatin1String("."))) {
                    qCritical() << "Failed to make config dir" << configPath.path();
                    reject(QCoreApplication::tr("Failed to make config dir %1").arg(configPath.path()));
                    return;
                }

                db = QSqlDatabase::addDatabase(QLatin1String("QSQLITE"));
                db.setDatabaseName(configPath.filePath(QLatin1String("vtx.sqlite")));
            }

            if (!db.open()) {
                qCritical() << "Database init error:" << db.lastError();
                reject(QCoreApplication::tr("Database init error: %1").arg(db.lastError().text()));
                return;
            }

            initTables();
            initPreparedStatements();
            Result<void, QString> migrationRes = checkMigrations();
            if (migrationRes.isErr()) reject(migrationRes);

            resolve();
        );
    }

    void Database::deinit() {
        ASYNC_DB_TASK(void,
            Q_UNUSED(reject); // This can't fail

            if (transactionDepth > 0) {
                qCritical() << "Database is about to close yet there are ongoing transactions!";
            } else {
                // Only attempt optimizations if we know no transaction is happening
                qDebug() << "Asking to optimize the local db";
                db.exec(QLatin1String("PRAGMA optimize"));
                if (db.lastError().isValid()) {
                    qCritical() << "Local database error" << db.lastError();
                }
            }

            qDebug() << "Closing local db connection";
            QString dbName = db.connectionName();
            db.close();

            db = QSqlDatabase(); // Need to remove the current QSqlDatabase before calling removeDatabase
            QSqlDatabase::removeDatabase(dbName);

            resolve();
        ).wait();
    }

    void Database::initTables() {
        Q_ASSERT(QThread::currentThread() == dbThread);

        initMetaTable();

        QSqlQuery query;
        query.exec(CREATE_TABLE_EVENTS);
        query.exec(CREATE_TABLE_ROOM_CURRENT_STATE_EVENTS);
        query.exec(CREATE_TABLE_TIMELINE_SECTION_PREV_BATCH_TOKENS);
        query.exec(CREATE_TABLE_USERS);
        query.exec(CREATE_TABLE_USER_ROOMS);
    }

    void Database::initMetaTable() {
        Q_ASSERT(QThread::currentThread() == dbThread);

        QSqlQuery query;
        query.exec(CREATE_TABLE_VTX_META);

        query.prepare(QLatin1String("INSERT OR ABORT INTO vtx_meta (key, value) VALUES (\"migration_version\", ?)"));
        query.addBindValue(CURRENT_DB_MIGRATION_VERSION);
        query.exec();
    }

    void Database::initPreparedStatements() {
        Q_ASSERT(QThread::currentThread() == dbThread);

        QSqlQuery queryInsertEvent(db);
        queryInsertEvent.prepare(QLatin1String(
                    "INSERT OR REPLACE INTO "
                    "    events (room_id, event_id, event_json, loc_section, loc_order) "
                    "VALUES "
                    "    (?, ?, ?, ?, ?)"
                    ));
        this->queryInsertEvent = queryInsertEvent;

        QSqlQuery queryInsertCurrentStateEvent(db);
        queryInsertCurrentStateEvent.prepare(QLatin1String(
                    "INSERT OR REPLACE INTO "
                    "    room_current_state_events (room_id, event_type, event_state_key, event_id) "
                    "VALUES "
                    "    (?, ?, ?, ?)"
                    ));
        this->queryInsertCurrentStateEvent = queryInsertCurrentStateEvent;

        QSqlQuery queryGetNewestEvents(db);
        queryGetNewestEvents.prepare(QLatin1String(
                    "SELECT "
                    "    event_id, event_json, loc_section, loc_order "
                    "FROM "
                    "    events "
                    "WHERE "
                    "    room_id = ? AND "
                    "    loc_section = ? "
                    "ORDER BY "
                    "    loc_order DESC "
                    "LIMIT "
                    "    ?"
                    ));
        this->queryGetNewestEvents = queryGetNewestEvents;

        QSqlQuery queryGetEventsBefore(db);
        queryGetEventsBefore.prepare(QLatin1String(
                    "SELECT "
                    "    event_id, event_json, loc_section, loc_order "
                    "FROM "
                    "    events "
                    "WHERE "
                    "    room_id = ? AND "
                    "    loc_section = ? AND "
                    "    loc_order IS NOT NULL AND "
                    "    loc_order < ? "
                    "ORDER BY "
                    "    loc_section DESC, "
                    "    loc_order DESC "
                    "LIMIT "
                    "    ?"
                    ));
        this->queryGetEventsBefore = queryGetEventsBefore;

        QSqlQuery queryGetTimelineSectionOrderRange(db);
        queryGetTimelineSectionOrderRange.prepare(QLatin1String(
                    "SELECT "
                    "    MIN(loc_order), MAX(loc_order) "
                    "FROM "
                    "    events "
                    "WHERE "
                    "    room_id = ? AND "
                    "    loc_section = ? AND "
                    "    loc_order IS NOT NULL"
                    ));
        this->queryGetTimelineSectionOrderRange = queryGetTimelineSectionOrderRange;

        QSqlQuery queryDeleteFromTimelineSectionBeforeOrder(db);
        queryDeleteFromTimelineSectionBeforeOrder.prepare(QLatin1String(
                    "DELETE FROM "
                    "    events "
                    "WHERE "
                    "    room_id = ? AND "
                    "    loc_section = ? AND "
                    "    loc_order IS NOT NULL AND "
                    "    loc_order < ?"
                    ));
        this->queryDeleteFromTimelineSectionBeforeOrder = queryDeleteFromTimelineSectionBeforeOrder;

        QSqlQuery queryGetCurrentStateEvent(db);
        queryGetCurrentStateEvent.prepare(QLatin1String(
                    "SELECT "
                    "    l.event_id, l.event_json, l.loc_section, l.loc_order "
                    "FROM "
                    "    events l "
                    "INNER JOIN room_current_state_events r ON "
                    "    r.room_id = l.room_id AND "
                    "    r.event_id = l.event_id "
                    "WHERE "
                    "    l.room_id = ? AND "
                    "    r.event_type = ? AND "
                    "    r.event_state_key = ?"
                    ));
        this->queryGetCurrentStateEvent = queryGetCurrentStateEvent;

        QSqlQuery queryInsertPrevBatchToken(db);
        queryInsertPrevBatchToken.prepare(QLatin1String(
                    "INSERT OR REPLACE INTO "
                    "    timeline_section_prev_batch_tokens (room_id, section_id, user_id, token_prev_batch) "
                    "VALUES "
                    "    (?, ?, ?, ?)"
                    ));
        this->queryInsertPrevBatchToken = queryInsertPrevBatchToken;

        QSqlQuery queryDeletePrevBatchToken(db);
        queryDeletePrevBatchToken.prepare(QLatin1String(
                    "DELETE FROM "
                    "    timeline_section_prev_batch_tokens "
                    "WHERE "
                    "    room_id = ? AND "
                    "    section_id = ? AND "
                    "    user_id = ?"
                    ));
        this->queryDeletePrevBatchToken = queryDeletePrevBatchToken;

        QSqlQuery queryGetPrevBatchToken(db);
        queryGetPrevBatchToken.prepare(QLatin1String(
                    "SELECT "
                    "    token_prev_batch "
                    "FROM "
                    "    timeline_section_prev_batch_tokens "
                    "WHERE "
                    "    room_id = ? AND "
                    "    section_id = ? AND "
                    "    user_id = ?"
                    ));
        this->queryGetPrevBatchToken = queryGetPrevBatchToken;

        QSqlQuery queryInsertUser(db);
        queryInsertUser.prepare(QLatin1String(
                    "INSERT OR REPLACE INTO "
                    "    users (user_id, access_token, homeserver_url, sync_next_batch_token) "
                    "VALUES "
                    "    (?, ?, ?, ?)"
                    ));
        this->queryInsertUser = queryInsertUser;

        QSqlQuery queryGetAllUsers(db);
        queryGetAllUsers.prepare(QLatin1String(
                    "SELECT "
                    "    user_id, access_token, homeserver_url, sync_next_batch_token "
                    "FROM "
                    "    users"
                    ));
        this->queryGetAllUsers = queryGetAllUsers;

        QSqlQuery queryInsertUserRoom(db);
        queryInsertUserRoom.prepare(QLatin1String(
                    "INSERT OR REPLACE INTO "
                    "    user_rooms (room_id, user_id, membership) "
                    "VALUES "
                    "    (?, ?, ?)"
                    ));
        this->queryInsertUserRoom = queryInsertUserRoom;

        QSqlQuery queryGetAllRoomMemberships(db);
        queryGetAllRoomMemberships.prepare(QLatin1String(
                    "SELECT "
                    "    room_id, user_id, membership "
                    "FROM "
                    "    user_rooms"
                    ));
        this->queryGetAllRoomMemberships = queryGetAllRoomMemberships;

        QSqlQuery queryGetTimelineMaxSectionId(db);
        queryGetTimelineMaxSectionId.prepare(QLatin1String(
                    "SELECT "
                    "    MAX(loc_section) "
                    "FROM "
                    "    events "
                    "WHERE "
                    "    room_id = ? AND "
                    "    loc_section IS NOT NULL"
                    ));
        this->queryGetTimelineMaxSectionId = queryGetTimelineMaxSectionId;

        QSqlQuery queryGetEventLocForId(db);
        queryGetEventLocForId.prepare(QLatin1String(
                    "SELECT "
                    "    loc_section, loc_order "
                    "FROM "
                    "    events "
                    "WHERE "
                    "    room_id = ? AND "
                    "    event_id = ?"
                    ));
        this->queryGetEventLocForId = queryGetEventLocForId;

        QSqlQuery queryShiftEventLocUp(db);
        queryShiftEventLocUp.prepare(QLatin1String(
                    "UPDATE "
                    "    events "
                    "SET "
                    "    loc_order = loc_order + 1 "
                    "WHERE "
                    "    room_id = ? AND "
                    "    loc_section = ?"
                    ));
        this->queryShiftEventLocUp = queryShiftEventLocUp;

        QSqlQuery queryMakeSavepoint(db);
        queryMakeSavepoint.prepare(QLatin1String("SAVEPOINT ?"));
        this->queryMakeSavepoint = queryMakeSavepoint;

        QSqlQuery queryReleaseSavepoint(db);
        queryReleaseSavepoint.prepare(QLatin1String("RELEASE ?"));
        this->queryReleaseSavepoint = queryReleaseSavepoint;

        QSqlQuery queryRollbackSavepoint(db);
        queryRollbackSavepoint.prepare(QLatin1String("ROLLBACK TO ?"));
        this->queryRollbackSavepoint = queryRollbackSavepoint;
    }

    Result<void, QString> Database::checkMigrations() {
        Q_ASSERT(QThread::currentThread() == dbThread);

        QSqlQuery query;
        if (!query.exec(QLatin1String("SELECT value FROM vtx_meta WHERE key = \"migration_version\""))) return Err(QCoreApplication::tr("Database querying metadata failed"));
        if (!query.next()) return Err(QCoreApplication::tr("Database migration_version not found"));

        int version = query.value(0).toInt();
        qDebug() << "Local database version:" << version;

        if (version > CURRENT_DB_MIGRATION_VERSION) {
            return Err(QCoreApplication::tr("Local database was last modified using a newer version of vtx"));
        } else if (version < 1) {
            return Err(QCoreApplication::tr("Local database version is less than 1 - This should not happen!"));
        }

        // TODO: Implement migrations when we have more than 1 db version

        return Ok();
    }

    QtPromise::QPromise<void> Database::insertEvent(const Mx::Event *event, const Mx::Room *room) {
        Vmx::InString roomId = room->getRoomId().fullId;
        Vmx::InString eventId = event->getEventId().fullId;
        std::optional<Mx::EventLoc> eventLoc = event->getEventLoc();
        QJsonObject json = event->getJson();

        return ASYNC_DB_TASK(void,
            queryInsertEvent.addBindValue(roomId.toQString());
            queryInsertEvent.addBindValue(eventId.toQString());

            QJsonDocument doc(json);
            queryInsertEvent.addBindValue(doc.toJson(QJsonDocument::Compact));

            if (eventLoc) {
                queryInsertEvent.addBindValue(eventLoc->section);
                queryInsertEvent.addBindValue(eventLoc->order);
            } else {
                queryInsertEvent.addBindValue(QVariant());
                queryInsertEvent.addBindValue(QVariant());
            }

            if (!queryInsertEvent.exec()) {
                qCritical() << "Local database error" << queryInsertEvent.lastError();
                reject(queryInsertEvent.lastError());
            }

            resolve();
        );
    }

    QtPromise::QPromise<void> Database::insertEventInMiddle(const Mx::Event *event, const Mx::Room *room) {
        Q_ASSERT(event->getEventLoc().has_value());

        Vmx::InString roomId = room->getRoomId().fullId;
        Vmx::InString eventId = event->getEventId().fullId;
        std::optional<Mx::EventLoc> eventLoc = event->getEventLoc();
        int section = event->getEventLoc()->section;
        QJsonObject json = event->getJson();

        return ASYNC_DB_TASK(void,
            static const QString SAVEPOINT_NAME = QLatin1String("insertEventInMiddle");

            startTransaction();

            // We do 2 SQL queries here - if one fails, we want to rollback
            if (!makeSavepoint(SAVEPOINT_NAME)) {
                commitTransaction();
                reject(queryInsertEvent.lastError());
                return;
            }

            QString roomIdStr = roomId.toQString();

            // First, shift everything up //
            queryShiftEventLocUp.addBindValue(roomIdStr);
            queryShiftEventLocUp.addBindValue(section);

            if (!queryShiftEventLocUp.exec()) {
                qCritical() << "Local database error" << queryShiftEventLocUp.lastError();
                rollbackSavepoint(SAVEPOINT_NAME);
                commitTransaction();
                reject(queryInsertEvent.lastError());
                return;
            }

            // Then insert the event //
            queryInsertEvent.addBindValue(roomIdStr);
            queryInsertEvent.addBindValue(eventId.toQString());

            QJsonDocument doc(json);
            queryInsertEvent.addBindValue(doc.toJson(QJsonDocument::Compact));

            if (eventLoc) {
                queryInsertEvent.addBindValue(eventLoc->section);
                queryInsertEvent.addBindValue(eventLoc->order);
            } else {
                queryInsertEvent.addBindValue(QVariant());
                queryInsertEvent.addBindValue(QVariant());
            }

            if (!queryInsertEvent.exec()) {
                qCritical() << "Local database error" << queryInsertEvent.lastError();
                rollbackSavepoint(SAVEPOINT_NAME);
                commitTransaction();
                reject(queryInsertEvent.lastError());
                return;
            }

            releaseSavepoint(SAVEPOINT_NAME);
            commitTransaction();

            resolve();
        );
    }

    QtPromise::QPromise<void> Database::insertCurrentStateEvent(const Mx::Event *event, const Mx::Room *room) {
        Q_ASSERT(event->isStateEvent());

        Vmx::InString roomId = room->getRoomId().fullId;
        Vmx::InString eventId = event->getEventId().fullId;
        Vmx::InString eventType = event->getType();
        QString stateKey = *event->getStateKey();

        return ASYNC_DB_TASK(void,
            queryInsertCurrentStateEvent.addBindValue(roomId.toQString());
            queryInsertCurrentStateEvent.addBindValue(eventType.toQString());
            queryInsertCurrentStateEvent.addBindValue(stateKey);
            queryInsertCurrentStateEvent.addBindValue(eventId.toQString());

            if (!queryInsertCurrentStateEvent.exec()) {
                qCritical() << "Local database error" << queryInsertCurrentStateEvent.lastError();
                reject(queryInsertCurrentStateEvent.lastError());
                return;
            }

            resolve();
        );
    }

    QtPromise::QPromise<QVector<Ref<Mx::Event>>> Database::getNewestEvents(const Mx::Room *room, int num, int sectionId) {
        Vmx::InString roomId = room->getRoomId().fullId;

        return ASYNC_DB_TASK(QVector<Ref<Mx::Event>>,
            queryGetNewestEvents.addBindValue(roomId.toQString());
            queryGetNewestEvents.addBindValue(sectionId);
            queryGetNewestEvents.addBindValue(num);

            QVector<Ref<Mx::Event>> results;

            if (!queryGetNewestEvents.exec()) {
                qCritical() << "Local database error" << queryGetNewestEvents.lastError();
                reject(queryGetNewestEvents.lastError());
                return;
            }

            results.reserve(num);

            while (queryGetNewestEvents.next()) {
                QString eventIdStr = queryGetNewestEvents.value(0).toString();
                QByteArray eventJsonStr = queryGetNewestEvents.value(1).toByteArray();
                int locSection = queryGetNewestEvents.value(2).toInt();
                int locOrder = queryGetNewestEvents.value(3).toInt();

                QJsonDocument doc = QJsonDocument::fromJson(eventJsonStr);

                Ref<Mx::Event> event = Ref<Mx::Event>::create(eventIdStr, doc.object(), Mx::EventLoc{locSection, locOrder});
                results.append(event);
            }

            resolve(results);
        );
    }

    QtPromise::QPromise<QVector<Ref<Mx::Event>>> Database::getEventsBefore(const Mx::Room *room, int num, const Mx::EventLoc &loc) {
        Vmx::InString roomId = room->getRoomId().fullId;

        return ASYNC_DB_TASK(QVector<Ref<Mx::Event>>,
            queryGetEventsBefore.addBindValue(roomId.toQString());
            queryGetEventsBefore.addBindValue(loc.section);
            queryGetEventsBefore.addBindValue(loc.order);
            queryGetEventsBefore.addBindValue(num);

            QVector<Ref<Mx::Event>> results;

            if (!queryGetEventsBefore.exec()) {
                qCritical() << "Local database error" << queryGetEventsBefore.lastError();
                reject(queryGetEventsBefore.lastError());
                return;
            }

            results.reserve(num);

            while (queryGetEventsBefore.next()) {
                QString eventIdStr = queryGetEventsBefore.value(0).toString();
                QByteArray eventJsonStr = queryGetEventsBefore.value(1).toByteArray();
                int locSection = queryGetEventsBefore.value(2).toInt();
                int locOrder = queryGetEventsBefore.value(3).toInt();

                QJsonDocument doc = QJsonDocument::fromJson(eventJsonStr);

                Ref<Mx::Event> event = Ref<Mx::Event>::create(eventIdStr, doc.object(), Mx::EventLoc{locSection, locOrder});
                results.append(event);
            }

            resolve(results);
        );
    }

    QtPromise::QPromise<std::optional<Mx::EventLocRange>> Database::getTimelineSectionOrderRange(const Mx::TimelineSection *section) {
        Vmx::InString roomId = section->getTimeline()->getRoom()->getRoomId().fullId;
        int sectionId = section->getSectionId();

        return ASYNC_DB_TASK(std::optional<Mx::EventLocRange>,
            queryGetTimelineSectionOrderRange.addBindValue(roomId.toQString());
            queryGetTimelineSectionOrderRange.addBindValue(sectionId);

            if (!queryGetTimelineSectionOrderRange.exec()) {
                qCritical() << "Local database error" << queryGetTimelineSectionOrderRange.lastError();
                reject(queryGetTimelineSectionOrderRange.lastError());
                return;
            }

            if (queryGetTimelineSectionOrderRange.next()) {
                if (queryGetTimelineSectionOrderRange.value(0).isNull()) {
                    resolve(std::nullopt);
                    return;
                } else {
                    resolve(Mx::EventLocRange{
                        .min = {sectionId, queryGetTimelineSectionOrderRange.value(0).toInt()},
                        .max = {sectionId, queryGetTimelineSectionOrderRange.value(1).toInt()},
                    });
                    return;
                }
            } else {
                resolve(std::nullopt);
                return;
            }
        );
    }

    QtPromise::QPromise<void> Database::deleteSectionEventsBefore(const Mx::Room *room, const Mx::EventLoc &loc) {
        Vmx::InString roomId = room->getRoomId().fullId;
        int section = loc.section;
        int order = loc.order;

        return ASYNC_DB_TASK(void,
            queryDeleteFromTimelineSectionBeforeOrder.addBindValue(roomId.toQString());
            queryDeleteFromTimelineSectionBeforeOrder.addBindValue(section);
            queryDeleteFromTimelineSectionBeforeOrder.addBindValue(order);

            if (!queryDeleteFromTimelineSectionBeforeOrder.exec()) {
                qCritical() << "Local database error" << queryDeleteFromTimelineSectionBeforeOrder.lastError();
                reject(queryDeleteFromTimelineSectionBeforeOrder.lastError());
                return;
            }

            resolve();
        );
    }

    QtPromise::QPromise<Ref<Mx::Event>> Database::getCurrentStateEvent(const Mx::Room *room, QString type, QString stateKey) {
        Q_ASSERT(!type.isNull());
        Q_ASSERT(!stateKey.isNull());

        Vmx::InString roomId = room->getRoomId().fullId;

        return ASYNC_DB_TASK(Ref<Mx::Event>,
            queryGetCurrentStateEvent.addBindValue(roomId.toQString());
            queryGetCurrentStateEvent.addBindValue(type);
            queryGetCurrentStateEvent.addBindValue(stateKey);

            if (!queryGetCurrentStateEvent.exec()) {
                qCritical() << "Local database error" << queryGetCurrentStateEvent.lastError();
                reject(queryGetCurrentStateEvent.lastError());
                return;
            }

            if (queryGetCurrentStateEvent.next()) {
                // Found a state events
                QString eventIdStr = queryGetCurrentStateEvent.value(0).toString();
                QByteArray eventJsonStr = queryGetCurrentStateEvent.value(1).toByteArray();
                QJsonDocument doc = QJsonDocument::fromJson(eventJsonStr);

                // Check if it has a loc
                if (!queryGetCurrentStateEvent.value(2).isNull()) {
                    int locSection = queryGetCurrentStateEvent.value(2).toInt();
                    int locOrder = queryGetCurrentStateEvent.value(3).toInt();

                    resolve(Ref<Mx::Event>::create(eventIdStr, doc.object(), Mx::EventLoc{locSection, locOrder}));
                } else {
                    resolve(Ref<Mx::Event>::create(eventIdStr, doc.object(), std::nullopt));
                }
            } else {
                // Not found
                resolve(Ref<Mx::Event>());
            }
        );
    }

    QtPromise::QPromise<void> Database::insertPrevBatchToken(
            const Mx::TimelineSection *section,
            const UserInfo *user,
            QString prevBatchToken
            ) {
        Vmx::InString roomId = section->getTimeline()->getRoom()->getRoomId().fullId;
        int sectionId = section->getSectionId();
        Vmx::InString userId = user->getUserId().fullId;

        return ASYNC_DB_TASK(void,
            queryInsertPrevBatchToken.addBindValue(roomId.toQString());
            queryInsertPrevBatchToken.addBindValue(sectionId);
            queryInsertPrevBatchToken.addBindValue(userId.toQString());
            queryInsertPrevBatchToken.addBindValue(prevBatchToken);

            if (!queryInsertPrevBatchToken.exec()) {
                qCritical() << "Local database error" << queryInsertPrevBatchToken.lastError();
                reject(queryInsertPrevBatchToken.lastError());
                return;
            }

            resolve();
        );
    }

    QtPromise::QPromise<void> Database::deletePrevBatchToken(const Mx::TimelineSection *section, const UserInfo *user) {
        Vmx::InString roomId = section->getTimeline()->getRoom()->getRoomId().fullId;
        int sectionId = section->getSectionId();
        Vmx::InString userId = user->getUserId().fullId;

        return ASYNC_DB_TASK(void,
            queryDeletePrevBatchToken.addBindValue(roomId.toQString());
            queryDeletePrevBatchToken.addBindValue(sectionId);
            queryDeletePrevBatchToken.addBindValue(userId.toQString());

            if (!queryDeletePrevBatchToken.exec()) {
                qCritical() << "Local database error" << queryDeletePrevBatchToken.lastError();
                reject(queryDeletePrevBatchToken.lastError());
                return;
            }

            resolve();
        );
    }

    QtPromise::QPromise<std::optional<QString>> Database::getPrevBatchToken(const Mx::TimelineSection *section, const UserInfo *user) {
        Vmx::InString roomId = section->getTimeline()->getRoom()->getRoomId().fullId;
        int sectionId = section->getSectionId();
        Vmx::InString userId = user->getUserId().fullId;

        return ASYNC_DB_TASK(std::optional<QString>,
            queryGetPrevBatchToken.addBindValue(roomId.toQString());
            queryGetPrevBatchToken.addBindValue(sectionId);
            queryGetPrevBatchToken.addBindValue(userId.toQString());

            if (!queryGetPrevBatchToken.exec()) {
                qCritical() << "Local database error" << queryGetPrevBatchToken.lastError();
                reject(queryGetPrevBatchToken.lastError());
                return;
            }

            if (queryGetPrevBatchToken.next()) {
                resolve(queryGetPrevBatchToken.value(0).toString());
            } else {
                resolve(std::nullopt);
            }
        );
    }

    QtPromise::QPromise<std::optional<int>> Database::getMaxSectionIdForRoom(const Mx::Room *room) {
        Vmx::InString roomId = room->getRoomId().fullId;

        return ASYNC_DB_TASK(std::optional<int>,
            queryGetTimelineMaxSectionId.addBindValue(roomId.toQString());

            if (!queryGetTimelineMaxSectionId.exec()) {
                qCritical() << "Local database error" << queryGetTimelineMaxSectionId.lastError();
                reject(queryGetTimelineMaxSectionId.lastError());
                return;
            }

            if (queryGetTimelineMaxSectionId.next()) {
                QVariant result = queryGetTimelineMaxSectionId.value(0);
                if (result.isNull()) {
                    resolve(std::nullopt);
                } else {
                    resolve(result.toInt());
                }
            } else {
                resolve(std::nullopt);
            }
        );
    }

    QtPromise::QPromise<std::optional<std::optional<Mx::EventLoc>>> Database::getEventLocForId(Vmx::Id::RoomId roomId, Vmx::Id::EventId eventId) {
        return ASYNC_DB_TASK(std::optional<std::optional<Mx::EventLoc>>,
            queryGetEventLocForId.addBindValue(roomId.fullId.toQString());
            queryGetEventLocForId.addBindValue(eventId.fullId.toQString());

            if (!queryGetEventLocForId.exec()) {
                qCritical() << "Local database error" << queryGetEventLocForId.lastError();
                reject(queryGetEventLocForId.lastError());
                return;
            }

            if (queryGetEventLocForId.next()) {
                if (queryGetEventLocForId.value(0).isNull()) {
                    resolve(std::optional<std::optional<Mx::EventLoc>>(std::optional<Mx::EventLoc>()));
                } else {
                    resolve(Mx::EventLoc{
                        queryGetEventLocForId.value(0).toInt(),
                        queryGetEventLocForId.value(1).toInt()
                    });
                }
            } else {
                // No matching events
                resolve(std::nullopt);
            }
        );
    }

    void Database::saveUsers() {
        ASYNC_DB_TASK(void,
            startTransaction().wait();

            // Save all users
            for (const UserInfo *user : VtxInstance::getInstance()->getMatrix()->getUsers()) {
                Q_ASSERT(user->getSession().has_value());

                queryInsertUser.addBindValue(user->getUserId().fullId.toQString());
                queryInsertUser.addBindValue(user->getSession()->accessToken);
                queryInsertUser.addBindValue(user->getSession()->homeserverUrl);

                std::optional<QString> nextBatchToken = user->getSyncNextBatchToken();
                if (nextBatchToken) {
                    queryInsertUser.addBindValue(*nextBatchToken);
                } else {
                    queryInsertUser.addBindValue(QVariant(QVariant::String)); // Null QVariant
                }

                if (!queryInsertUser.exec()) {
                    qCritical() << "Local database error" << queryInsertUser.lastError();
                    reject(queryInsertUser.lastError());
                    return;
                }
            }

            // Save the rooms that users are in
            for (const Mx::Room *room : VtxInstance::getInstance()->getMatrix()->getRooms()) {
                QString roomId = room->getRoomId().fullId;

                const QHash<const UserInfo*, Vmx::Membership>& memberships = room->getUserMemberships();
                for (auto memberIter = memberships.begin(); memberIter != memberships.end(); ++memberIter) {
                    queryInsertUserRoom.addBindValue(roomId);
                    queryInsertUserRoom.addBindValue(memberIter.key()->getUserId().fullId.toQString());
                    queryInsertUserRoom.addBindValue(Vmx::Serde::valueToString(memberIter.value()));

                    if (!queryInsertUserRoom.exec()) {
                        qCritical() << "Local database error" << queryInsertUserRoom.lastError();
                        reject(queryInsertUserRoom.lastError());
                        return;
                    }
                }
            }

            commitTransaction().wait();

            resolve();
        ).wait();
    }

    void Database::loadUsers() {
        // Load users
        if (!queryGetAllUsers.exec()) {
            qCritical() << "Local database error" << queryGetAllUsers.lastError();
            return;
        }

        VtxInstance *inst = VtxInstance::getInstance();

        while (queryGetAllUsers.next()) {
            QString userId = queryGetAllUsers.value(0).toString();
            QString accessToken = queryGetAllUsers.value(1).toString();
            QString homeserverUrl = queryGetAllUsers.value(2).toString();
            QString syncNextBatchToken = queryGetAllUsers.value(3).toString();

            UserInfo *user = new UserInfo();
            user->setUserId(userId);
            user->setSession(Vmx::Session {
                .accessToken = accessToken,
                .homeserverUrl = homeserverUrl,
            });

            if (!syncNextBatchToken.isEmpty()) {
                user->setSyncNextBatchToken(syncNextBatchToken);
            }

            qDebug() << "Adding user from local db" << userId;
            inst->getMatrix()->addUser(user);
        }

        // Load room list and user memberships
        if (!queryGetAllRoomMemberships.exec()) {
            qCritical() << "Local database error" << queryGetAllRoomMemberships.lastError();
            return;
        }

        while (queryGetAllRoomMemberships.next()) {
            QString roomId = queryGetAllRoomMemberships.value(0).toString();
            QString userId = queryGetAllRoomMemberships.value(1).toString();
            QString membership = queryGetAllRoomMemberships.value(2).toString();

            // Find the user
            UserInfo *user = inst->getMatrix()->findUserById(userId);
            if (user == nullptr) {
                qCritical() << "Local database has join state" << membership << "for room" << roomId << "for unknown user" << userId;
                // TODO: Delete the offending entry
                continue;
            }

            // Convert the membership string to an enum
            Vmx::Util::Result<Vmx::Membership, Vmx::Serde::DeserializeError> membershipEnum = Vmx::Serde::valueFromString<Vmx::Membership>(membership);
            if (membershipEnum.isErr()) {
                qCritical() << "Local database has unknown join state" << membership << "for room" << roomId << "for user" << userId;
                // TODO: Delete the offending entry
                continue;
            }

            // Find the room if it already exists, else create it
            Mx::Room *room = inst->getMatrix()->getRooms().findById(roomId);
            if (room == nullptr) {
                qDebug() << "Adding room from local db" << roomId;

                room = new Mx::Room(roomId);
                inst->getMatrix()->getRooms().append(room);
            }

            room->setUserMembership(user, membershipEnum.unwrap());
        }
    }

    QtPromise::QPromise<void> Database::startTransaction() {
        return ASYNC_DB_TASK(void,
            transactionDepth++;

            if (transactionDepth == 1) {
                bool success = db.transaction();
                if (!success) {
                    qCritical() << "Starting local database transaction error" << db.lastError();
                    reject(db.lastError());
                    return;
                }

                resolve();
            } else {
                resolve();
            }
        );
    }

    QtPromise::QPromise<void> Database::commitTransaction() {
        return ASYNC_DB_TASK(void,
            transactionDepth--;

            if (transactionDepth == 0) {
                bool success = db.commit();
                if (!success) {
                    qCritical() << "Commiting local database transaction error" << db.lastError();
                    reject(db.lastError());
                    return;
                }

                resolve();
            } else {
                resolve();
            }
        );
    }

    bool Database::makeSavepoint(const QString &name) {
        Q_ASSERT(QThread::currentThread() == dbThread);

        queryMakeSavepoint.addBindValue(name);

        if (!queryMakeSavepoint.exec()) {
            qCritical() << "Local database error" << queryMakeSavepoint.lastError();
            return false;
        }

        return true;
    }

    bool Database::releaseSavepoint(const QString &name) {
        Q_ASSERT(QThread::currentThread() == dbThread);

        queryReleaseSavepoint.addBindValue(name);

        if (!queryReleaseSavepoint.exec()) {
            qCritical() << "Local database error" << queryReleaseSavepoint.lastError();
            return false;
        }

        return true;
    }

    bool Database::rollbackSavepoint(const QString &name) {
        Q_ASSERT(QThread::currentThread() == dbThread);

        queryRollbackSavepoint.addBindValue(name);

        if (!queryRollbackSavepoint.exec()) {
            qCritical() << "Local database error" << queryRollbackSavepoint.lastError();
            return false;
        }

        return true;
    }
}
