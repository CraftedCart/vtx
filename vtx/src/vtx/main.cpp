#include "vmx/clientapi/r0/session/Login.hpp"
#include "vtx/ui/VtxWindow.hpp"
#include "vtx/VtxInstance.hpp"
#include "vtx/CompilerSupport.hpp"
#include "vtx_export.h"
#include <QFile>
#include <QDir>
#include <QStyleFactory>
#include <QTranslator>
#include <QApplication>
#include <QLocale>

#ifdef VTX_IS_POSIX
    // For isatty
    #include <unistd.h>
#endif

namespace Vtx {
    VTX_EXPORT int vtxLaunch(int argc, char *argv[]) {
        // Only check if we should output in color on POSIX systems (with isatty)
        // And only output backtraces if we're using glibc (since Qt's %{backtrace} only supports that)
        #ifdef VTX_IS_POSIX
        bool stdoutSupportsColor = isatty(fileno(stdout));
        if (stdoutSupportsColor) {
            #ifdef VTX_USING_GLIBC
            qSetMessagePattern(QLatin1String("%{if-debug}\033[1;35m%{endif}%{if-info}\033[1;36m%{endif}%{if-warning}\033[1;33m%{endif}%{if-critical}\033[1;31m%{endif}%{if-fatal}\033[1;31m%{endif}[%{time} - %{if-debug}DEBG%{endif}%{if-info}INFO%{endif}%{if-warning}WARN%{endif}%{if-critical}CRIT%{endif}%{if-fatal}FATL%{endif} - Thread %{threadid} - %{function}]\033[0m %{message}%{if-critical}\n    (At %{file}:%{line})\n    ==== Backtrace ====\n    %{backtrace depth=20 separator=\"\n    \"}%{endif}"));
            #else
            qSetMessagePattern(QLatin1String("%{if-debug}\033[1;35m%{endif}%{if-info}\033[1;36m%{endif}%{if-warning}\033[1;33m%{endif}%{if-critical}\033[1;31m%{endif}%{if-fatal}\033[1;31m%{endif}[%{time} - %{if-debug}DEBG%{endif}%{if-info}INFO%{endif}%{if-warning}WARN%{endif}%{if-critical}CRIT%{endif}%{if-fatal}FATL%{endif} - Thread %{threadid} - %{function}]\033[0m %{message}%{if-critical}\n    (At %{file}:%{line})%{endif}"));
            #endif
        } else {
        #endif
            #ifdef __GLIBC__
            qSetMessagePattern(QLatin1String("[%{time} - %{if-debug}DEBG%{endif}%{if-info}INFO%{endif}%{if-warning}WARN%{endif}%{if-critical}CRIT%{endif}%{if-fatal}FATL%{endif} - %{function}] %{message}%{if-critical}\n    (At %{file}:%{line})\n    ==== Backtrace ====\n    %{backtrace depth=20 separator=\"\n    \"}%{endif}"));
            #else
            qSetMessagePattern(QLatin1String("[%{time} - %{if-debug}DEBG%{endif}%{if-info}INFO%{endif}%{if-warning}WARN%{endif}%{if-critical}CRIT%{endif}%{if-fatal}FATL%{endif} - %{function}] %{message}%{if-critical}\n    (At %{file}:%{line})%{endif}"));
            #endif
        #ifdef VTX_IS_POSIX
        }
        #endif

        // Init Vtx
        bool success = VtxInstance::createInstance(argc, argv);
        if (!success) {
            VtxInstance::destroyInstance();
            return EXIT_FAILURE;
        }

        VtxInstance *vtxInstance = VtxInstance::getInstance();

        // TODO: Bundle some sort of emoji font with the app
        QFont::insertSubstitution(QApplication::font().family(), QLatin1String("Noto Color Emoji"));

        // Load translations
        QTranslator translator;
        if (translator.load(QLocale(), QLatin1String("lang"), QLatin1String("_"), QDir(QCoreApplication::applicationDirPath()).filePath("../share/vtx/lang"))) {
            qApp->installTranslator(&translator);
        } else if (translator.load(QLocale(), QLatin1String("lang"), QLatin1String("_"), QCoreApplication::applicationDirPath())) {
            // If the software was never installed after build, the translations will be alongside the executable
            qApp->installTranslator(&translator);
        } else if (translator.load("lang_en_US", QDir(QCoreApplication::applicationDirPath()).filePath("../share/vtx/lang"))) {
            // If we can't find a suitable translation, try en_US
            qApp->installTranslator(&translator);
        } else if (translator.load("lang_en_US", QCoreApplication::applicationDirPath())) {
            // If we can't find a suitable translation, try en_US - if not intalled
            qApp->installTranslator(&translator);
        }

        vtxInstance->getAppConfig().readSync();
        vtxInstance->getDatabase().loadUsers();

        QStyle *style = QStyleFactory::create(QLatin1String("Fusion"));
        if (style != nullptr) qApp->setStyle(style);

        QFile sheetFile(":/Styles/FlatDark/FlatDark.qss");
        sheetFile.open(QFile::ReadOnly);
        QString sheet(sheetFile.readAll());
        sheetFile.close();
        qApp->setStyleSheet(sheet);

        UI::VtxWindow *w = new UI::VtxWindow();
        w->show();

        // Enter the event loop until we request to quit
        int ret = vtxInstance->execApp();

        // w should delete itself when closed
        // delete w;

        VtxInstance::destroyInstance();

        return ret;
    }
}
