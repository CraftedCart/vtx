#include "vtx/model/RoomListModel.hpp"

namespace Vtx::Model {
    RoomListModel::RoomListModel(Mx::RoomList *roomList, QObject *parent) : QAbstractListModel(parent), roomList(roomList) {
        connect(roomList, &Mx::RoomList::onBeginInsertRows, this, &RoomListModel::beginInsertRows);
        connect(roomList, &Mx::RoomList::onEndInsertRows, this, &RoomListModel::endInsertRows);
    }

    int RoomListModel::rowCount(const QModelIndex &parent) const {
        Q_UNUSED(parent);

        return roomList->length();
    }

    QVariant RoomListModel::data(const QModelIndex &index, int role) const {
        if (!index.isValid()) return QVariant();
        if (index.row() >= roomList->length()) return QVariant();

        if (role == Qt::DisplayRole) {
            return roomList->at(index.row())->getDisplayName();
        } else {
            return QVariant();
        }
    }

    QVariant RoomListModel::headerData(int section, Qt::Orientation orientation, int role) const {
        if (role != Qt::DisplayRole) return QVariant();

        if (orientation == Qt::Horizontal) {
            return QStringLiteral("Column %1").arg(section);
        } else {
            return QStringLiteral("Row %1").arg(section);
        }
    }

    const Vmx::Id::RoomId& RoomListModel::roomIdForIndex(const QModelIndex &index) {
        return roomList->at(index.row())->getRoomId();
    }
}
