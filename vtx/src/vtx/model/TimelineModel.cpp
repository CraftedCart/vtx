#include "vtx/model/TimelineModel.hpp"

namespace Vtx::Model {
    TimelineModel::TimelineModel(QObject *parent) : QAbstractListModel(parent) {}

    void TimelineModel::setSection(Mx::Room *room, Ref<Mx::TimelineSection> section) {
        beginResetModel();

        if (this->room != nullptr) {
            disconnect(this->section, &Mx::TimelineSection::onBeginInsertRows, this, &TimelineModel::beginInsertRows);
            disconnect(this->section, &Mx::TimelineSection::onEndInsertRows, this, &TimelineModel::endInsertRows);
            disconnect(this->section, &Mx::TimelineSection::onBeginRemoveRows, this, &TimelineModel::beginRemoveRows);
            disconnect(this->section, &Mx::TimelineSection::onEndRemoveRows, this, &TimelineModel::endRemoveRows);
            disconnect(this->section, &Mx::TimelineSection::beginRequestingPastMessages, this, &TimelineModel::beginRequestingPastMessages);
            disconnect(this->section, &Mx::TimelineSection::finishedRequestingPastMessages, this, &TimelineModel::finishedRequestingPastMessages);
            disconnect(&this->room->getTimeline(), &Mx::Timeline::currentSectionIdChanged, this, &TimelineModel::currentSectionIdChanged);
        }

        this->room = room;
        this->section = section;

        if (room != nullptr) {
            connect(section, &Mx::TimelineSection::onBeginInsertRows, this, &TimelineModel::beginInsertRows);
            connect(section, &Mx::TimelineSection::onEndInsertRows, this, &TimelineModel::endInsertRows);
            connect(section, &Mx::TimelineSection::onBeginRemoveRows, this, &TimelineModel::beginRemoveRows);
            connect(section, &Mx::TimelineSection::onEndRemoveRows, this, &TimelineModel::endRemoveRows);
            connect(section, &Mx::TimelineSection::beginRequestingPastMessages, this, &TimelineModel::beginRequestingPastMessages);
            connect(section, &Mx::TimelineSection::finishedRequestingPastMessages, this, &TimelineModel::finishedRequestingPastMessages);
            connect(&room->getTimeline(), &Mx::Timeline::currentSectionIdChanged, this, &TimelineModel::currentSectionIdChanged);
        }

        endResetModel();
    }

    Mx::Room* TimelineModel::getRoom() {
        return room;
    }

    Ref<Mx::TimelineSection>& TimelineModel::getSection() {
        return section;
    }

    int TimelineModel::rowCount(const QModelIndex &parent) const {
        Q_UNUSED(parent);

        if (room == nullptr) return 0;
        return section->length();
    }

    QVariant TimelineModel::data(const QModelIndex &index, int role) const {
        if (room == nullptr) return QVariant();
        if (!index.isValid()) return QVariant();
        if (index.row() >= section->length()) return QVariant();

        if (role == ROLE_REF_EVENT) {
            Ref<Mx::Event> event = section->at(index.row());
            return QVariant::fromValue(event);
        } else if (role == Qt::DisplayRole) {
            Ref<Mx::Event> event = section->at(index.row());
            return event->getSender().fullId + ": " + event->getContent()["body"].toString();
        } else {
            return QVariant();
        }
    }

    QVariant TimelineModel::headerData(int section, Qt::Orientation orientation, int role) const {
        Q_UNUSED(section);
        Q_UNUSED(orientation);
        Q_UNUSED(role);

        return QVariant();
    }

    void TimelineModel::requestPastMessages(UserInfo *user) {
        Q_ASSERT(room != nullptr);

        section->requestPastMessages(user);
    }

    bool TimelineModel::isRequestingPastMessages() {
        return section->isRequestingPastMessages();
    }
}
