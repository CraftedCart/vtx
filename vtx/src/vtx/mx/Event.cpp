#include "vtx/mx/Event.hpp"
#include "vmx/event/RoomEvent.hpp"

static const QLatin1String STATE_KEY = QLatin1String("state_key");
static const QLatin1String CONTENT = QLatin1String("content");
static const QLatin1String SENDER = QLatin1String("sender");
static const QLatin1String ORIGIN_SERVER_TS = QLatin1String("origin_server_ts");
static const QLatin1String UNSIGNED = QLatin1String("unsigned");
static const QLatin1String TYPE = QLatin1String("type");

namespace Vtx::Mx {
    Event::Event(const Vmx::Event::SyncRoomEvent &event) :
        sender(event.sender),
        eventId(event.eventId),
        originServerTs(event.originServerTs),
        unsignedData(event.unsignedData),
        type(event.type),
        content(event.content),
        json(event.json) {}

    Event::Event(const Vmx::Id::EventId &eventId, const QJsonObject &json, const std::optional<EventLoc> &loc) :
        eventId(eventId),
        json(json),
        eventLoc(loc) {

        sender = json[SENDER].toString();
        originServerTs = QDateTime::fromMSecsSinceEpoch(json[ORIGIN_SERVER_TS].toDouble());

        if (json.contains(UNSIGNED) && json[UNSIGNED].isObject()) {
            Vmx::Util::Result<Vmx::Event::UnsignedData, Vmx::Serde::DeserializeError> res =
                Vmx::Serde::deserialize<Vmx::Event::UnsignedData>(json[UNSIGNED].toObject());

            if (res.isOk()) {
                unsignedData = res.unwrap();
            } else {
                qWarning() << "Failed to deserialize event 'unsigned'";
            }
        }

        type = json[TYPE].toString();
        content = json[CONTENT].toObject();
    }

    const Vmx::Id::UserId& Event::getSender() const {
        return sender;
    }

    const Vmx::Id::EventId& Event::getEventId() const {
        return eventId;
    }

    const QDateTime& Event::getOriginServerTs() const {
        return originServerTs;
    }

    const std::optional<Vmx::Event::UnsignedData>& Event::getUnsignedData() const {
        return unsignedData;
    }

    const Vmx::InString& Event::getType() const {
        return type;
    }

    const QJsonObject& Event::getContent() const {
        return content;
    }

    const QJsonObject& Event::getJson() const {
        return json;
    }

    std::optional<QString> Event::getStateKey() const {
        if (json.contains(STATE_KEY)) {
            QJsonValue key = json[STATE_KEY];
            if (key.isString()) {
                return key.toString();
            }
        }

        return {};
    }

    void Event::setPrevEvent(Event *prevEvent) {
        this->prevEvent = prevEvent;
    }

    const Event* Event::getPrevEvent() const {
        return prevEvent;
    }

    void Event::setEventLoc(const std::optional<EventLoc> &eventLoc) {
        this->eventLoc = eventLoc;
    }

    const std::optional<EventLoc>& Event::getEventLoc() const {
        return eventLoc;
    }

    bool Event::isStateEvent() const {
        return json.contains(STATE_KEY);
    }

    Ref<Event> Event::clone() const {
        Ref<Event> other = Ref<Event>::create();

        other->sender = sender;
        other->eventId = eventId;
        other->originServerTs = originServerTs;
        other->unsignedData = unsignedData;
        other->type = type;
        other->content = content;
        other->json = json;
        other->prevEvent = prevEvent;
        other->eventLoc = eventLoc;

        return other;
    }
}
