#include "vtx/mx/EventLoc.hpp"
#include <QDebug>

QDebug operator<<(QDebug dbg, const Vtx::Mx::EventLoc &value) {
    dbg.nospace() << "EventLoc{" << value.section << ", " << value.order << "}";
    return dbg.maybeSpace();
}
