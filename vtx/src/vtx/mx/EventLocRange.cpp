#include "vtx/mx/EventLocRange.hpp"
#include <QDebug>

QDebug operator<<(QDebug dbg, const Vtx::Mx::EventLocRange &value) {
    dbg.nospace() << "EventLocRange{{" << value.min.section << ", " << value.min.order << "}, {"
        << value.max.section << ", " << value.max.order << "}}";
    return dbg.maybeSpace();
}

