#include "vtx/mx/Matrix.hpp"
#include "vtx/VtxInstance.hpp"
#include "vmx/mediaapi/r0/Download.hpp"
#include "vmx/event/RoomEvent.hpp"
#include <QDebug>
#include <memory>

namespace Vtx::Mx {
    Matrix::Matrix(QObject *parent) : QObject(parent) {
        client = new Vmx::Client(this);
        globalQueue = new PromiseQueue(this);

        pixmapCache.setMaxCost(30 * 1024 * 1024); // 30 MiB

        // Purge unused stuff from memory every so often
        connect(&purgeTimer, &QTimer::timeout, this, &Matrix::purgeUnused);
        purgeTimer.setTimerType(Qt::VeryCoarseTimer);
        purgeTimer.start(30 * 1000); // 30s
    }

    Matrix::~Matrix() {
        qDeleteAll(users);
    }

    Vmx::Client* Matrix::getClient() {
        return client;
    }

    void Matrix::addUser(UserInfo *user) {
        Q_ASSERT(QThread::currentThread() == qApp->thread());

        user->setParent(this);
        users.append(user);
        emit userAdded(user);
    }

    void Matrix::removeUser(UserInfo *user) {
        Q_ASSERT(QThread::currentThread() == qApp->thread());

        users.removeOne(user);
        syncingUsers.removeOne(user);
        emit user->removed(user);
    }

    const QVector<UserInfo*>& Matrix::getUsers() const {
        return users;
    }

    UserInfo* Matrix::findUserById(const Vmx::Id::UserId &id) {
        auto iter = std::find_if(users.begin(), users.end(), [&id](UserInfo* user) { return user->getUserId() == id; });
        if (iter != users.end()) {
            return *iter;
        } else {
            return nullptr;
        }
    }

    RoomList& Matrix::getRooms() {
        return rooms;
    }

    const RoomList& Matrix::getRooms() const {
        return rooms;
    }

    void Matrix::mergeSyncedRoom(const UserInfo *user, const Vmx::Id::RoomId &roomId, const Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom &joinedRoom) {
        Room *room = rooms.findById(roomId);
        if (room != nullptr) {
            room->mergeSyncedRoom(user, joinedRoom);
        } else {
            room = new Mx::Room(user, roomId, joinedRoom);
            rooms.append(room);
        }
    }

    void Matrix::purgeUnused() {
        for (Room *room : rooms) {
            room->purgeUnused();
        }
    }

    void Matrix::stopAutoPurge() {
        purgeTimer.stop();
    }

    QtPromise::QPromise<Vmx::Arc<QPixmap>> Matrix::getMxcPixmap(
            const QUrl &mxc,
            const Vmx::Session &session
            ) {
        // TODO: What if we try download the same image twice? Well it'll get queued up twice - not ideal

        return QtPromise::QPromise<Vmx::Arc<QPixmap>>{[&](
                const QtPromise::QPromiseResolve<Vmx::Arc<QPixmap>>& resolve,
                const QtPromise::QPromiseReject<Vmx::Arc<QPixmap>>& reject) {
            Vmx::Arc<QPixmap> *cachedImage = pixmapCache.object(mxc);
            if (cachedImage != nullptr) {
                resolve(*cachedImage);
                return;
            }

            // It's not cached - try download it
            Vmx::MediaApi::R0::Download::GetRequest req;
            req.serverName = mxc.host();
            req.mediaId = mxc.path().remove(0, 1); // Remove leading slash

            Q_ASSERT(!req.serverName.isEmpty() && "Empty host name");
            Q_ASSERT(!req.mediaId.isEmpty() && "Empty media id");

            requestQueuedGlobal(req, session)
            .then([this, resolve, reject, mxc](const QByteArray &data) {
                // QByteArray formatBytes; // Store this on the stack as format might be a pointer into this var
                // const char *format = nullptr;

                // if (mime) {
                    // const QStringList& suffixes = (*mime).suffixes();
                    // if (suffixes.length() > 0) {
                        // formatBytes = suffixes.first().toLatin1();
                        // format = formatBytes.data();
                    // }
                // }

                Vmx::Arc<QPixmap> img = std::make_shared<QPixmap>();
                if (img->loadFromData(data /*, format */)) {
                    int size = qMin(((size_t) img->width() * (size_t) img->height() * (size_t) img->depth()) / 8, (size_t) INT_MAX);

                    if (size < pixmapCache.maxCost()) {
                        pixmapCache.insert(mxc, new Vmx::Arc<QPixmap>(img), size);
                    }

                    resolve(img);
                } else {
                    reject(CouldNotParseImage());
                }
            })
            .fail([reject](const Vmx::ClientApi::ApiError &err) {
                reject(err);
            })
            .fail([reject](const Vmx::NetError &err) {
                reject(err);
            })
            .fail([reject]() {
                reject();
            });
        }};
    }

    Vmx::Util::Result<void, LoginRejectError> Matrix::onLoginSuccess(const Vmx::ClientApi::R0::Session::Login::PostResponse &resp) {
        // Check if this user is already logged in
        for (UserInfo *user : users) {
            if (user->getSession()) {
                // Check if we have this access token already, from the same homeserver
                if (
                        resp.accessToken == user->getSession()->accessToken &&
                        resp.homeserver == user->getSession()->homeserverUrl
                        ) {
                    return Vmx::Util::Err(LoginRejectError(AlreadyLoggedIn()));
                }
            }

            // Check if we have this MXID already
            if (resp.userId == user->getUserId()) {
                return Vmx::Util::Err(LoginRejectError(AlreadyLoggedIn()));
            }
        }

        UserInfo *user = new UserInfo();
        user->setSession(resp.makeSession());
        user->setUserId(resp.userId);
        addUser(user);

        // Start syncing this user
        syncingUsers.append(user);
        initialSyncThenSyncUserRepeat(user);

        emit userAdded(user);

        return Vmx::Util::Ok();
    }

    void Matrix::syncUserRepeat(UserInfo *user) {
        using namespace Vmx;
        using namespace Vmx::ClientApi;
        using namespace Vmx::ClientApi::R0::Sync;
        using namespace Vmx::ClientApi::R0::Filter; // TODO: Use a filter stored on the server

        Q_ASSERT(user->getSyncNextBatchToken().has_value());

        SyncEvents::GetRequest req = {
            .filter = FilterDef{
                .room = RoomFilter{
                    .state = StateFilter{ .lazyLoadMembers = true },
                    .timeline = RoomEventFilter{ .lazyLoadMembers = true },
                },
            }
        };
        req.since = user->getSyncNextBatchToken();
        req.timeout = 30 * 1000; // 30s

        requestImmediate(req, user->getSession().value(), {})
            .then([this, user](const SyncEvents::GetResponse &resp) {
                Db::Database& db = VtxInstance::getInstance()->getDatabase();

                db.startTransaction();
                if (resp.rooms) {
                    for (auto joinIter = resp.rooms->join.begin(); joinIter != resp.rooms->join.end(); ++joinIter) {
                        const Id::RoomId & roomId = joinIter.key();
                        const SyncEvents::JoinedRoom &joinedRoom = joinIter.value();

                        mergeSyncedRoom(user, roomId, joinedRoom);
                    }
                }

                // TODO: We should probably check whether a user still exists since a user might log out of a
                // user
                Q_ASSERT(users.contains(user) && "Sync finished for user that is no longer registered in Matrix object");

                user->setSyncNextBatchToken(resp.nextBatch);
                db.commitTransaction();

                // Sync again!
                syncUserRepeat(user);
            })
            .fail([](const ApiError &err) {
                qDebug() << "ERR" << err.httpStatus;
                qDebug() << "ERR" << err.message;

                // TODO: Wait a while and retry
            });
    }

    void Matrix::initialSyncThenSyncUserRepeat(UserInfo *user) {
        using namespace Vmx;
        using namespace Vmx::ClientApi;
        using namespace Vmx::ClientApi::R0::Sync;
        using namespace Vmx::ClientApi::R0::Filter; // TODO: Use a filter stored on the server

        SyncEvents::GetRequest req = {
            .filter = FilterDef{
                .room = RoomFilter{
                    .state = StateFilter{ .lazyLoadMembers = true },
                    .timeline = RoomEventFilter{ .lazyLoadMembers = true },
                },
            }
        };

        // TODO: This probably shouldn't be globally queued
        requestQueuedGlobal(req, user->getSession().value(), new TaskInfo(tr("Initial syncing for user %1").arg(user->getUserId().fullId)))
            .then([this, user](const SyncEvents::GetResponse &resp) {
                Db::Database& db = VtxInstance::getInstance()->getDatabase();

                db.startTransaction();
                if (resp.rooms) {
                    for (auto joinIter = resp.rooms->join.begin(); joinIter != resp.rooms->join.end(); ++joinIter) {
                        const Id::RoomId & roomId = joinIter.key();
                        const SyncEvents::JoinedRoom &joinedRoom = joinIter.value();

                        qDebug() << "Merging room" << roomId.fullId;
                        VtxInstance::getInstance()->getMatrix()->mergeSyncedRoom(user, roomId, joinedRoom);
                    }
                }

                // TODO: We should probably check whether a user still exists since a user might log out of a user
                Q_ASSERT(users.contains(user) && "Sync finished for user that is no longer registered in Matrix object");

                user->setSyncNextBatchToken(resp.nextBatch);

                // After an initial sync, make sure we persist login info and rooms in the local db
                db.saveUsers();

                db.commitTransaction();

                // Sync again!
                syncUserRepeat(user);
            })
            .fail([](const ApiError &err) {
                qDebug() << "ERR" << err.httpStatus;
                qDebug() << "ERR" << err.message;

                // TODO: Wait a while and retry
            });
    }

    void Matrix::startSyncingAllUsers() {
        for (UserInfo *user : users) {
            if (syncingUsers.contains(user)) continue;

            syncingUsers.append(user);

            if (user->getSyncNextBatchToken()) {
                syncUserRepeat(user);
            } else {
                initialSyncThenSyncUserRepeat(user);
            }
        }
    }
}
