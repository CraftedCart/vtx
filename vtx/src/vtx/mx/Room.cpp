#include "vtx/mx/Room.hpp"
#include "vtx/VtxInstance.hpp"
#include "vmx/event/RoomEvent.hpp"

static const Vmx::InString M_ROOM_NAME = Vmx::InString(QLatin1String("m.room.name"));
static const Vmx::InString M_ROOM_CANONICAL_ALIAS = Vmx::InString(QLatin1String("m.room.canonical_alias"));
static const Vmx::InString M_ROOM_MEMBER = Vmx::InString(QLatin1String("m.room.member"));
static const QString QS_STATE_KEY = QLatin1String("state_key");

namespace Vtx::Mx {
    Room::Room(const Vmx::Id::RoomId &roomId) : roomId(roomId) {
        // See if we can figure out the name from state events stored in the local db
        updateDisplayName();
    }

    Room::Room(const UserInfo *user, const Vmx::Id::RoomId &roomId, const Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom &room) : roomId(roomId) {
        mergeSyncedRoom(user, room);

        userMemberships[user] = Vmx::Membership::JOIN;
        connect(user, &UserInfo::removed, this, &Room::onUserRemoved);
    }

    void Room::mergeSyncedRoom(const UserInfo *user, const Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom &room) {
        Db::Database& db = VtxInstance::getInstance()->getDatabase();
        db.startTransaction();

        Ref<TimelineSection> section;
        if (room.timeline.limited && *room.timeline.limited) {
            // If we get a limited response, we should figure out if we know about any of the recieved events
            // If we do, then we'll add events to the same timeline section as the events we have stored
            // If not, then we need to create a new section
            // ugh deduping events for multi-account support is a bit of a mess >.<
            for (const Vmx::Event::SyncRoomEvent &event : room.timeline.events) {
                bool wantBreak = false;
                db.getEventLocForId(roomId, event.eventId)
                    .then([this, &section, &wantBreak](std::optional<std::optional<EventLoc>> existingEventLoc) {
                        if (existingEventLoc && *existingEventLoc) {
                            // We found an existing event in the local db already, use its section ID
                            section = timeline.getSection((**existingEventLoc).section);
                            wantBreak = true;
                            return;
                        }
                    })
                    .wait();

                if (wantBreak) break;
            }

            // If no existing matching event was found in the db, make a new section
            if (section.isNull()) {
                section = timeline.appendNewSection();
            }

        } else {
            // Not a limited sync response
            section = timeline.getCurrentSection();
        }

        // If we don't have a prev batch token, save it
        if (room.timeline.prevBatch && !section->hasPrevBatchTokenForUser(user)) {
            section->setPrevBatchToken(user, *room.timeline.prevBatch);
        }

        for (const Vmx::Event::SyncStateEvent &event : room.state.events) {
            // No need to dedup state events really... the old state event can be overwritten
            // if (db.getEventLocForId(roomId, event.eventId).has_value()) {
                // continue;
            // }
            Ref<Event> vtxEvent = currentState.mergeEvent(event);
            db.insertEvent(vtxEvent, this);
            db.insertCurrentStateEvent(vtxEvent, this);
        }

        bool gotStateEvents = !room.state.events.isEmpty();

        std::optional<EventLoc> prevEventLoc;
        for (const Vmx::Event::SyncRoomEvent &event : room.timeline.events) {
            bool wantContinue = false;
            db.getEventLocForId(roomId, event.eventId)
                .then([this, &prevEventLoc, &event, &section, &gotStateEvents, &wantContinue](std::optional<std::optional<EventLoc>> existingEventLoc) {
                    if (existingEventLoc) {
                        if (*existingEventLoc) {
                            // We found an event in the local db and its location isn't null
                            // so dedup it
                            qDebug() << "Deduping timeline event" << event.eventId.fullId.toQString();
                            prevEventLoc = **existingEventLoc;
                            wantContinue = true;
                            return;
                        } else {
                            // An event with this ID already exists, but it has no location
                            // See if we can fetch the event from the room's current sttae
                            if (event.json.contains(QS_STATE_KEY)) {
                                std::optional<Ref<Event>> otherEvent = currentState.get(event.type, event.json[QS_STATE_KEY].toString());
                                if (otherEvent) {
                                    section->maybeAppendSyncedEvent(*otherEvent);
                                    gotStateEvents = true;
                                }
                                // ...otherwise continue on with creating/appending it
                            } else {
                                qCritical() << "Found existing event without a location, but it wasn't a state event";
                            }
                        }
                    }
                })
                .wait();

            if (wantContinue) continue;

            Ref<Event> vtxEvent = Ref<Event>::create(event);
            if (prevEventLoc && section->getPersistentRange() && *prevEventLoc < section->getPersistentRange()->max) {
                // We're inserting an event in the middle of the timeline
                vtxEvent->setEventLoc(*prevEventLoc + 1);
                section->insert(vtxEvent);
            } else {
                // We're appending an event to the end of the timeline
                section->maybeAppendSyncedEvent(vtxEvent);
            }
            db.insertEvent(vtxEvent, this);
            prevEventLoc = vtxEvent->getEventLoc();
            Q_ASSERT(prevEventLoc.has_value());

            if (vtxEvent->isStateEvent()) {
                gotStateEvents = true;
                currentState.mergeEvent(vtxEvent);
                db.insertCurrentStateEvent(vtxEvent, this);
            }
        }

        mergeSummary(user, room.summary);

        // TODO: Only do this when we get a room name, canonical alias, or heroes, or user display name (in the case of
        //       heroes) change
        if (gotStateEvents) {
            updateDisplayName();
        }

        db.commitTransaction();
    }

    void Room::mergeSummary(const UserInfo *user, const Vmx::ClientApi::R0::Sync::SyncEvents::RoomSummary &summary) {
        if (summary.heroes) {
            heroes[user] = *summary.heroes;
        }

        // TODO: Member counts
    }

    QVector<Vmx::Id::UserId> Room::dedupHeroes() {
        QVector<Vmx::Id::UserId> deduped;

        QHash<const UserInfo*, QVector<Vmx::Id::UserId>>::iterator iter = heroes.begin();
        while (iter != heroes.end()) {
            for (const Vmx::Id::UserId &user : iter.value()) {
                if (!deduped.contains(user)) {
                    deduped.append(user);
                }
            }

            ++iter;
        }

        return deduped;
    }

    void Room::updateDisplayName() {
        std::optional<Ref<Event>> nameEvent = currentState.get(M_ROOM_NAME, Vmx::InString::EMPTY);
        std::optional<Ref<Event>> aliasEvent = currentState.get(M_ROOM_CANONICAL_ALIAS, Vmx::InString::EMPTY);

        // We check against an empty string instead of using std::optional here
        // This is intentional - we don't want a room's display name to be empty if a room for some reason sets their
        // name to an empty string
        QString roomName = nameEvent ? (*nameEvent)->getContent()[QLatin1String("name")].toString() : QString();
        QString canonAlias = aliasEvent ? (*aliasEvent)->getContent()[QLatin1String("alias")].toString() : QString();

        if (!roomName.isEmpty()) {
            displayName = roomName;
        } else if (!canonAlias.isEmpty()) {
            displayName = canonAlias;
        } else {
            QVector<Vmx::Id::UserId> dedupedHeroes = dedupHeroes();
            if (!dedupedHeroes.isEmpty()) {
                QStringList list;
                for (const Vmx::Id::UserId &user : dedupedHeroes) {
                    list << getUserDisplayName(user);
                }

                displayName = list.join(QLatin1String(", "));
            } else {
                // TODO: Try generate a room name ourself depending on the first few members in the room

                displayName = roomId.fullId;
            }
        }
    }

    const Vmx::Id::RoomId& Room::getRoomId() const {
        return roomId;
    }

    const QString& Room::getDisplayName() const {
        return displayName;
    }

    const Timeline& Room::getTimeline() const {
        return timeline;
    }

    Timeline& Room::getTimeline() {
        return timeline;
    }

    RoomState& Room::getCurrentState() {
        return currentState;
    }

    const RoomState& Room::getCurrentState() const {
        return currentState;
    }

    QString Room::getUserDisplayName(const Vmx::Id::UserId &userId) {
        std::optional<Ref<Event>> mRoomMemberEvent = getCurrentState().get(M_ROOM_MEMBER, userId.fullId);
        if (mRoomMemberEvent) {
            const Ref<Event> &event = *mRoomMemberEvent;
            return event->getContent()[QLatin1String("displayname")].toString(userId.fullId);
        } else {
            return userId.fullId;
        }
    }

    void Room::setUserMembership(const UserInfo *user, Vmx::Membership membership) {
        // Don't store anything for LEAVE
        if (membership == Vmx::Membership::LEAVE) {
            userMemberships.remove(user);
        } else {
            userMemberships[user] = membership;
        }
    }

    Vmx::Membership Room::getUserMembership(const UserInfo *user) {
        auto membershipIter = userMemberships.find(user);
        if (membershipIter != userMemberships.end()) {
            return *membershipIter;
        } else {
            return Vmx::Membership::LEAVE;
        }
    }

    const QHash<const UserInfo*, Vmx::Membership>& Room::getUserMemberships() const {
        return userMemberships;
    }

    bool Room::isTimelineEventPurgeable(const Ref<Event> &event) {
        int refCount = event.getRefCount();
        if (refCount == 1) {
            return true;
        } else if (refCount == 2 && event->isStateEvent()) {
            // State events may be stored in 2 places at once (in the timeline, and in the current state)
            // So check if the ref count is 2, and if the other place it's stored is in the room's current state
            return currentState.hasEvent(event);
        }

        return false;
    }

    void Room::purgeUnused() {
        timeline.purgeUnused();

        // Purge state events
        int removedState = 0;
        currentState.removeAll([&removedState](const Vmx::InString &type, const Vmx::InString &stateKey, const Ref<Event> &event) {
            Q_UNUSED(type);
            Q_UNUSED(stateKey);

            if (event.getRefCount() == 1) {
                removedState++;
                return true;
            } else {
                return false;
            }
        });

        // The userMemberships hash should rarely ever change size
        userMemberships.squeeze();
    }

    void Room::onUserRemoved(UserInfo *user) {
        userMemberships.remove(user);
    }
}
