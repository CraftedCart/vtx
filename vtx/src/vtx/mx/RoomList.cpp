#include "vtx/mx/RoomList.hpp"
#include <algorithm>

namespace Vtx::Mx {
    RoomList::~RoomList() {
        qDeleteAll(rooms);
    }

    void RoomList::append(Room *value) {
        int len = rooms.length();

        emit onBeginInsertRows(QModelIndex(), len, len);
        rooms.append(value);
        emit onEndInsertRows();

        // Claim ownership of the room
        value->setParent(this);
    }

    const Room* RoomList::at(int i) const {
        return rooms.at(i);
    }

    int RoomList::length() const {
        return rooms.length();
    }

    Room* RoomList::findById(const Vmx::Id::RoomId &id) {
        auto iter = std::find_if(rooms.begin(), rooms.end(), [&id](Room* room) { return room->getRoomId() == id; });
        if (iter != rooms.end()) {
            return *iter;
        } else {
            return nullptr;
        }
    }

    Room* RoomList::operator[](int i) {
        return rooms[i];
    }

    const Room* RoomList::operator[](int i) const {
        return rooms[i];
    }

    RoomList::iterator RoomList::begin() {
        return rooms.begin();
    }

    RoomList::const_iterator RoomList::begin() const {
        return rooms.begin();
    }

    RoomList::iterator RoomList::end() {
        return rooms.end();
    }

    RoomList::const_iterator RoomList::end() const {
        return rooms.end();
    }
}
