#include "vtx/mx/RoomState.hpp"
#include "vtx/VtxInstance.hpp"

namespace Vtx::Mx {
    RoomState::RoomState(Room *room) : room(room) {}

    Ref<Event> RoomState::mergeEvent(const Vmx::Event::SyncStateEvent &value) {
        Ref<Event> event = Ref<Event>::create(value);

        state[value.type][value.stateKey] = event;

        return event;
    }

    int RoomState::cachedSize() const {
        return state.size();
    }

    void RoomState::mergeEvent(const Ref<Event> &value) {
        Q_ASSERT(value->isStateEvent());

        state[value->getType()][*(value->getStateKey())] = value;
    }

    std::optional<Ref<Event>> RoomState::mergeEventIfNew(const Vmx::Event::SyncStateEvent &value) {
        Vmx::InString type = value.type;
        Vmx::InString stateKey = value.stateKey;

        QHash<Vmx::InString, Ref<Event>> &typeHash = state[type];

        auto keyIter = typeHash.find(stateKey);
        if (keyIter == typeHash.end()) {
            Ref<Event> event = Ref<Event>::create(value);

            typeHash.insert(stateKey, event);
            return event;
        }

        return {};
    }

    bool RoomState::mergeEventIfNew(const Ref<Event> &value) {
        Q_ASSERT(value->isStateEvent());

        QHash<Vmx::InString, Ref<Event>> &typeHash = state[value->getType()];

        Vmx::InString stateKey = *value->getStateKey();
        auto keyIter = typeHash.find(stateKey);
        if (keyIter == typeHash.end()) {
            typeHash.insert(stateKey, value);
            return true;
        }

        return false;
    }

    bool RoomState::hasEvent(const Ref<Event> &event) {
        Q_ASSERT(event->isStateEvent());

        std::optional<Ref<Event>> stored = getCached(event->getType(), *event->getStateKey());
        if (!stored) return false;

        return *stored == event;
    }

    std::optional<Ref<Event>> RoomState::get(const Vmx::InString &type, const Vmx::InString &stateKey) const {
        const auto &typeIter = state.find(type);
        if (typeIter != state.end()) {
            const QHash<Vmx::InString, Ref<Event>> &typeHash = *typeIter;

            const auto &keyIter = typeHash.find(stateKey);
            if (keyIter != typeHash.end()) {
                return *keyIter;
            }
        }

        // The event wasn't found in the `state` hash, try fetch it from the local db if we can
        Db::Database& db = VtxInstance::getInstance()->getDatabase();

        std::optional<Ref<Event>> ret;
        db.getCurrentStateEvent(room, type, stateKey)
            .then([this, &ret, type, stateKey](Ref<Event> event) {
                if (event.isNull()) {
                    ret = std::nullopt;
                } else {
                    // Store the event in memory before returning it
                    state[type][stateKey] = event;
                    ret = event;
                }
             })
            .wait();

        return ret;
    }

    std::optional<Ref<Event>> RoomState::getCached(const Vmx::InString &type, const Vmx::InString &stateKey) const {
        const auto &typeIter = state.find(type);
        if (typeIter != state.end()) {
            const QHash<Vmx::InString, Ref<Event>> &typeHash = *typeIter;

            const auto &keyIter = typeHash.find(stateKey);
            if (keyIter != typeHash.end()) {
                return *keyIter;
            }
        }

        // Not found
        return {};
    }

    void RoomState::removeAll(std::function<bool(const Vmx::InString&, const Vmx::InString, const Ref<Event>&)> predicate) {
        QMutableHashIterator<Vmx::InString, QHash<Vmx::InString, Ref<Event>>> typeIter(state);
        while (typeIter.hasNext()) {
            typeIter.next();

            QMutableHashIterator<Vmx::InString, Ref<Event>> stateKeyIter(typeIter.value());
            while (stateKeyIter.hasNext()) {
                stateKeyIter.next();

                if (predicate(typeIter.key(), stateKeyIter.key(), stateKeyIter.value())) {
                    stateKeyIter.remove();
                }
            }

            if (typeIter.value().size() > 0) {
                typeIter.value().squeeze();
            } else {
                typeIter.remove();
            }
        }

        state.squeeze();
    }
}
