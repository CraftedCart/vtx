#include "vtx/mx/Timeline.hpp"
#include "vtx/VtxInstance.hpp"
#include "vtx/Iter.hpp"

namespace Vtx::Mx {
    Timeline::Timeline(Room *room) : room(room) {
        Db::Database& db = VtxInstance::getInstance()->getDatabase();

        db.getMaxSectionIdForRoom(room)
            .then([this](std::optional<int> sectionId) { currentSectionId = sectionId.value_or(0); })
            .fail([this]() { currentSectionId = 0; })
            .wait();
    }

    Room* Timeline::getRoom() {
        return room;
    }

    const Room* Timeline::getRoom() const {
        return room;
    }

    Ref<TimelineSection> Timeline::getCurrentSection() {
        return getSection(currentSectionId);
    }

    Ref<TimelineSection> Timeline::getSection(int sectionId) {
        if (sections.contains(sectionId)) {
            return sections[sectionId];
        } else {
            Ref<TimelineSection> section = Ref<TimelineSection>::create(this, sectionId);
            sections.insert(sectionId, section);
            return section;
        }
    }

    Ref<TimelineSection> Timeline::appendNewSection() {
        currentSectionId++;
        Ref<TimelineSection> section = Ref<TimelineSection>::create(this, currentSectionId);
        sections.insert(currentSectionId, section);

        emit currentSectionIdChanged(currentSectionId - 1, currentSectionId);

        return section;
    }

    int Timeline::numCachedSections() const {
        return sections.size();
    }

    void Timeline::purgeUnused() {
        // TODO: If we do multithreading stuff, we might want to lock access to the timeline or state or what not while
        //       we purge
        for (auto iter = sections.begin(); iter != sections.end();) {
            // Purge from the beginning of the timeline
            int toRemoveFromStart = 0;
            for (const Ref<Event> &event : **iter) {
                if (room->isTimelineEventPurgeable(event)) {
                    toRemoveFromStart++;
                } else {
                    break;
                }
            }

            if (toRemoveFromStart > 0) {
                iter.value()->removeFromStart(toRemoveFromStart);
            }

            // Purge from the end of the timeline
            int toRemoveFromEnd = 0;
            for (const Ref<Event> &event : Iter::reverse(**iter)) {
                if (room->isTimelineEventPurgeable(event)) {
                    toRemoveFromEnd++;
                } else {
                    break;
                }
            }

            if (toRemoveFromEnd > 0) {
                iter.value()->removeFromEnd(toRemoveFromEnd);
            }

            // TODO: When Qt 6 rolls around, QList should have a .squeeze func
            // timeline.squeeze();

            // Remove empty sections, if we hold the only ref to a section
            if (iter.value()->isEmpty() && iter.value().getRefCount() == 1) {
                iter = sections.erase(iter);
            } else {
                ++iter;
            }
        }
    }

    Ref<TimelineSection>& Timeline::operator[](int i) {
        return sections[i];
    }

    const Ref<TimelineSection> Timeline::operator[](int i) const {
        return sections[i];
    }
}
