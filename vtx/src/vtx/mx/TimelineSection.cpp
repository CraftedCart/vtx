#include "vtx/mx/TimelineSection.hpp"
#include "vtx/mx/Room.hpp"
#include "vtx/Iter.hpp"
#include "vtx/VtxInstance.hpp"
#include "vmx/clientapi/r0/message/GetMessageEvents.hpp"
#include "vmx/event/RoomEvent.hpp"
#include <QDir>
#include <QStandardPaths>
#include <QSqlDatabase>
#include <QSqlError>

namespace Vtx::Mx {
    TimelineSection::TimelineSection(Timeline *timeline, int sectionId) :
        timeline(timeline),
        sectionId(sectionId) {

        Db::Database& db = VtxInstance::getInstance()->getDatabase();
        db.getTimelineSectionOrderRange(this)
            .then([this](std::optional<EventLocRange> persistentRange) {
                this->persistentRange = persistentRange;
            })
            .wait();
    }

    void TimelineSection::append(Ref<Event> &value) {
        int len = events.length();

        value->setPrevEvent(events.length() > 0 ? &*events.last() : nullptr);

        if (range) {
            value->setEventLoc(++range->max);
            if (range->max.order > persistentRange->max.order) {
                persistentRange->max.order = range->max.order;
            }
        } else {
            if (persistentRange) {
                EventLoc newLoc = ++persistentRange->max;

                range = EventLocRange{
                    .min = newLoc,
                    .max = newLoc,
                };

                value->setEventLoc(newLoc);
            } else {
                range = EventLocRange{
                    .min = {sectionId, 0},
                    .max = {sectionId, 0},
                };
                persistentRange = EventLocRange{
                    .min = {sectionId, 0},
                    .max = {sectionId, 0},
                };

                value->setEventLoc(EventLoc{sectionId, 0});
            }
        }

        emit onBeginInsertRows(QModelIndex(), len, len);
        events.append(value);
        emit onEndInsertRows();

        Q_ASSERT(range->max <= persistentRange->max);
    }

    void TimelineSection::insert(Ref<Event> &value) {
        Q_ASSERT(value->getEventLoc().has_value());
        Q_ASSERT(value->getEventLoc()->section == sectionId);

        if (range) {
            // Drop events outside of the range
            if (*value->getEventLoc() > range->max || *value->getEventLoc() < range->min) return;

            int index = value->getEventLoc()->order - range->min.order;

            // Insert it
            events.insert(index, value);

            // Shift all future events up by 1
            for (int i = index + 1; i < events.length(); i++) {
                Ref<Event> event = events[i];
                event->setEventLoc((*event->getEventLoc()) + 1);
            }

            // Increase the range max
            ++range->max;
            ++persistentRange->max;
        } else {
            events.append(value);
            range = EventLocRange{*value->getEventLoc(), *value->getEventLoc()};

            if (persistentRange) {
                if (*value->getEventLoc() > persistentRange->max) {
                    persistentRange->max.order = value->getEventLoc()->order;
                } else if (*value->getEventLoc() < persistentRange->min) {
                    persistentRange->min.order = value->getEventLoc()->order;
                }
            } else {
                persistentRange = range;
            }
        }
    }

    bool TimelineSection::maybeAppendSyncedEvent(Ref<Event> &value) {
        if (!range || !persistentRange) {
            append(value);
            return true;
        }

        if (range->max == persistentRange->max) {
            append(value);
            return true;
        } else {
            value->setEventLoc(++persistentRange->max);
            return false;
        }
    }

    void TimelineSection::prepend(Ref<Event> &value) {
        if (events.length() > 0) {
            events.last()->setPrevEvent(&*value);
        }

        if (range) {
            value->setEventLoc(--range->min);
        } else {
            range = EventLocRange{
                .min = {sectionId, 0},
                .max = {sectionId, 0},
            };

            value->setEventLoc(EventLoc{sectionId, 0});
        }

        emit onBeginInsertRows(QModelIndex(), 0, 0);
        events.prepend(value);
        emit onEndInsertRows();
    }

    void TimelineSection::prependNoUpdate(Ref<Event> &value) {
        if (events.length() > 0) {
            events.last()->setPrevEvent(value);
        }

        if (range) {
            #ifndef QT_NO_DEBUG
            if (value->getEventLoc()) {
                Q_ASSERT(value->getEventLoc()->order == range->min.order - 1);
            }
            #endif

            if (range->min.order == persistentRange->min.order) --persistentRange->min;

            value->setEventLoc(--range->min);
        } else {
            EventLoc startLoc = value->getEventLoc() ? *value->getEventLoc() : EventLoc{sectionId, 0};
            Q_ASSERT(startLoc.section == sectionId);

            range = EventLocRange{
                .min = startLoc,
                .max = startLoc,
            };

            value->setEventLoc(startLoc);
        }

        events.prepend(value);
    }

    void TimelineSection::removeFromStart(int num) {
        emit onBeginRemoveRows(QModelIndex(), 0, num - 1);
        for (int i = 0; i < num; i++) {
            events.removeFirst();
        }

        range->min += num;
        if (range->min > range->max) range = {};

        emit onEndRemoveRows();
    }

    void TimelineSection::removeFromEnd(int num) {
        int len = length();

        emit onBeginRemoveRows(QModelIndex(), len - num, len - 1);
        for (int i = 0; i < num; i++) {
            events.removeLast();
        }

        range->max -= num;
        if (range->min > range->max) range = {};

        emit onEndRemoveRows();
    }

    const Ref<Event>& TimelineSection::at(int i) const {
        return events.at(i);
    }

    int TimelineSection::length() const {
        return events.length();
    }

    bool TimelineSection::isEmpty() const {
        return events.isEmpty();
    }

    const std::optional<EventLocRange>& TimelineSection::getRange() {
        return range;
    }

    const std::optional<EventLocRange>& TimelineSection::getPersistentRange() {
        return persistentRange;
    }

    Ref<Event>& TimelineSection::operator[](int i) {
        return events[i];
    }

    const Ref<Event>& TimelineSection::operator[](int i) const {
        return events[i];
    }

    TimelineSection::iterator TimelineSection::begin() {
        return events.begin();
    }

    TimelineSection::const_iterator TimelineSection::begin() const {
        return events.begin();
    }

    TimelineSection::iterator TimelineSection::end() {
        return events.end();
    }

    TimelineSection::const_iterator TimelineSection::end() const {
        return events.end();
    }

    TimelineSection::reverse_iterator TimelineSection::rbegin() {
        return events.rbegin();
    }

    TimelineSection::const_reverse_iterator TimelineSection::rbegin() const {
        return events.rbegin();
    }

    TimelineSection::reverse_iterator TimelineSection::rend() {
        return events.rend();
    }

    TimelineSection::const_reverse_iterator TimelineSection::rend() const {
        return events.rend();
    }

    TimelineSection::iterator TimelineSection::erase(TimelineSection::iterator pos) {
        return events.erase(pos);
    }

    int TimelineSection::getSectionId() const {
        return sectionId;
    }

    Timeline* TimelineSection::getTimeline() {
        return timeline;
    }

    const Timeline* TimelineSection::getTimeline() const {
        return timeline;
    }

    void TimelineSection::setPrevBatchToken(const UserInfo *user, const QString &prevBatchToken) {
        if (!prevBatchTokens.contains(user)) {
            // If a user is destroyed, forget about it's prev batch token
            connect(user, &UserInfo::removed, this, &TimelineSection::onUserRemoved);
        }

        prevBatchTokens.insert(user, prevBatchToken);

        // Persist it in the local db
        Db::Database& db = VtxInstance::getInstance()->getDatabase();
        db.insertPrevBatchToken(this, user, prevBatchToken);
    }

    std::optional<QString> TimelineSection::getPrevBatchToken(const UserInfo *user) const {
        auto iter = prevBatchTokens.find(user);
        if (iter != prevBatchTokens.end()) {
            return *iter;
        } else {
            // If it's not stored in memory, try fetch it from the local db
            Db::Database& db = VtxInstance::getInstance()->getDatabase();

            std::optional<QString> token;
            db.getPrevBatchToken(this, user)
                .then([&token](std::optional<QString> dbToken) { token = dbToken; })
                .fail([&token]() { token = std::nullopt; })
                .wait();

            if (token) prevBatchTokens[user] = *token; // Store it in memory

            return token;
        }
    }

    bool TimelineSection::hasPrevBatchTokenForUser(const UserInfo *user) const {
        return prevBatchTokens.contains(user);
    }

    void TimelineSection::removePrevBatchToken(const UserInfo *user) {
        disconnect(user, &UserInfo::removed, this, &TimelineSection::onUserRemoved);
        prevBatchTokens.remove(user);

        // Remove it from the local db
        Db::Database& db = VtxInstance::getInstance()->getDatabase();
        db.deletePrevBatchToken(this, user);
    }

    void TimelineSection::onUserRemoved(UserInfo *user) {
        prevBatchTokens.remove(user);
    }

    void TimelineSection::requestPastMessages(UserInfo *user, int max, bool allowRecurseOnError) {
        Db::Database& db = VtxInstance::getInstance()->getDatabase();

        if (range && persistentRange && range->min == persistentRange->min) {
            // We've reached the end of what we can fetch from the local db, fetch from the network instead
            requestPastMessagesFromNetwork(user, max);
            return;
        }

        QtPromise::QPromise<QVector<Ref<Event>>> fetchedPromise = isEmpty() ?
            db.getNewestEvents(timeline->getRoom(), max, sectionId) :
            db.getEventsBefore(timeline->getRoom(), max, range->min);

        fetchedPromise
            .then([&](QVector<Ref<Event>> fetched) {
                if (fetched.length() > 0) {
                    // We check that the order numbers are contiguous
                    int prevOrder = range ? range->min.order : fetched[0]->getEventLoc()->order;
                    int numToSkip = range ? 0 : 1;
                    int numToInsert = numToSkip;
                    for (Ref<Event> &event : Iter::skip(fetched, numToSkip)) {
                        int order = event->getEventLoc()->order;
                        if (order != prevOrder - 1) {
                            // It's not contiguous, wuh oh >.<
                            qCritical() << "Received non-contiguous list of events from the local db! Room" <<
                                timeline->getRoom()->getDisplayName() << "Section" << sectionId;

                            // Try and fix this up by deleting previous events from the local db
                            qInfo() << "Fixing up by deleting older non-contiguous events in section";
                            db.deleteSectionEventsBefore(timeline->getRoom(), EventLoc{sectionId, prevOrder});

                            persistentRange->min.order = prevOrder;

                            // TODO: Fix up the prev batch token too if any users have a prev batch token before the new min
                            //       persistent range

                            break;
                        }

                        numToInsert++;
                        prevOrder = order;
                    }

                    emit onBeginInsertRows(QModelIndex(), 0, numToInsert - 1);
                    for (Ref<Event> &event : Iter::take(fetched, numToInsert)) {
                        prependNoUpdate(event);
                    }
                    emit onEndInsertRows();
                } else {
                    // This could happen if we overwrite some events in the local db
                    qCritical() << "persistentRange got out of sync with the local db! Room" <<
                        timeline->getRoom()->getDisplayName() << "Section" << sectionId;

                    // Re-fetch the actual persistentRange
                    db.getTimelineSectionOrderRange(this)
                        .then([this](std::optional<EventLocRange> persistentRange) {
                            this->persistentRange = persistentRange;
                        })
                        .wait();

                    // Try again
                    if (allowRecurseOnError) requestPastMessages(user, max, false);
                }
            })
            .fail([=]() {

            })
            .wait();
    }

    void TimelineSection::requestPastMessagesFromNetwork(UserInfo *user, int max) {
        using namespace Vmx::ClientApi;
        using namespace Vmx::ClientApi::R0::Message;
        using namespace Vmx::ClientApi::R0::Filter; // TODO: Use a filter stored on the server

        if (requestingPastMessages) return;

        std::optional<QString> prevToken = getPrevBatchToken(user);
        if (!prevToken) return;

        requestingPastMessages = true;

        emit beginRequestingPastMessages();

        VtxInstance *inst = VtxInstance::getInstance();
        GetMessageEvents::GetRequest req = {
            .roomId = timeline->getRoom()->getRoomId(),
            .from = *prevToken,
            .dir = GetMessageEvents::Direction::BACKWARDS,
            .limit = max,
            .filter = FilterDef{
                .room = RoomFilter{
                    .state = StateFilter{ .lazyLoadMembers = true },
                    .timeline = RoomEventFilter{ .lazyLoadMembers = true },
                },
            },
        };

        // Prevent ourselves from being destroyed while the request is in-flight
        refFromThis().ref();
        inst->getMatrix()->requestImmediate(req, user->getSession().value(), new TaskInfo(tr("Fetching past messages")))
            .then([this, inst, user](const GetMessageEvents::GetResponse &resp) {
                Db::Database& db = VtxInstance::getInstance()->getDatabase();
                db.startTransaction();

                for (const Vmx::Event::SyncStateEvent &event : resp.state) {
                    std::optional<Ref<Event>> newEvent = timeline->getRoom()->getCurrentState().mergeEventIfNew(event);

                    // If the event is new, add it to the local db
                    if (newEvent) db.insertCurrentStateEvent(*newEvent, timeline->getRoom());
                }

                emit onBeginInsertRows(QModelIndex(), 0, resp.chunk.length() - 1);
                for (const Vmx::Event::SyncRoomEvent &event : resp.chunk) {
                    Ref<Event> vtxEvent = Ref<Event>::create(event);
                    prependNoUpdate(vtxEvent);
                    db.insertEvent(vtxEvent, timeline->getRoom());

                    if (vtxEvent->isStateEvent()) {
                        if (timeline->getRoom()->getCurrentState().mergeEventIfNew(vtxEvent)) {
                            // If the event is a new state event, add it to the local db
                            db.insertCurrentStateEvent(vtxEvent, timeline->getRoom());
                        }
                    }
                }
                emit onEndInsertRows();

                // TODO: We should probably check whether a user still exists since a user might log out of a
                // user
                Q_ASSERT(inst->getMatrix()->getUsers().contains(user) && "Fetched past messages for user that is no longer registered in Matrix object");

                if (resp.end) {
                    setPrevBatchToken(user, *resp.end);
                } else {
                    removePrevBatchToken(user);
                }

                db.commitTransaction();

                requestingPastMessages = false;

                emit finishedRequestingPastMessages();
            })
            .fail([](const ApiError &err) {
                qDebug() << "ERR" << err.httpStatus;
                qDebug() << "ERR" << err.message;

                // TODO: Wait a while and retry
            })
            .finally([this]() {
                // Network request done - we don't need to prevent ourself from being destroyed any more
                refFromThis().deref();
            });
    }

    bool TimelineSection::isRequestingPastMessages() {
        return requestingPastMessages;
    }
}
