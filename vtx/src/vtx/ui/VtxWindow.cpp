#include "vtx/ui/VtxWindow.hpp"
#include "vtx/ui_VtxWindow.h"
#include "vtx/widget/LoginWidget.hpp"
#include "vtx/widget/MainView.hpp"
#include "vtx/widget/StatusTasksLabel.hpp"
#include "vtx/VtxInstance.hpp"
#include "vmx/clientapi/r0/account/WhoAmI.hpp"
#include "vmx/util/Adt.hpp"
#include <QNetworkRequest>
#include <QHttpMultiPart>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonDocument>
#include <QDebug>

using namespace Vmx::Util;

namespace Vtx::UI {
    VtxWindow::VtxWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::VtxWindow) {
        ui->setupUi(this);

        // Quit the app if this main window is closed, regardless of if any other windows are open
        setAttribute(Qt::WA_DeleteOnClose);
        connect(this, &QObject::destroyed, qApp, &QCoreApplication::quit, Qt::QueuedConnection);

        Widget::StatusTasksLabel *statusTasksLabel = new Widget::StatusTasksLabel(this);
        statusBar()->addWidget(statusTasksLabel);

        VtxInstance *inst = VtxInstance::getInstance();

        QVector<UserInfo*> loggedInUsers;
        for (UserInfo *user : inst->getMatrix()->getUsers()) {
            if (user->getSession()) {
                loggedInUsers.append(user);
            }
        }

        if (loggedInUsers.length() > 0) {
            // TODO: Do this for all accounts
            fetchAccountData(loggedInUsers[0]).then([this]() { switchToMainView(); });
        } else {
            Widget::LoginWidget *loginWidget = new Widget::LoginWidget(this);
            connect(loginWidget, &Widget::LoginWidget::loginSuccess, this, &VtxWindow::onLoginSuccess, Qt::QueuedConnection);
            setCentralWidget(loginWidget);
        }
    }

    VtxWindow::~VtxWindow() {
        delete ui;
    }

    void VtxWindow::onLoginSuccess(const Vmx::ClientApi::R0::Session::Login::PostResponse &resp) {
        VtxInstance::getInstance()->getMatrix()->onLoginSuccess(resp).expect("Initial login should not be rejected");
        switchToMainView();
    }

    QtPromise::QPromise<void> VtxWindow::fetchAccountData(UserInfo *user) {
        TaskInfo *task = new TaskInfo(tr("Fetching account data for %1").arg(user->getUserId().fullId));
        task->start();

        // Figure out who we are
        return VtxInstance::getInstance()->getMxClient()->request(
                Vmx::ClientApi::R0::Account::WhoAmI::GetRequest(),
                *user->getSession()
                )
        .then([user](const Vmx::ClientApi::R0::Account::WhoAmI::GetResponse &resp) {
            user->setUserId(resp.userId);
        })
        .fail([=](const Vmx::ClientApi::ApiError &error) {
            QString friendlyErrorString;
            QString detailErrorString;

            Adt::matchVariant(error.errorCode) (
                [&](const Vmx::ClientApi::MxApiError &e) {
                    friendlyErrorString = error.message;
                    detailErrorString = Vmx::ClientApi::errorCodeToString(e);
                },
                [&](const Vmx::ClientApi::OtherApiError &e) {
                    friendlyErrorString = error.message;
                    detailErrorString = e.errorCode;
                },
                [&](const Vmx::ClientApi::BadResponseError &e) {
                    friendlyErrorString = tr("Received a bad response from the server");
                    detailErrorString = Adt::matchVariant(e.error) (
                        [&](const Vmx::Serde::WrongFormat&) { return tr("Response was in the wrong format"); },
                        [&](const Vmx::Serde::InvalidType&) { return tr("Invalid type in response"); },
                        [&](const Vmx::Serde::InvalidValue&) { return tr("Invalid value in response"); },
                        [&](const Vmx::Serde::MissingField&) { return tr("Missing field in response"); }
                    );
                }
            );

            qCritical() << "API error logging in" << friendlyErrorString << "/" << detailErrorString;

            // TODO: Show this on the UI somehow
        })
        .fail([=](const Vmx::NetError &error) {
            qCritical() << "Network error";
            // TODO: Show this on the UI somehow
        })
        .finally([=]() {
            task->finishAndDelete();
        });
    }

    void VtxWindow::switchToMainView() {
        centralWidget()->deleteLater();
        setCentralWidget(new Widget::MainView(this));
    }
}
