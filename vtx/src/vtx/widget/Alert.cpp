#include "vtx/widget/Alert.hpp"
#include <QVBoxLayout>
#include <QStyleOption>
#include <QPainter>

namespace Vtx::Widget {
    Alert::Alert(QWidget *parent) : QWidget(parent) {
        label = new QLabel(this);
        detailLabel = new QLabel(this);

        QFont font = detailLabel->font();
        font.setPointSize(8);
        detailLabel->setFont(font);

        setLayout(new QVBoxLayout(this));

        layout()->addWidget(label);
        layout()->addWidget(detailLabel);
    }

    // We override paintEvent so we can apply custom styles to this class in a stylesheet
    void Alert::paintEvent(QPaintEvent *event) {
        Q_UNUSED(event);

        QStyleOption opt;
        opt.init(this);
        QPainter p(this);
        style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
    }

    void Alert::setText(const QString &text, const std::optional<QString> &detailText) {
        label->setText(text);

        if (detailText) {
            detailLabel->setText(*detailText);
            detailLabel->setVisible(true);
        } else {
            detailLabel->setVisible(false);
        }
    }
}
