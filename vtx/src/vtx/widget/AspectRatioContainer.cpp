#include "vtx/widget/AspectRatioContainer.hpp"
#include <QLayout>

namespace Vtx::Widget {

    AspectRatioContainer::AspectRatioContainer(QWidget *parent) : QWidget(parent) {
        QSizePolicy policy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        policy.setHeightForWidth(true);
        this->setSizePolicy(policy);
    }

    int AspectRatioContainer::heightForWidth(int width) const {
        return getInnerWidgetSize(QSize(width, maximumHeight())).height();
    }

    bool AspectRatioContainer::hasHeightForWidth() const {
        return true;
    }

    QSize AspectRatioContainer::sizeHint() const {
        return getInnerWidgetSize(maximumSize());
    }

    QSize AspectRatioContainer::minimumSizeHint() const {
        return QSize(0, 0);
    }

    void AspectRatioContainer::setWidget(QWidget *widget) {
        this->widget = widget;
        widget->setParent(this);
        widget->show();
        resizeInnerWidget();
    }

    void AspectRatioContainer::setDesiredSize(const QSize &desiredSize) {
        this->desiredSize = desiredSize;
        resizeInnerWidget();
    }

    const QSize& AspectRatioContainer::getDesiredSize() const {
        return desiredSize;
    }

    void AspectRatioContainer::resizeEvent(QResizeEvent *event) {
        Q_UNUSED(event);

        resizeInnerWidget();
    }

    void AspectRatioContainer::resizeInnerWidget() {
        if (widget == nullptr) return;
        widget->setFixedSize(getInnerWidgetSize(this->size()));
    }

    QSize AspectRatioContainer::getInnerWidgetSize(const QSize &maxSize) const {
        QSize size = desiredSize;

        if (size.width() > maxSize.width()) {
            float scaleFac = (float) size.width() / maxSize.width();
            size /= scaleFac;
        }

        if (size.height() > maxSize.height()) {
            float scaleFac = (float) size.height() / maxSize.height();
            size /= scaleFac;
        }

        return size;
    }

}
