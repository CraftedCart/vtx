#include "vtx/widget/ImageLabel.hpp"
#include <QResizeEvent>

namespace Vtx::Widget {
    void ImageLabel::resizeEvent(QResizeEvent *event) {
        if (pm) {
            QLabel::setPixmap(pm->scaled(event->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
        }
    }

    void ImageLabel::setPixmap(Vmx::Arc<QPixmap> pm) {
        this->pm = pm;

        QLabel::setPixmap(pm->scaled(size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
    }
}
