#include "vtx/widget/LoginWidget.hpp"
#include "vtx/ui_LoginWidget.h"
#include "vtx/TaskInfo.hpp"
#include "vtx/VtxInstance.hpp"
#include "vmx/util/Adt.hpp"
#include <QInputDialog>

using namespace Vmx::Util;

namespace Vtx::Widget {
    LoginWidget::LoginWidget(QWidget *parent) : QWidget(parent), ui(new Ui::LoginWidget) {
        ui->setupUi(this);

        ui->alert->setVisible(false);

        connect(ui->loginButton, &QPushButton::clicked, this, &LoginWidget::logIn);
        connect(ui->changeHomeserverButton, &QPushButton::clicked, this, &LoginWidget::changeHomeserver);
    }

    LoginWidget::~LoginWidget() {
        delete ui;
    }

    void LoginWidget::logIn() {
        TaskInfo *task = new TaskInfo(tr("Logging in"));
        task->start();

        ui->signInContainer->setEnabled(false);

        VtxInstance::getInstance()->getMxClient()->logInWithPassword(
                ui->usernameLineEdit->text(),
                ui->passwordLineEdit->text(),
                homeserverUrl
                )
        .then([this](const Vmx::ClientApi::R0::Session::Login::PostResponse &resp) {
            emit loginSuccess(resp);
        })
        .fail([=](const Vmx::ClientApi::ApiError &error) {
            QString friendlyErrorString;
            QString detailErrorString;

            Adt::matchVariant(error.errorCode) (
                [&](const Vmx::ClientApi::MxApiError &e) {
                    friendlyErrorString = error.message;
                    detailErrorString = Vmx::ClientApi::errorCodeToString(e);
                },
                [&](const Vmx::ClientApi::OtherApiError &e) {
                    friendlyErrorString = error.message;
                    detailErrorString = e.errorCode;
                },
                [&](const Vmx::ClientApi::BadResponseError &e) {
                    friendlyErrorString = tr("Received a bad response from the server");
                    detailErrorString = Adt::matchVariant(e.error) (
                        [&](const Vmx::Serde::WrongFormat&) { return tr("Response was in the wrong format"); },
                        [&](const Vmx::Serde::InvalidType&) { return tr("Invalid type in response"); },
                        [&](const Vmx::Serde::InvalidValue&) { return tr("Invalid value in response"); },
                        [&](const Vmx::Serde::MissingField&) { return tr("Missing field in response"); }
                    );
                }
            );

            qDebug() << "API error logging in" << friendlyErrorString << "/" << detailErrorString;

            ui->signInContainer->setEnabled(true);
            ui->alert->setVisible(true);
            ui->alert->setText(friendlyErrorString, detailErrorString);
        })
        .fail([=](const Vmx::NetError &error) {
            qDebug() << "Network error logging in" << error.error << error.errorString;

            ui->signInContainer->setEnabled(true);
            ui->alert->setVisible(true);
            ui->alert->setText(tr("Network error: %1").arg(error.errorString));
        })
        .finally([=]() {
            task->finishAndDelete();
        });
    }

    void LoginWidget::changeHomeserver() {
        // TODO: Pretty this up, check what login options a homeserver provides, check if a homeserver can be reached

        bool ok;
        QString newUrl = QInputDialog::getText(
                this,
                tr("Change homeserver"),
                tr("Homeserver URL"),
                QLineEdit::Normal,
                homeserverUrl.toString(),
                &ok
                );

        if (ok && !newUrl.isEmpty()) {
            homeserverUrl = QUrl::fromUserInput(newUrl);

            ui->loginLabel->setText(
                    tr("<html><head/><body><p>Sign in to your Matrix account on <span style=\"font-weight:600;\">%1</span></p></body></html>")
                    .arg(homeserverUrl.toString().toHtmlEscaped())
                    );
        }
    }

    void LoginWidget::showErrorMessage(const QString &friendlyErrorString, const std::optional<QString> &detailErrorString) {
        ui->signInContainer->setEnabled(true);
        ui->alert->setVisible(true);
        ui->alert->setText(friendlyErrorString, detailErrorString);
    }
}
