#include "vtx/widget/MainView.hpp"
#include "vtx/ui_MainView.h"
#include "vtx/TaskInfo.hpp"
#include "vtx/VtxInstance.hpp"
#include "vmx/clientapi/r0/message/CreateMessageEvent.hpp"
#include "vmx/util/Adt.hpp"

/** @brief Strips whitespace from the end of a QString */
static QString rstrip(const QString& str) {
    int n = str.size() - 1;
    for (; n >= 0; --n) {
        if (!str.at(n).isSpace()) {
            return str.left(n + 1);
        }
    }

    return QString();
}

namespace Vtx::Widget {
    MainView::MainView(QWidget *parent) : QWidget(parent), ui(new Ui::MainView) {
        ui->setupUi(this);

        connect(ui->sendButton, &QPushButton::clicked, this, &MainView::send);

        roomListModel = new Model::RoomListModel(&VtxInstance::getInstance()->getMatrix()->getRooms(), this);
        ui->roomListView->setModel(roomListModel);

        connect(ui->roomListView->selectionModel(), &QItemSelectionModel::currentChanged, this, &MainView::roomSelectionChanged);

        timelineModel = new Model::TimelineModel(this);
        ui->timelineView->setModel(timelineModel);

        ui->roomListView->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);

        connect(ui->messageTextEdit, &MessageTextEdit::sendRequested, this, &MainView::send);

        VtxInstance::getInstance()->getMatrix()->startSyncingAllUsers();
    }

    MainView::~MainView() {
        delete ui;
    }

    void MainView::send() {
        QString body = rstrip(ui->messageTextEdit->toPlainText());
        if (body.isEmpty()) return;

        VtxInstance *inst = VtxInstance::getInstance();

        QDateTime now = QDateTime::currentDateTime();

        Vmx::ClientApi::R0::Message::CreateMessageEvent::PutRequest req;
        req.roomId = roomListModel->roomIdForIndex(ui->roomListView->selectionModel()->selectedIndexes()[0]);
        req.txnId = QString::number(now.toMSecsSinceEpoch()); // TODO: Append incrementing counter value
        req.eventType = QLatin1String("m.room.message");
        req.content["msgtype"] = "m.text";
        req.content["body"] = body;

        ui->messageTextEdit->clear();

        // TODO: Use a room queue
        inst->getMatrix()->requestQueuedGlobal(req, inst->getActiveUser()->getSession().value(), new TaskInfo(tr("Sending message")))
            .then([](const Vmx::ClientApi::R0::Message::CreateMessageEvent::PutResponse &resp) {
                qDebug() << "Sent message and got event ID" << resp.eventId.fullId;
            })
            .fail([](const Vmx::ClientApi::ApiError &err) {
                // TODO: Handle this better
                qDebug() << "ERR" << err.httpStatus << err.message;

                Vmx::Util::Adt::matchVariant(err.errorCode) (
                    [](const Vmx::ClientApi::MxApiError &e) {
                        qDebug() << "MXAPIERROR" << (int) e;
                    },
                    [](const Vmx::ClientApi::OtherApiError &e) {
                        qDebug() << "OTHERAPUERROR" << e.errorCode;
                    },
                    [](const Vmx::ClientApi::BadResponseError&) {
                        qDebug() << "BADRESPONSEERROR";
                    }
                );
            })
            .fail([](const Vmx::NetError &error) {
                // TODO: Handle this better
                qDebug() << "ERR" << error.error << error.errorString;
            });
    }

    void MainView::roomSelectionChanged(const QModelIndex &current, const QModelIndex &previous) {
        Q_UNUSED(previous);

        Mx::Room *room = VtxInstance::getInstance()->getMatrix()->getRooms()[current.row()];
        timelineModel->setSection(room, room->getTimeline().getCurrentSection());
    }
}
