#include "vtx/widget/MessageTextEdit.hpp"
#include <QKeyEvent>
#include <QTimer>

namespace Vtx::Widget {
    MessageTextEdit::MessageTextEdit(QWidget *parent) : QTextEdit(parent) {
        installEventFilter(this);

        connect(this, &QTextEdit::textChanged, this, &MessageTextEdit::onTextChanged);
    }

    QSize MessageTextEdit::sizeHint() const {
        QTextDocument *doc = document();

        float docHeight = doc->size().toSize().height();
        float fontHeight = QFontMetrics(doc->defaultFont()).height() + (doc->documentMargin() * 2);

        return QSize(0, qMax(docHeight, fontHeight));
    }

    QSize MessageTextEdit::minimumSizeHint() const {
        return sizeHint();
    }

    bool MessageTextEdit::eventFilter(QObject *watched, QEvent *event) {
        Q_UNUSED(watched);

        if (event->type() == QEvent::KeyPress) {
            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
            switch (keyEvent->key()) {
                case Qt::Key_Return:
                case Qt::Key_Enter:
                    if (!(keyEvent->modifiers() & Qt::ShiftModifier)) {
                        emit sendRequested();
                        return true;
                    }
            }
        }

        return false;
    }

    void MessageTextEdit::onTextChanged() {
        updateGeometry();
    }
}
