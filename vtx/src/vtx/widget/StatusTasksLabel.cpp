#include "vtx/widget/StatusTasksLabel.hpp"
#include "vtx/VtxInstance.hpp"
#include <QHBoxLayout>
#include <QResizeEvent>

namespace Vtx::Widget {
    StatusTasksLabel::StatusTasksLabel(QWidget *parent) : QWidget(parent) {
        throbber = new Throbber(this);
        label = new QLabel(this);

        setLayout(new QHBoxLayout(this));
        layout()->setContentsMargins(0, 0, 0, 0);

        layout()->addWidget(throbber);
        layout()->addWidget(label);

        throbber->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

        connect(VtxInstance::getInstance(), &VtxInstance::taskListChanged, this, &StatusTasksLabel::onTaskListChanged);

        // Default to hidden
        setVisible(false);
    }

    void StatusTasksLabel::resizeEvent(QResizeEvent *event) {
        QWidget::resizeEvent(event);

        throbber->setMinimumSize(event->size().height(), event->size().height());
    }

    void StatusTasksLabel::onTaskListChanged(const QVector<TaskInfo*> &tasks) {
        if (tasks.length() == 0) {
            setVisible(false);
        } else {
            setVisible(true);

            if (tasks.length() == 1) {
                label->setText(tasks[0]->getName());
            } else {
                label->setText(tr("%1 tasks").arg(tasks.length()));
            }
        }
    }
}
