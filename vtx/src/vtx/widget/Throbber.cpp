#include "vtx/widget/Throbber.hpp"
#include <QPainter>
#include <QtMath>

namespace Vtx::Widget {
    Throbber::Throbber(QWidget *parent) : QWidget(parent) {
        elapsedTimer.start();
    }

    Throbber::~Throbber() {}

    void Throbber::paintEvent(QPaintEvent *event) {
        Q_UNUSED(event);

        const qreal WIDTH = 4;
        const qreal HALF_WIDTH = WIDTH / 2;

        QRect drawingRect;
        drawingRect.setX(rect().x() + HALF_WIDTH);
        drawingRect.setY(rect().y() + HALF_WIDTH);
        drawingRect.setWidth(rect().width() - WIDTH);
        drawingRect.setHeight(rect().height() - WIDTH);

        QPainter painter(this);
        painter.setRenderHints(QPainter::Antialiasing);

        qint64 turn = -elapsedTimer.elapsed() * 4;
        int start = turn - (qSin(turn * 0.0004) * 0.4 + 0.6) * 100 * 16;
        qreal startPercent = (start % 5760) / 5760.0;

        QConicalGradient gradient;
        gradient.setCenter(drawingRect.center());
        gradient.setAngle(startPercent * 360.0);
        gradient.setColorAt(0.0, QColor(255, 255, 255));
        gradient.setColorAt(qSin(turn * 0.0004) * 0.4 + 0.6, QColor(255, 255, 255, 0));

        QPen pen = painter.pen();
        pen.setStyle(Qt::SolidLine);
        pen.setBrush(gradient);
        pen.setWidth(WIDTH);
        painter.setPen(pen);

        painter.drawEllipse(drawingRect);

        update();
    }
}
