#include "vtx/widget/TimelineItem.hpp"
#include "vtx/widget/AspectRatioContainer.hpp"
#include "vtx/widget/ImageLabel.hpp"
#include "vtx/VtxInstance.hpp"
#include <QVBoxLayout>
#include <QHash>

static const Vmx::InString M_ROOM_MESSAGE = Vmx::InString(QLatin1String("m.room.message"));
static const Vmx::InString M_ROOM_MEMBER = Vmx::InString(QLatin1String("m.room.member"));

static int hashCode(const QString &str) {
    int hash = 0;
    if (str.length() == 0) {
        return hash;
    }
    for (int i = 0; i < str.length(); i++) {
        ushort chr = str.at(i).unicode();
        hash = ((hash << 5) - hash) + chr;
    }
    return qAbs(hash);
}

static QColor colorForUser(const Vmx::Id::UserId &id) {
    // Colors pinched from the Element client
    static const QColor USERNAME_COLORS[] = {
        "#368BD6",
        "#AC3BA8",
        "#0DBD8B",
        "#E64F7A",
        "#FF812D",
        "#2DC2C5",
        "#5C56F5",
        "#74D12C",
    };

    const int colorNumber = hashCode(id.fullId) % 8;

    return USERNAME_COLORS[colorNumber];
}

struct MemberChangedText {
    /** @brief The state_key was the same as the sender */
    const char *stateKeySame;

    /** @brief The state_key was not the same as the sender */
    const char *stateKeyDifferent;

    MemberChangedText() = default;
    MemberChangedText(const char *text) : stateKeySame(text), stateKeyDifferent(text) {};
    MemberChangedText(const char *stateKeySame, const char *stateKeyDifferent) : stateKeySame(stateKeySame), stateKeyDifferent(stateKeyDifferent) {};
};

static const QHash<QString, QHash<QString, MemberChangedText>> MEMBERSHIP_CHANGED_TEXT = {
    {QLatin1String("invite"), {
        {QLatin1String("invite"), MemberChangedText(QT_TRANSLATE_NOOP("TimelineItem", "$actor reinvited $affected"))},
        {QLatin1String("join"), MemberChangedText(QT_TRANSLATE_NOOP("TimelineItem", "$affected accepted the invite"))},
        {QLatin1String("leave"), MemberChangedText(QT_TRANSLATE_NOOP("TimelineItem", "$affected rejected the invite"), QT_TRANSLATE_NOOP("TimelineItem", "$actor withdrew $affected's invitation"))},
        {QLatin1String("ban"), MemberChangedText(QT_TRANSLATE_NOOP("TimelineItem", "$actor banned $affected"))},
    }},
    {QLatin1String("join"), {
        {QLatin1String("join"), MemberChangedText(QT_TRANSLATE_NOOP("TimelineItem", "$affected changed their name/avatar"))}, // TODO: Figure out which one was changed, or maybe both, or maybe neither
        {QLatin1String("leave"), MemberChangedText(QT_TRANSLATE_NOOP("TimelineItem", "$affected left the room"), QT_TRANSLATE_NOOP("TimelineItem", "$actor kicked $affected"))},
        {QLatin1String("ban"), MemberChangedText(QT_TRANSLATE_NOOP("TimelineItem", "$actor banned $affected"))},
    }},
    {QLatin1String("leave"), {
        {QLatin1String("invite"), MemberChangedText(QT_TRANSLATE_NOOP("TimelineItem", "$actor invited $affected"))},
        {QLatin1String("join"), MemberChangedText(QT_TRANSLATE_NOOP("TimelineItem", "$affected joined the room"))},
        {QLatin1String("leave"), MemberChangedText(QT_TRANSLATE_NOOP("TimelineItem", "$affected is still not in the room"))},
        {QLatin1String("ban"), MemberChangedText(QT_TRANSLATE_NOOP("TimelineItem", "$actor banned $affected"))},
    }},
    {QLatin1String("ban"), {
        {QLatin1String("leave"), MemberChangedText(QT_TRANSLATE_NOOP("TimelineItem", "$actor unbanned $affected"))},
        {QLatin1String("ban"), MemberChangedText(QT_TRANSLATE_NOOP("TimelineItem", "$affected is still banned"))},
    }},
};

namespace Vtx::Widget {
    TimelineItem::TimelineItem(QWidget *parent) : QWidget(parent) {
        QVBoxLayout *layout = new QVBoxLayout(this);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(0);
        setLayout(layout);

        setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum));
    }

    void TimelineItem::setEvent(const Ref<Mx::Event> &event, const Mx::Room *room) {
        Q_ASSERT(this->event.isNull() && "Tried to set a TimelineItem's event twice");
        Q_ASSERT(this->room == nullptr && "Tried to set a TimelineItem's room twice");

        this->event = event;
        this->room = room;

        switch (getStyleForEvent(event)) {
            case EventStyle::MESSAGE:
                styleAsMessage();
                break;
            case EventStyle::STATUS:
                styleAsStatus();
                break;
            case EventStyle::DEBUG:
                styleAsDebug();
                break;
        }

        // QLabel *locLabel = new QLabel(this);
        // layout()->addWidget(locLabel);

        // if (event->getEventLoc()) {
            // locLabel->setText(QLatin1String("LOC: %1 %2").arg(QString::number(event->getEventLoc()->section), QString::number(event->getEventLoc()->order)));
        // } else {
            // locLabel->setText(QLatin1String("LOC: None"));
        // }
    }

    void TimelineItem::styleAsMessage() {
        QLabel *userNameLabel = new QLabel(this);
        QLabel *contentLabel = new QLabel(this);

        userNameLabel->setTextFormat(Qt::PlainText);
        userNameLabel->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
        userNameLabel->setCursor(Qt::IBeamCursor);
        QFont nameFont = userNameLabel->font();
        nameFont.setBold(true);
        userNameLabel->setFont(nameFont);

        contentLabel->setTextFormat(Qt::PlainText);
        contentLabel->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
        contentLabel->setWordWrap(true);
        contentLabel->setCursor(Qt::IBeamCursor);

        contentLabel->setText(event->getContent()["body"].toString());

        layout()->addWidget(userNameLabel);
        layout()->addWidget(contentLabel);

        if (event->getContent()[QLatin1String("msgtype")].toString() == QLatin1String("m.image")) {
            VtxInstance *inst = VtxInstance::getInstance();
            Q_ASSERT(inst->getActiveUser()->getSession() && "Active user has no session!");

            QPointer<TimelineItem> self(this);

            QString imageUrl = event->getContent()["info"].toObject()["thumbnail_url"].toString();
            if (imageUrl.isEmpty()) {
                // No thumbnail - use the actual image then
                imageUrl = event->getContent()["url"].toString();
            }

            if (!imageUrl.isEmpty()) {
                QUrl url = QUrl(imageUrl);
                if (!url.host().isEmpty() && !url.path().remove(0, 1).isEmpty()) {
                    inst->getMatrix()->getMxcPixmap(
                            url,
                            *inst->getActiveUser()->getSession()
                            )
                    .then([self](Vmx::Arc<QPixmap> image) {
                        if (self) {
                            // TODO: Support animated images (.gif, maybe .apng)
                            // also TODO: Handle scrolling changes on widget resize when an image loads in or what not
                            ImageLabel *imageLabel = new ImageLabel(self);
                            imageLabel->setPixmap(image);

                            AspectRatioContainer *container = new AspectRatioContainer(self);
                            container->setMaximumHeight(800);
                            container->setDesiredSize(QSize(image->width(), image->height()));
                            container->setWidget(imageLabel);

                            self->layout()->addWidget(container);
                        }
                    });
                }
            }
        }

        // Determine if we should even show the user name label at all by checking if previous events should have it
        // shown
        // TODO: Update this as we load more history
        const Mx::Event *prevEvent = event->getPrevEvent();
        if (prevEvent == nullptr) {
            setUserNameVisibility(true, userNameLabel);
            return;
        }

        if (!(prevEvent->getSender() == event->getSender() && getStyleForEvent(prevEvent) == EventStyle::MESSAGE)) {
            setUserNameVisibility(true, userNameLabel);
            return;
        }

        setUserNameVisibility(false, userNameLabel);
    }

    void TimelineItem::styleAsStatus() {
        QLabel *contentLabel = new QLabel(this);
        layout()->addWidget(contentLabel);

        contentLabel->setTextFormat(Qt::PlainText);
        contentLabel->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
        contentLabel->setWordWrap(true);
        contentLabel->setCursor(Qt::IBeamCursor);
        contentLabel->setStyleSheet(QLatin1String("color: #AAFFFFFF;"));

        const QString &messageType = event->getType();
        if (messageType == QLatin1String("m.room.member")) {
            styleMembershipEvent(contentLabel);
        } else {
            contentLabel->setText(tr("Vtx error: Cannot style event as a status: %1").arg(event->getType()));
        }
    }

    void TimelineItem::styleMembershipEvent(QLabel *label) {
        QString membership = event->getContent()[QLatin1String("membership")].toString();
        QString affectedUserId = event->getStateKey().value_or(QLatin1String("???"));
        QString affectedUser = event->getContent()[QLatin1String("displayname")].toString(affectedUserId);
        const Vmx::Id::UserId &actor = event->getSender();

        QString prevMembership;
        if (event->getUnsignedData()) {
            prevMembership = (*event->getUnsignedData()).json[QLatin1String("prev_content")].toObject()[QLatin1String("membership")].toString();
        }
        if (prevMembership.isEmpty()) prevMembership = QLatin1String("leave");

        if (MEMBERSHIP_CHANGED_TEXT.contains(prevMembership)) {
            const QHash<QString, MemberChangedText> &texts = MEMBERSHIP_CHANGED_TEXT[prevMembership];
            if (texts.contains(membership)) {
                const char *textKey = affectedUserId == actor ? texts[membership].stateKeySame : texts[membership].stateKeyDifferent;

                QString text = tr(textKey)
                    .replace(QLatin1String("$actor"), actor.fullId)
                    .replace(QLatin1String("$affected"), affectedUser);

                // Add the reason on if there was one
                if (event->getContent().contains(QLatin1String("reason")) && event->getContent()[QLatin1String("reason")].isString()) {
                    text += QLatin1String("\n") + tr("Reason: %1").arg(event->getContent()["reason"].toString());
                }

                label->setText(text);
            } else {
                label->setText(tr("User %1's membership went from %2 -> %3").arg(affectedUser, prevMembership, membership));
            }
        } else {
            label->setText(tr("User %1's membership went from %2 -> %3").arg(affectedUser, prevMembership, membership));
        }
    }

    void TimelineItem::styleAsDebug() {
        QLabel *contentLabel = new QLabel(this);
        layout()->addWidget(contentLabel);

        contentLabel->setTextFormat(Qt::PlainText);
        contentLabel->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
        contentLabel->setWordWrap(true);
        contentLabel->setCursor(Qt::IBeamCursor);
        contentLabel->setStyleSheet(QLatin1String("color: #AAFFFFFF;")); // TODO: Not hardcode this color

        contentLabel->setText(QLatin1String("type: %1").arg(event->getType()));
    }

    void TimelineItem::setUserNameVisibility(bool visible, QLabel *nameLabel) {
        static const QString DISPLAYNAME = QLatin1String("displayname");

        if (visible) {
            std::optional<Ref<Mx::Event>> userState = room->getCurrentState().get(M_ROOM_MEMBER, event->getSender().fullId);

            QString displayName = event->getSender().fullId;
            if (userState) {
                const QJsonObject &content = (*userState)->getContent();

                if (content.contains(DISPLAYNAME) && content[DISPLAYNAME].isString()) {
                    displayName = content[DISPLAYNAME].toString();
                }
            } else {
                qWarning() << "Couldn't find state event m.room.member for" << event->getSender().fullId;
            }

            nameLabel->setText(displayName);
            nameLabel->setStyleSheet(QLatin1String("color: %1;").arg(colorForUser(event->getSender()).name()));
        }

        nameLabel->setVisible(visible);
    }

    TimelineItem::EventStyle TimelineItem::getStyleForEvent(const Mx::Event *event) {
        const Vmx::InString &messageType = event->getType();

        if (messageType == M_ROOM_MESSAGE) {
            return EventStyle::MESSAGE;
        } else if (messageType == M_ROOM_MEMBER) {
            return EventStyle::STATUS;
        } else {
            return EventStyle::DEBUG;
        }
    }
}
