#include "vtx/widget/TimelineView.hpp"
#include "vtx/VtxInstance.hpp"
#include <QScrollBar>
#include <QResizeEvent>

static const int LOAD_PAST_SCROLL_OFFSET = 32;

static void clearLayout(QLayout *layout) {
    QLayoutItem *item;
    while ((item = layout->takeAt(0))) {
        if (item->layout() != nullptr) {
            clearLayout(item->layout());
            delete item->layout();
        }
        if (item->widget() != nullptr) {
           delete item->widget();
        }
        delete item;
    }
}

namespace Vtx::Widget {
    TimelineView::TimelineView(QWidget *parent) : QScrollArea(parent) {
        content = new QWidget(this);
        setWidget(content);
        setAlignment(Qt::AlignBottom);
        setWidgetResizable(true);
        setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

        QVBoxLayout *layout = new QVBoxLayout(this);
        layout->setSpacing(4);
        content->setLayout(layout);

        layout->addStretch();

        topThrobber = new Throbber(this);
        topThrobber->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
        topThrobber->setMinimumSize(32, 32);
        topThrobber->setVisible(false);
        layout->addWidget(topThrobber);
        layout->setAlignment(topThrobber, Qt::AlignHCenter);

        messagesLayout = new QVBoxLayout();
        layout->addLayout(messagesLayout);

        // Throbber *bottomThrobber = new Throbber(this);
        // bottomThrobber->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
        // bottomThrobber->setMinimumSize(32, 32);
        // layout->addWidget(bottomThrobber);
        // layout->setAlignment(bottomThrobber, Qt::AlignHCenter);

        content->setFixedWidth(width() - verticalScrollBar()->width());

        connect(verticalScrollBar(), &QScrollBar::valueChanged, this, &TimelineView::onScroll);
        connect(verticalScrollBar(), &QScrollBar::rangeChanged, this, &TimelineView::onScrollRangeChanged);
    }

    void TimelineView::setModel(Model::TimelineModel *model) {
        Q_ASSERT(this->model == nullptr && "Tried to set a TimelineView model twice!");
        this->model = model;

        populateMessages();

        connect(model, &QAbstractItemModel::modelReset, this, &TimelineView::onModelReset);
        connect(model, &QAbstractItemModel::rowsInserted, this, &TimelineView::onRowsInserted);
        connect(model, &Model::TimelineModel::beginRequestingPastMessages, this, &TimelineView::onBeginRequestingPastMessages);
        connect(model, &Model::TimelineModel::finishedRequestingPastMessages, this, &TimelineView::onFinishedRequestingPastMessages);

        connect(model, &Model::TimelineModel::currentSectionIdChanged, this, &TimelineView::onCurrentSectionIdChanged);
    }

    void TimelineView::resizeEvent(QResizeEvent *event) {
        QScrollArea::resizeEvent(event);

        content->setFixedWidth(width() - verticalScrollBar()->width());
    }

    TimelineItem* TimelineView::widgetFromIndex(const QModelIndex &index) {
        Q_ASSERT(index.isValid());

        Ref<Mx::Event> event = model->data(
                index,
                Model::TimelineModel::ROLE_REF_EVENT
                ).value<Ref<Mx::Event>>();
        Q_ASSERT(!event.isNull());

        TimelineItem *item = new TimelineItem(this);
        item->setEvent(event, model->getRoom());

        return item;
    }

    void TimelineView::populateMessages() {
        int rows = model->rowCount();

        // Only load the last 50
        int rowBegin = qMax(0, rows - 50);

        for (int i = rowBegin; i < rows; i++) {
            TimelineItem *item = widgetFromIndex(model->index(i, 0));
            messagesLayout->addWidget(item);
        }

        if (rows > 0) {
            minLoaded = model->index(rowBegin, 0);
            maxLoaded = model->index(rows - 1, 0);
        } else {
            minLoaded = QModelIndex();
            maxLoaded = QModelIndex();
        }
    }

    void TimelineView::offsetScroll(int offset) {
        QScrollBar *vBar = verticalScrollBar();
        vBar->setValue(vBar->value() + offset);
    }

    void TimelineView::scrollToBottom() {
        QScrollBar *vBar = verticalScrollBar();
        vBar->setValue(vBar->maximum());

        // verticalScrollBar()->triggerAction(QScrollBar::SliderToMaximum);
    }

    bool TimelineView::isAtBottom() {
        QScrollBar *vBar = verticalScrollBar();
        return vBar->value() == vBar->maximum();
    }

    void TimelineView::loadMorePast(bool shouldRecurse) {
        int minRow = minLoaded.row();

        if (minLoaded.row() > 0) {
            // If the model has more data to load, fetch it
            int newMin = qMax(0, minRow - 50);

            for (int i = newMin; i < minRow; i++) {
                TimelineItem *item = widgetFromIndex(model->index(i, 0));
                messagesLayout->insertWidget(i - newMin, item);
            }

            minLoaded = model->index(newMin, 0);
        } else {
            // Otherwise request the model to load more
            model->requestPastMessages(VtxInstance::getInstance()->getActiveUser());
            // In case stuff was immediately loaded (from the local db), see if we still need to fetch more (in the case
            // that we didn't fetch enough to fill the widget's height)
            if (shouldRecurse) {
                checkShouldLoadMore(false);
            }
        }
    }

    void TimelineView::onModelReset() {
        clearLayout(messagesLayout);
        topThrobber->setVisible(false);
        populateMessages();

        // Triggers a scroll-to-bottom when the scroll bar's range updates
        scrollMax = 0;

        if (model->getRoom() != nullptr) {
            topThrobber->setVisible(model->isRequestingPastMessages());
        } else {
            topThrobber->setVisible(false);
        }

        // On the next update (after the scroll bars have been updated), check if we need to load more history
        QTimer::singleShot(0, this, &TimelineView::checkShouldLoadMore_timer);
    }

    void TimelineView::onRowsInserted(const QModelIndex &parent, int first, int last) {
        int minRow = minLoaded.isValid() ? minLoaded.row() : 0;
        int maxRow = maxLoaded.isValid() ? maxLoaded.row() : 0;

        if (first > maxRow + 1 || last < minRow - 1) return;

        // Disable updates while we do a whole bunch of stuff in batch
        setUpdatesEnabled(false);

        for (int i = first; i <= last; i++) {
            TimelineItem *item = widgetFromIndex(model->index(i, 0, parent));

            int insertIndex;
            if (i < minRow) {
                insertIndex = i - first;
            } else if (i > maxRow) {
                insertIndex = messagesLayout->count();
            } else {
                insertIndex = i - minRow;
            }

            messagesLayout->insertWidget(insertIndex, item);

            // Scroll down a bit if we should
            // Do it after the height's been calculated
            QTimer::singleShot(0, item, [this, item]() {
                if (item->y() > verticalScrollBar()->value() + height()) {
                    offsetScroll(-(item->height() + messagesLayout->spacing()));
                }
            });
        }

        if (first < minRow) {
            minLoaded = QPersistentModelIndex(model->index(first, 0, parent));
        }

        if (last > maxRow) {
            maxLoaded = QPersistentModelIndex(model->index(last, 0, parent));
        }

        QTimer::singleShot(0, this, [this]() {
            setUpdatesEnabled(true);
        });
    }

    void TimelineView::onScroll(int offsetPx) {
        if (model->getRoom() != nullptr && offsetPx < LOAD_PAST_SCROLL_OFFSET && !topThrobber->isVisible()) {
            loadMorePast();
        }
    }

    void TimelineView::onScrollRangeChanged(int min, int max) {
        Q_UNUSED(min);

        // If we're already at the bottom, scroll to the bottom again
        // if (verticalScrollBar()->value() >= scrollMax) {
            // verticalScrollBar()->setValue(max);
        // }

        int prevDistanceFromBottom = scrollMax - verticalScrollBar()->value();
        verticalScrollBar()->setValue(max - prevDistanceFromBottom);

        scrollMax = max;
    }

    void TimelineView::onBeginRequestingPastMessages() {
        if (!topThrobber->isVisible()) {
            topThrobber->setVisible(true);
            offsetScroll(topThrobber->height());
        }
    }

    void TimelineView::onFinishedRequestingPastMessages() {
        if (topThrobber->isVisible()) {
            topThrobber->setVisible(false);
            offsetScroll(-topThrobber->height());
        }
    }

    void TimelineView::checkShouldLoadMore_timer() {
        checkShouldLoadMore(true);
    }

    void TimelineView::checkShouldLoadMore(bool shouldRecurse) {
        if (verticalScrollBar()->value() < LOAD_PAST_SCROLL_OFFSET && model->getRoom() != nullptr) {
            loadMorePast(shouldRecurse);
        }
    }

    void TimelineView::onCurrentSectionIdChanged(int oldId, int newId) {
        Q_UNUSED(newId);

        // Check if we're looking at what we thought to be current messages, and if we were, we should switch the
        // section we're looking at to the new current section
        if (model->getSection()->getSectionId() == oldId && isAtBottom()) {
            model->setSection(model->getRoom(), model->getRoom()->getTimeline().getCurrentSection());
        }
    }
}
