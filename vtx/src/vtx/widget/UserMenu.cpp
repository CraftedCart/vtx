#include "vtx/widget/UserMenu.hpp"
#include "vtx/VtxInstance.hpp"
#include <QPainter>
#include <QMenu>
#include <QAction>

static const int PROFILE_PICTURE_SIZE = 40;

namespace Vtx::Widget {
    UserMenu::UserMenu(QWidget *parent) : QPushButton(parent) {
        Q_ASSERT(!pixmapExpandMore.isNull());

        setMinimumHeight(48);
        setAttribute(Qt::WA_Hover); // Recieve hover events so we can update our look

        menu = new QMenu(this);
        connect(menu, &QMenu::aboutToShow, this, &UserMenu::buildMenu);

        setMenu(menu);

        connect(VtxInstance::getInstance(), &VtxInstance::activeUserChanged, this, &UserMenu::onActiveUserChanged);
    }

    void UserMenu::paintEvent(QPaintEvent *event) {
        Q_UNUSED(event);

        QPainter painter(this);

        QFont font = painter.font();

        QFont nameFont = font;
        nameFont.setBold(true);
        nameFont.setPointSizeF(nameFont.pointSizeF() + 2.0f);

        QFont idFont = font;

        QBrush backgroundBrush = underMouse() ? palette().highlight() : palette().window();

        UserInfo *activeUser = VtxInstance::getInstance()->getActiveUser();

        // Draw a background
        painter.fillRect(
                0,
                0,
                width(),
                height(),
                backgroundBrush
                );

        // Profile picture placeholder
        painter.fillRect(
                4,
                (height() / 2) - (PROFILE_PICTURE_SIZE / 2),
                PROFILE_PICTURE_SIZE,
                PROFILE_PICTURE_SIZE,
                QRgb(0xFF00FF)
                );

        // User display name
        painter.setFont(nameFont);
        painter.drawText(
                QRect(
                    8 + PROFILE_PICTURE_SIZE,
                    0,
                    width() - 8 + PROFILE_PICTURE_SIZE,
                    height() / 2
                    ),
                Qt::AlignLeft | Qt::AlignBottom,
                "TODO: Active user display name here"
                );

        // User MXID
        painter.setFont(idFont);
        painter.drawText(
                QRect(
                    8 + PROFILE_PICTURE_SIZE,
                    height() / 2,
                    width() - 8 + PROFILE_PICTURE_SIZE,
                    height() / 2
                    ),
                Qt::AlignLeft | Qt::AlignTop,
                activeUser->getUserId().fullId
                );

        // Gradient on the right, to fade out any long strings of text
        QLinearGradient gradient(width() - 60, 0, width() - 20, 0);
        QColor transparentBg = backgroundBrush.color();
        transparentBg.setAlpha(0);
        gradient.setColorAt(0, transparentBg);
        gradient.setColorAt(1, backgroundBrush.color());
        painter.fillRect(
                width() - 60,
                0,
                60,
                height(),
                gradient
                );

        painter.drawPixmap(
                width() - pixmapExpandMore.width(),
                (height() / 2) - (pixmapExpandMore.height() / 2),
                pixmapExpandMore
                );
    }

    void UserMenu::buildMenu() {
        menu->clear(); // Also deletes owning QActions

        menu->setMinimumWidth(width());

        UserInfo *activeUser = VtxInstance::getInstance()->getActiveUser();
        QVector<UserInfo*> users = VtxInstance::getInstance()->getMatrix()->getUsers();
        for (UserInfo *user : users) {
            // TODO: Use user display name here
            QAction *userAction = new QAction(user->getUserId().fullId, this);
            userAction->setEnabled(user != activeUser);
            menu->addAction(userAction);
            connect(userAction, &QAction::triggered, [user]() {
                VtxInstance::getInstance()->setActiveUser(user);
            });
        }

        menu->addSeparator();

        QAction *newUserAction = new QAction(tr("Add new user"), this);
        connect(newUserAction, &QAction::triggered, this, &UserMenu::addNewUser);
        menu->addAction(newUserAction);

        // TODO: Add a manage users action perhaps?
    }

    void UserMenu::addNewUser() {
        loginWidget = new LoginWidget();
        loginWidget->setAttribute(Qt::WA_DeleteOnClose);

        connect(loginWidget, &LoginWidget::loginSuccess, this, &UserMenu::onLoginSuccess);

        loginWidget->show();
    }

    void UserMenu::onLoginSuccess(const Vmx::ClientApi::R0::Session::Login::PostResponse &resp) {
        Vmx::Util::Result<void, Mx::LoginRejectError> res = VtxInstance::getInstance()->getMatrix()->onLoginSuccess(resp);
        if (res.isOk()) {
            loginWidget->close(); // Also should delete it
        } else {
            loginWidget->showErrorMessage(tr("You are already logged in as this account"), {});
        }
    }

    void UserMenu::onActiveUserChanged(UserInfo *user) {
        Q_UNUSED(user);
        update(); // Force a repaint
    }
}
