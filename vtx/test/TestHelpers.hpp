#ifndef VTX_VTX_TEST_TESTHELPERS_HPP
#define VTX_VTX_TEST_TESTHELPERS_HPP

#include "vtx/VtxInstance.hpp"
#include <cstdlib>

#define VTX_TEST_MAIN(TestClass) \
    int main(int argc, char *argv[]) { \
        bool success = Vtx::VtxInstance::createInstance(argc, argv, true); \
        if (!success) return EXIT_FAILURE; \
        qApp->setAttribute(Qt::AA_Use96Dpi, true); \
        \
        TestClass tc; \
        int ret = QTest::qExec(&tc, argc, argv); \
        \
        Vtx::VtxInstance::destroyInstance(); \
        return ret; \
    }

namespace TestHelpers {
    inline bool resetApp() {
        return Vtx::VtxInstance::resetInstance(true);
    }
}

#endif
