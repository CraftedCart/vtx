#include "vtx/mx/Room.hpp"
#include "vtx/db/Database.hpp"
#include "TestHelpers.hpp"
#include <QtTest/QtTest>

using namespace Vtx;
using namespace Vtx::Db;
using namespace Vtx::Mx;
using namespace Vmx::Id;

static Ref<Event> mRoomName = Ref<Event>::create(
        QString("$cmfAyePJH4n7AnzWDGlJyS6g3RRkNDOGBTtbnTrWgzs"),
        QJsonDocument::fromJson(
            "{"
            "  \"type\": \"m.room.name\","
            "  \"sender\": \"@abuse:matrix.org\","
            "  \"content\": {"
            "    \"name\": \"Matrix HQ\""
            "  },"
            "  \"state_key\": \"\","
            "  \"origin_server_ts\": 1574383781154,"
            "  \"unsigned\": {"
            "    \"age\": 28761945174"
            "  },"
            "  \"event_id\": \"$cmfAyePJH4n7AnzWDGlJyS6g3RRkNDOGBTtbnTrWgzs\","
            "  \"room_id\": \"!OGEhHVWSdvArJzumhm:matrix.org\""
            "}"
            ).object()
        );

static Ref<Event> mRoomName_invalid = Ref<Event>::create(
        QString("$cmfAyePJH4n7AnzWDGlJyS6g3RRkNDOGBTtbnTrWgzs"),
        QJsonDocument::fromJson(
            "{"
            "  \"type\": \"m.room.name\","
            "  \"sender\": \"@abuse:matrix.org\","
            "  \"content\": {"
            // "    \"name\": \"Matrix HQ\"" // Intentionally missing `name` field
            "  },"
            "  \"state_key\": \"\","
            "  \"origin_server_ts\": 1574383781154,"
            "  \"unsigned\": {"
            "    \"age\": 28761945174"
            "  },"
            "  \"event_id\": \"$cmfAyePJH4n7AnzWDGlJyS6g3RRkNDOGBTtbnTrWgzs\","
            "  \"room_id\": \"!OGEhHVWSdvArJzumhm:matrix.org\""
            "}"
            ).object()
        );

static Ref<Event> mRoomCanonocalAlias = Ref<Event>::create(
        QString("$izHO0x6YbxTMk133RFzkflWRtYjJAJRG7-7pRp31Cq4"),
        QJsonDocument::fromJson(
            "{"
            "  \"type\": \"m.room.canonical_alias\","
            "  \"sender\": \"@abuse:matrix.org\","
            "  \"content\": {"
            "    \"alias\": \"#matrix:matrix.org\""
            "  },"
            "  \"state_key\": \"\","
            "  \"origin_server_ts\": 1574383813062,"
            "  \"unsigned\": {"
            "    \"age\": 28761913266"
            "  },"
            "  \"event_id\": \"$izHO0x6YbxTMk133RFzkflWRtYjJAJRG7-7pRp31Cq4\","
            "  \"room_id\": \"!OGEhHVWSdvArJzumhm:matrix.org\""
            "}"
            ).object()
        );

static Ref<Event> mRoomCanonocalAlias_invalid = Ref<Event>::create(
        QString("$izHO0x6YbxTMk133RFzkflWRtYjJAJRG7-7pRp31Cq4"),
        QJsonDocument::fromJson(
            "{"
            "  \"type\": \"m.room.canonical_alias\","
            "  \"sender\": \"@abuse:matrix.org\","
            "  \"content\": {"
            // "    \"alias\": \"#matrix:matrix.org\"" // Intentially missing `alias` field
            "  },"
            "  \"state_key\": \"\","
            "  \"origin_server_ts\": 1574383813062,"
            "  \"unsigned\": {"
            "    \"age\": 28761913266"
            "  },"
            "  \"event_id\": \"$izHO0x6YbxTMk133RFzkflWRtYjJAJRG7-7pRp31Cq4\","
            "  \"room_id\": \"!OGEhHVWSdvArJzumhm:matrix.org\""
            "}"
            ).object()
        );

static Ref<Event> mRoomMember_self = Ref<Event>::create(
        QString("$IR7VsDkNtKCAzAQHdKzMv5AIRFngizOw5e3HsKYrjro"),
        QJsonDocument::fromJson(
            "{"
            "  \"content\": {"
            "    \"displayname\": \"Alice\","
            "    \"membership\": \"join\""
            "  },"
            "  \"origin_server_ts\": 1574383824286,"
            "  \"sender\": \"@alice:example.com\","
            "  \"state_key\": \"@alice:example.com\","
            "  \"type\": \"m.room.member\","
            "  \"unsigned\": {"
            "    \"age\": 28761902042"
            "  },"
            "  \"event_id\": \"$IR7VsDkNtKCAzAQHdKzMv5AIRFngizOw5e3HsKYrjro\","
            "  \"room_id\": \"!OGEhHVWSdvArJzumhm:matrix.org\""
            "}"
            ).object()
        );

static Ref<Event> mRoomMember_bob = Ref<Event>::create(
        QString("$totally_legit_event_id"),
        QJsonDocument::fromJson(
            "{"
            "  \"content\": {"
            "    \"displayname\": \"Bob\","
            "    \"membership\": \"join\""
            "  },"
            "  \"origin_server_ts\": 1574383824286,"
            "  \"sender\": \"@bob:example.com\","
            "  \"state_key\": \"@bob:example.com\","
            "  \"type\": \"m.room.member\","
            "  \"unsigned\": {"
            "    \"age\": 28761902042"
            "  },"
            "  \"event_id\": \"$totally_legit_event_id\","
            "  \"room_id\": \"!OGEhHVWSdvArJzumhm:matrix.org\""
            "}"
            ).object()
        );

static const Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncJoinedRoom = {
    .summary = {
        .heroes = std::nullopt,
        .joinedMemberCount = 5,
        .invitedMemberCount = 1,
    },
    .state = {
        .events = {
            Vmx::Event::SyncStateEvent(
                QString("m.room.name"),
                QJsonDocument::fromJson("{\"name\":\"Matrix HQ\"}").object(),
                QJsonDocument::fromJson("{\"type\":\"m.room.name\",\"sender\":\"@abuse:matrix.org\",\"content\":{\"name\":\"MatrixHQ\"},\"state_key\":\"\",\"origin_server_ts\":1574383781154,\"unsigned\":{\"age\":28761945174},\"event_id\":\"$cmfAyePJH4n7AnzWDGlJyS6g3RRkNDOGBTtbnTrWgzs\",\"room_id\":\"!OGEhHVWSdvArJzumhm:matrix.org\"}").object(),
                QString("@abuse:matrix.org"),
                QString("$cmfAyePJH4n7AnzWDGlJyS6g3RRkNDOGBTtbnTrWgzs"),
                QDateTime::fromMSecsSinceEpoch(1574383781154),
                std::nullopt, // Too lazy to add unsigned data rn
                QString(""),
                std::nullopt
            ),
            Vmx::Event::SyncStateEvent(
                QString("m.room.canonical_alias"),
                QJsonDocument::fromJson("{\"alias\":\"#matrix:matrix.org\"}").object(),
                QJsonDocument::fromJson("{\"type\":\"m.room.canonical_alias\",\"sender\":\"@abuse:matrix.org\",\"content\":{\"alias\":\"#matrix:matrix.org\"},\"state_key\":\"\",\"origin_server_ts\":1574383813062,\"unsigned\":{\"age\":28761913266},\"event_id\":\"$izHO0x6YbxTMk133RFzkflWRtYjJAJRG7-7pRp31Cq4\",\"room_id\":\"!OGEhHVWSdvArJzumhm:matrix.org\"}").object(),
                QString("@abuse:matrix.org"),
                QString("$izHO0x6YbxTMk133RFzkflWRtYjJAJRG7-7pRp31Cq4"),
                QDateTime::fromMSecsSinceEpoch(1574383813062),
                std::nullopt, // Too lazy to add unsigned data rn
                QString(""),
                std::nullopt
            ),
        },
    },
    .timeline = {
        .events = {
            Vmx::Event::SyncRoomEvent(
                QString("m.room.avatar"),
                QJsonDocument::fromJson("{\"url\":\"mxc://matrix.org/DRevoaEiuzbkOznknySKuMmE\"}").object(),
                QJsonDocument::fromJson("{\"type\":\"m.room.avatar\",\"sender\":\"@abuse:matrix.org\",\"content\":{\"url\":\"mxc://matrix.org/DRevoaEiuzbkOznknySKuMmE\"},\"state_key\":\"\",\"origin_server_ts\":1574383781349,\"unsigned\":{\"age\":28761944979},\"event_id\":\"$3545MJuBm8kLpM5U3ZgRsST-Mt7jBjF0wco__XBkN0M\",\"room_id\":\"!OGEhHVWSdvArJzumhm:matrix.org\"}").object(),
                QString("@abuse:matrix.org"),
                QString("$3545MJuBm8kLpM5U3ZgRsST-Mt7jBjF0wco__XBkN0M"),
                QDateTime::fromMSecsSinceEpoch(1574383781349),
                std::nullopt // Too lazy to add unsigned data rn
            ),
            Vmx::Event::SyncRoomEvent(
                QString("m.room.message"),
                QJsonDocument::fromJson("{\"body\":\"boop\",\"msgtype\":\"m.text\"}").object(),
                QJsonDocument::fromJson("{\"content\":{\"body\":\"boop\",\"msgtype\":\"m.text\"},\"origin_server_ts\":1603565480279,\"sender\":\"@craftedcart:matrix.org\",\"type\":\"m.room.message\",\"unsigned\":{\"age\":424,\"transaction_id\":\"m1603565480161.45\"},\"event_id\":\"$zgyaYYZF1TTGl5JuoxD18rISXR9FdDbzAryaOIlc5jc\",\"room_id\":\"!CsFUWTEUAwMFunkALQ:matrix.org\"}").object(),
                QString("@craftedcart:matrix.org"),
                QString("$zgyaYYZF1TTGl5JuoxD18rISXR9FdDbzAryaOIlc5jc"),
                QDateTime::fromMSecsSinceEpoch(1603565480279),
                std::nullopt // Too lazy to add unsigned data rn
            ),
        },
        .limited = std::nullopt,
        .prevBatch = QString("fancy_token"),
    },
    .ephemeral = {},
    .accountData = {},
    .unreadNotifications = {},
};

static const Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncJoinedRoom2 = {
    .summary = {},
    .state = { .events = {}, },
    .timeline = {
        .events = {
            Vmx::Event::SyncRoomEvent(
                QString("m.room.message"),
                QJsonDocument::fromJson("{\"body\":\"owo\",\"msgtype\":\"m.text\"}").object(),
                QJsonDocument::fromJson("{\"content\":{\"body\":\"owo\",\"msgtype\":\"m.text\"},\"origin_server_ts\":1603565480279,\"sender\":\"@craftedcart:matrix.org\",\"type\":\"m.room.message\",\"unsigned\":{\"age\":424,\"transaction_id\":\"m1603565480161.47\"},\"event_id\":\"$helloooooagain\",\"room_id\":\"!CsFUWTEUAwMFunkALQ:matrix.org\"}").object(),
                QString("@craftedcart:matrix.org"),
                QString("$helloooooagain"),
                QDateTime::fromMSecsSinceEpoch(1603565480279),
                std::nullopt // Too lazy to add unsigned data rn
            ),
        },
        .limited = false,
        .prevBatch = QString("fancy_token2"),
    },
    .ephemeral = {},
    .accountData = {},
    .unreadNotifications = {},
};

static const Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncLimitedJoinedRoom = {
    .summary = {
        .heroes = std::nullopt,
        .joinedMemberCount = 5,
        .invitedMemberCount = 1,
    },
    .state = { .events = {}, },
    .timeline = {
        .events = {
            Vmx::Event::SyncRoomEvent(
                QString("m.room.message"),
                QJsonDocument::fromJson("{\"body\":\"boop\",\"msgtype\":\"m.text\"}").object(),
                QJsonDocument::fromJson("{\"content\":{\"body\":\"boop\",\"msgtype\":\"m.text\"},\"origin_server_ts\":1603565480279,\"sender\":\"@craftedcart:matrix.org\",\"type\":\"m.room.message\",\"unsigned\":{\"age\":424,\"transaction_id\":\"m1603565480161.46\"},\"event_id\":\"$hellooooo\",\"room_id\":\"!CsFUWTEUAwMFunkALQ:matrix.org\"}").object(),
                QString("@craftedcart:matrix.org"),
                QString("$hellooooo"),
                QDateTime::fromMSecsSinceEpoch(1603565480279),
                std::nullopt // Too lazy to add unsigned data rn
            ),
        },
        .limited = true,
        .prevBatch = QString("fancy_token_limited"),
    },
    .ephemeral = {},
    .accountData = {},
    .unreadNotifications = {},
};

class TestRoom : public QObject {
    Q_OBJECT

    private:
        Room *room;

    private:
        void recreateRoom() {
            delete room;
            room = new Room(Vmx::InString(QLatin1String("!testroom:example.com")));
        }

        Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom makeJoinedRoom(int numEvents, int startIndex = 0) {
            QVector<Vmx::Event::SyncRoomEvent> events;
            for (int i = startIndex; i < startIndex + numEvents; i++) {
                events.append(
                        Vmx::Event::SyncRoomEvent(
                            QString("m.room.message"),
                            QJsonDocument::fromJson((QString("{\"body\":\"test_") + QString::number(i) + QString("\",\"msgtype\":\"m.text\"}")).toUtf8()).object(),
                            QJsonDocument::fromJson((QString("{\"content\":{\"body\":\"test_") + QString::number(i) + QString("\",\"msgtype\":\"m.text\"},\"origin_server_ts\":") + QString::number(i) + QString(",\"sender\":\"@craftedcart:matrix.org\",\"type\":\"m.room.message\",\"event_id\":\"$test_") + QString::number(i) + QString("\",\"room_id\":\"!CsFUWTEUAwMFunkALQ:matrix.org\"}")).toUtf8()).object(),
                            QString("@craftedcart:matrix.org"),
                            QString("$test_") + QString::number(i),
                            QDateTime::fromMSecsSinceEpoch(i),
                            std::nullopt // unsignedData
                            )
                        );
            }

            return {
                .summary = {},
                .state = { .events = {}, },
                .timeline = {
                    .events = events,
                    .limited = false,
                    .prevBatch = QString("fancy_token2"),
                },
                .ephemeral = {},
                .accountData = {},
                .unreadNotifications = {},
            };
        }

        QVector<Ref<Event>> makeMxEvents(int numEvents, int sectionId, int startOrder) {
            QVector<Ref<Event>> events;
            for (int i = 0; i < numEvents; i++) {
                Ref<Event> event = Ref<Event>::create(
                        Vmx::Event::SyncRoomEvent(
                            QString("m.room.message"),
                            QJsonDocument::fromJson("{\"body\":\"owo\",\"msgtype\":\"m.text\"}").object(),
                            QJsonDocument::fromJson((QString("{\"content\":{\"body\":\"owo\",\"msgtype\":\"m.text\"},\"origin_server_ts\":") + QString::number(i) + QString(",\"sender\":\"@craftedcart:matrix.org\",\"type\":\"m.room.message\",\"event_id\":\"$test_") + QString::number(i) + QString("\",\"room_id\":\"!CsFUWTEUAwMFunkALQ:matrix.org\"}")).toUtf8()).object(),
                            QString("@craftedcart:matrix.org"),
                            QString("$test_") + QString::number(i),
                            QDateTime::fromMSecsSinceEpoch(i),
                            std::nullopt // unsignedData
                            )
                        );
                event->setEventLoc(EventLoc{sectionId, startOrder + i});

                events.append(event);
            }

            return events;
        }

    private slots:
        void init() {
            room = new Room(Vmx::InString(QLatin1String("!testroom:example.com")));
        }

        // TODO: Test room with only 1 user
        // TODO: Test room with no m.room.name/m.room.canonical_alias

        void testRoomHasRightDisplayNameFromStateEvents_data() {
            QTest::addColumn<QVector<Ref<Event>>>("stateEvents");
            QTest::addColumn<QVector<UserId>>("selfHeroes");
            QTest::addColumn<QString>("expectedName");

            QTest::newRow("Just m.room.name") <<
                    QVector{mRoomName} <<
                    QVector<UserId>() <<
                    "Matrix HQ";

            QTest::newRow("Just m.room.canonical_alias") <<
                    QVector{mRoomCanonocalAlias} <<
                    QVector<UserId>() <<
                    "#matrix:matrix.org";

            QTest::newRow("Both m.room.name and m.room.canonical_alias") <<
                    QVector{mRoomName, mRoomCanonocalAlias} <<
                    QVector<UserId>() <<
                    "Matrix HQ";

            QTest::newRow("Invalid m.room.name and valid m.room.canonical_alias") <<
                    QVector{mRoomName_invalid, mRoomCanonocalAlias} <<
                    QVector<UserId>() <<
                    "#matrix:matrix.org";

            QTest::newRow("m.room.name with heroes") <<
                    QVector{mRoomName, mRoomMember_bob} <<
                    QVector<UserId>{QString("@bob:example.com")} <<
                    "Matrix HQ";

            QTest::newRow("Only Bob hero") <<
                    QVector{mRoomMember_bob} <<
                    QVector<UserId>{QString("@bob:example.com")} <<
                    "Bob";

            QTest::newRow("Self and Bob heroes") <<
                    QVector{mRoomMember_self, mRoomMember_bob} <<
                    QVector<UserId>{QString("@alice:example.com"), QString("@bob:example.com")} <<
                    "Alice, Bob";

            // TODO: Enable these tests when the functionality actually... works

            // TODO: These should actually fall back to either "Empty room" or the names of prominent members
            // QTest::newRow("Just invalid m.room.name") <<
                    // QVector{mRoomName_invalid} <<
                    // QVector<UserId>() <<
                    // "!testroom:example.com";

            // QTest::newRow("Invalid m.room.name and invalid m.room.canonical_alias") <<
                    // QVector{mRoomName_invalid, mRoomCanonocalAlias} <<
                    // QVector<UserId>() <<
                    // "!testroom:example.com";

            // QTest::newRow("No state events") << QVector<Ref<Event>>() << "Empty room";
            // QTest::newRow("Only self in the room") << QVector{mRoomMember_self} << "Alice";
            // QTest::newRow("Only Bob in the room") << QVector{mRoomMember_bob} << "Alice, Bob";
            // QTest::newRow("Self and Bob in the room") << QVector{mRoomMember_self, mRoomMember_bob} << "Alice, Bob";
            // TODO: Add some stuff with power levels (admins should be displayed first)
        }

        void testRoomHasRightDisplayNameFromStateEvents() {
            QFETCH(QVector<Ref<Event>>, stateEvents);
            QFETCH(QVector<UserId>, selfHeroes);
            QFETCH(QString, expectedName);

            UserInfo *mainUser = new UserInfo();
            mainUser->setUserId(QString("@alice:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(mainUser);

            for (const UserId &hero : selfHeroes) {
                room->heroes[mainUser].append(hero);
            }

            for (const Ref<Event> &event : stateEvents) {
                room->getCurrentState().mergeEvent(event);
            }
            room->updateDisplayName();

            QCOMPARE(room->getDisplayName(), expectedName);
        }

        void testGetCurrentStateEventFromLocalDb() {
            VtxInstance::getInstance()->getDatabase().insertEvent(mRoomMember_self, room);
            VtxInstance::getInstance()->getDatabase().insertCurrentStateEvent(mRoomMember_self, room);

            std::optional<Ref<Mx::Event>> eventOpt = room->getCurrentState().get(QString("m.room.member"), QString("@alice:example.com"));
            QVERIFY(eventOpt.has_value());

            const Ref<Mx::Event> &event = *eventOpt;
            QCOMPARE(event->getJson(), mRoomMember_self->getJson());
            QCOMPARE(event->getContent(), mRoomMember_self->getContent());
            QCOMPARE(event->getEventId(), mRoomMember_self->getEventId());
            QCOMPARE(event->getEventLoc(), mRoomMember_self->getEventLoc());
            QCOMPARE(event->getOriginServerTs(), mRoomMember_self->getOriginServerTs());
            QCOMPARE(event->getPrevEvent(), mRoomMember_self->getPrevEvent());
            QCOMPARE(event->getSender(), mRoomMember_self->getSender());
        }

        void testUserMemberships() {
            // Setup our dummy user
            UserInfo *mainUser = new UserInfo();
            mainUser->setUserId(QString("@alice:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(mainUser);

            // The room should start off with having the user marked as LEAVE
            // And also the user should not be in the memberships map
            QCOMPARE(room->getUserMembership(mainUser), Vmx::Membership::LEAVE);
            QVERIFY(room->getUserMemberships().find(mainUser) == room->getUserMemberships().end());

            // Changing membership from LEAVE -> LEAVE should do nothing
            room->setUserMembership(mainUser, Vmx::Membership::LEAVE);
            QCOMPARE(room->getUserMembership(mainUser), Vmx::Membership::LEAVE);
            QVERIFY(room->getUserMemberships().find(mainUser) == room->getUserMemberships().end());

            // Changing membership from LEAVE -> JOIN should be remembered
            room->setUserMembership(mainUser, Vmx::Membership::JOIN);
            QCOMPARE(room->getUserMembership(mainUser), Vmx::Membership::JOIN);
            QVERIFY(room->getUserMemberships().find(mainUser) != room->getUserMemberships().end());

            // Changing membership from JOIN -> INVITE should be remembered
            room->setUserMembership(mainUser, Vmx::Membership::INVITE);
            QCOMPARE(room->getUserMembership(mainUser), Vmx::Membership::INVITE);
            QVERIFY(room->getUserMemberships().find(mainUser) != room->getUserMemberships().end());

            // Changing membership from JOIN -> BAN should be remembered
            room->setUserMembership(mainUser, Vmx::Membership::BAN);
            QCOMPARE(room->getUserMembership(mainUser), Vmx::Membership::BAN);
            QVERIFY(room->getUserMemberships().find(mainUser) != room->getUserMemberships().end());

            // Changing membership from BAN to LEAVE should not be remembered
            room->setUserMembership(mainUser, Vmx::Membership::LEAVE);
            QCOMPARE(room->getUserMembership(mainUser), Vmx::Membership::LEAVE);
            QVERIFY(room->getUserMemberships().find(mainUser) == room->getUserMemberships().end());
        }

        void testEventPurgability() {
            // An event that lives only in the timeline should be purgeable
            {
                Ref<Event> cloneMRoomName = mRoomName->clone();
                room->getTimeline().getCurrentSection()->append(cloneMRoomName);
            }
            QVERIFY(room->isTimelineEventPurgeable(room->getTimeline().getCurrentSection()->at(0)));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 1);
            room->purgeUnused();
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 0);

            recreateRoom();

            // An event that lives in the timeline and has another reference outside should not be purgeable
            {
                Ref<Event> cloneMRoomName = mRoomName->clone();
                room->getTimeline().getCurrentSection()->append(cloneMRoomName);
                QVERIFY(!room->isTimelineEventPurgeable(room->getTimeline().getCurrentSection()->at(0)));
                QCOMPARE(room->getTimeline().getCurrentSection()->length(), 1);
                room->purgeUnused();
                QCOMPARE(room->getTimeline().getCurrentSection()->length(), 1);
            }

            recreateRoom();

            // An event that lives in the timeline and in the room's current state should be purgeable
            {
                Ref<Event> cloneMRoomName = mRoomName->clone();
                room->getTimeline().getCurrentSection()->append(cloneMRoomName);
                room->getCurrentState().mergeEvent(cloneMRoomName);
            }
            QVERIFY(room->isTimelineEventPurgeable(room->getTimeline().getCurrentSection()->at(0)));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 1);
            QCOMPARE(room->getCurrentState().cachedSize(), 1);
            room->purgeUnused();
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 0);
            QCOMPARE(room->getCurrentState().cachedSize(), 0);

            recreateRoom();

            // An event that lives in the timeline and in the room's current state and also is referenced outside should not be purgeable
            {
                Ref<Event> cloneMRoomName = mRoomName->clone();
                room->getTimeline().getCurrentSection()->append(cloneMRoomName);
                room->getCurrentState().mergeEvent(cloneMRoomName);
                QVERIFY(!room->isTimelineEventPurgeable(room->getTimeline().getCurrentSection()->at(0)));
                QCOMPARE(room->getTimeline().getCurrentSection()->length(), 1);
                QCOMPARE(room->getCurrentState().cachedSize(), 1);
                room->purgeUnused();
                QCOMPARE(room->getTimeline().getCurrentSection()->length(), 1);
                QCOMPARE(room->getCurrentState().cachedSize(), 1);
            }
        }

        void testMergeSyncResponse() {
            UserInfo *mainUser = new UserInfo();
            mainUser->setUserId(QString("@alice:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(mainUser);

            {
                room->mergeSyncedRoom(mainUser, syncJoinedRoom);

                QCOMPARE(room->getDisplayName(), "Matrix HQ");

                // Check that state events in the timeline are added to the room's current state
                QCOMPARE(room->getCurrentState().cachedSize(), 3);
                std::optional<Ref<Event>> event = room->getCurrentState().get(QString("m.room.avatar"), QString(""));
                QVERIFY(event);
                QCOMPARE((*event)->getContent()["url"].toString(), "mxc://matrix.org/DRevoaEiuzbkOznknySKuMmE");

                QCOMPARE(room->getTimeline().getCurrentSection()->getSectionId(), 0);
                QCOMPARE(room->getTimeline().getCurrentSection()->length(), 2);

                QCOMPARE(room->getTimeline().getCurrentSection()->at(0)->getEventLoc(), (EventLoc{0, 0}));
                QCOMPARE(room->getTimeline().getCurrentSection()->at(1)->getEventLoc(), (EventLoc{0, 1}));
                QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 1}}));
                QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 1}}));

                QCOMPARE(room->getTimeline().getCurrentSection()->getPrevBatchToken(mainUser), "fancy_token");

                room->mergeSyncedRoom(mainUser, syncLimitedJoinedRoom);

                QCOMPARE(room->getTimeline().numCachedSections(), 2);
                QCOMPARE(room->getTimeline().getCurrentSection()->getSectionId(), 1);
                QCOMPARE(room->getTimeline().getCurrentSection()->length(), 1);

                QCOMPARE(room->getTimeline().getCurrentSection()->at(0)->getEventLoc(), (EventLoc{1, 0}));
                QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{1, 0}, EventLoc{1, 0}}));
                QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{1, 0}, EventLoc{1, 0}}));

                QCOMPARE(room->getTimeline().getCurrentSection()->getPrevBatchToken(mainUser), "fancy_token_limited");
            }

            // We really do need the scoping blocks around purgeUnused...
            room->purgeUnused();

            {
                QCOMPARE(room->getTimeline().numCachedSections(), 0);
                QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), std::nullopt);
                QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{1, 0}, EventLoc{1, 0}}));

                // Check that the token is still here after making the current section
                QCOMPARE(room->getTimeline().getCurrentSection()->getPrevBatchToken(mainUser), "fancy_token_limited");

                room->mergeSyncedRoom(mainUser, syncJoinedRoom2);

                QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{1, 1}, EventLoc{1, 1}}));
                QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{1, 0}, EventLoc{1, 1}}));

                // Getting *future* synced events should not affect the *past* prev_batck token
                QCOMPARE(room->getTimeline().getCurrentSection()->getPrevBatchToken(mainUser), "fancy_token_limited");
            }

            room->purgeUnused();

            // Fetch past messages from the local db
            {
                QCOMPARE(room->getTimeline().numCachedSections(), 0);

                room->getTimeline().getCurrentSection()->requestPastMessages(mainUser, 100, false);
                QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{1, 0}, EventLoc{1, 1}}));
                QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{1, 0}, EventLoc{1, 1}}));

                QCOMPARE(room->getTimeline().getCurrentSection()->getPrevBatchToken(mainUser), "fancy_token_limited");

                // Check that we got the past messages back in the right order
                QCOMPARE(room->getTimeline().getCurrentSection()->at(0)->getEventId(), QString("$hellooooo"));
                QCOMPARE(room->getTimeline().getCurrentSection()->at(1)->getEventId(), QString("$helloooooagain"));

                QCOMPARE(room->getTimeline().getCurrentSection()->at(0)->getEventLoc(), (EventLoc{1, 0}));
                QCOMPARE(room->getTimeline().getCurrentSection()->at(1)->getEventLoc(), (EventLoc{1, 1}));
            }
        }

        void testFetchPastMessagesFromDb() {
            // Make a sync joined room response with 250 messages
            const Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom joinedRoom = makeJoinedRoom(250);

            // Make our user
            UserInfo *mainUser = new UserInfo();
            mainUser->setUserId(QString("@alice:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(mainUser);

            room->mergeSyncedRoom(mainUser, joinedRoom);

            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 249}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 249}}));

            // Purge events from RAM except for the last event
            Ref<Event> lastEvent = room->getTimeline().getCurrentSection()->at(249);
            room->purgeUnused();

            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 249}, EventLoc{0, 249}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 249}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 1);

            QCOMPARE(room->getTimeline().getCurrentSection()->at(0), lastEvent);
            QCOMPARE(room->getTimeline().getCurrentSection()->at(0)->getEventId(), QString("$test_249"));

            // Fetch past 100
            room->getTimeline().getCurrentSection()->requestPastMessages(mainUser, 100, false);

            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 149}, EventLoc{0, 249}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 249}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 101);

            for (int i = 0; i < 101; i++) {
                QCOMPARE(room->getTimeline().getCurrentSection()->at(i)->getEventId(), QString("$test_") + QString::number(i + 149));
                QCOMPARE(room->getTimeline().getCurrentSection()->at(i)->getEventLoc(), (EventLoc{0, i + 149}));
            }

            // Fetch past 300 (we don't have enough events in the local db for 300 though)
            room->getTimeline().getCurrentSection()->requestPastMessages(mainUser, 300, false);

            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 249}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 249}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 250);

            for (int i = 0; i < 250; i++) {
                QCOMPARE(room->getTimeline().getCurrentSection()->at(i)->getEventId(), QString("$test_") + QString::number(i));
                QCOMPARE(room->getTimeline().getCurrentSection()->at(i)->getEventLoc(), (EventLoc{0, i}));
            }
        }

        void testNonContiguousEventsInDb() {
            // Make our user
            UserInfo *mainUser = new UserInfo();
            mainUser->setUserId(QString("@alice:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(mainUser);

            {
                QVector<Ref<Event>> events = makeMxEvents(250, 0, -20);

                // Mess with the first event to make this batch non-contiguous
                events[0]->setEventLoc(EventLoc{0, -50});

                // Insert all these into the db
                VtxInstance::getInstance()->getDatabase().startTransaction();
                for (const Ref<Event> &event : events) {
                    VtxInstance::getInstance()->getDatabase().insertEvent(event, room);
                }
                VtxInstance::getInstance()->getDatabase().insertPrevBatchToken(room->getTimeline().getCurrentSection(), mainUser, "towoken");
                VtxInstance::getInstance()->getDatabase().commitTransaction();
            }

            room->getTimeline().getCurrentSection()->requestPastMessages(mainUser, 300, false);

            // Check that we only got 249 events, not 250
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{{0, -19}, {0, 229}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{{0, -19}, {0, 229}}));

            // Check that the oldest event has a loc of 0, -19 (not 0, -20)
            QCOMPARE(room->getTimeline().getCurrentSection()->at(0)->getEventLoc(), (EventLoc{0, -19}));

            // The prev batch token should be have been removed too as it's no longer valid when we delete old events
            // TODO: Do this when we can store where prev batch tokens point to
            // QCOMPARE(room->getTimeline().getCurrentSection()->getPrevBatchToken(mainUser), std::nullopt);
        }

        void testTwoUsersSyncingSameRoomWithNoIgnoredUsersSameNumberOfEvents() {
            // Make our users
            UserInfo *userAlice = new UserInfo();
            userAlice->setUserId(QString("@alice:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(userAlice);

            UserInfo *userBob = new UserInfo();
            userBob->setUserId(QString("@bob:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(userBob);

            Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncAliceJoined = makeJoinedRoom(100);
            Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncBobJoined = makeJoinedRoom(100);

            room->mergeSyncedRoom(userAlice, syncAliceJoined);
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 100);

            // Range/length should be unchanged
            room->mergeSyncedRoom(userBob, syncBobJoined);
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 100);
        }

        void testTwoUsersSyncingSameRoomWithNoIgnoredUsersDifferentNumberOfEventsSmallerThenLarger() {
            // Make our users
            UserInfo *userAlice = new UserInfo();
            userAlice->setUserId(QString("@alice:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(userAlice);

            UserInfo *userBob = new UserInfo();
            userBob->setUserId(QString("@bob:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(userBob);

            Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncAliceJoined = makeJoinedRoom(50);
            Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncBobJoined = makeJoinedRoom(100);

            room->mergeSyncedRoom(userAlice, syncAliceJoined);
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 49}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 49}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 50);

            room->mergeSyncedRoom(userBob, syncBobJoined);
            qDebug() << *room->getTimeline().getCurrentSection()->getRange();
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 100);
        }

        void testTwoUsersSyncingSameRoomWithNoIgnoredUsersDifferentNumberOfEventsLargerThenSmaller() {
            // Make our users
            UserInfo *userAlice = new UserInfo();
            userAlice->setUserId(QString("@alice:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(userAlice);

            UserInfo *userBob = new UserInfo();
            userBob->setUserId(QString("@bob:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(userBob);

            Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncAliceJoined = makeJoinedRoom(100);
            Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncBobJoined = makeJoinedRoom(50);

            room->mergeSyncedRoom(userAlice, syncAliceJoined);
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 100);

            room->mergeSyncedRoom(userBob, syncBobJoined);
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 100);
        }

        void testTwoUsersSyncingSameRoomWithIgnoredUsers() {
            // Make our users
            UserInfo *userAlice = new UserInfo();
            userAlice->setUserId(QString("@alice:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(userAlice);

            UserInfo *userBob = new UserInfo();
            userBob->setUserId(QString("@bob:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(userBob);

            Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncAliceJoined = makeJoinedRoom(100);
            Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncBobJoined = makeJoinedRoom(80, 20);

            // Pretend Alice has ignored a user, meaning she misses a message
            syncAliceJoined.timeline.events.remove(30);

            room->mergeSyncedRoom(userAlice, syncAliceJoined);
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 98}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 98}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 99);

            room->mergeSyncedRoom(userBob, syncBobJoined);
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 100);

            // Check that the missing event was inserted in the right place
            QCOMPARE(room->getTimeline().getCurrentSection()->at(50)->getContent()["body"].toString(), "test_50");
            // ...and not at the end
            QCOMPARE(room->getTimeline().getCurrentSection()->at(99)->getContent()["body"].toString(), "test_99");

            // Check that it has the right loc
            QCOMPARE(room->getTimeline().getCurrentSection()->at(50)->getEventLoc(), (EventLoc{0, 50}));
            // ...and that the rest was shifted over by 1
            for (int i = 51; i < 100; i++) {
                QCOMPARE(room->getTimeline().getCurrentSection()->at(i)->getEventLoc(), (EventLoc{0, i}));
            }

            // TODO: Check local db
        }

        void testTwoUsersSyncingLimitedResultsInContiguousEventRange() {
            // Make our users
            UserInfo *userAlice = new UserInfo();
            userAlice->setUserId(QString("@alice:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(userAlice);

            UserInfo *userBob = new UserInfo();
            userBob->setUserId(QString("@bob:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(userBob);

            Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncAliceJoined = makeJoinedRoom(50);
            Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncBobJoined = makeJoinedRoom(80, 20);
            syncAliceJoined.timeline.limited = true;
            syncBobJoined.timeline.limited = true;

            room->mergeSyncedRoom(userAlice, syncAliceJoined);
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{1, 0}, EventLoc{1, 49}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{1, 0}, EventLoc{1, 49}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 50);

            room->mergeSyncedRoom(userBob, syncBobJoined);
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{1, 0}, EventLoc{1, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{1, 0}, EventLoc{1, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 100);

            // Check that everything has the right event loc in memory
            for (int i = 0; i < 100; i++) {
                QCOMPARE(room->getTimeline().getCurrentSection()->at(i)->getEventLoc(), (EventLoc{1, i}));
            }

            syncAliceJoined = makeJoinedRoom(20, 100);
            syncBobJoined = makeJoinedRoom(10, 100);

            room->mergeSyncedRoom(userAlice, syncAliceJoined);
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{1, 0}, EventLoc{1, 119}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{1, 0}, EventLoc{1, 119}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 120);

            room->mergeSyncedRoom(userBob, syncBobJoined);
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{1, 0}, EventLoc{1, 119}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{1, 0}, EventLoc{1, 119}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 120);

            // Check that everything has the right event loc in memory
            for (int i = 0; i < 120; i++) {
                QCOMPARE(room->getTimeline().getCurrentSection()->at(i)->getEventLoc(), (EventLoc{1, i}));
            }
        }

        void testTwoUsersWithDifferentEventOrder() {
            // Make our users
            UserInfo *userAlice = new UserInfo();
            userAlice->setUserId(QString("@alice:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(userAlice);

            UserInfo *userBob = new UserInfo();
            userBob->setUserId(QString("@bob:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(userBob);

            Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncAliceJoined = makeJoinedRoom(100);
            Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom syncBobJoined = makeJoinedRoom(100);

            // Swap some stuff around so Bob sees the timeline in a different order
            std::swap(syncBobJoined.timeline.events[20], syncBobJoined.timeline.events[22]);
            std::swap(syncBobJoined.timeline.events[15], syncBobJoined.timeline.events[16]);
            std::swap(syncBobJoined.timeline.events[50], syncBobJoined.timeline.events[70]);
            std::swap(syncBobJoined.timeline.events[12], syncBobJoined.timeline.events[22]);

            room->mergeSyncedRoom(userAlice, syncAliceJoined);
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 100);

            room->mergeSyncedRoom(userBob, syncBobJoined);
            QCOMPARE(room->getTimeline().getCurrentSection()->getRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->getPersistentRange(), (EventLocRange{EventLoc{0, 0}, EventLoc{0, 99}}));
            QCOMPARE(room->getTimeline().getCurrentSection()->length(), 100);

            // Check that everything has the right event loc in memory
            for (int i = 0; i < 100; i++) {
                QCOMPARE(room->getTimeline().getCurrentSection()->at(i)->getEventLoc(), (EventLoc{0, i}));
            }

            // The first syncer should win in event ordering
            for (int i = 0; i < 100; i++) {
                QCOMPARE(room->getTimeline().getCurrentSection()->at(i)->getContent()["body"].toString(), QString("test_") + QString::number(i));
            }
        }

        void cleanup() {
            delete room;
            TestHelpers::resetApp();
        }
};

VTX_TEST_MAIN(TestRoom)
#include "TestRoom.moc"
