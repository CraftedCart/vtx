#include "vtx/widget/TimelineView.hpp"
#include "vtx/mx/Room.hpp"
#include "vtx/Iter.hpp"
#include "TestHelpers.hpp"
#include <QScrollBar>
#include <QtTest/QtTest>

using namespace Vtx;
using namespace Vtx::Db;
using namespace Vtx::Mx;
using namespace Vtx::Model;
using namespace Vtx::Widget;
using namespace Vmx::Id;

class TestTimelineView : public QObject {
    Q_OBJECT

    private:
        Room *room;
        TimelineModel *model;
        TimelineView *view;

    private:
        void recreateRoom() {
            delete room;
            room = new Room(Vmx::InString(QLatin1String("!testroom:example.com")));
        }

        Vmx::ClientApi::R0::Sync::SyncEvents::JoinedRoom makeLimitedJoinedRoom(int numEvents) {
            QVector<Vmx::Event::SyncRoomEvent> events;
            for (int i = 0; i < numEvents; i++) {
                events.append(
                        Vmx::Event::SyncRoomEvent(
                            QString("m.room.message"),
                            QJsonDocument::fromJson("{\"body\":\"owo\",\"msgtype\":\"m.text\"}").object(),
                            QJsonDocument::fromJson((QString("{\"content\":{\"body\":\"owo\",\"msgtype\":\"m.text\"},\"origin_server_ts\":") + QString::number(i) + QString(",\"sender\":\"@craftedcart:matrix.org\",\"type\":\"m.room.message\",\"event_id\":\"$test_") + QString::number(i) + QString("\",\"room_id\":\"!CsFUWTEUAwMFunkALQ:matrix.org\"}")).toUtf8()).object(),
                            QString("@craftedcart:matrix.org"),
                            QString("$test_") + QString::number(i),
                            QDateTime::fromMSecsSinceEpoch(i),
                            std::nullopt // unsignedData
                            )
                        );
            }

            return {
                .summary = {},
                .state = { .events = {}, },
                .timeline = {
                    .events = events,
                    .limited = true,
                    .prevBatch = QString("fancy_token2"),
                },
                .ephemeral = {},
                .accountData = {},
                .unreadNotifications = {},
            };
        }

        QVector<Ref<Event>> makeMxEvents(int numEvents, int sectionId, int startOrder) {
            QVector<Ref<Event>> events;
            for (int i = 0; i < numEvents; i++) {
                Ref<Event> event = Ref<Event>::create(
                        Vmx::Event::SyncRoomEvent(
                            QString("m.room.message"),
                            QJsonDocument::fromJson((QString("{\"body\":\"message_") + QString::number(sectionId) + QString("_") + QString::number(startOrder + i) + QString("\",\"msgtype\":\"m.text\"}")).toUtf8()).object(),
                            QJsonDocument::fromJson((QString("{\"content\":{\"body\":\"message_") + QString::number(sectionId) + QString("_") + QString::number(startOrder + i) + QString("\",\"msgtype\":\"m.text\"},\"origin_server_ts\":") + QString::number(i) + QString(",\"sender\":\"@craftedcart:matrix.org\",\"type\":\"m.room.message\",\"event_id\":\"$test_") + QString::number(i) + QString("\",\"room_id\":\"!CsFUWTEUAwMFunkALQ:matrix.org\"}")).toUtf8()).object(),
                            QString("@craftedcart:matrix.org"),
                            QString("$test_") + QString::number(i),
                            QDateTime::fromMSecsSinceEpoch(i),
                            std::nullopt // unsignedData
                            )
                        );
                event->setEventLoc(EventLoc{sectionId, startOrder + i});

                events.append(event);
            }

            return events;
        }

        /** @brief Checks if any child of the given widget has a label with the given text */
        bool containsLabelText(QWidget *widget, const QString &text) {
            if (widget->layout() == nullptr) return false;

            for (int i = 0; i < widget->layout()->count(); i++) {
                QLayoutItem *item = widget->layout()->itemAt(i);
                QLabel *label = qobject_cast<QLabel*>(item->widget());
                if (label == nullptr) continue;

                // qDebug() << label->text();

                if (label->text() == text) return true;
            }

            return false;
        }

    private slots:
        void init() {
            room = new Room(Vmx::InString(QLatin1String("!testroom:example.com")));
            model = new TimelineModel();
            view = new TimelineView();
        }

        void testAppendAndPrependMessagesToModel() {
            // Sanity check: A new timeline view should have zero message items in it
            QCOMPARE(view->messagesLayout->count(), 0);

            Ref<TimelineSection> section = room->getTimeline().getCurrentSection();
            model->setSection(room, section);
            view->setModel(model);

            // Check that we start at section 0
            QCOMPARE(section->getSectionId(), 0);

            // Should still be empty
            QCOMPARE(view->messagesLayout->count(), 0);

            // Add 3 events
            for (Ref<Event> event : makeMxEvents(3, 0, 0)) {
                section->append(event);
            }

            QCOMPARE(view->messagesLayout->count(), 3);

            // Check that they contain the message text in the right order
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(0)->widget(), "message_0_0"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(1)->widget(), "message_0_1"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(2)->widget(), "message_0_2"));

            // Add another event
            section->append(makeMxEvents(1, 0, 3)[0]);

            // Check that they contain the message text in the right order
            QCOMPARE(view->messagesLayout->count(), 4);
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(0)->widget(), "message_0_0"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(1)->widget(), "message_0_1"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(2)->widget(), "message_0_2"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(3)->widget(), "message_0_3"));

            // Prepend an event
            section->prepend(makeMxEvents(1, 0, -1)[0]);

            // Check that they contain the message text in the right order
            QCOMPARE(view->messagesLayout->count(), 5);
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(0)->widget(), "message_0_-1"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(1)->widget(), "message_0_0"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(2)->widget(), "message_0_1"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(3)->widget(), "message_0_2"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(4)->widget(), "message_0_3"));

            // Prepend 3 events
            section->onBeginInsertRows(QModelIndex(), 0, 2);
            QVector<Ref<Event>> events = makeMxEvents(3, 0, -4);
            for (Ref<Event> event : Iter::reverse(events)) {
                section->prependNoUpdate(event);
            }
            section->onEndInsertRows();

            // Check that they contain the message text in the right order
            QCOMPARE(view->messagesLayout->count(), 8);
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(0)->widget(), "message_0_-4"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(1)->widget(), "message_0_-3"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(2)->widget(), "message_0_-2"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(3)->widget(), "message_0_-1"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(4)->widget(), "message_0_0"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(5)->widget(), "message_0_1"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(6)->widget(), "message_0_2"));
            QVERIFY(containsLabelText(view->messagesLayout->itemAt(7)->widget(), "message_0_3"));
        }

        void testSwitchToNullRoom() {
            // Sanity check: A new timeline view should have zero message items in it
            QCOMPARE(view->messagesLayout->count(), 0);

            Ref<TimelineSection> section = room->getTimeline().getCurrentSection();
            model->setSection(room, section);
            view->setModel(model);

            // Should still be empty
            QCOMPARE(view->messagesLayout->count(), 0);

            // Add 3 events
            for (Ref<Event> event : makeMxEvents(3, 0, 0)) {
                section->append(event);
            }

            QCOMPARE(view->messagesLayout->count(), 3);

            model->setSection(nullptr, {});
            QCOMPARE(view->messagesLayout->count(), 0);
        }

        void testLookingAtLatestMessagesWhenRecievingALimitedSyncResponseShouldSwitchToTheRecievedSyncResponseTimelineSection() {
            Ref<TimelineSection> section = room->getTimeline().getCurrentSection();
            model->setSection(room, section);
            view->setModel(model);

            // Check that we start at section 0
            QCOMPARE(model->getSection()->getSectionId(), 0);

            // Add 150 events
            for (Ref<Event> event : makeMxEvents(150, 0, 0)) {
                section->append(event);
            }

            QCOMPARE(view->messagesLayout->count(), 150);

            // Make sure we're scrolled to the bottom
            qApp->processEvents();
            view->scrollToBottom();
            qApp->processEvents();

            UserInfo *mainUser = new UserInfo();
            mainUser->setUserId(QString("@alice:example.com"));
            VtxInstance::getInstance()->getMatrix()->addUser(mainUser);

            room->mergeSyncedRoom(mainUser, makeLimitedJoinedRoom(100));

            // We should be switched to a new section ID
            QCOMPARE(model->getSection()->getSectionId(), 1);

            // And we should only have the 100 messages from this section
            QCOMPARE(view->messagesLayout->count(), 100);

            // And we should still be scrolled to the bottom
            qApp->processEvents();
            QCOMPARE(view->verticalScrollBar()->value(), view->verticalScrollBar()->maximum());
        }

        void cleanup() {
            delete view;
            delete model;
            delete room;
            TestHelpers::resetApp();
        }
};

VTX_TEST_MAIN(TestTimelineView)
#include "TestTimelineView.moc"
